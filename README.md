# Climbing Tracker

This app lets you log all of your climbing activities. Enjoy to use it!

## Features

- Log climbing sessions
Log all of your climbing activities. Easily save your ascends in this app. Specify the route grade, ascending style, name, and give it a rating. Clear statistics are created from the data, so you always have an optimal overview of your progress.

- Session summaries
After each session, a summary with the most important key points is created to give you a simple overview of your performance. You can easily share your summary directly with your friends.

- Find routes you've already climbed
Who does not know it, you are climbing and wondering whether you have already climbed this route? An overview of all your climbed routes promises help.

- Statistics and Graphics
View your previous successes in clear graphics. Compare yourself to friends. See your progress in great charts and see your most difficult routes at a glance.

- Data protection
Your data is only stored locally, so your data cannot fall into the wrong hands. Of course, you can still create a backup and save it in your favorite cloud.