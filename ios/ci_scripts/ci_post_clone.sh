#!/bin/sh

# The default execution directory of this script is the ci_scripts directory.
echo "Changing from '$(pwd)' to '$CI_PRIMARY_REPOSITORY_PATH'..."
cd $CI_PRIMARY_REPOSITORY_PATH # change working directory to the root of your cloned repo.

# Install Flutter using git.
echo "Installing Flutter..."
git clone https://github.com/flutter/flutter.git -b stable $HOME/flutter
export PATH="$PATH:$HOME/flutter/bin"

# Install Flutter artifacts for iOS (--ios), or macOS (--macos) platforms.
echo "Precaching iOS Artifacts..."
flutter precache --ios

# Install Flutter dependencies.
echo "Installing Flutter dependencies..."
flutter pub get

# Install CocoaPods using Homebrew.
HOMEBREW_NO_AUTO_UPDATE=1 # disable homebrew's automatic updates.
echo "Installing CocoaPods..."
brew install cocoapods

# Install CocoaPods dependencies.
echo "Installing CocoaPods Dependencies..."
cd ios
pod install # run `pod install` in the `ios` directory.

echo "ci_post_clone successfully executed."
exit 0
