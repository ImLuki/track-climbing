import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color background = Color(0xffF5F5F5);

  static const Color textPrimary = Colors.black;
  static const Color textSecondary = Colors.white;
  static const Color accentColor = Color(0xFF8d252e);
  static const Color accentColor2 = Color(0xFF878C8F);
  static const Color accentColor3 = Color(0xFF2A4849);

  static const Color color3 = Color(0xFF2E7647);
  static const Color color2 = Color(0xFF3D5A80);
  static const Color color1 = Color(0xFFE28413);

  static const List<Color> chart_colors = [
    Color(0xFFa62546),
    Color(0xFF47acb1),
    Color(0xFFf26522),
    Color(0xFFffcd34),
    Color(0xFF99C24D),
    Color(0xFF446DF6),
    Color(0xFFffad69),
    Color(0xFF9D79BC)
  ];
}
