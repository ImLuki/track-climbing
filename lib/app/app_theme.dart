import 'package:climbing_track/app/app_colors.dart';
import 'package:flutter/material.dart';

class AppThemeDataFactory {
  static ThemeData prepareThemeData({useMaterial3 = true}) => ThemeData(
        useMaterial3: useMaterial3,
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blueAccent),
        textTheme: const TextTheme(
          bodyLarge: TextStyle(
            letterSpacing: 0.0,
            wordSpacing: 0.0,
            height: 0.0,
            fontSize: 18,
          ),
          bodyMedium: TextStyle(
            fontSize: 14,
            letterSpacing: 0.0,
            wordSpacing: 0.0,
            height: 0.0,
          ),
          titleMedium: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
            color: AppColors.textPrimary,
            letterSpacing: 0.0,
            wordSpacing: 0.0,
            height: 0.0,
          ),
        ),
      ).copyWith(
        pageTransitionsTheme: const PageTransitionsTheme(
          builders: {
            TargetPlatform.android: CupertinoPageTransitionsBuilder(),
            TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
          },
        ),
        dialogTheme: const DialogTheme(
          surfaceTintColor: Colors.transparent,
        ),
        switchTheme: SwitchThemeData(
          trackColor: WidgetStateProperty.resolveWith(
            (states) => states.contains(WidgetState.selected) ? null : AppColors.background,
          ),
        ),
        popupMenuTheme: const PopupMenuThemeData(
          surfaceTintColor: Color(0xffBDBDBD),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(20.0),
            ),
          ),
        ),
      );
}
