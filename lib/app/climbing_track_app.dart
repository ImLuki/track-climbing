import 'dart:io';

import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/ui/dialog/rate_dialog.dart';
import 'package:climbing_track/ui/pages/ascends/ascents_pages.dart';
import 'package:climbing_track/ui/pages/home/home_view.dart';
import 'package:climbing_track/ui/pages/overview/overview_page.dart';
import 'package:climbing_track/ui/pages/sessions/session_overview.dart';
import 'package:climbing_track/ui/pages/settings/settings_page.dart';
import 'package:climbing_track/ui/pages/statistics/chart_page.dart';
import 'package:climbing_track/backend/handler/notifications_handler.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:showcaseview/showcaseview.dart';

class ClimbingTrackApp extends StatefulWidget {
  static const int MIN_SESSIONS_FOR_RATE_DIALOG = 5;
  static const String NOT_SHOW_RATE_DIALOG = "not_show_rate_dialog_again";
  static const String FEEDBACK_DISMISS_COUNTER = "feedback_dismiss_counter";

  final int selectedIndex;

  const ClimbingTrackApp({super.key, this.selectedIndex = _ClimbingTrackAppState.MAIN_PAGE_INDEX});

  @override
  _ClimbingTrackAppState createState() => _ClimbingTrackAppState();
}

class _ClimbingTrackAppState extends State<ClimbingTrackApp> {
  static const int MAIN_PAGE_INDEX = 0;
  final List<Widget> pages = [
    const HomeView(),
    const OverviewPage(),
    const SessionsOverview(),
    const AscendsPage(),
    const ChartPage(),
    const SettingsPage(),
  ];

  late int _selectedIndex;
  bool canBack = false;

  @override
  void initState() {
    super.initState();
    _selectedIndex = widget.selectedIndex;

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      showRateAndDonationDialog();
    });
  }

  @override
  Widget build(BuildContext context) {
    NotificationHandler().init();
    return KeyboardVisibilityProvider(
      child: Scaffold(
        body: ShowCaseWidget(
          onStart: (index, key) {
            debugPrint('onStart: $index, $key');
          },
          onComplete: (index, key) {
            debugPrint('onComplete: $index, $key');
          },
          builder: (context) => createBody(),
        ),
        bottomNavigationBar: createBottomNavigationBar(),
      ),
    );
  }

  void _onItemTapped(int index) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    setState(() {
      _selectedIndex = index;
    });
  }

  Widget createBody() {
    return Stack(
      children: <Widget>[
        const Positioned.fill(
          child: Image(
            image: AssetImage('assets/images/background.jpg'),
            fit: BoxFit.fill,
          ),
        ),
        pages[_selectedIndex],
      ],
    );
  }

  Widget createBottomNavigationBar() {
    return Container(
      decoration: const BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black12,
            spreadRadius: 1.0,
            blurRadius: 10,
          ),
        ],
      ),
      child: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: const Icon(Icons.home_outlined),
            label: AppLocalizations.of(context)!.home_title,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.person_search),
            label: AppLocalizations.of(context)!.overview_title,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.place_outlined),
            label: AppLocalizations.of(context)!.sessions_title,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.list_alt),
            label: AppLocalizations.of(context)!.ascends_title,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.bar_chart),
            label: AppLocalizations.of(context)!.statistics_title,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.settings),
            label: AppLocalizations.of(context)!.settings_title,
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        unselectedItemColor: Colors.black45,
        onTap: _onItemTapped,
        showUnselectedLabels: false,
        unselectedFontSize: 14.0,
      ),
    );
  }

  Future<void> showRateAndDonationDialog() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool notShowAgain = prefs.getBool(ClimbingTrackApp.NOT_SHOW_RATE_DIALOG) ?? false;

    if (notShowAgain) return;
    if ((prefs.getInt(ClimbingTrackApp.FEEDBACK_DISMISS_COUNTER) ?? 0 % 10) >= 3) return;

    List<Map<String, dynamic>> sessions = await DBController().queryAllRows(DBController.sessionsTable);
    if (sessions.length < ClimbingTrackApp.MIN_SESSIONS_FOR_RATE_DIALOG) return;
    if (sessions.any((e) => e[DBController.sessionStatus] == Session.SESSION_ACTIVE_STATE)) return;
    if (!Platform.isAndroid || !mounted) return;

    showDialog<bool>(
      context: context,
      builder: (BuildContext context) {
        return const AlertDialog(
          titlePadding: EdgeInsets.zero,
          contentPadding: EdgeInsets.zero,
          actionsPadding: EdgeInsets.zero,
          content: RateMyAppDialog(),
        );
      },
    );
  }
}
