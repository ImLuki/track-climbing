import 'dart:convert';
import 'dart:io';

import 'package:climbing_track/backend/backup/backup_handler.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/backup/json_file_parser.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

class BackupCreator {
  Future<String> createJsonString() async {
    List<Map<String, dynamic>> sessions = await _loadTable(table: DBController.sessionsTable);
    List<Map<String, dynamic>> ascents = await _loadTable(table: DBController.ascendsTable);
    List<Map<String, dynamic>> locations = await _loadTable(table: DBController.locationsTable);
    return jsonEncode({
      DBController.sessionsTable: sessions,
      DBController.locationsTable: locations,
      DBController.ascendsTable: ascents,
      BackupHandler.VERSION_CODE: BackupHandler.CURRENT_VERSION,
    });
  }

  Future<List<Map<String, dynamic>>> _loadTable({required String table}) async {
    final DBController dbController = DBController();
    List<Map<String, dynamic>> elements = await dbController.queryAllRows(table);
    return elements;
  }

  /// Create Backup file from current database
  /// Store file in temporary directory
  /// Return filepath to share file
  Future<String> createBackupFile() async {
    final directory = (await getTemporaryDirectory()).path;

    // get current datetime string
    DateFormat dateFormat = DateFormat("yyyy-MM-dd");
    String currentDateTime = dateFormat.format(DateTime.now());

    // create backup file
    String filePath = '$directory/backup-climbing-tracker-$currentDateTime.json';
    File file = File(filePath);
    String jsonString = await createJsonString();
    await file.writeAsString(jsonString);
    return filePath;
  }

  Future<void> saveAutomaticBackup(String directory, bool deleteBackups) async {
    if (deleteBackups) {
      List<FileSystemEntity> files = Directory(directory).listSync();
      files.sort((a, b) => b.path.compareTo(a.path));
      if (files.length > 4) {
        for (FileSystemEntity toDelete in files.getRange(4, files.length)) {
          try {
            File(toDelete.path).deleteSync();
          } catch (_) {}
        }
      }
    }

    String date = DateFormat("yyyy-MM-dd").format(DateTime.now());
    String filePath = "$directory/$date-climbing-tracker.json";

    String jsonString = await createJsonString();

    JsonFileParser.writeJsonString(jsonString, filePath);
  }
}
