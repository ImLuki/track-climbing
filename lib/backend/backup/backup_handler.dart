import 'dart:convert';
import 'dart:io';

import 'package:climbing_track/backend/backup/backup_creator.dart';
import 'package:climbing_track/backend/backup/loader/backup_loader.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/active_session.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:climbing_track/backend/backup/json_file_parser.dart';
import 'package:climbing_track/backend/handler/notifications_handler.dart';
import 'package:climbing_track/view_model/google_drive_backup_handler.dart';
import 'package:climbing_track/view_model/google_drive_backup_result.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/foundation.dart';

class BackupHandler {
  static const String LAST_DRIVE_BACKUP_KEY = "last_google_drive_backup_key";
  static const String VERSION_CODE = "version";
  static const int CURRENT_VERSION = 2;

  /// Restore data from backup file
  ///
  /// Returns true if backup successful, false otherwise
  Future<bool> loadFilBackupFromFile(String? filePath) async {
    if (filePath == null) {
      return false;
    }
    if (!filePath.endsWith('.json')) {
      return false;
    }

    // clear Data
    deleteDataBase();

    dynamic jsonObject = await JsonFileParser.loadFile(filePath);
    if (jsonObject == null) return false;
    return await BackupLoader(jsonObject).restoreBackup();
  }

  /// Restore data from google drive backup
  ///
  /// Returns null if no data could be downloaded, true if backup restoring was successfully, false otherwise
  Future<bool?> restoreGoogleDriveBackup() async {
    final GoogleDriveDownloadResult? driveDownloadResult = await GoogleDriveBackupHandler().downloadGoogleDriveFile();

    if (driveDownloadResult == null || driveDownloadResult.content == null) {
      // no data
      return null;
    } else {
      try {
        deleteDataBase();

        return await BackupLoader(jsonDecode(driveDownloadResult.content!)).restoreBackup();
      } catch (e) {
        return false;
      }
    }
  }

  /// If automatic backups are activated backup of all data is created and stored locally.
  ///
  /// If backup failed Error-Notification is scheduled
  ///
  /// Returns true if backup succeeded, false otherwise
  Future<bool> createAutomaticBackup() async {
    if (!DataStore().settingsDataStore.automaticBackups) return false;

    String dir;
    if (Platform.isAndroid) {
      List<Directory>? path = await getExternalStorageDirectories();

      if (path == null) return false;
      dir = path[0].path;
    } else if (Platform.isIOS) {
      Directory documentsDirectory = await getApplicationDocumentsDirectory();
      dir = join(documentsDirectory.path, "Climbing_Tracker");
    } else {
      return false;
    }

    await BackupCreator().saveAutomaticBackup(dir, DataStore().settingsDataStore.overwriteAutomaticBackups);

    return true;
  }

  /// If google drive backups are activated, backup of all data is created and uploaded to google drive.
  /// Current time will be written to shared preferences in $LAST\_DRIVE\_BACKUP\_KEY
  ///
  /// If backup failed Error-Notification is scheduled
  ///
  /// Returns true if backup succeeded, false otherwise
  Future<bool> createGoogleDriveBackup({
    String failNotificationTitle = "",
    String failNotificationBody = "",
    bool sendNotificationOnError = false,
    bool signIn = false,
  }) async {
    try {
      // if deactivated quit
      if (!DataStore().settingsDataStore.googleDriveActivated) return false;

      // create backup and upload to google drive
      final String content = await BackupCreator().createJsonString();
      GoogleDriveBackupResult googleDriveBackupResult = await GoogleDriveBackupHandler().uploadFileToGoogleDrive(
        content,
        signeIn: signIn,
      );
      debugPrint("Created Google Drive Backup: ${googleDriveBackupResult.result}");
      if (googleDriveBackupResult.result) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString(LAST_DRIVE_BACKUP_KEY, DateTime.now().toIso8601String());
        return true;
      } else {
        if (sendNotificationOnError) {
          NotificationHandler().sendNotification(
            title: failNotificationTitle,
            body: kDebugMode
                ? "$failNotificationBody\n\nError Message\n${googleDriveBackupResult.errorMessage}"
                : failNotificationBody,
            notificationId: NotificationHandler.NOTIFICATION_ID_BACKUP,
          );
        }
        return false;
      }
    } catch (e) {
      debugPrint(e.toString());
      return false;
    }
  }

  void deleteDataBase() {
    DBController().deleteAll();
    SessionState().setState(SessionStateEnum.none);
    ActiveSession().clear();
    DataStore().clear();
  }
}
