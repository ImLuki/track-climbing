import 'dart:io';

import 'package:climbing_track/backend/backup/json_parser.dart';
import 'package:climbing_track/backend/util/extensions.dart';

class JsonFileParser {
  /// Loads content from jason file
  /// Returns json object
  static Future<dynamic> loadFile(String path) async {
    try {
      JsonParser jsonParser = JsonParser();
      return jsonParser.parseJsonFile(path);
    } catch (e) {
      return null;
    }
  }

  /// Write content to json file
  static void writeJsonString(String jsonString, String filePath) {
    File file = File(filePath);
    Directory(file.directory).createSync();
    file.writeAsString(jsonString);
  }
}
