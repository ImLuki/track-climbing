import 'dart:convert';
import 'dart:io';

import 'package:flutter/services.dart';

class JsonParser {
  Future<String> _loadFromAsset(String fileName) async {
    return await rootBundle.loadString("assets/files/$fileName");
  }

  Future parseAssetJsonFile(String fileName) async {
    String jsonString = await _loadFromAsset(fileName);
    return jsonDecode(jsonString);
  }

  Future parseJsonFile(String filePath) async {
    try {
      String jsonString = await readFile(filePath);
      return jsonDecode(jsonString);
    } on Exception catch (e) {
      Future.error(e);
    }
  }

  Future<String> readFile(String filePath) async {
    try {
      return await File(filePath).readAsString();
    } catch (e) {
      return "";
    }
  }
}
