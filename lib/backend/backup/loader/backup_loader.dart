import 'package:climbing_track/backend/backup/backup_handler.dart';
import 'package:climbing_track/backend/backup/loader/deprecated_resolver_backup_loader.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/active_session.dart';
import 'package:climbing_track/backend/model/charts/statistic_charts.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:flutter/material.dart';

@protected
class BackupLoader {
  dynamic jsonObject;

  BackupLoader(this.jsonObject);

  Future<bool> restoreBackup() async {
    if (jsonObject[BackupHandler.VERSION_CODE] == null) {
      return DeprecatedResolverBackupLoader(jsonObject).importData();
    } else {
      switch (jsonObject[BackupHandler.VERSION_CODE]) {
        case 1:
        case 2:
          return _importDataVersion1();
        default:
          return false;
      }
    }
  }

  Future<bool> _importDataVersion1() async {
    /// consistency check:
    /// two active sessions
    /// each session has at least one ascend
    /// location for each session
    /// null check for all parameters necessary
    /// start date < end date
    /// type check for all parameters
    /// check for duplicated id

    // load locations
    await DBController().batchInsert(DBController.locationsTable, jsonObject[DBController.locationsTable]);

    // load sessions
    await DBController().batchInsert(DBController.sessionsTable, jsonObject[DBController.sessionsTable]);
    await DataStore().locationsDataStore.init();

    // load ascents
    // Mapping ascend order to ascendID if null.
    // This is for older backup file version because they didn't contain this key
    for (Map<String, dynamic> element in jsonObject[DBController.ascendsTable]) {
      if (element[DBController.ascendOrder] == null) {
        element[DBController.ascendOrder] = element[DBController.ascendId];
      }
    }
    await DBController().batchInsert(DBController.ascendsTable, jsonObject[DBController.ascendsTable]);

    await DataStore().init();
    await SessionState().init();
    await ActiveSession().init();
    await StatisticCharts.init();

    try {
      return true;
    } catch (e) {
      debugPrint(e.toString());
      return false;
    }
  }
}
