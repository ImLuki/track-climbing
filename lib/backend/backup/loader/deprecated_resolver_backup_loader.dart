// ignore_for_file: deprecated_member_use_from_same_package

import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/location.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:flutter/material.dart';
import 'package:latlong2/latlong.dart';

@protected
class DeprecatedResolverBackupLoader {
  dynamic jsonObject;
  Map<String, double> standardHeightMap = {};
  Map<String, LatLng> positionsMap = {};

  DeprecatedResolverBackupLoader(this.jsonObject);

  Future<bool> importData() async {
    // loading Location Position
    final DBController dbController = DBController();

    _loadCoordinates(dbController, jsonObject["positions"]);
    _loadStandardHeights(dbController, jsonObject["standard_height_table"]);

    await _loadSessions(jsonObject["sessions"]);
    await _loadAscents(jsonObject["routes"]);

    await DataStore().settingsDataStore.loadFirstYear();
    return true;
  }

  Future<void> _loadSessions(List<dynamic> sessionsData) async {
    for (Map<String, dynamic> sessionData in sessionsData) {
      Location location = Location(
        null,
        sessionData[DBController.sessionLocation],
        positionsMap[sessionData[DBController.sessionLocation]],
        standardHeightMap[sessionData[DBController.sessionLocation]] ?? 0.0,
        "",
        sessionData[DBController.sessionOutdoor] == 1,
      );
      location = await DataStore().locationsDataStore.createUpdateOrGetLocation(location);
      sessionData[DBController.locationId] = location.locationId;
      sessionData[DBController.sessionStatus] = sessionData[DBController.sessionStatus] ?? Session.SESSION_CLOSED_STATE;
      sessionData.remove(DBController.sessionOutdoor);
      sessionData.remove(DBController.sessionLocation);
    }

    await DBController().batchInsert(DBController.sessionsTable, sessionsData);
  }

  void _loadCoordinates(DBController dbController, var locations) {
    if (locations == null) {
      return; //For backwards compatibility
    }
    for (Map<String, dynamic> location in locations) {
      if (location[DBController.longitude] == null || location[DBController.latitude] == null) {
        continue;
      }
      positionsMap[location[DBController.sessionLocation]] = LatLng(
        location[DBController.latitude],
        location[DBController.longitude],
      );
    }
  }

  Future<void> _loadAscents(List<dynamic> ascentsData) async {
    for (Map<String, dynamic> ascendData in ascentsData) {
      if (ascendData[DBController.ascendType] == null) {
        ascendData[DBController.ascendType] = ClimbingType.SPORT_CLIMBING.name;
      }

      // because in old backup files type of [Ascend.SPORT_CLIMBING] was 'ROUTE'
      if (ascendData[DBController.ascendType] == "ROUTE") {
        ascendData[DBController.ascendType] = ClimbingType.SPORT_CLIMBING.name;
      }
    }
    await DBController().batchInsert(DBController.ascendsTable, ascentsData);
  }

  void _loadStandardHeights(DBController dbController, var standardHeights) {
    if (standardHeights == null) {
      return; //For backwards compatibility
    }

    for (var standardHeight in standardHeights) {
      standardHeightMap[standardHeight[DBController.sessionLocation]] = standardHeight[DBController.standardHeight];
    }
  }
}
