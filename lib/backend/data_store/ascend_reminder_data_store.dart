import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';

class AscendReminderDataStore {
  final List<Ascend> _ascentsToRemind = [];
  final Map<int, DateTime> _endDateMapping = {};
  final Set<int> _ascentsToRemindSet = {};

  Future<void> init() async {
    clear();
    List<Map<String, dynamic>> data = await _fetchData();
    _ascentsToRemind.addAll(data.map<Ascend>((e) => Ascend.fromMap(e)));
    _endDateMapping.addEntries(
      data.where((element) => element[DBController.reminderEndDate] != null).map<MapEntry<int, DateTime>>(
            (e) => MapEntry(e[DBController.ascendId], DateTime.parse(e[DBController.reminderEndDate])),
          ),
    );
    _ascentsToRemindSet.addAll(data.map<int>((e) => e[DBController.ascendId]));
  }

  void clear() {
    _ascentsToRemind.clear();
    _endDateMapping.clear();
    _ascentsToRemindSet.clear();
  }

  Future<List<Map<String, dynamic>>> _fetchData() async {
    return DBController().rawQuery(
      "SELECT * FROM ${DBController.reminderTable} "
      "JOIN ${DBController.ascendsTable} using (${DBController.ascendId}) "
      "JOIN ${DBController.sessionsTable} using (${DBController.sessionId})",
    );
  }

  List<Ascend> getAscends({int? locationId, bool checkEndDate = false}) {
    Iterable<Ascend> out = _ascentsToRemind;

    if (locationId != null) {
      out = _ascentsToRemind.where(
        (element) => DataStore().locationsDataStore.locationById(locationId).name == element.location,
      );
    }

    if (checkEndDate) {
      DateTime now = DateTime.now();
      out = _ascentsToRemind.where((element) => _endDateMapping[element.ascendId]?.isBefore(now) ?? true);
    }
    return out.toList();
  }

  bool hasReminder({required int ascendId}) {
    return _ascentsToRemindSet.contains(ascendId);
  }

  void toggleReminder({required Ascend ascend, DateTime? endDate}) {
    if (hasReminder(ascendId: ascend.ascendId)) {
      deleteEntry(ascendId: ascend.ascendId);
    } else {
      addReminder(ascend: ascend, endDate: endDate);
    }
  }

  void addReminder({required Ascend ascend, DateTime? endDate}) {
    _ascentsToRemind.add(ascend);
    _ascentsToRemindSet.add(ascend.ascendId);
    if (endDate != null) _endDateMapping[ascend.ascendId] = endDate;

    Map<String, dynamic> entry = {
      DBController.ascendId: ascend.ascendId,
      DBController.reminderCreatedDate: DateTime.now().toIso8601String(),
      DBController.reminderEndDate: endDate,
    };
    DBController().insert(DBController.reminderTable, entry);
  }

  void deleteEntry({required int ascendId}) {
    _ascentsToRemind.removeWhere((element) => element.ascendId == ascendId);
    _endDateMapping.remove(ascendId);
    _ascentsToRemindSet.remove(ascendId);
    DBController().deleteAscendReminder(ascendId);
  }
}
