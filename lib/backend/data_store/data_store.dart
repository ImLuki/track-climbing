import 'package:climbing_track/backend/data_store/ascend_reminder_data_store.dart';
import 'package:climbing_track/backend/data_store/locations_data_store.dart';
import 'package:climbing_track/backend/data_store/setting_data_store.dart';
import 'package:climbing_track/backend/data_store/tooltips_data_store.dart';
import 'package:climbing_track/backend/data_store/search_settings.dart';

class DataStore {
  /// Singleton attributes and settings
  static final DataStore _instance = DataStore._privateConstructor();

  DataStore._privateConstructor();

  factory DataStore() {
    return _instance;
  }

  SettingsDataStore settingsDataStore = SettingsDataStore();
  ToolTipsDataStore toolTipsDataStore = ToolTipsDataStore();
  SearchSettingsDataStore searchSettingsDataStore = SearchSettingsDataStore();
  LocationsDataStore locationsDataStore = LocationsDataStore();
  AscendReminderDataStore ascendReminderDataStore = AscendReminderDataStore();

  Future<void> init() async {
    await Future.wait(
      [
        settingsDataStore.init(),
        toolTipsDataStore.init(),
        searchSettingsDataStore.init(),
        locationsDataStore.init(),
        ascendReminderDataStore.init(),
      ],
    );
  }

  void clear() {
    locationsDataStore.clear();
    ascendReminderDataStore.clear();
  }
}
