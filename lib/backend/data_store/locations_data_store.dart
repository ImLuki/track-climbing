import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/location.dart';
import 'package:geolocator/geolocator.dart';

class LocationsDataStore {
  final List<Location> _locations = [];
  final Map<int, Location> _locationMap = {};

  Future<void> init() async {
    clear();

    List<Map<String, dynamic>> locationsData = await DBController()
        .rawQuery("SELECT ${DBController.locationId}, ${DBController.locationName}, ${DBController.longitude}, "
            "${DBController.latitude}, ${DBController.standardLocationHeight}, ${DBController.locationComment}, "
            "${DBController.locationOutdoor}, ${DBController.locationStandardAscendType}, "
            "${DBController.locationStandardDifficulty}, "
            "MAX(${DBController.sessionTimeStart}) as last_visited FROM ${DBController.locationsTable} "
            "JOIN ${DBController.sessionsTable} using (${DBController.locationId}) "
            "GROUP BY ${DBController.locationId}");

    for (Map<String, dynamic> entry in locationsData) {
      Location location = Location.fromMap(entry);
      _locations.add(location);
      _locationMap[location.locationId!] = location;
    }
  }

  void clear() {
    _locations.clear();
    _locationMap.clear();
  }

  Location locationById(int idx) {
    return _locationMap[idx]!;
  }

  /// Returns location with specified name and outdoor values if exists, otherwise null
  Location? findExistingLocation(String name, bool outdoor) {
    if (_locations.any((element) => element.name.toLowerCase() == name.toLowerCase() && element.outdoor == outdoor)) {
      return _locations.firstWhere(
        (element) => element.name.toLowerCase() == name.toLowerCase() && element.outdoor == outdoor,
      );
    } else {
      return null;
    }
  }

  Future<Location> createUpdateOrGetLocation(Location location) async {
    Location? tmpLocation = findExistingLocation(location.name, location.outdoor);

    if (tmpLocation == null) {
      await addLocation(location);
      return location;
    } else {
      tmpLocation.coordinates = location.coordinates;
      tmpLocation.locationComment = location.locationComment;
      tmpLocation.updateStandardHeight(location.standardHeight, false);
      return tmpLocation;
    }
  }

  /// Remove location with locationId from data store and database,
  /// but only if no session containing this locationId is left
  Future<void> removeLocationIfNoMoreSessionsExists({required int? locationId, required int removedSessionId}) async {
    if (locationId == null) return;
    List<Map<String, dynamic>> result = await DBController().queryRows(
      DBController.sessionsTable,
      where: "${DBController.locationId} == (?) AND ${DBController.sessionId} != (?)",
      whereArgs: [locationId, removedSessionId],
    );

    if (result.isEmpty) {
      _locations.removeWhere((element) => element.locationId == locationId);
      _locationMap.remove(locationId);

      DBController().deleteLocation(locationId);
    }
  }

  /// Adding not existing Location to database and local storage
  /// Location should not exist!
  ///
  /// Will set location_id!
  Future<void> addLocation(Location location) async {
    int id = await DBController().insert(DBController.locationsTable, location.toMap());
    location.locationId = id;
    _locations.add(location);
    _locationMap[location.locationId!] = location;
  }

  List<Location> getIndoorLocations() {
    return _locations.where((element) => !element.outdoor).toList();
  }

  List<Location> getOutdoorLocations() {
    return _locations.where((element) => element.outdoor).toList();
  }

  List<Location> get locations => _locations;

  Location? findNearestLocation(Position coordinates, {int maxDistance = 2500}) {
    double nearestDistance = double.infinity;
    Location? nearestLocation;
    for (Location location in _locations) {
      if (location.coordinates == null) {
        continue;
      }
      double dist = Geolocator.distanceBetween(
        coordinates.latitude,
        coordinates.longitude,
        location.coordinates!.latitude,
        location.coordinates!.longitude,
      );

      if (dist < nearestDistance) {
        nearestDistance = dist;
        nearestLocation = location;
      }
    }

    return nearestDistance <= maxDistance ? nearestLocation : null;
  }
}
