import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:climbing_track/backend/model/active_session.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SearchSettingsDataStore {
  static const String ALL_LOCATIONS_KEY = "all_locations_key";
  static const String ALL_TYPES_KEY = "all_types_key";
  static const String ALL_STYLES_KEY = "all_styles_key";

  /// shared preferences keys
  static const String ASCEND_LIST_SORTING_KEY = "ascend_list_sorting";
  static const String ONLY_CURRENT_LOCATION_KEY = "only_current_location_key";
  static const String INDOOR_KEY = "indoor_key";
  static const String OUTDOOR_KEY = "outdoor_key";
  static const String ONLY_NOT_FINISHED_KEY = "only_not_finished_key";
  static const String ONLY_REDPOINT_KEY = "only_redpoint_key";
  static const String ONLY_MARKED_KEY = "only_marked_key";
  static const String ONLY_BEST_ASCEND_KEY = "only_best_ascend_key";
  static const String ONLY_WITH_NAME_KEY = "only_with_name_key";
  static const String MAP_BY_NAME_KEY = "map_by_name_key";
  static const String CURRENT_LOCATION_KEY = "current_location_key";
  static const String ONLY_TYPE_KEY = "onyl_type_key";
  static const String LEAD_TOPROPE_SELECTION_KEY = "lead_top_selection_key";
  static const String DATE_KEY = "date_key";
  static const String IS_DATE_BEFORE_KEY = "is_date_before_key";
  static const String CLIMBING_STYLE_KEY = "climbing_style_key";

  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _currentSorting = Sorting.values[prefs.getInt(ASCEND_LIST_SORTING_KEY) ?? 0];
    _onlyCurrentLocation = prefs.getBool(ONLY_CURRENT_LOCATION_KEY) ?? false;
    _indoor = prefs.getBool(INDOOR_KEY) ?? true;
    _outdoor = prefs.getBool(OUTDOOR_KEY) ?? true;
    _onlyNotFinished = prefs.getBool(ONLY_NOT_FINISHED_KEY) ?? false;
    _onlyNotAtLeastRedpoint = prefs.getBool(ONLY_REDPOINT_KEY) ?? false;
    _onlyMarked = prefs.getBool(ONLY_MARKED_KEY) ?? false;
    _onlyBestAscend = prefs.getBool(ONLY_BEST_ASCEND_KEY) ?? false;
    _onlyWithName = prefs.getBool(ONLY_WITH_NAME_KEY) ?? false;
    _mapByName = prefs.getBool(MAP_BY_NAME_KEY) ?? false;
    _currentLocation = prefs.getString(CURRENT_LOCATION_KEY) ?? ALL_LOCATIONS_KEY;
    _type = prefs.getString(ONLY_TYPE_KEY) ?? ALL_TYPES_KEY;
    _leadTopropeSelection = prefs.getInt(LEAD_TOPROPE_SELECTION_KEY) ?? -1;
    _dateTime = prefs.getString(DATE_KEY) == null ? null : DateTime.parse(prefs.getString(DATE_KEY)!);
    _isBeforeDate = prefs.getBool(IS_DATE_BEFORE_KEY) ?? false;
    _climbingStyle = prefs.getString(CLIMBING_STYLE_KEY) ?? ALL_STYLES_KEY;
  }

  bool _onlyCurrentLocation = false;
  bool _indoor = true;
  bool _outdoor = true;
  bool _onlyNotFinished = false;
  bool _onlyNotAtLeastRedpoint = false;
  bool _onlyMarked = false;
  bool _onlyBestAscend = false;
  bool _onlyWithName = false;
  bool _mapByName = false;
  int _leadTopropeSelection = -1;
  Sorting _currentSorting = Sorting.NEWEST;
  String _currentLocation = ALL_LOCATIONS_KEY;
  String _type = ALL_TYPES_KEY;
  String _climbingStyle = ALL_STYLES_KEY;
  DateTime? _dateTime;
  bool _isBeforeDate = false;

  void reset() {
    onlyCurrentLocation = false;
    indoor = true;
    outdoor = true;
    onlyNotFinished = false;
    onlyNotAtLeastRedpoint = false;
    onlyMarked = false;
    onlyWithName = false;
    mapByName = false;
    leadTopropeSelection = -1;
    currentSorting = Sorting.NEWEST;
    currentLocation = ALL_LOCATIONS_KEY;
    type = ALL_TYPES_KEY;
    dateTime = null;
    isBeforeDate = false;
    climbingStyle = ALL_STYLES_KEY;
  }

  bool hasCustomSetting() {
    return _onlyCurrentLocation ||
        !_indoor ||
        !_outdoor ||
        _onlyNotFinished ||
        _onlyNotAtLeastRedpoint ||
        _onlyMarked ||
        _onlyBestAscend ||
        _onlyWithName ||
        _mapByName ||
        _leadTopropeSelection != -1 ||
        _currentLocation != ALL_LOCATIONS_KEY ||
        _type != ALL_TYPES_KEY ||
        _dateTime != null ||
        _climbingStyle != ALL_STYLES_KEY;
  }

  List<Map<String, dynamic>> filterData(List<Map<String, dynamic>> data) {
    return data
        .where((element) =>
            dateTimeEval(element) &&
            locationEvaluate(element) &&
            indoorOutdoorEval(element) &&
            typeEval(element) &&
            emptyNameEval(element) &&
            leadTopRopeSelectionEval(element) &&
            onlyMarkedEval(element) &&
            notFinishedEval(element) &&
            redpointEval(element) &&
            evalClimbingStyle(element))
        .toList();
  }

  static String toKey(Map<String, dynamic> dataPoint) {
    return toKeyV(
      dataPoint[DBController.locationName],
      dataPoint[DBController.ascendName],
      dataPoint[DBController.routeTopRope],
    );
  }

  static String toKeyV(String locationName, String ascendName, int topRope) {
    return "$locationName#$ascendName#$topRope";
  }

  /// Returns tuple of
  /// first: all ascents, mapped by name. Every route only once
  /// second: best ascent map
  /// third: Map>String, List>, containing all ascents, mapped by name
  dynamic applyMapByName(List<Map<String, dynamic>> data) {
    List<Map<String, dynamic>> result = [];
    Map<String, Map<String, dynamic>> bestAscents = {};
    Map<String, List<dynamic>> allAscentsMap = {};
    for (Map<String, dynamic> dataPoint in data) {
      if (dataPoint[DBController.ascendName].isEmpty) {
        result.add(dataPoint);
        continue;
      }

      String key = toKey(dataPoint);

      if (!bestAscents.containsKey(key) || _isBetter(dataPoint, bestAscents[key]!)) {
        bestAscents[key] = dataPoint;
      }

      if (!allAscentsMap.containsKey(key)) {
        allAscentsMap[key] = [];
      }
      allAscentsMap[key]!.add(dataPoint);
    }
    result.addAll(bestAscents.values);
    return [result, bestAscents, allAscentsMap];
  }

  bool _isBetter(Map<String, dynamic> d1, Map<String, dynamic> d2) {
    if (StyleController.order[d1[DBController.ascendStyleId]] ==
        StyleController.order[d2[DBController.ascendStyleId]]) {
      if (d1[DBController.routeTopRope] == d2[DBController.routeTopRope]) {
        if ((d1[DBController.ascendTries] ?? 0) == (d2[DBController.ascendTries] ?? 0)) {
          return DateTime.parse(d1[DBController.sessionTimeStart])
              .isAfter(DateTime.parse(d2[DBController.sessionTimeStart]));
        }
        return (d1[DBController.ascendTries] ?? 0) <= (d2[DBController.ascendTries] ?? 0);
      }
      return d1[DBController.routeTopRope] <= d2[DBController.routeTopRope];
    }

    return StyleController.order[d1[DBController.ascendStyleId]]! <=
        StyleController.order[d2[DBController.ascendStyleId]]!;
  }

  bool locationEvaluate(dynamic element) {
    if (_onlyCurrentLocation && ActiveSession().activeSession != null) {
      return element[DBController.locationName] ==
          DataStore().locationsDataStore.locationById(ActiveSession().activeSession!.locationId).name;
    } else if (_currentLocation != ALL_LOCATIONS_KEY) {
      return element[DBController.locationName] == _currentLocation;
    }
    return true;
  }

  bool indoorOutdoorEval(dynamic element) {
    if (!_indoor || !_outdoor) {
      if (!_indoor && !_outdoor) {
        return false;
      } else if (_indoor) {
        return element[DBController.locationOutdoor] == 0;
      } else if (_outdoor) {
        return element[DBController.locationOutdoor] == 1;
      }
    }
    return true;
  }

  bool notFinishedEval(dynamic element) {
    return _onlyNotFinished ? StyleController.getNotFinishedIds().contains(element[DBController.ascendStyleId]) : true;
  }

  bool redpointEval(dynamic element) {
    List<int> redpointIDs = StyleController.getRedpointIds();
    redpointIDs.addAll(StyleController.getBoulderTopIds());
    return _onlyNotAtLeastRedpoint ? !redpointIDs.contains(element[DBController.ascendStyleId]) : true;
  }

  bool leadTopRopeSelectionEval(dynamic element) {
    return _leadTopropeSelection == -1 ? true : element[DBController.routeTopRope] == _leadTopropeSelection;

    // ||
    //             !(ClimbingType.values
    //                 .byName(element[DBController.ascendType] ?? ClimbingType.SPORT_CLIMBING.name)
    //                 .hasLeadAndToprope(speedType: element[DBController.speedType]))
  }

  bool typeEval(dynamic element) {
    return _type != ALL_TYPES_KEY ? element[DBController.ascendType] == _type : true;
  }

  bool emptyNameEval(dynamic element) {
    return _onlyWithName ? element[DBController.ascendName] != "" : true;
  }

  bool onlyMarkedEval(dynamic element) {
    return _onlyMarked ? element[DBController.ascendMarked] == 1 : true;
  }

  bool dateTimeEval(dynamic element) {
    if (_dateTime == null) return true;
    return _isBeforeDate
        ? DateTime.parse(element[DBController.sessionTimeStart]).isBefore(_dateTime!)
        : DateTime.parse(element[DBController.sessionTimeStart]).isAfter(_dateTime!);
  }

  bool evalClimbingStyle(dynamic element) {
    return _climbingStyle != ALL_STYLES_KEY
        ? element[DBController.ascendStyleId] == ClimbingStyle.values.byName(_climbingStyle).id
        : true;
  }

  Sorting get currentSorting => _currentSorting;

  String get type => _type;

  String get currentLocation => _currentLocation;

  String get climbingStyle => _climbingStyle;

  bool get mapByName => _mapByName;

  bool get onlyWithName => _onlyWithName;

  bool get onlyBestAscend => _onlyBestAscend;

  bool get onlyMarked => _onlyMarked;

  bool get onlyNotAtLeastRedpoint => _onlyNotAtLeastRedpoint;

  bool get onlyNotFinished => _onlyNotFinished;

  bool get outdoor => _outdoor;

  bool get indoor => _indoor;

  bool get onlyCurrentLocation => _onlyCurrentLocation;

  bool get isBeforeDate => _isBeforeDate;

  int get leadTopropeSelection => _leadTopropeSelection;

  DateTime? get dateTime => _dateTime;

  set currentSorting(Sorting value) {
    _currentSorting = value;
    SharedPreferences.getInstance().then((pref) => pref.setInt(ASCEND_LIST_SORTING_KEY, value.index));
  }

  set onlyCurrentLocation(bool value) {
    _onlyCurrentLocation = value;
    SharedPreferences.getInstance().then((pref) => pref.setBool(ONLY_CURRENT_LOCATION_KEY, _onlyCurrentLocation));
  }

  set indoor(bool value) {
    _indoor = value;
    SharedPreferences.getInstance().then((pref) => pref.setBool(INDOOR_KEY, value));
  }

  set outdoor(bool value) {
    _outdoor = value;
    SharedPreferences.getInstance().then((pref) => pref.setBool(OUTDOOR_KEY, value));
  }

  set onlyNotFinished(bool value) {
    _onlyNotFinished = value;
    SharedPreferences.getInstance().then((pref) => pref.setBool(ONLY_NOT_FINISHED_KEY, value));
  }

  set onlyNotAtLeastRedpoint(bool value) {
    _onlyNotAtLeastRedpoint = value;
    SharedPreferences.getInstance().then((pref) => pref.setBool(ONLY_REDPOINT_KEY, value));
  }

  set onlyMarked(bool value) {
    _onlyMarked = value;
    SharedPreferences.getInstance().then((pref) => pref.setBool(ONLY_MARKED_KEY, value));
  }

  set onlyBestAscend(bool value) {
    _onlyBestAscend = value;
    SharedPreferences.getInstance().then((pref) => pref.setBool(ONLY_BEST_ASCEND_KEY, value));
  }

  set onlyWithName(bool value) {
    _onlyWithName = value;
    SharedPreferences.getInstance().then((pref) => pref.setBool(ONLY_WITH_NAME_KEY, value));
  }

  set mapByName(bool value) {
    _mapByName = value;
    SharedPreferences.getInstance().then((pref) => pref.setBool(MAP_BY_NAME_KEY, value));
  }

  set currentLocation(String value) {
    _currentLocation = value;
    SharedPreferences.getInstance().then((pref) => pref.setString(CURRENT_LOCATION_KEY, value));
  }

  set type(String value) {
    _type = value;
    SharedPreferences.getInstance().then((pref) => pref.setString(ONLY_TYPE_KEY, value));
  }

  set leadTopropeSelection(int value) {
    _leadTopropeSelection = value;
    SharedPreferences.getInstance().then((pref) => pref.setInt(LEAD_TOPROPE_SELECTION_KEY, value));
  }

  set dateTime(DateTime? value) {
    _dateTime = value;
    if (value == null) {
      SharedPreferences.getInstance().then((pref) => pref.remove(DATE_KEY));
    } else {
      SharedPreferences.getInstance().then((pref) => pref.setString(DATE_KEY, value.toIso8601String()));
    }
  }

  set isBeforeDate(bool value) {
    _isBeforeDate = value;
    SharedPreferences.getInstance().then((pref) => pref.setBool(IS_DATE_BEFORE_KEY, value));
  }

  set climbingStyle(String value) {
    _climbingStyle = value;
    SharedPreferences.getInstance().then((pref) => pref.setString(CLIMBING_STYLE_KEY, value));
  }

  List<Map<String, dynamic>> sortData(List<Map<String, dynamic>> data) {
    switch (_currentSorting) {
      case Sorting.NEWEST:
        data.sort((b, a) => a[DBController.ascendOrder].compareTo(b[DBController.ascendOrder]));
        data.sort((b, a) => a[DBController.sessionTimeStart].compareTo(b[DBController.sessionTimeStart]));
      case Sorting.OLDEST:
        data.sort((a, b) => a[DBController.ascendOrder].compareTo(b[DBController.ascendOrder]));
        data.sort((a, b) => a[DBController.sessionTimeStart].compareTo(b[DBController.sessionTimeStart]));
      case Sorting.ALPHABETIC:
        data.sort((a, b) {
          if (a[DBController.ascendName] == '') {
            return 1; // a comes after b if a's name is empty
          } else if (b[DBController.ascendName] == '') {
            return -1; // a comes before b if b's name is empty
          } else {
            // Otherwise, compare names as usual
            return a[DBController.ascendName].compareTo(b[DBController.ascendName]);
          }
        });
      case Sorting.EASIEST:
        data.sort((a, b) => a[DBController.ascendGradeId].compareTo(b[DBController.ascendGradeId]));
      case Sorting.HARDEST:
        data.sort((b, a) => a[DBController.ascendGradeId].compareTo(b[DBController.ascendGradeId]));
      case Sorting.BEST:
        data.sort((b, a) => a[DBController.sessionTimeStart].compareTo(b[DBController.sessionTimeStart]));
        data.sort((b, a) => a[DBController.ascendRating].compareTo(b[DBController.ascendRating]));
    }
    return data;
  }

  static List<String> climbingMethodNames(BuildContext context) {
    return [
      AppLocalizations.of(context)!.search_settings_all,
      "${AppLocalizations.of(context)!.style_lead} / ${AppLocalizations.of(context)!.style_leading}",
      "${AppLocalizations.of(context)!.style_top} / ${AppLocalizations.of(context)!.style_following}",
      AppLocalizations.of(context)!.style_alternate_leading
    ];
  }
}

enum Sorting { NEWEST, OLDEST, ALPHABETIC, EASIEST, HARDEST, BEST }

extension SortingExtension on Sorting {
  String toName(BuildContext context) {
    switch (this) {
      case Sorting.NEWEST:
        return AppLocalizations.of(context)!.search_setting_newest;
      case Sorting.OLDEST:
        return AppLocalizations.of(context)!.search_setting_oldest;
      case Sorting.ALPHABETIC:
        return AppLocalizations.of(context)!.search_setting_alphabetic;
      case Sorting.EASIEST:
        return AppLocalizations.of(context)!.search_setting_easiest;
      case Sorting.HARDEST:
        return AppLocalizations.of(context)!.search_setting_hardest;
      case Sorting.BEST:
        return AppLocalizations.of(context)!.search_setting_best;
    }
  }
}
