import 'dart:io';

import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/enums/date_format.dart';
import 'package:climbing_track/backend/model/enums/enums.dart';
import 'package:climbing_track/backend/model/enums/height_measurement.dart';
import 'package:climbing_track/backend/model/enums/search_order_enum.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsDataStore {
  /// SharedPreferences keys
  static const String _LIMIT_CHART_KEY = "limit_chart_entries";
  static const String _CHART_ANIMATION_KEY = "chart_animation";
  static const String _NOTIFICATIONS_KEY = "notifications_activated";
  static const String _SESSION_REMINDER_DURATION_KEY = "session_reminder_duration";
  static const String _AUTOMATIC_BACKUP_KEY = "automatic_backups";
  static const String _OVERWRITE_AUTOMATIC_BACKUP_KEY = "overwrite_automatic_backup";
  static const String _HEIGHT_MEASUREMENT_KEY = "height_measurement";
  static const String _LANGUAGE_KEY = "language_key";
  static const String _TICHY_MODE_KEY = "tichy_mode_key";
  static const String _GOOGLE_DRIVE_KEY = "google_drive_key";
  static const String _SESSIONS_ORDER_KEY = "sessions_order_key";
  static const String _CHART_ORDER_KEY = "chart_order_key";
  static const String _FIRST_YEAR_KEY = "first_year_key";
  static const String _GRADE_EXPANDED_SYSTEM_KEY = "expanded_system";
  static const String _EXPANDED_Y_AXIS_KEY = "expanded_y_axis";
  static const String _OVERVIEW_LEADERBOARD_SORTING_INDOOR_KEY = "overview_leaderboard_sorting_indoor";
  static const String _OVERVIEW_LEADERBOARD_ACTIVE_INDOOR_KEY = "overview_leaderboard_active_indoor";
  static const String _OVERVIEW_LEADERBOARD_SORTING_OUTDOOR_KEY = "overview_leaderboard_sorting_outdoor";
  static const String _OVERVIEW_LEADERBOARD_ACTIVE_OUTDOOR_KEY = "overview_leaderboard_active_outdoor";
  static const String _ROUTE_STYLE_PICKER_EXPANDED_KEY = "route_style_picker_expanded";
  static const String _STEEPNESS_EXPANDED_KEY = "steepness_picker_expanded";
  static const String _CURRENT_DATE_FORMAT_KEY = "current_date_format";
  static const String _DO_NOT_SHOW_LOCATION_INFO_AGAIN_KEY = "do_not_show_location_info_again";
  static const String _SHOW_TIMER_BETWEEN_GOES = "show_timer_between_goes";

  /// Default values
  static const int STANDARD_REMINDER_DURATION = 180;

  /// boolean values
  late bool _limitCharts;
  late bool _chartAnimation;
  late bool _notificationsActivated;
  late bool _automaticBackups;
  late bool _overwriteAutomaticBackups;
  late bool _tichyMode;
  late bool _googleDriveActivated;
  late bool _isGradeSystemExpanded;
  late bool _isYAxisExpanded;
  late bool _isRouteStylePickerExpanded;
  late bool _isSteepnessPickerExpanded;
  late bool _showTimerBetweenGoes;

  /// integer values
  late int _firstYear;
  late int _sessionReminderDuration;

  /// String values
  String? _chartOrder;
  late String _language;

  /// enum values
  late SearchOrder _sessionSearchOrder;
  late HeightMeasurement _heightMeasurement;
  late CustomDateFormat _currentDateFormat;

  /// List values
  late List<Classes> _overviewLeaderboardSortingIndoor;
  late List<bool> _overviewLeaderboardActiveIndoor;
  late List<Classes> _overviewLeaderboardSortingOutdoor;
  late List<bool> _overviewLeaderboardActiveOutdoor;
  late List<int> _doNotShowLocationInfoAgain;

  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _limitCharts = prefs.getBool(_LIMIT_CHART_KEY) ?? false;
    _chartAnimation = prefs.getBool(_CHART_ANIMATION_KEY) ?? true;
    _notificationsActivated = prefs.getBool(_NOTIFICATIONS_KEY) ?? true;
    _automaticBackups = prefs.getBool(_AUTOMATIC_BACKUP_KEY) ?? false;
    _overwriteAutomaticBackups = prefs.getBool(_OVERWRITE_AUTOMATIC_BACKUP_KEY) ?? true;
    _tichyMode = prefs.getBool(_TICHY_MODE_KEY) ?? false;
    _googleDriveActivated = prefs.getBool(_GOOGLE_DRIVE_KEY) ?? false;
    _isGradeSystemExpanded = prefs.getBool(_GRADE_EXPANDED_SYSTEM_KEY) ?? true;
    _isYAxisExpanded = prefs.getBool(_EXPANDED_Y_AXIS_KEY) ?? false;
    _isRouteStylePickerExpanded = prefs.getBool(_ROUTE_STYLE_PICKER_EXPANDED_KEY) ?? true;
    _isSteepnessPickerExpanded = prefs.getBool(_STEEPNESS_EXPANDED_KEY) ?? true;
    _showTimerBetweenGoes = prefs.getBool(_SHOW_TIMER_BETWEEN_GOES) ?? true;

    _sessionReminderDuration = prefs.getInt(_SESSION_REMINDER_DURATION_KEY) ?? STANDARD_REMINDER_DURATION;
    _firstYear = prefs.getInt(_FIRST_YEAR_KEY) ?? await loadFirstYear();

    _language = prefs.getString(_LANGUAGE_KEY) ?? Platform.localeName.split("_")[0];
    _chartOrder = prefs.getString(_CHART_ORDER_KEY);

    _sessionSearchOrder = SearchOrder.values.byName(prefs.getString(_SESSIONS_ORDER_KEY) ?? SearchOrder.newest.name);
    String defaultUnit = Platform.localeName == "en_US" ? HeightMeasurement.FEET.name : HeightMeasurement.METER.name;
    _heightMeasurement = HeightMeasurement.values.byName(prefs.getString(_HEIGHT_MEASUREMENT_KEY) ?? defaultUnit);
    _currentDateFormat = prefs.getString(_CURRENT_DATE_FORMAT_KEY) == null
        ? CustomDateFormat.SYSTEM
        : CustomDateFormat.values.byName(prefs.getString(_CURRENT_DATE_FORMAT_KEY)!);

    // overview leaderboard
    List<String>? tmp = prefs.getStringList(_OVERVIEW_LEADERBOARD_SORTING_INDOOR_KEY);
    if (tmp == null || tmp.length != Classes.values.length) {
      _overviewLeaderboardSortingIndoor = Classes.values;
      _overviewLeaderboardActiveIndoor = [true, true, true, true, false, false, false, false, false];
    } else {
      _overviewLeaderboardSortingIndoor = tmp.map((e) => Classes.values.byName(e)).toList();

      tmp = prefs.getStringList(_OVERVIEW_LEADERBOARD_ACTIVE_INDOOR_KEY);
      _overviewLeaderboardActiveIndoor = tmp != null
          ? tmp.map((e) => e == "1" ? true : false).toList()
          : [true, true, true, true, false, false, false, false, false];
    }

    List<String>? tmp2 = prefs.getStringList(_OVERVIEW_LEADERBOARD_SORTING_OUTDOOR_KEY);
    if (tmp2 == null || tmp2.length != Classes.values.length) {
      _overviewLeaderboardSortingOutdoor = Classes.values;
      _overviewLeaderboardActiveOutdoor = [true, true, true, false, true, true, true, true, true];
    } else {
      _overviewLeaderboardSortingOutdoor = tmp2.map((e) => Classes.values.byName(e)).toList();
      tmp2 = prefs.getStringList(_OVERVIEW_LEADERBOARD_ACTIVE_OUTDOOR_KEY);
      _overviewLeaderboardActiveOutdoor = tmp2 != null
          ? tmp2.map((e) => e == "1" ? true : false).toList()
          : [true, true, true, false, true, true, true, true, true];
    }

    _doNotShowLocationInfoAgain =
        (prefs.getStringList(_DO_NOT_SHOW_LOCATION_INFO_AGAIN_KEY) ?? []).map((e) => int.parse(e)).toList();
  }

  Future<int> loadFirstYear() async {
    List<Map<String, dynamic>> result = await DBController().queryRows(
      DBController.sessionsTable,
      orderBy: DBController.sessionTimeStart,
      limit: 1,
    );
    if (result.isEmpty) {
      DateTime now = DateTime.now();
      firstYear = now.year;
      return now.year;
    } else {
      DateTime dateTime = DateTime.parse(result.first[DBController.sessionTimeStart]);
      firstYear = dateTime.year;
      return dateTime.year;
    }
  }

  bool get limitCharts => _limitCharts;

  bool get chartAnimation => _chartAnimation;

  int get sessionReminderDuration => _sessionReminderDuration;

  bool get notificationsActivated => _notificationsActivated;

  bool get overwriteAutomaticBackups => _overwriteAutomaticBackups;

  bool get automaticBackups => _automaticBackups;

  HeightMeasurement get heightMeasurement => _heightMeasurement;

  String get language => _language;

  bool get tichyMode => _tichyMode;

  bool get googleDriveActivated => _googleDriveActivated;

  SearchOrder get sessionSearchOrder => _sessionSearchOrder;

  String? get chartOrder => _chartOrder;

  int get firstYear => _firstYear;

  bool get isGradeSystemExpanded => _isGradeSystemExpanded;

  bool get isYAxisExpanded => _isYAxisExpanded;

  List<Classes> get overviewLeaderboardSortingIndoor => _overviewLeaderboardSortingIndoor;

  List<Classes> get overviewLeaderboardSortingOutdoor => _overviewLeaderboardSortingOutdoor;

  List<bool> get overviewLeaderboardActiveIndoor => _overviewLeaderboardActiveIndoor;

  List<bool> get overviewLeaderboardActiveOutdoor => _overviewLeaderboardActiveOutdoor;

  bool get isSteepnessPickerExpanded => _isSteepnessPickerExpanded;

  bool get isRouteStylePickerExpanded => _isRouteStylePickerExpanded;

  CustomDateFormat get currentDateFormat => _currentDateFormat;

  List<int> get doNotShowLocationInfoAgain => _doNotShowLocationInfoAgain;

  bool get showTimerBetweenGoes => _showTimerBetweenGoes;

  set limitCharts(bool value) {
    _limitCharts = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_LIMIT_CHART_KEY, _limitCharts);
    });
  }

  set chartAnimation(bool value) {
    _chartAnimation = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_CHART_ANIMATION_KEY, _chartAnimation);
    });
  }

  set sessionReminderDuration(int value) {
    _sessionReminderDuration = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setInt(_SESSION_REMINDER_DURATION_KEY, _sessionReminderDuration);
    });
  }

  set notificationsActivated(bool value) {
    _notificationsActivated = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_NOTIFICATIONS_KEY, _notificationsActivated);
    });
  }

  set overwriteAutomaticBackups(bool value) {
    _overwriteAutomaticBackups = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_OVERWRITE_AUTOMATIC_BACKUP_KEY, _overwriteAutomaticBackups);
    });
  }

  set automaticBackups(bool value) {
    _automaticBackups = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_AUTOMATIC_BACKUP_KEY, _automaticBackups);
    });
  }

  set heightMeasurement(HeightMeasurement value) {
    _heightMeasurement = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setString(_HEIGHT_MEASUREMENT_KEY, _heightMeasurement.name);
    });
  }

  set language(String value) {
    _language = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setString(_LANGUAGE_KEY, _language);
    });
  }

  set tichyMode(bool value) {
    _tichyMode = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_TICHY_MODE_KEY, _tichyMode);
    });
  }

  set googleDriveActivated(bool value) {
    _googleDriveActivated = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_GOOGLE_DRIVE_KEY, _googleDriveActivated);
    });
  }

  set sessionSearchOrder(SearchOrder value) {
    _sessionSearchOrder = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setString(_SESSIONS_ORDER_KEY, _sessionSearchOrder.name);
    });
  }

  set chartOrder(String? value) {
    _chartOrder = value;

    SharedPreferences.getInstance().then((prefs) {
      if (value == null) {
        prefs.remove(_CHART_ORDER_KEY);
      } else {
        prefs.setString(_CHART_ORDER_KEY, value);
      }
    });
  }

  void resetChartOrder() {
    _chartOrder = null;
    SharedPreferences.getInstance().then((prefs) {
      prefs.remove(_CHART_ORDER_KEY);
    });
  }

  set firstYear(int value) {
    _firstYear = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setInt(_FIRST_YEAR_KEY, _firstYear);
    });
  }

  set isGradeSystemExpanded(bool value) {
    _isGradeSystemExpanded = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_GRADE_EXPANDED_SYSTEM_KEY, _isGradeSystemExpanded);
    });
  }

  set isYAxisExpanded(bool value) {
    _isYAxisExpanded = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_EXPANDED_Y_AXIS_KEY, _isYAxisExpanded);
    });
  }

  set overviewLeaderboardSortingOutdoor(List<Classes> value) {
    _overviewLeaderboardSortingOutdoor = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setStringList(
        _OVERVIEW_LEADERBOARD_SORTING_OUTDOOR_KEY,
        _overviewLeaderboardSortingOutdoor.map((e) => e.name).toList(),
      );
    });
  }

  set overviewLeaderboardSortingIndoor(List<Classes> value) {
    _overviewLeaderboardSortingIndoor = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setStringList(
        _OVERVIEW_LEADERBOARD_SORTING_INDOOR_KEY,
        _overviewLeaderboardSortingIndoor.map((e) => e.name).toList(),
      );
    });
  }

  set overviewLeaderboardActiveIndoor(List<bool> value) {
    _overviewLeaderboardActiveIndoor = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setStringList(
        _OVERVIEW_LEADERBOARD_ACTIVE_INDOOR_KEY,
        _overviewLeaderboardActiveIndoor.map((e) => e ? "1" : "0").toList(),
      );
    });
  }

  set overviewLeaderboardActiveOutdoor(List<bool> value) {
    _overviewLeaderboardActiveOutdoor = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setStringList(
        _OVERVIEW_LEADERBOARD_ACTIVE_OUTDOOR_KEY,
        _overviewLeaderboardActiveOutdoor.map((e) => e ? "1" : "0").toList(),
      );
    });
  }

  set isSteepnessPickerExpanded(bool value) {
    _isSteepnessPickerExpanded = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_STEEPNESS_EXPANDED_KEY, _isSteepnessPickerExpanded);
    });
  }

  set isRouteStylePickerExpanded(bool value) {
    _isRouteStylePickerExpanded = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_ROUTE_STYLE_PICKER_EXPANDED_KEY, _isRouteStylePickerExpanded);
    });
  }

  set currentDateFormat(CustomDateFormat value) {
    _currentDateFormat = value;

    SharedPreferences.getInstance().then((prefs) {
      if (_currentDateFormat == CustomDateFormat.SYSTEM) {
        prefs.remove(_CURRENT_DATE_FORMAT_KEY);
      } else {
        prefs.setString(_CURRENT_DATE_FORMAT_KEY, _currentDateFormat.name);
      }
    });
  }

  set doNotShowLocationInfoAgain(List<int> value) {
    _doNotShowLocationInfoAgain = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setStringList(
        _DO_NOT_SHOW_LOCATION_INFO_AGAIN_KEY,
        _doNotShowLocationInfoAgain.map((e) => e.toString()).toList(),
      );
    });
  }

  set showTimerBetweenGoes(bool value) {
    _showTimerBetweenGoes = value;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setBool(_SHOW_TIMER_BETWEEN_GOES, _showTimerBetweenGoes);
    });
  }
}
