// ignore_for_file: deprecated_member_use_from_same_package

import 'dart:io';

import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/location.dart';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DBController {
  @protected
  static const _databaseName = 'TrackClimbing.db';

  static const sessionsTable = 'sessions';
  static const ascendsTable = 'routes';
  static const locationsTable = 'locations';
  static const reminderTable = 'reminder';

  /// session table columns
  static const sessionId = 'session_id';
  static const sessionStatus = 'session_status';
  static const sessionTimeStart = 'time_start';
  static const sessionTimeEnd = 'time_end';
  static const sessionComment = 'session_comment';

  /// ascents table columns
  static const ascendId = "route_id";
  static const ascendGradeId = "grade_id";
  static const ascendOriginalGradeSystem = "original_grade_system";
  static const ascendStyleId = "style_id";
  static const routeTopRope = "top_rope";
  static const ascendName = "route_name";
  static const ascendRating = "rating";
  static const ascendMarked = "marked";
  static const ascendComment = "comment";
  static const ascendType = "route_type";
  static const speedTime = "speed_time";
  static const speedType = "speed_type";
  static const ascendHeight = "ascend_height";
  static const ascendTries = "ascend_tries";
  static const ascendCharacteristic = "ascend_characteristic";
  static const ascendSteepness = "steepness";
  static const ascendOrder = "ascend_order";

  /// location table columns
  static const locationId = 'location_id';
  static const locationName = 'location_name';
  static const longitude = 'longitude';
  static const latitude = 'latitude';
  static const standardLocationHeight = 'standard_location_height';
  static const locationComment = 'location_comment';
  static const locationOutdoor = 'location_outdoor';
  static const locationStandardAscendType = 'location_standard_ascend_type';
  static const locationStandardDifficulty = 'location_standard_difficulty';

  /// reminder table columns
  static const reminderId = 'reminder_id';
  static const reminderCreatedDate = 'reminder_created_date';
  static const reminderEndDate = 'reminder_end_date';

  /// deprecated
  @Deprecated("This is an old database schema that is no longer used.")
  static const sessionLocation = 'location';
  @Deprecated("This is an old database schema that is no longer used.")
  static const standardHeightTable = 'standard_height_table';
  @Deprecated("This is an old database schema that is no longer used.")
  static const standardHeightId = 'height_id';
  @Deprecated("This is an old database schema that is no longer used.")
  static const standardHeight = 'height';
  @Deprecated("This is an old database schema that is no longer used.")
  static const sessionOutdoor = 'outdoor';
  @Deprecated("This is an old database schema that is no longer used.")
  static const sessionTableOld = 'session';

  DBController._privateConstructor();

  static final DBController instance = DBController._privateConstructor();

  factory DBController() {
    return instance;
  }

  @protected
  static Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDatabase();
    return _database!;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path, version: 15, onCreate: _onCreate, onUpgrade: _onUpgrade);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $sessionsTable (
            $sessionId INTEGER PRIMARY KEY AUTOINCREMENT,
            $sessionStatus TEXT NOT NULL,
            $sessionTimeStart DATE NOT NULL,
            $sessionTimeEnd DATE,
            $locationId INTEGER,
            $sessionComment TEXT
          )
          ''');
    await db.execute('''
          CREATE TABLE $ascendsTable (
            $ascendId INTEGER PRIMARY KEY AUTOINCREMENT,
            $sessionId INTEGER NOT NULL,
            $ascendGradeId INTEGER NOT NULL,
            $ascendOriginalGradeSystem TEXT,
            $ascendStyleId INTEGER NOT NULL,
            $routeTopRope INTEGER NOT NULL,
            $ascendName TEXT NOT NULL,
            $ascendRating INTEGER NOT NULL,
            $ascendMarked BOOLEAN NOT NULL,
            $ascendComment TEXT,
            $ascendType TEXT,
            $speedTime INTEGER NOT NULL DEFAULT (0),
            $speedType INTEGER NOT NULL DEFAULT (0),
            $ascendHeight DOUBLE NOT NULL DEFAULT (0),
            $ascendTries INTEGER NULL,
            $ascendCharacteristic INTEGER NULL,
            $ascendSteepness INTEGER NULL,
            $ascendOrder INTEGER
          )
          ''');
    await db.execute('''
          CREATE TABLE $locationsTable (
            $locationId INTEGER PRIMARY KEY AUTOINCREMENT ,
            $locationName TEXT ,
            $longitude DOUBLE,
            $latitude DOUBLE,
            $standardLocationHeight DOUBLE,
            $locationComment TEXT,
            $locationOutdoor BOOLEAN NOT NULL,
            $locationStandardAscendType TEXT,
            $locationStandardDifficulty INTEGER
          )
          ''');
    await db.execute('''
          CREATE TABLE $reminderTable (
            $reminderId INTEGER PRIMARY KEY AUTOINCREMENT,
            $ascendId INTEGER NOT NULL,
            $reminderCreatedDate DATE NOT NULL,
            $reminderEndDate DATE
          )
          ''');
  }

  void _onUpgrade(Database db, int oldVersion, int newVersion) async {
    debugPrint("[DATABASE]: on upgrade version $newVersion");

    //add location table
    if (oldVersion <= 3) {
      await db.execute('''
          CREATE TABLE $locationsTable (
            $sessionLocation TEXT PRIMARY KEY,
            $longitude DOUBLE,
            $latitude DOUBLE
          )
          ''');
    }
    // add route type column
    if (oldVersion < 6) {
      await db.execute("ALTER TABLE $ascendsTable ADD COLUMN $ascendType TEXT;");
      await db.execute("UPDATE $ascendsTable SET $ascendType = (?)", [ClimbingType.SPORT_CLIMBING.name]);
    }

    // update route type
    if (oldVersion < 7) {
      await db.update(
        ascendsTable,
        {ascendType: ClimbingType.SPORT_CLIMBING.name},
        where: "$ascendType == (?)",
        whereArgs: ["ROUTE"],
      );
      await db.execute("ALTER TABLE $ascendsTable ADD COLUMN $speedTime INTEGER NOT NULL DEFAULT (0);");
      await db.execute("ALTER TABLE $ascendsTable ADD COLUMN $speedType INTEGER NOT NULL DEFAULT (0);");

      // height
      await db.execute("ALTER TABLE $ascendsTable ADD COLUMN $ascendHeight DOUBLE NOT NULL DEFAULT (0);");
      await db.execute('''
          CREATE TABLE $standardHeightTable (
            $standardHeightId INTEGER PRIMARY KEY,
            $sessionLocation TEXT,
            $standardHeight DOUBLE NOT NULL
          )
          ''');
    }

    // added tries
    if (oldVersion < 8) {
      // height
      await db.execute("ALTER TABLE $ascendsTable ADD COLUMN $ascendTries INTEGER NULL;");
    }

    // changed database scheme to one table for locations
    // added location comment
    if (oldVersion < 9) {
      const String locationsTableNew = "new_locations";

      // query data
      List<Map<String, Object?>> query = await db.rawQuery(
        "SELECT "
        "$sessionLocation as $locationName, "
        "$longitude, $latitude, "
        "$standardHeight as $standardLocationHeight, "
        "$sessionOutdoor as $locationOutdoor "
        "FROM $sessionTableOld "
        "LEFT JOIN ${DBController.standardHeightTable} using(${DBController.sessionLocation}) "
        "LEFT JOIN ${DBController.locationsTable} using(${DBController.sessionLocation}) "
        "GROUP BY ${DBController.sessionLocation}, ${DBController.sessionOutdoor}",
      );

      // create new table
      await db.execute('''
          CREATE TABLE IF NOT EXISTS $locationsTableNew (
            $locationId INTEGER PRIMARY KEY AUTOINCREMENT ,
            $locationName TEXT ,
            $longitude DOUBLE,
            $latitude DOUBLE,
            $standardLocationHeight DOUBLE,
            $locationComment TEXT,
            $locationOutdoor BOOLEAN NOT NULL
          )
          ''');

      // copy all data
      Batch batch = db.batch();
      for (Map<String, dynamic> row in query) {
        batch.insert(locationsTableNew, row);
      }
      await batch.commit();

      // drop old table
      await db.rawQuery("DROP TABLE IF EXISTS $locationsTable");
      await db.rawQuery("DROP TABLE IF EXISTS $standardHeightTable");

      // rename the new one
      await db.rawQuery("ALTER TABLE $locationsTableNew RENAME TO $locationsTable;");

      // updating sessions table
      //      # renaming from session -> sessions
      //      # removing outdoor and location attribute and replacing with location_id

      // query data
      List<Map<String, Object?>> sessionsQuery = await db.rawQuery(
        "SELECT "
        "$sessionId, $sessionStatus, $sessionTimeStart, $sessionTimeEnd, $locationId "
        "FROM $sessionTableOld s "
        "JOIN ${DBController.locationsTable} l on l.$locationName == s.$sessionLocation "
        "AND l.$locationOutdoor == s.$sessionOutdoor ",
      );

      // create new table
      await db.execute('''
          CREATE TABLE $sessionsTable (
            $sessionId INTEGER PRIMARY KEY AUTOINCREMENT,
            $sessionStatus TEXT NOT NULL,
            $sessionTimeStart DATE NOT NULL,
            $sessionTimeEnd DATE,
            $locationId INTEGER
          )
          ''');

      // copy all data
      Batch batchSessions = db.batch();
      for (Map<String, dynamic> row in sessionsQuery) {
        batchSessions.insert(sessionsTable, row);
      }
      await batchSessions.commit();

      // drop old table
      await db.rawQuery("DROP TABLE IF EXISTS $sessionTableOld");
    }

    // added ascend characteristic + steepness
    if (oldVersion < 10) {
      await db.execute("ALTER TABLE $ascendsTable ADD COLUMN $ascendCharacteristic INTEGER NULL;");
      await db.execute("ALTER TABLE $ascendsTable ADD COLUMN $ascendSteepness INTEGER NULL;");
    }

    // session comment
    if (oldVersion < 11) {
      await db.execute("ALTER TABLE $sessionsTable ADD COLUMN $sessionComment TEXT;");
    }

    // original grading system
    if (oldVersion < 12) {
      await db.execute("ALTER TABLE $ascendsTable ADD COLUMN $ascendOriginalGradeSystem TEXT;");
    }

    // order key
    if (oldVersion < 13) {
      await db.execute("ALTER TABLE $ascendsTable ADD COLUMN $ascendOrder INTEGER;");
      await db.execute("UPDATE $ascendsTable SET $ascendOrder = $ascendId;");
    }

    // reminder table
    if (oldVersion < 14) {
      await db.execute('''
          CREATE TABLE IF NOT EXISTS $reminderTable (
            $reminderId INTEGER PRIMARY KEY AUTOINCREMENT,
            $ascendId INTEGER NOT NULL,
            $reminderCreatedDate DATE NOT NULL,
            $reminderEndDate DATE
          )
          ''');
    }

    if (oldVersion < 15) {
      await db.execute("ALTER TABLE $locationsTable ADD COLUMN $locationStandardAscendType TEXT;");
      await db.execute("ALTER TABLE $locationsTable ADD COLUMN $locationStandardDifficulty INTEGER;");
    }
  }

  Future<int> insert(String table, Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(table, row);
  }

  Future<void> batchInsert(String table, Iterable<dynamic> rows) async {
    Database db = await instance.database;
    Batch batch = db.batch();
    for (dynamic row in rows) {
      batch.insert(table, row);
    }

    await batch.commit();
  }

  Future<List<Map<String, dynamic>>> queryAllRows(String table) async {
    Database db = await instance.database;
    return await db.query(table);
  }

  Future<List<Map<String, dynamic>>> rawQuery(String sql, [List<Object?>? arguments]) async {
    Database db = await instance.database;
    return await db.rawQuery(sql, arguments);
  }

  Future<List<Map<String, dynamic>>> queryRows(
    String table, {
    String? where,
    List<Object?>? whereArgs,
    String? groupBy,
    String? orderBy,
    int? limit,
  }) async {
    Database db = await instance.database;
    return await db.query(
      table,
      where: where,
      whereArgs: whereArgs,
      orderBy: orderBy,
      groupBy: groupBy,
      limit: limit,
    );
  }

  Future<int> deleteAscend(int id) async {
    Database db = await instance.database;
    deleteAscendReminder(id);
    return await db.delete(DBController.ascendsTable, where: '${DBController.ascendId} = ?', whereArgs: [id]);
  }

  Future<int> deleteAscends(int id) async {
    Database db = await instance.database;

    // delete ascend reminders
    List<Map<String, dynamic>> deleteAscends = await queryAscendsBySessionId(id);
    for (Map<String, dynamic> row in deleteAscends) {
      deleteAscendReminder(row[DBController.ascendId]);
    }

    // delete ascends
    return await db.delete(DBController.ascendsTable, where: '${DBController.sessionId} == (?)', whereArgs: [id]);
  }

  Future<int> deleteLocation(int id) async {
    Database db = await instance.database;
    return await db.delete(DBController.locationsTable, where: '${DBController.locationId} == (?)', whereArgs: [id]);
  }

  Future<int> deleteSession({required int id, required int locationId}) async {
    DataStore().locationsDataStore.removeLocationIfNoMoreSessionsExists(locationId: locationId, removedSessionId: id);
    Database db = await instance.database;
    return await db.delete(DBController.sessionsTable, where: '${DBController.sessionId} == (?)', whereArgs: [id]);
  }

  Future<List<Map<String, dynamic>>> queryAscendsBySessionId(int id) async {
    Database db = await instance.database;
    return await db.query(DBController.ascendsTable, where: '${DBController.sessionId} = ?', whereArgs: [id]);
  }

  Future<void> updateSession({required Map<String, dynamic> row, required int sessionId}) async {
    Database db = await instance.database;
    await db.update(DBController.sessionsTable, row, where: "${DBController.sessionId} = ?", whereArgs: [sessionId]);
  }

  /// Update location entry by location id
  Future<void> updateLocation(Location location) async {
    Database db = await instance.database;
    await db.update(
      locationsTable,
      location.toMap(),
      where: "${DBController.locationId} = ?",
      whereArgs: [location.locationId],
    );
  }

  Future<void> updateAscend(Map<String, dynamic> row, int ascendId) async {
    Database db = await instance.database;
    await db.update(DBController.ascendsTable, row, where: "${DBController.ascendId} = ?", whereArgs: [ascendId]);
  }

  void deleteAll() async {
    Database db = await instance.database;
    db.delete(DBController.ascendsTable);
    db.delete(DBController.sessionsTable);
    db.delete(DBController.locationsTable);
    db.delete(DBController.reminderTable);
  }

  Future<void> updateAscendHeights(double oldHeight, double newHeight, int locationId) async {
    Database db = await instance.database;
    await db.rawQuery(
      "UPDATE ${DBController.ascendsTable} "
      "SET ${DBController.ascendHeight} = (?) "
      "WHERE (${DBController.ascendHeight} == (?) OR ${DBController.ascendHeight} == (?)) AND "
      "${DBController.ascendsTable}.${DBController.sessionId} IN "
      "(SELECT ${DBController.sessionId} FROM ${DBController.sessionsTable} WHERE ${DBController.locationId} == (?))",
      [newHeight, oldHeight, 0.0, locationId],
    );
  }

  Future<int> deleteAscendReminder(int ascendId) async {
    Database db = await instance.database;
    return await db.delete(DBController.reminderTable, where: '${DBController.ascendId} = ?', whereArgs: [ascendId]);
  }
}
