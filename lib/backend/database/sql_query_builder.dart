import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';

class SQLQueryBuilder {
  static String getRedpointCondition() {
    return "(${DBController.ascendStyleId} <= ${ClimbingStyle.REDPOINT.id} OR "
        "${DBController.ascendStyleId} == ${ClimbingStyle.REPEAT.id})";
  }

  static String getBoulderTopCondition() {
    return "(${DBController.ascendStyleId} <= ${ClimbingStyle.TOP.id} OR "
        "${DBController.ascendStyleId} == ${ClimbingStyle.REPEAT_BOULDER.id})";
  }
}
