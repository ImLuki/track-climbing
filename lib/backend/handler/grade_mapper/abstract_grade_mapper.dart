import 'dart:math';

import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/backup/json_parser.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class AbstractGradeMapper {
  static const String SYSTEM_KEY = "systems";
  static const String EXTENDED_KEY = "extended";
  static const String GRADES_KEY = "grades";

  late String _currentGradingSystem;
  late String currentGradePickerSystem;

  /// Map containing grade labels as String for each grade_id and grade system.
  ///
  /// Structure: [Map]<[gradeId], [Map]<[gradingSystem], [gradeLabel]>>
  final Map<int, dynamic> _idGradeMapping = {};

  /// Map containing List of extended grade_ids (more difficult levels) grades for each grading system.
  ///
  /// Structure: [Map]<[gradingSystem], [List]<[gradeId]>>
  late Map<String, dynamic> _extendedGrades;

  /// Map containing List of grade_ids (only basic difficult levels) grades for each grading system.
  ///
  /// Structure: [Map]<[gradingSystem], [List]<[gradeId]>>
  late Map<String, dynamic> _normalGrades;

  /// Reversed Mapping from grade label to grade id
  ///
  /// /// Structure: [Map]<[gradeLabel], [gradeId]>
  final Map<String, int> _reverseMapper = {};

  /// Mapping [gradeId] to index of [_normalGrades] List
  ///
  /// Structure: [Map]<[gradeId], [Map]<[gradingSystem], [normalizedIndex]>>
  final Map<int, Map<String, int>> _reverseMapperNormalized = {};

  /// Mapping [gradeId] to index of [_extendedGrades] List
  ///
  /// Structure: [Map]<[gradeId], [Map]<[gradingSystem], [normalizedIndex]>>
  final Map<int, Map<String, int>> _reverseMapperNormalizedExtended = {};

  /// Map containing List of grade labels (only basic difficult levels) for each grading system.
  ///
  /// Structure: [Map]<[gradingSystem], [List]<[gradeLabel]>>
  final Map<String, List<String>> _gradeLabels = {};

  /// Map containing List of grade_ids (only basic difficult levels) grades for each grading system.
  ///
  /// Structure: [Map]<[gradingSystem], [List]<[gradeLabel]>>
  final Map<String, List<String>> _extendedGradeLabels = {};

  Future<bool> init() async {
    _currentGradingSystem = getStandardGradeSystem();
    await _loadGradingSystem();
    await _loadSystem();
    _loadLabels();
    return true;
  }

  Future<void> _loadSystem() async {
    currentGradePickerSystem = _currentGradingSystem;

    JsonParser jsonParser = JsonParser();
    var mapping = await jsonParser.parseAssetJsonFile(getFile());

    _extendedGrades = mapping[EXTENDED_KEY];
    _normalGrades = mapping[SYSTEM_KEY];

    _idGradeMapping;
    for (MapEntry<String, dynamic> item in mapping[GRADES_KEY].entries) {
      _idGradeMapping[int.parse(item.key)] = item.value;
    }

    for (MapEntry<int, dynamic> item in _idGradeMapping.entries) {
      for (MapEntry<String, dynamic> gradeLabels in item.value.entries) {
        _reverseMapper[gradeLabels.value] = item.key;
      }
    }
  }

  void _loadLabels() {
    // normal grades
    for (MapEntry<String, dynamic> item in _normalGrades.entries) {
      // for each grading system
      // map ids to label

      _gradeLabels[item.key] = List<String>.from(item.value.map((e) => _idGradeMapping[e][item.key]));
    }

    // extended grades
    for (MapEntry<String, dynamic> item in _extendedGrades.entries) {
      // for each grading system
      // map ids to label
      _extendedGradeLabels[item.key] = List<String>.from(item.value.map((e) => _idGradeMapping[e][item.key]));
    }

    // compute reversed normalized mapper
    final List<int> counters = List.generate(getGradeSystems().length, (index) => 0);
    final List<int> countersExtended = List.generate(getGradeSystems().length, (index) => 0);
    for (int gradeId in _idGradeMapping.keys) {
      _reverseMapperNormalized[gradeId] = {};
      _reverseMapperNormalizedExtended[gradeId] = {};
      for (int index = 0; index < getGradeSystems().length; index++) {
        String system = getGradeSystems()[index].toLowerCase();
        if (gradeId >= _normalGrades[system][counters[index] + 1]) {
          counters[index]++;
        }
        if (gradeId >= _extendedGrades[system][countersExtended[index] + 1]) {
          countersExtended[index]++;
        }

        _reverseMapperNormalized[gradeId]![system] = counters[index];
        _reverseMapperNormalizedExtended[gradeId]![system] = countersExtended[index];
      }
    }
  }

  String get currentGradingSystem => _currentGradingSystem;

  set currentGradingSystem(String value) {
    _currentGradingSystem = value;
    _saveToSharedFiles(getGradingSystemKey(), _currentGradingSystem);
  }

  _saveToSharedFiles(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  _loadGradingSystem() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _currentGradingSystem = prefs.getString(getGradingSystemKey()) ?? getStandardGradeSystem();
  }

  String getFile();

  String lowestValueKey();

  List<String> getGradeSystems();

  String getStandardGradeSystem();

  String getGradingSystemKey();

  /// Get grade label from [gradeId] for specific [gradingSystem].
  /// If [gradingSystem] is null, [_currentGradingSystem] will be used
  ///
  /// Return grade label as [String]
  String gradeFromId(int gradeId, {String? gradingSystem}) {
    // if non existing grading system
    if (!getGradeSystems().any((element) => element.toLowerCase() == gradingSystem)) gradingSystem = null;
    return _idGradeMapping[gradeId][gradingSystem ?? _currentGradingSystem.toLowerCase()];
  }

  /// Get grade label from [gradeId] for specific [gradingSystem].
  /// Pays attention to [isGradeSystemExpanded] and returns only appropriate labels
  /// If [gradingSystem] is null, [_currentGradingSystem] will be used
  ///
  /// Return grade label as [String]
  String roundedGradeFromId(int gradeId, {String? gradingSystem, bool? expandedSystem}) {
    int index = idToIndex(gradeId, expandedSystem: expandedSystem, gradingSystem: gradingSystem);
    return labelFromIndex(index, expandedSystem: expandedSystem, gradingSystem: gradingSystem);
  }

  /// Get gradeId from grade label.
  /// Works for all grading systems.
  ///
  /// Return gradId as [int]
  int? idFromGrade(String grade) {
    return _reverseMapper[grade];
  }

  List<String> getPickerGrades() {
    return DataStore().settingsDataStore.isGradeSystemExpanded
        ? _extendedGradeLabels[currentGradePickerSystem.toLowerCase()]!
        : _gradeLabels[currentGradePickerSystem.toLowerCase()]!;
  }

  /// Normalize grade between 0 and 1
  double normalize(int gradeId, {bool? expandedSystem}) {
    return expandedSystem ?? DataStore().settingsDataStore.isGradeSystemExpanded
        ? _reverseMapperNormalizedExtended[gradeId]![currentGradingSystem.toLowerCase()]! /
            _extendedGrades[currentGradingSystem.toLowerCase()].length
        : _reverseMapperNormalized[gradeId]![currentGradingSystem.toLowerCase()]! /
            _normalGrades[currentGradingSystem.toLowerCase()].length;
  }

  /// Return normalized list length
  int maxIndexOfNormalized({bool? expandedSystem, String? gradingSystem}) {
    return getLabels(expandedSystem: expandedSystem, gradingSystem: gradingSystem).length - 1;
  }

  int indexFromNormalized(double normalized, {bool? expandedSystem, String? gradingSystem}) {
    return expandedSystem ?? DataStore().settingsDataStore.isGradeSystemExpanded
        ? (normalized * _extendedGrades[gradingSystem ?? currentGradingSystem.toLowerCase()].length).round()
        : (normalized * _normalGrades[gradingSystem ?? currentGradingSystem.toLowerCase()].length).round();
  }

  String labelFromIndex(int index, {bool? expandedSystem, String? gradingSystem}) {
    return getLabels(expandedSystem: expandedSystem, gradingSystem: gradingSystem)[
        max(min(maxIndexOfNormalized(expandedSystem: expandedSystem, gradingSystem: gradingSystem), index), 0)];
  }

  int idToIndex(int gradeId, {bool? expandedSystem, String? gradingSystem}) {
    return expandedSystem ?? DataStore().settingsDataStore.isGradeSystemExpanded
        ? _reverseMapperNormalizedExtended[gradeId]![gradingSystem ?? currentGradingSystem.toLowerCase()]!
        : _reverseMapperNormalized[gradeId]![gradingSystem ?? currentGradingSystem.toLowerCase()]!;
  }

  List<String> getLabels({bool? expandedSystem, String? gradingSystem}) {
    return expandedSystem ?? DataStore().settingsDataStore.isGradeSystemExpanded
        ? _extendedGradeLabels[gradingSystem ?? _currentGradingSystem.toLowerCase()]!
        : _gradeLabels[gradingSystem ?? _currentGradingSystem.toLowerCase()]!;
  }

  int gradeIdFromNormalized(double normalized, {bool? expandedSystem, String? gradingSystem}) {
    var grades = expandedSystem ?? DataStore().settingsDataStore.isGradeSystemExpanded
        ? _extendedGrades[gradingSystem ?? _currentGradingSystem.toLowerCase()]
        : _normalGrades[gradingSystem ?? _currentGradingSystem.toLowerCase()];
    return grades[indexFromNormalized(normalized, expandedSystem: expandedSystem, gradingSystem: gradingSystem)];
  }
}
