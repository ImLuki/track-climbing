import 'dart:io';

import 'package:climbing_track/backend/handler/grade_mapper/abstract_grade_mapper.dart';

class BoulderGradeMapper extends AbstractGradeMapper {
  @override
  String getFile() {
    return "boulder_system.json";
  }

  @override
  List<String> getGradeSystems() {
    return ["Fontainebleau", "V-Scale"];
  }

  @override
  String getGradingSystemKey() {
    return "boulder_grading_system";
  }

  @override
  String getStandardGradeSystem() {
    String languageCode = Platform.localeName;
    if (languageCode == "en_US") {
      return "V-Scale";
    }
    return "Fontainebleau";
  }

  @override
  String lowestValueKey() {
    return "9700";
  }
}
