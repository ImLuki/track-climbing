import 'dart:io';

import 'package:climbing_track/backend/handler/grade_mapper/abstract_grade_mapper.dart';

class ClimbingGradeMapper extends AbstractGradeMapper {
  @override
  String getFile() {
    return "grade_system.json";
  }

  @override
  List<String> getGradeSystems() {
    return ["UIAA", "French", "YDS", "AUS"];
  }

  @override
  String getGradingSystemKey() {
    return "grading_system";
  }

  @override
  String getStandardGradeSystem() {
    String languageCode = Platform.localeName;
    if (languageCode.startsWith("de")) {
      return "UIAA";
    } else if (languageCode == "en_AU ") {
      return "AUS";
    } else if (languageCode == "en_US") {
      return "YDS";
    }
    return "French";
  }

  @override
  String lowestValueKey() {
    return "100";
  }
}
