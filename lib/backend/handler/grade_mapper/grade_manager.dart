import 'package:climbing_track/backend/handler/grade_mapper/abstract_grade_mapper.dart';
import 'package:climbing_track/backend/handler/grade_mapper/boulder_grade_mapper.dart';
import 'package:climbing_track/backend/handler/grade_mapper/climbing_grade_mapper.dart';
import 'package:climbing_track/backend/handler/grade_mapper/ice_grade_mapper.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';

class GradeManager {
  GradeManager._privateConstructor();

  static final GradeManager _instance = GradeManager._privateConstructor();

  factory GradeManager() {
    return _instance;
  }

  static const int STANDARD_ROUTE_GRADE = 1100;
  static const int STANDARD_BOULDER_GRADE = 11700;
  static const int STANDARD_ICE_GRADE = 20;

  late AbstractGradeMapper climbingMapper;
  late AbstractGradeMapper boulderMapper;
  late AbstractGradeMapper iceMapper;

  Future<void> init() async {
    climbingMapper = ClimbingGradeMapper();
    boulderMapper = BoulderGradeMapper();
    iceMapper = IceGradeMapper();
    await Future.wait(
      [
        climbingMapper.init(),
        boulderMapper.init(),
        iceMapper.init(),
      ],
    );
  }

  AbstractGradeMapper getGradeMapper({required ClimbingType type}) {
    switch (type) {
      case ClimbingType.ICE_CLIMBING:
        return iceMapper;
      case ClimbingType.BOULDER:
        return boulderMapper;
      case ClimbingType.SPEED_CLIMBING:
      case ClimbingType.FREE_SOLO:
      case ClimbingType.DEEP_WATER_SOLO:
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.MULTI_PITCH:
      case ClimbingType.TRAD_CLIMBING:
        return climbingMapper;
    }
  }

  int getStandardGrade(ClimbingType? type) {
    if (type == null) return STANDARD_ROUTE_GRADE;
    switch (type) {
      case ClimbingType.ICE_CLIMBING:
        return STANDARD_ICE_GRADE;
      case ClimbingType.BOULDER:
        return STANDARD_BOULDER_GRADE;
      case ClimbingType.SPEED_CLIMBING:
      case ClimbingType.FREE_SOLO:
      case ClimbingType.DEEP_WATER_SOLO:
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.MULTI_PITCH:
      case ClimbingType.TRAD_CLIMBING:
        return STANDARD_ROUTE_GRADE;
    }
  }
}
