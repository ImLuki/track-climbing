import 'package:climbing_track/backend/handler/grade_mapper/abstract_grade_mapper.dart';

class IceGradeMapper extends AbstractGradeMapper {
  @override
  String getFile() {
    return "ice_climbing.json";
  }

  @override
  List<String> getGradeSystems() {
    return ["WI-Scale"];
  }

  @override
  String getGradingSystemKey() {
    return "ice_grading_system";
  }

  @override
  String getStandardGradeSystem() {
    return "WI-Scale";
  }

  @override
  String lowestValueKey() {
    return "5";
  }
}
