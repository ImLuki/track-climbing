import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocationHandler {
  static const String LOCATION_ENABLED_KEY = "location_enabled";

  LocationHandler._privateConstructor();

  static final LocationHandler _instance = LocationHandler._privateConstructor();

  factory LocationHandler() {
    return _instance;
  }

  late bool _saveLocation;

  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _saveLocation = prefs.getBool(LOCATION_ENABLED_KEY) ?? false;
  }

  bool get saveLocation => _saveLocation;

  Future<void> setLocation(bool value) async {
    _saveLocation = value;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(LOCATION_ENABLED_KEY, value);
  }

  Future<bool> hasPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return false;
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return false;
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse && permission != LocationPermission.always) {
        return false;
      }
    }

    return true;
  }

  Future<Position> determinePosition() async {
    if (await hasPermission()) {
      return await Geolocator.getCurrentPosition();
    } else {
      return Future.error("");
    }
  }
}
