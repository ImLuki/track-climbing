// ignore_for_file: use_build_context_synchronously
import 'package:climbing_track/app/climbing_track_app.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/active_session.dart';
import 'package:climbing_track/main.dart';
import 'package:climbing_track/ui/dialog/confirm_dialog.dart';
import 'package:climbing_track/view_model/home_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

class NotificationHandler {
  NotificationHandler._privateConstructor();

  factory NotificationHandler() {
    return _instance;
  }

  static const int NOTIFICATION_ID_REMINDER = 1;
  static const int NOTIFICATION_ID_BACKUP = 2;
  static const String REMIND_ME_LATER_LABEL = "remind_later";
  static final NotificationHandler _instance = NotificationHandler._privateConstructor();

  static FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  bool _initialized = false;

  void init() async {
    if (_initialized) {
      return;
    }
    _initialized = true;

    bool? request = await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
        ?.requestNotificationsPermission();

    if (request != true) {
      DataStore().settingsDataStore.notificationsActivated = false;
    }

    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_icon_transparent');
    final DarwinInitializationSettings initializationSettingsDarwin =
        DarwinInitializationSettings(onDidReceiveLocalNotification: onDidReceiveIOSNotification);
    const LinuxInitializationSettings initializationSettingsLinux =
        LinuxInitializationSettings(defaultActionName: 'Open notification');

    final InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsDarwin,
      linux: initializationSettingsLinux,
    );
    flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onDidReceiveNotificationResponse: onDidReceiveLocalNotification,
      onDidReceiveBackgroundNotificationResponse: notificationTapBackground,
    );
    await _configureLocalTimeZone();
  }

  Future<void> _configureLocalTimeZone() async {
    tz.initializeTimeZones();
  }

  void onDidReceiveLocalNotification(NotificationResponse notificationResponse) async {
    int index;
    if (notificationResponse.id == NOTIFICATION_ID_BACKUP) {
      index = 5;
    } else {
      index = 0;
    }

    InitializationApp.navigatorKey.currentState!.pushReplacement(
      MaterialPageRoute<void>(
        builder: (BuildContext context) => ClimbingTrackApp(selectedIndex: index),
      ),
    );

    if (notificationResponse.id == NOTIFICATION_ID_REMINDER) {
      ScaffoldMessenger.of(InitializationApp.navigatorKey.currentContext!).removeCurrentSnackBar();

      if (ActiveSession().activeSession == null) {
        return;
      }

      ConfirmDialog().showEndSessionDialog(
        context: InitializationApp.navigatorKey.currentContext!,
        homeViewModel: HomeViewModel(),
        session: ActiveSession().activeSession!,
        routeCount: ActiveSession().ascents.length,
      );
    }
  }

  void onDidReceiveIOSNotification(int id, String? title, String? body, String? payload) async {}

  Future<void> cancelNotification() async {
    await NotificationHandler.flutterLocalNotificationsPlugin.cancelAll();
  }

  Future<void> scheduleReminderNotification(BuildContext context, {DateTime? startTime}) async {
    if (!DataStore().settingsDataStore.notificationsActivated) {
      return;
    }

    await cancelNotification();
    startTime ??= DateTime.now();

    int reminderDurationInMinutes = DataStore().settingsDataStore.sessionReminderDuration;
    int minutesSinceStart = DateTime.now().difference(startTime).inMinutes.abs();

    int minutesAtReminderSinceStart = minutesSinceStart + reminderDurationInMinutes;

    // Using payload to store translations because in background service AppLocalization isn't available
    String startTimeString = startTime.toIso8601String();
    String payload = "$startTimeString;"
        "${AppLocalizations.of(context)!.notification_reminder((minutesAtReminderSinceStart / 60).toStringAsFixed(1))};"
        "${AppLocalizations.of(context)!.notification_reminder_question};"
        "${AppLocalizations.of(context)!.remind_me_later}";

    NotificationDetails androidNotificationDetails = NotificationDetails(
      android: AndroidNotificationDetails(
        'notifications_id',
        'reminder_channel',
        priority: Priority.high,
        importance: Importance.high,
        fullScreenIntent: false,
        enableVibration: true,
        actions: [
          AndroidNotificationAction(REMIND_ME_LATER_LABEL, AppLocalizations.of(context)!.remind_me_later),
        ],
      ),
    );

    await NotificationHandler.flutterLocalNotificationsPlugin.zonedSchedule(
      NOTIFICATION_ID_REMINDER,
      AppLocalizations.of(context)!.notification_reminder((minutesAtReminderSinceStart / 60).toStringAsFixed(1)),
      AppLocalizations.of(context)!.notification_reminder_question,
      tz.TZDateTime.now(tz.local).add(Duration(minutes: reminderDurationInMinutes)),
      androidNotificationDetails,
      androidScheduleMode: AndroidScheduleMode.exactAllowWhileIdle,
      uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime,
      payload: payload,
    );
  }

  Future<void> sendNotification({
    required String title,
    required String body,
    int notificationId = 0,
    bool playSound = true,
    bool enableVibration = true,
  }) async {
    BigTextStyleInformation bigTextStyleInformation = BigTextStyleInformation(body); //multi-line show style

    NotificationDetails androidNotificationDetails = NotificationDetails(
      android: AndroidNotificationDetails(
        'notifications_id',
        'reminder_channel',
        priority: Priority.high,
        importance: Importance.high,
        fullScreenIntent: false,
        enableVibration: enableVibration,
        playSound: playSound,
        styleInformation: bigTextStyleInformation,
      ),
    );

    await NotificationHandler.flutterLocalNotificationsPlugin.show(
      notificationId,
      title,
      body,
      androidNotificationDetails,
    );
  }

  static void notificationTapBackground(NotificationResponse notificationResponse) async {
    if (notificationResponse.actionId == REMIND_ME_LATER_LABEL) {
      await _remindMeLaterSchedule(notificationResponse);
    }
  }

  static Future<void> _remindMeLaterSchedule(NotificationResponse notificationResponse) async {
    // restoring title, subtitle and button translation from payload data
    List<String> payload = notificationResponse.payload!.split(";");
    DateTime starTime = DateTime.parse(payload[0]);
    int reminderTime = DateTime.now().difference(starTime).inMinutes + 60;
    String timeString = payload[1].replaceAll(RegExp(r'[^0-9|.]'), '');
    String title = payload[1].replaceAll(timeString, ((reminderTime / 30).round() / 2).toStringAsFixed(1));

    await flutterLocalNotificationsPlugin.cancelAll();
    tz.initializeTimeZones();
    NotificationDetails androidNotificationDetails = NotificationDetails(
      android: AndroidNotificationDetails(
        'notifications_id',
        'reminder_channel',
        priority: Priority.high,
        importance: Importance.high,
        fullScreenIntent: false,
        enableVibration: true,
        actions: [
          AndroidNotificationAction(REMIND_ME_LATER_LABEL, payload[3]),
        ],
      ),
    );

    flutterLocalNotificationsPlugin.zonedSchedule(
      NOTIFICATION_ID_REMINDER,
      title,
      payload[2],
      tz.TZDateTime.now(tz.local).add(const Duration(minutes: 30)),
      androidNotificationDetails,
      androidScheduleMode: AndroidScheduleMode.exactAllowWhileIdle,
      uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime,
      payload: notificationResponse.payload,
    );
  }
}
