import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/enums/enums.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class StyleController {
  static Map<int, int> order = {
    ClimbingStyle.ONSIGHT.id: 0,
    ClimbingStyle.FLASH.id: 1,
    ClimbingStyle.REDPOINT.id: 2,
    ClimbingStyle.REPEAT.id: 3,
    ClimbingStyle.AF.id: 4,
    ClimbingStyle.HANGDOGGING.id: 4,
    ClimbingStyle.AID.id: 6,
    ClimbingStyle.THREE_QUARTERS.id: 7,
    ClimbingStyle.HALF.id: 8,
    ClimbingStyle.ONE_QUARTER.id: 9,
    ClimbingStyle.FLASH_BOULDERING.id: 10,
    ClimbingStyle.TOP.id: 11,
    ClimbingStyle.REPEAT_BOULDER.id: 12,
    ClimbingStyle.PROJECT.id: 13,
  };

  static ClimbingStyle getDefaultClimbingStyle(ClimbingType? climbingType) {
    if (climbingType == null) return ClimbingStyle.REDPOINT;
    switch (climbingType) {
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.SPEED_CLIMBING:
      case ClimbingType.FREE_SOLO:
      case ClimbingType.DEEP_WATER_SOLO:
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.MULTI_PITCH:
      case ClimbingType.TRAD_CLIMBING:
        return ClimbingStyle.REDPOINT;
      case ClimbingType.BOULDER:
        return ClimbingStyle.TOP;
    }
  }

  static List<int> getClimbingStyleIdsByClimbingType(ClimbingType climbingType) {
    switch (climbingType) {
      case ClimbingType.BOULDER:
        return [100, 101, 103, 102];
      case ClimbingType.DEEP_WATER_SOLO:
        return [1, 2, 3, 10, 6, 7, 8];
      case ClimbingType.SPEED_CLIMBING:
      case ClimbingType.FREE_SOLO:
        return [3];
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.MULTI_PITCH:
      case ClimbingType.TRAD_CLIMBING:
        return [1, 2, 3, 10, 4, 5, 9, 6, 7, 8];
    }
  }

  static List<ClimbingStyle> getClimbingStylesByClimbingType(ClimbingType climbingType) {
    return getClimbingStyleIdsByClimbingType(climbingType).map((e) => ClimbingStyle.getById(e)).toList();
  }

  static List<int> getBoulderTopIds() => [
        ClimbingStyle.FLASH_BOULDERING.id,
        ClimbingStyle.TOP.id,
        ClimbingStyle.REPEAT_BOULDER.id,
      ];

  static List<int> getRedpointIds() => [
        ClimbingStyle.ONSIGHT.id,
        ClimbingStyle.FLASH.id,
        ClimbingStyle.REDPOINT.id,
        ClimbingStyle.REPEAT.id,
      ];

  static List<int> getNotFinishedIds() => [
        ClimbingStyle.ONE_QUARTER.id,
        ClimbingStyle.HALF.id,
        ClimbingStyle.THREE_QUARTERS.id,
        ClimbingStyle.PROJECT.id,
      ];

  /// Maps style id to [StyleType] instance
  ///
  /// Return [StyleType] instance
  static StyleType mapClimbingStyleToStyleType(int style) {
    switch (ClimbingStyle.getById(style)) {
      case ClimbingStyle.ONSIGHT:
        return StyleType.ONSIGHT;
      case ClimbingStyle.FLASH:
      case ClimbingStyle.FLASH_BOULDERING:
        return StyleType.FLASH;
      case ClimbingStyle.REDPOINT:
      case ClimbingStyle.REPEAT:
        return StyleType.REDPOINT;
      case ClimbingStyle.AF:
      case ClimbingStyle.HANGDOGGING:
      case ClimbingStyle.TOP:
      case ClimbingStyle.REPEAT_BOULDER:
        return StyleType.TOP;
      case ClimbingStyle.PROJECT:
        return StyleType.PROJECT;
      case ClimbingStyle.THREE_QUARTERS:
      case ClimbingStyle.HALF:
      case ClimbingStyle.ONE_QUARTER:
      case ClimbingStyle.AID:
        return StyleType.NONE;
    }
  }

  static int getMinTriesValue(int styleId) {
    switch (ClimbingStyle.getById(styleId)) {
      case ClimbingStyle.ONSIGHT:
      case ClimbingStyle.FLASH:
      case ClimbingStyle.AF:
      case ClimbingStyle.FLASH_BOULDERING:
      case ClimbingStyle.PROJECT:
      case ClimbingStyle.HANGDOGGING:
      case ClimbingStyle.REPEAT:
      case ClimbingStyle.REPEAT_BOULDER:
        return 1;
      case ClimbingStyle.REDPOINT:
      case ClimbingStyle.TOP:
        return 2;
      case ClimbingStyle.THREE_QUARTERS:
      case ClimbingStyle.HALF:
      case ClimbingStyle.ONE_QUARTER:
      case ClimbingStyle.AID:
        return 0;
    }
  }
}

enum ClimbingStyle {
  ONSIGHT(1),
  FLASH(2),
  REDPOINT(3),
  AF(4),
  HANGDOGGING(5),
  THREE_QUARTERS(6),
  HALF(7),
  ONE_QUARTER(8),
  AID(9),
  REPEAT(10),
  FLASH_BOULDERING(100),
  TOP(101),
  PROJECT(102),
  REPEAT_BOULDER(103);

  const ClimbingStyle(this.id);

  final int id;

  static ClimbingStyle getById(int id) {
    return ClimbingStyle.values.firstWhere((x) => x.id == id);
  }
}

extension StyleTypeExtension on ClimbingStyle {
  /// Translates StyleType enum to translated String for UI
  String toTranslatedString(BuildContext context, {boulderExtra = false}) {
    switch (this) {
      case ClimbingStyle.ONSIGHT:
        return AppLocalizations.of(context)!.style_onsight;
      case ClimbingStyle.FLASH_BOULDERING:
        if (boulderExtra) {
          return "${AppLocalizations.of(context)!.style_flash} (${AppLocalizations.of(context)!.style_bouldering})";
        }
        return AppLocalizations.of(context)!.style_flash;
      case ClimbingStyle.FLASH:
        return AppLocalizations.of(context)!.style_flash;
      case ClimbingStyle.REDPOINT:
        return AppLocalizations.of(context)!.style_redpoint;
      case ClimbingStyle.AF:
        return AppLocalizations.of(context)!.style_af;
      case ClimbingStyle.HANGDOGGING:
        return AppLocalizations.of(context)!.style_hangdogging;
      case ClimbingStyle.THREE_QUARTERS:
        return '3/4';
      case ClimbingStyle.HALF:
        return '1/2';
      case ClimbingStyle.ONE_QUARTER:
        return '1/4';
      case ClimbingStyle.AID:
        return AppLocalizations.of(context)!.style_aid;
      case ClimbingStyle.TOP:
        return AppLocalizations.of(context)!.style_top;
      case ClimbingStyle.PROJECT:
        return AppLocalizations.of(context)!.style_project;
      case ClimbingStyle.REPEAT:
        if (boulderExtra) {
          return "${AppLocalizations.of(context)!.style_repeat} (${AppLocalizations.of(context)!.style_bouldering})";
        }
        return AppLocalizations.of(context)!.style_repeat;
      case ClimbingStyle.REPEAT_BOULDER:
        return AppLocalizations.of(context)!.style_repeat;
    }
  }

  bool isFirstTry() {
    switch (this) {
      case ClimbingStyle.ONSIGHT:
      case ClimbingStyle.FLASH:
      case ClimbingStyle.FLASH_BOULDERING:
        return true;
      case ClimbingStyle.REDPOINT:
      case ClimbingStyle.REPEAT:
      case ClimbingStyle.AF:
      case ClimbingStyle.HANGDOGGING:
      case ClimbingStyle.THREE_QUARTERS:
      case ClimbingStyle.HALF:
      case ClimbingStyle.ONE_QUARTER:
      case ClimbingStyle.AID:
      case ClimbingStyle.TOP:
      case ClimbingStyle.PROJECT:
      case ClimbingStyle.REPEAT_BOULDER:
        return false;
    }
  }

  /// Returns true if tries stand for attempts, false otherwise (then tries == hangs)
  /// Also true if tries stands for repetitions
  bool hasAttempts() {
    switch (this) {
      case ClimbingStyle.REDPOINT:
      case ClimbingStyle.TOP:
      case ClimbingStyle.PROJECT:
        return true;
      case ClimbingStyle.ONSIGHT:
      case ClimbingStyle.FLASH:
      case ClimbingStyle.FLASH_BOULDERING:
      case ClimbingStyle.REPEAT:
      case ClimbingStyle.REPEAT_BOULDER:
      case ClimbingStyle.AF:
      case ClimbingStyle.HANGDOGGING:
      case ClimbingStyle.THREE_QUARTERS:
      case ClimbingStyle.HALF:
      case ClimbingStyle.ONE_QUARTER:
      case ClimbingStyle.AID:
        return false;
    }
  }

  /// Returns true if tries stand for attempts, false otherwise (then tries == hangs)
  bool hasRepetitions() {
    switch (this) {
      case ClimbingStyle.REPEAT:
      case ClimbingStyle.REPEAT_BOULDER:
        return true;
      case ClimbingStyle.TOP:
      case ClimbingStyle.PROJECT:
      case ClimbingStyle.ONSIGHT:
      case ClimbingStyle.FLASH:
      case ClimbingStyle.FLASH_BOULDERING:
      case ClimbingStyle.REDPOINT:
      case ClimbingStyle.AF:
      case ClimbingStyle.HANGDOGGING:
      case ClimbingStyle.THREE_QUARTERS:
      case ClimbingStyle.HALF:
      case ClimbingStyle.ONE_QUARTER:
      case ClimbingStyle.AID:
        return false;
    }
  }

  /// Returns string tries repetitions or hangs - string, corresponding to climbing style
  String getTriesString(int count, BuildContext context) {
    switch (this) {
      case ClimbingStyle.ONSIGHT:
      case ClimbingStyle.FLASH:
      case ClimbingStyle.FLASH_BOULDERING:
      case ClimbingStyle.REDPOINT:
      case ClimbingStyle.TOP:
      case ClimbingStyle.PROJECT:
        return "$count. ${AppLocalizations.of(context)!.ascend_attempts(1)}";
      case ClimbingStyle.REPEAT:
      case ClimbingStyle.REPEAT_BOULDER:
        return "$count. ${AppLocalizations.of(context)!.general_repetition(count)}";
      case ClimbingStyle.AF:
      case ClimbingStyle.HANGDOGGING:
      case ClimbingStyle.THREE_QUARTERS:
      case ClimbingStyle.HALF:
      case ClimbingStyle.ONE_QUARTER:
      case ClimbingStyle.AID:
        return "$count ${AppLocalizations.of(context)!.ascend_hangs(count)}";
    }
  }
}
