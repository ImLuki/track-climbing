import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/session.dart';

class ActiveSession {
  /// Singleton attributes and settings
  static final ActiveSession _instance = ActiveSession._privateConstructor();

  ActiveSession._privateConstructor();

  factory ActiveSession() {
    return _instance;
  }

  List<Ascend> ascents = [];
  Session? activeSession;

  Future<void> init() async {
    List<Map<String, dynamic>> result = await DBController().queryRows(
      DBController.sessionsTable,
      where: "${DBController.sessionStatus} == (?)",
      whereArgs: [Session.SESSION_ACTIVE_STATE],
    );
    if (result.isEmpty) {
      activeSession = null;
      ascents = [];
    } else {
      activeSession = Session.fromMap(result.first);

      List<Map<String, dynamic>> ascentsQuery = await DBController().queryRows(
        DBController.ascendsTable,
        where: "${DBController.sessionId} == (?)",
        whereArgs: [activeSession!.sessionId],
      );

      ascents = List.generate(ascentsQuery.length, (index) => Ascend.fromMap(ascentsQuery[index]));
    }
  }

  void clear() {
    activeSession = null;
    ascents.clear();
  }
}
