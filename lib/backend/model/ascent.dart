import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:flutter/cupertino.dart';

class Ascend with ChangeNotifier {
  int ascendId;
  int _gradeID;
  String? originalGradeSystem;
  int styleID;
  int toprope;
  int rating;
  String _name;
  DateTime? dateTime;
  String? location;
  bool marked;
  String comment;
  ClimbingType type;
  int speedTime;
  int speedType;
  double height;
  int? tries;
  int characteristic;
  int steepness;
  int? orderKey;

  Ascend(
    this._gradeID,
    this.originalGradeSystem,
    this.styleID,
    this.toprope,
    this.rating,
    this._name,
    this.dateTime,
    this.location,
    this.marked, {
    this.comment = "",
    this.ascendId = -1,
    this.type = ClimbingType.SPORT_CLIMBING,
    this.speedTime = 0,
    this.speedType = 0,
    this.height = 0,
    this.tries = 2,
    this.characteristic = 0,
    this.steepness = 0,
    this.orderKey,
  });

  static Ascend getDefault({
    required DateTime? startDate,
    required String? locationName,
    required double standardHeight,
    ClimbingType? climbingType,
    int? gradeId,
  }) {
    return Ascend(
      gradeId ?? GradeManager().getStandardGrade(climbingType),
      null,
      StyleController.getDefaultClimbingStyle(climbingType).id,
      0,
      3,
      "",
      startDate,
      locationName,
      false,
      height: standardHeight,
      tries: 2,
      type: climbingType ?? ClimbingType.SPORT_CLIMBING,
    );
  }

  static Ascend fromMap(Map<String, dynamic> data) => Ascend(
        data[DBController.ascendGradeId],
        data[DBController.ascendOriginalGradeSystem],
        data[DBController.ascendStyleId]!,
        data[DBController.routeTopRope] ?? 0,
        data[DBController.ascendRating] ?? 3,
        data[DBController.ascendName] ?? "",
        data[DBController.sessionTimeStart] == null ? null : DateTime.parse(data[DBController.sessionTimeStart]),
        data[DBController.locationId] == null
            ? null
            : DataStore().locationsDataStore.locationById(data[DBController.locationId]).name,
        data[DBController.ascendMarked] == 1,
        ascendId: data[DBController.ascendId] ?? -1,
        comment: data[DBController.ascendComment] ?? "",
        type: ClimbingType.values.byName(data[DBController.ascendType] ?? ClimbingType.SPORT_CLIMBING.name),
        tries: data[DBController.ascendTries],
        speedTime: data[DBController.speedTime] ?? 0,
        speedType: data[DBController.speedType] ?? 0,
        height: data[DBController.ascendHeight] ?? 0,
        characteristic: data[DBController.ascendCharacteristic] ?? 0,
        steepness: data[DBController.ascendSteepness] ?? 0,
        orderKey: data[DBController.ascendOrder],
      );

  Ascend copy() {
    return Ascend(
      _gradeID,
      originalGradeSystem,
      styleID,
      toprope,
      rating,
      _name,
      dateTime,
      location,
      marked,
      ascendId: ascendId,
      comment: comment,
      type: type,
      speedTime: speedTime,
      speedType: speedType,
      height: height,
      tries: tries,
      characteristic: characteristic,
      steepness: steepness,
      orderKey: orderKey,
    );
  }

  Map<String, dynamic> toMap(int sessionId) {
    return {
      DBController.sessionId: sessionId,
      DBController.ascendGradeId: _gradeID,
      DBController.ascendOriginalGradeSystem: originalGradeSystem,
      DBController.ascendStyleId: styleID,
      DBController.routeTopRope: toprope,
      DBController.ascendName: _name,
      DBController.ascendRating: rating,
      DBController.ascendMarked: marked ? 1 : 0,
      DBController.ascendComment: comment,
      DBController.ascendType: type.name,
      DBController.speedTime: speedTime,
      DBController.speedType: speedType,
      DBController.ascendHeight: height,
      DBController.ascendTries: tries,
      DBController.ascendCharacteristic: characteristic,
      DBController.ascendSteepness: steepness,
      DBController.ascendOrder: orderKey,
    };
  }

  Map<String, dynamic> toJson(int sessionId, int routeId) {
    return {
      DBController.ascendId: routeId,
      DBController.sessionId: sessionId,
      DBController.ascendGradeId: _gradeID,
      DBController.ascendOriginalGradeSystem: originalGradeSystem,
      DBController.ascendStyleId: styleID,
      DBController.routeTopRope: toprope,
      DBController.ascendName: _name,
      DBController.ascendRating: rating,
      DBController.ascendMarked: marked ? 1 : 0,
      DBController.ascendComment: comment,
      DBController.ascendType: type.name,
      DBController.speedTime: speedTime,
      DBController.speedType: speedType,
      DBController.ascendHeight: height,
      DBController.ascendTries: tries,
      DBController.ascendCharacteristic: characteristic,
      DBController.ascendSteepness: steepness,
      DBController.ascendOrder: orderKey,
    };
  }

  bool isSame(Ascend ascend) {
    return gradeID == ascend.gradeID &&
        styleID == ascend.styleID &&
        toprope == ascend.toprope &&
        name == ascend.name &&
        rating == ascend.rating &&
        comment == ascend.comment &&
        type == ascend.type &&
        speedTime == ascend.speedTime &&
        speedType == speedType &&
        height == ascend.height &&
        tries == ascend.tries &&
        characteristic == ascend.characteristic &&
        steepness == ascend.steepness &&
        orderKey == ascend.orderKey;
  }

  bool isBoulder() {
    return type.isBoulder();
  }

  String get name => _name;

  set name(String value) {
    _name = value;
    notifyListeners();
  }

  int get gradeID => _gradeID;

  set gradeID(int value) {
    _gradeID = value;
    notifyListeners();
  }
}
