class DataPoint<T> {
  final T xPoint;
  final double yPoint;

  DataPoint(this.xPoint, this.yPoint);

  @override
  String toString() {
    return "$xPoint - $yPoint";
  }
}

class DateTimeDataPoint {
  final DateTime xPoint;
  final double? yPoint;

  DateTimeDataPoint(this.xPoint, this.yPoint);
}

class DoubleDataPoint extends DataPoint<double> {
  DoubleDataPoint(super.xPoint, super.yPoint);
}

class StringDataPoint extends DataPoint<String> {
  StringDataPoint(super.xPoint, super.yPoint);
}
