import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/average_boulder_grade_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/best_and_average_grades_boulder.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/best_grades_boulder.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/boulder_grades_distribution_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/boulder_last_year_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/characteristics_boulder_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/steepness_boulder_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/styles_boulder_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/timeline_boulder_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/average_climbing_grade_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/best_and_average_grades_climbing.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/best_grades_climbing.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/characteristics_climbing_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/climbing_grades_distribution_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/climbing_last_year_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/lead_toprope_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/steepness_climbing_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/styles_climbing.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/timeline_climbing.dart';
import 'package:climbing_track/ui/pages/statistics/factory/dws/average_dws_grade_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/dws/best_and_average_grades_dws_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/dws/best_grades_dws.dart';
import 'package:climbing_track/ui/pages/statistics/factory/dws/characteristics_dws_cahrt.dart';
import 'package:climbing_track/ui/pages/statistics/factory/dws/dws_grades_distribution_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/dws/dws_last_year_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/dws/steepness_dws_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/dws/styles_dws_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/dws/timeline_dws_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/general/heatmap_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/general/height_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/general/location_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/general/route_count.dart';
import 'package:climbing_track/ui/pages/statistics/factory/general/session_time.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/average_ice_grade_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/best_and_average_grades_ice_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/best_grades_ice_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/characteristics_ice_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/ice_grades_distribution_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/ice_last_year_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/lead_toprope_ice_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/steepness_ice_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/styles_ice_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/timeline_ice_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/multi_pitch/average_multi_pitch_grade_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/multi_pitch/best_and_average_grades_multi_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/multi_pitch/best_grades_multi_pitch.dart';
import 'package:climbing_track/ui/pages/statistics/factory/multi_pitch/characteristics_multi_pitch_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/multi_pitch/lead_toprope_multi_pitch.dart';
import 'package:climbing_track/ui/pages/statistics/factory/multi_pitch/multi_pitch_grades_distribution.dart';
import 'package:climbing_track/ui/pages/statistics/factory/multi_pitch/multi_pitch_last_year_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/multi_pitch/steepness_multi_pitch_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/multi_pitch/styles_multi_pitch.dart';
import 'package:climbing_track/ui/pages/statistics/factory/multi_pitch/timeline_multi_pitch.dart';
import 'package:climbing_track/ui/pages/statistics/factory/solo/average_solo_grade_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/solo/best_and_average_grades_solo_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/solo/best_grades_solo.dart';
import 'package:climbing_track/ui/pages/statistics/factory/solo/characteristics_solo_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/solo/solo_grades_distribution_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/solo/solo_last_year_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/solo/steepness_solo_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/solo/timeline_solo_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/speed/best_speed_times.dart';
import 'package:climbing_track/ui/pages/statistics/factory/speed/speed_last_year_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/speed/timeline_speed_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/trad/average_trad_grade_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/trad/best_and_average_grades_trad_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/trad/best_grades_trad_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/trad/characteristics_trad_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/trad/grades_distribution_trad_charts.dart';
import 'package:climbing_track/ui/pages/statistics/factory/trad/last_year_trad_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/trad/lead_toprope_trad_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/trad/steepness_trad_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/trad/styles_trad_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/trad/timeline_trad_chart.dart';

class StatisticCharts {
  static const int MAX_SESSION_AMOUNT = 16;

  static Future<void> init({ChartType currentChartType = ChartType.ALL}) async {
    await Future.wait(chartList.map((e) => e.init()));
  }

  static final List<AbstractChart> chartList = [
    BestGradesClimbingChart(),
    ClimbingStyleChart(),
    RouteCountChart(),
    LeadTopropeChart(),
    SessionTimeChart(),
    LocationsChart(),
    AverageClimbingGradeChart(),
    LastYearClimbingChart(),
    ClimbingGradesDistributionChart(),
    HeightChart(),
    TimelineClimbingChart(),
    HeatMapChart(),
    SteepnessClimbingChart(),
    CharacteristicsClimbingChart(),
    BestAndAverageGradesClimbingChart(),

    // Boulder
    BestGradesBoulderChart(),
    BoulderStyleChart(),
    LastYearBoulderChart(),
    BoulderGradesDistributionChart(),
    AverageBoulderGradeChart(),
    TimelineBoulderChart(),
    SteepnessBoulderChart(),
    CharacteristicsBoulderChart(),
    BestAndAverageGradesBoulderChart(),

    // SPEED
    BestSpeedTimes(),
    LastYearSpeedChart(),
    TimelineSpeedChart(),

    // DWS
    BestGradesDwsChart(),
    DwsStylesChart(),
    AverageDwsGradeChart(),
    LastYearDwsChart(),
    DwsGradesDistributionChart(),
    TimelineDWSChart(),
    SteepnessDwsChart(),
    CharacteristicsDwsChart(),
    BestAndAverageGradesDwsChart(),

    // ICE
    BestGradesIceChart(),
    IceStylesChart(),
    LeadTopropeIceChart(),
    AverageIceGradeChart(),
    LastYearIceChart(),
    IceGradesDistributionChart(),
    TimelineIceChart(),
    SteepnessIceChart(),
    CharacteristicsIceChart(),
    BestAndAverageGradesIceChart(),

    //SOLO
    BestGradesFreeSoloChart(),
    AverageFreeSoloGradeChart(),
    LastYearFreeSoloChart(),
    SoloGradesDistributionChart(),
    TimelineSoloChart(),
    SteepnessSoloChart(),
    CharacteristicsSoloChart(),
    BestAndAverageGradesSoloChart(),

    // MULTI PITCH
    BestGradesMultiPitchChart(),
    MultiPitchStyleChart(),
    LeadTopropeMultiPitchChart(),
    AverageMultiPitchGradeChart(),
    LastYearMultiPitchChart(),
    MultiPitchGradesDistributionChart(),
    TimelineMultiPitchChart(),
    SteepnessMultiPitchChart(),
    CharacteristicsMultiPitchChart(),
    BestAndAverageGradesMultiChart(),

    // TRAD
    BestGradesTradChart(),
    TradStyleChart(),
    LeadTopropeTradChart(),
    AverageTradGradeChart(),
    LastYearTradChart(),
    TradGradesDistributionChart(),
    TimelineTradChart(),
    SteepnessTradChart(),
    CharacteristicsTradChart(),
    BestAndAverageGradesTradChart(),
  ];
}
