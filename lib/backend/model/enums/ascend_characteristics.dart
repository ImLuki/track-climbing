import 'package:climbing_track/ui/util/climbing_tracker_icons_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

enum AscendCharacteristics {
  CRUX(1 << 0),
  ATHLETIC(1 << 1),
  SLOPER(1 << 2),
  ENDURANCE(1 << 3),
  TECHNICAL(1 << 4),
  CRIMP(1 << 5),
  DYNAMIC(1 << 6),
  CRAG(1 << 7),
  POCKET(1 << 8),
  PINCH(1 << 9),
  SIT_START(1 << 10),
  CAMPUS(1 << 11);

  const AscendCharacteristics(this.value);

  final int value;

  /// Returns `true` if [routeStyle] has at least one member in [flag]
  static bool hasFlag(AscendCharacteristics routeStyle, int flag) {
    return routeStyle.value & flag != 0;
  }

  static List<AscendCharacteristics> getValuesFromFlag(int flag) {
    return AscendCharacteristics.values.where((element) => AscendCharacteristics.hasFlag(element, flag)).toList();
  }
}

extension AscendStylesExtension on AscendCharacteristics {
  /// Translates StyleType enum to translated String for UI
  String toTranslatedString(BuildContext context) {
    switch (this) {
      case AscendCharacteristics.CRUX:
        return AppLocalizations.of(context)!.ascend_style_crux;
      case AscendCharacteristics.ATHLETIC:
        return AppLocalizations.of(context)!.ascend_style_athletic;
      case AscendCharacteristics.SLOPER:
        return AppLocalizations.of(context)!.ascend_style_sloper;
      case AscendCharacteristics.ENDURANCE:
        return AppLocalizations.of(context)!.ascend_style_endurance;
      case AscendCharacteristics.TECHNICAL:
        return AppLocalizations.of(context)!.ascend_style_technical;
      case AscendCharacteristics.CRIMP:
        return AppLocalizations.of(context)!.ascend_style_crimp;
      case AscendCharacteristics.DYNAMIC:
        return AppLocalizations.of(context)!.ascend_style_dynamic;
      case AscendCharacteristics.CRAG:
        return AppLocalizations.of(context)!.ascend_style_crack;
      case AscendCharacteristics.POCKET:
        return AppLocalizations.of(context)!.ascend_style_pocket;
      case AscendCharacteristics.PINCH:
        return AppLocalizations.of(context)!.ascend_style_pinch;
      case AscendCharacteristics.SIT_START:
        return AppLocalizations.of(context)!.ascend_style_sit_start;
      case AscendCharacteristics.CAMPUS:
        return AppLocalizations.of(context)!.ascend_style_campus;
    }
  }

  IconData getIcon() {
    switch (this) {
      case AscendCharacteristics.CRUX:
        return Icons.key_outlined;
      case AscendCharacteristics.ATHLETIC:
        return Icons.fitness_center_outlined;
      case AscendCharacteristics.SLOPER:
        return ClimbingTrackerIcons.sloper5;
      case AscendCharacteristics.ENDURANCE:
        return Icons.timer_outlined;
      case AscendCharacteristics.TECHNICAL:
        return FontAwesomeIcons.gears;
      case AscendCharacteristics.CRIMP:
        return ClimbingTrackerIcons.crimp_neu;
      case AscendCharacteristics.DYNAMIC:
        return FontAwesomeIcons.bomb;
      case AscendCharacteristics.CRAG:
        return ClimbingTrackerIcons.crack;
      case AscendCharacteristics.POCKET:
        return ClimbingTrackerIcons.pocket;
      case AscendCharacteristics.PINCH:
        return ClimbingTrackerIcons.pincers;
      case AscendCharacteristics.SIT_START:
        return ClimbingTrackerIcons.chair_1;
      case AscendCharacteristics.CAMPUS:
        return ClimbingTrackerIcons.no_climbing_shoes;
    }
  }

  Widget getIconWidget(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(4.0),
      decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.surface,
          shape: BoxShape.circle,
          border: Border.all(color: Colors.black12)),
      child: Icon(getIcon(), size: 16),
    );
  }
}
