import 'package:flutter/cupertino.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

enum ClimbingType {
  SPORT_CLIMBING,
  BOULDER,
  SPEED_CLIMBING,
  MULTI_PITCH,
  TRAD_CLIMBING,
  ICE_CLIMBING,
  DEEP_WATER_SOLO,
  FREE_SOLO,
}

extension ClimbingTypeExtension on ClimbingType {
  /// Translates ClimbingType enum to translated String for UI
  String toName(BuildContext context) {
    switch (this) {
      case ClimbingType.SPORT_CLIMBING:
        return AppLocalizations.of(context)!.style_sport_climbing;
      case ClimbingType.BOULDER:
        return AppLocalizations.of(context)!.style_bouldering;
      case ClimbingType.SPEED_CLIMBING:
        return AppLocalizations.of(context)!.style_speed_climbing;
      case ClimbingType.FREE_SOLO:
        return AppLocalizations.of(context)!.style_free_solo_climbing;
      case ClimbingType.DEEP_WATER_SOLO:
        return AppLocalizations.of(context)!.style_dws_climbing;
      case ClimbingType.ICE_CLIMBING:
        return AppLocalizations.of(context)!.style_ice_climbing;
      case ClimbingType.MULTI_PITCH:
        return AppLocalizations.of(context)!.style_multi_pitch_climbing;
      case ClimbingType.TRAD_CLIMBING:
        return AppLocalizations.of(context)!.style_trad_climbing;
    }
  }

  String toTitle(BuildContext context) {
    switch (this) {
      case ClimbingType.SPORT_CLIMBING:
        return AppLocalizations.of(context)!.style_sport_climbing_ascent;
      case ClimbingType.SPEED_CLIMBING:
        return AppLocalizations.of(context)!.style_speed_ascend;
      case ClimbingType.DEEP_WATER_SOLO:
        return AppLocalizations.of(context)!.style_dws_ascent;
      case ClimbingType.FREE_SOLO:
        return AppLocalizations.of(context)!.style_free_solo_ascent;
      case ClimbingType.ICE_CLIMBING:
        return AppLocalizations.of(context)!.style_ice_ascent;
      case ClimbingType.BOULDER:
        return AppLocalizations.of(context)!.style_boulder;
      case ClimbingType.MULTI_PITCH:
        return AppLocalizations.of(context)!.style_multi_pitch_ascent;
      case ClimbingType.TRAD_CLIMBING:
        return AppLocalizations.of(context)!.style_trad_ascent;
    }
  }

  String getDescription(BuildContext context) {
    switch (this) {
      case ClimbingType.SPORT_CLIMBING:
        return AppLocalizations.of(context)!.info_dialog_sport_climbing_info;
      case ClimbingType.SPEED_CLIMBING:
        return AppLocalizations.of(context)!.info_dialog_speed_climbing_info;
      case ClimbingType.DEEP_WATER_SOLO:
        return AppLocalizations.of(context)!.info_dialog_dws_info;
      case ClimbingType.FREE_SOLO:
        return AppLocalizations.of(context)!.info_dialog_free_soloing_info;
      case ClimbingType.ICE_CLIMBING:
        return AppLocalizations.of(context)!.info_dialog_ice_climbing_info;
      case ClimbingType.BOULDER:
        return AppLocalizations.of(context)!.info_dialog_bouldering_info;
      case ClimbingType.MULTI_PITCH:
        return AppLocalizations.of(context)!.info_dialog_multi_pitch_info;
      case ClimbingType.TRAD_CLIMBING:
        return AppLocalizations.of(context)!.info_dialog_trad_info;
    }
  }

  String getClimbingStyleText(BuildContext context, {int count = 2}) {
    switch (this) {
      case ClimbingType.BOULDER:
        return AppLocalizations.of(context)!.boulder_style(count);
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.SPEED_CLIMBING:
      case ClimbingType.DEEP_WATER_SOLO:
      case ClimbingType.FREE_SOLO:
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.MULTI_PITCH:
      case ClimbingType.TRAD_CLIMBING:
        return AppLocalizations.of(context)!.climbing_styles(count);
    }
  }

  bool isBoulder() {
    switch (this) {
      case ClimbingType.BOULDER:
        return true;
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.SPEED_CLIMBING:
      case ClimbingType.DEEP_WATER_SOLO:
      case ClimbingType.FREE_SOLO:
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.MULTI_PITCH:
      case ClimbingType.TRAD_CLIMBING:
        return false;
    }
  }

  bool isSpeed() {
    switch (this) {
      case ClimbingType.SPEED_CLIMBING:
        return true;
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.BOULDER:
      case ClimbingType.DEEP_WATER_SOLO:
      case ClimbingType.FREE_SOLO:
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.MULTI_PITCH:
      case ClimbingType.TRAD_CLIMBING:
        return false;
    }
  }

  bool isFreeSolo() {
    switch (this) {
      case ClimbingType.FREE_SOLO:
        return true;
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.BOULDER:
      case ClimbingType.DEEP_WATER_SOLO:
      case ClimbingType.SPEED_CLIMBING:
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.MULTI_PITCH:
      case ClimbingType.TRAD_CLIMBING:
        return false;
    }
  }

  bool hasLeadAndToprope({int speedType = 0}) {
    switch (this) {
      case ClimbingType.FREE_SOLO:
      case ClimbingType.BOULDER:
      case ClimbingType.DEEP_WATER_SOLO:
        return false;
      case ClimbingType.SPEED_CLIMBING:
        return speedType != 0;
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.MULTI_PITCH:
      case ClimbingType.TRAD_CLIMBING:
        return true;
    }
  }

  bool shouldRenameTopropeToFollowing() {
    switch (this) {
      case ClimbingType.FREE_SOLO:
      case ClimbingType.BOULDER:
      case ClimbingType.DEEP_WATER_SOLO:
      case ClimbingType.SPEED_CLIMBING:
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.TRAD_CLIMBING:
        return false;
      case ClimbingType.MULTI_PITCH:
        return true;
    }
  }
}
