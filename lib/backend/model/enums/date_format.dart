enum CustomDateFormat {
  SYSTEM(""),
  MDY("MMMM d, y"),
  EDMY("EEEE, dd. MMM y"),
  DMY("dd/MM/y"),
  USA("MM/dd/y"),
  EUR("dd.MM.y"),
  ISO("y-MM-dd");

  const CustomDateFormat(this.formatString);

  final String formatString;
}
