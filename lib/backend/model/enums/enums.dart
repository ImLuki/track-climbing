import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/util/extensions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

enum Classes {
  LEAD,
  TOPROPE,
  BOULDER,
  SPEED,
  MULTI_PITCH,
  TRAD,
  ICE,
  WATER,
  FREE,
}

enum StyleType {
  ONSIGHT,
  FLASH,
  REDPOINT,
  TOP,
  PROJECT,
  NONE,
}

extension StyleTypeExtension on StyleType {
  /// Translates StyleType enum to translated String for UI
  String toTranslatedString(BuildContext context) {
    switch (this) {
      case StyleType.ONSIGHT:
        return AppLocalizations.of(context)!.style_onsight;
      case StyleType.FLASH:
        return AppLocalizations.of(context)!.style_flash;
      case StyleType.REDPOINT:
        return AppLocalizations.of(context)!.style_redpoint;
      case StyleType.TOP:
        return AppLocalizations.of(context)!.style_top;
      case StyleType.PROJECT:
        return AppLocalizations.of(context)!.style_project;
      case StyleType.NONE:
        return "";
    }
  }

  /// Get matching icon for style
  IconData getIcon() {
    switch (this) {
      case StyleType.ONSIGHT:
        return Icons.visibility_outlined;
      case StyleType.FLASH:
        return Icons.flash_on_outlined;
      case StyleType.REDPOINT:
        return Icons.adjust;
      case StyleType.TOP:
        return FontAwesomeIcons.circleUp;
      case StyleType.PROJECT:
        return Icons.construction;
      case StyleType.NONE:
        return Icons.warning_amber;
    }
  }

  /// Get matching icon for style
  double getIconSize() {
    switch (this) {
      case StyleType.ONSIGHT:
        return 24.0;
      case StyleType.FLASH:
      case StyleType.REDPOINT:
      case StyleType.PROJECT:
        return 22.0;
      case StyleType.TOP:
        return 19.0;
      case StyleType.NONE:
        return 0.0;
    }
  }
}

/// Translates Classes enum to translated String for UI
extension ClassesExtension on Classes {
  String toTranslatedString(BuildContext context) {
    switch (this) {
      case Classes.LEAD:
        return AppLocalizations.of(context)!.style_lead;
      case Classes.TOPROPE:
        return AppLocalizations.of(context)!.style_toprope;
      case Classes.BOULDER:
        return AppLocalizations.of(context)!.style_bouldering;
      case Classes.FREE:
        return AppLocalizations.of(context)!.style_free_solo;
      case Classes.WATER:
        return AppLocalizations.of(context)!.style_dws;
      case Classes.ICE:
        return AppLocalizations.of(context)!.style_ice_climbing;
      case Classes.SPEED:
        return AppLocalizations.of(context)!.style_speed_climbing;
      case Classes.MULTI_PITCH:
        return AppLocalizations.of(context)!.style_multi_pitch_climbing;
      case Classes.TRAD:
        return AppLocalizations.of(context)!.style_trad_climbing;
    }
  }

  String getClimbName(BuildContext context) {
    switch (this) {
      case Classes.LEAD:
        return AppLocalizations.of(context)!.style_lead.toTitleCase();
      case Classes.TOPROPE:
        return AppLocalizations.of(context)!.style_toprope.toTitleCase();
      case Classes.BOULDER:
        return AppLocalizations.of(context)!.style_bouldering.toTitleCase();
      case Classes.FREE:
        return AppLocalizations.of(context)!.style_free_solo.toTitleCase();
      case Classes.WATER:
        return AppLocalizations.of(context)!.style_dws_ascent.toTitleCase();
      case Classes.ICE:
        return AppLocalizations.of(context)!.style_ice_climbing.toTitleCase();
      case Classes.SPEED:
        return "${AppLocalizations.of(context)!.style_speed_climbing.toTitleCase()} "
            "(${AppLocalizations.of(context)!.chart_page_comp_wall.toTitleCase()})";
      case Classes.MULTI_PITCH:
        return AppLocalizations.of(context)!.style_multi_pitch_climbing.toTitleCase();
      case Classes.TRAD:
        return AppLocalizations.of(context)!.style_trad_climbing.toTitleCase();
    }
  }

  String getImagePath() {
    switch (this) {
      case Classes.LEAD:
        return 'assets/images/lead.png'; // https://icons8.de/icons/set/climbing
      case Classes.TOPROPE:
        return 'assets/images/toprope.png';
      case Classes.BOULDER:
        return 'assets/images/rock.png'; //https://www.flaticon.com/free-icon/rock_4953463?term=rock&page=1&position=96&origin=search&related_id=4953463
      case Classes.FREE:
        return 'assets/images/free.png'; //https://www.flaticon.com/free-icon/climbing_71443?term=rock+climbing&page=1&position=15&origin=search&related_id=71443
      case Classes.WATER:
        return 'assets/images/wave.png'; //https://www.flaticon.com/free-icon/wave_433643?term=ocean&page=1&position=35&origin=search&related_id=433643
      case Classes.ICE:
        return 'assets/images/ice-axe.png'; //https://www.flaticon.com/free-icon/ice-axe_2121328?term=ice+axe&page=1&position=20&origin=search&related_id=2121328
      case Classes.SPEED:
        return 'assets/images/speed.png'; // https://www.flaticon.com/free-icon/speedometer_2651472?term=speed&page=1&position=18&origin=search&related_id=2651472
      case Classes.MULTI_PITCH:
        return 'assets/images/multipitch.png'; // https://www.flaticon.com/free-icon/mountain_1551074?term=mountain&page=1&position=39&origin=search&related_id=1551074
      case Classes.TRAD:
        // return 'assets/images/camelot.png'; // https://www.flaticon.com/free-icon/spring-loaded-camming-device_12258231
        return 'assets/images/carabiner.png'; // https://www.flaticon.com/free-icon/carabiner_11225703
    }
  }

  Color getColorLight() {
    switch (this) {
      case Classes.LEAD:
        return const Color(0xff93dfe1);
      case Classes.TOPROPE:
        return const Color(0xffee6363);
      case Classes.BOULDER:
        return const Color(0xffe7a384);
      case Classes.FREE:
        return const Color(0xfff1d281);
      case Classes.WATER:
        return const Color(0xFF85A0FD);
      case Classes.ICE:
        return const Color(0xFFA0D38C);
      case Classes.SPEED:
        return const Color(0xffe1a7f1);
      case Classes.MULTI_PITCH:
        return const Color(0xffdae085);
      case Classes.TRAD:
        return const Color(0xff9e99c5);
    }
  }

  Color getColor() {
    switch (this) {
      case Classes.LEAD:
        return const Color(0xFF47acb1);
      case Classes.TOPROPE:
        return const Color(0xFFa62546);
      case Classes.BOULDER:
        return const Color(0xFFf26522);
      case Classes.FREE:
        return const Color(0xFFffcd34);
      case Classes.WATER:
        return const Color(0xFF446DF6);
      case Classes.ICE:
        return const Color(0xFF99C24D);
      case Classes.SPEED:
        return const Color(0xFF9D79BC);
      case Classes.MULTI_PITCH:
        return const Color(0xffa3a92a);
      case Classes.TRAD:
        return const Color(0xff7064a6);
    }
  }

  List<StyleType> getStyles() {
    switch (this) {
      case Classes.LEAD:
        return [StyleType.ONSIGHT, StyleType.FLASH, StyleType.REDPOINT, StyleType.TOP];
      case Classes.TOPROPE:
        return [StyleType.ONSIGHT, StyleType.FLASH, StyleType.REDPOINT, StyleType.TOP];
      case Classes.BOULDER:
        return [StyleType.FLASH, StyleType.TOP, StyleType.PROJECT];
      case Classes.FREE:
        return [StyleType.TOP];
      case Classes.WATER:
        return [StyleType.ONSIGHT, StyleType.FLASH, StyleType.REDPOINT];
      case Classes.ICE:
        return [StyleType.ONSIGHT, StyleType.FLASH, StyleType.REDPOINT, StyleType.TOP];
      case Classes.SPEED:
        return [];
      case Classes.MULTI_PITCH:
        return [StyleType.ONSIGHT, StyleType.FLASH, StyleType.REDPOINT, StyleType.TOP];
      case Classes.TRAD:
        return [StyleType.ONSIGHT, StyleType.FLASH, StyleType.REDPOINT, StyleType.TOP];
    }
  }

  Widget getIcon() {
    bool applyImagePadding = false;
    switch (this) {
      case Classes.TOPROPE:
      case Classes.FREE:
        applyImagePadding = false;
        break;
      case Classes.LEAD:
      case Classes.BOULDER:
      case Classes.WATER:
      case Classes.ICE:
      case Classes.SPEED:
      case Classes.MULTI_PITCH:
      case Classes.TRAD:
        applyImagePadding = true;
        break;
    }

    return Container(
      padding: applyImagePadding ? const EdgeInsets.all(6.0) : EdgeInsets.zero,
      height: 34.0,
      width: 34.0,
      decoration: applyImagePadding
          ? BoxDecoration(
              color: getColorLight(),
              borderRadius: const BorderRadius.all(Radius.circular(100)),
            )
          : BoxDecoration(
              color: getColorLight(),
              borderRadius: const BorderRadius.all(Radius.circular(100)),
              image: DecorationImage(
                image: AssetImage(getImagePath()),
                fit: BoxFit.fill,
              ),
            ),
      child: applyImagePadding
          ? Image(
              image: AssetImage(getImagePath()),
              fit: BoxFit.fill,
            )
          : Container(),
    );
  }

  ClimbingType getClimbingType() {
    switch (this) {
      case Classes.TOPROPE:
        return ClimbingType.SPORT_CLIMBING;
      case Classes.LEAD:
        return ClimbingType.SPORT_CLIMBING;
      case Classes.BOULDER:
        return ClimbingType.BOULDER;
      case Classes.WATER:
        return ClimbingType.DEEP_WATER_SOLO;
      case Classes.ICE:
        return ClimbingType.ICE_CLIMBING;
      case Classes.SPEED:
        return ClimbingType.SPEED_CLIMBING;
      case Classes.MULTI_PITCH:
        return ClimbingType.MULTI_PITCH;
      case Classes.TRAD:
        return ClimbingType.TRAD_CLIMBING;
      case Classes.FREE:
        return ClimbingType.FREE_SOLO;
    }
  }
}
