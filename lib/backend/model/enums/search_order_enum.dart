import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

enum SearchOrder {
  newest,
  oldest,
  alphabetic,
  alphabetic_reversed,
  duration,
  duration_reversed,
}

extension SearchOrderExtension on SearchOrder {
  String getText(BuildContext context) {
    switch (this) {
      case SearchOrder.newest:
        return AppLocalizations.of(context)!.search_setting_newest;
      case SearchOrder.oldest:
        return AppLocalizations.of(context)!.search_setting_oldest;
      case SearchOrder.alphabetic:
        return AppLocalizations.of(context)!.search_setting_alphabetic_az;
      case SearchOrder.alphabetic_reversed:
        return AppLocalizations.of(context)!.search_setting_alphabetic_za;
      case SearchOrder.duration:
        return AppLocalizations.of(context)!.search_setting_duration;
      case SearchOrder.duration_reversed:
        return AppLocalizations.of(context)!.search_setting_duration_reversed;
    }
  }

  int Function(dynamic, dynamic) compareFunction() {
    switch (this) {
      case SearchOrder.newest:
        return (b, a) => DateTime.parse(a[DBController.sessionTimeStart])
            .compareTo(DateTime.parse(b[DBController.sessionTimeStart]));
      case SearchOrder.oldest:
        return (a, b) => DateTime.parse(a[DBController.sessionTimeStart])
            .compareTo(DateTime.parse(b[DBController.sessionTimeStart]));
      case SearchOrder.alphabetic:
        return (a, b) => a[DBController.locationName].compareTo(b[DBController.locationName]);
      case SearchOrder.alphabetic_reversed:
        return (b, a) => a[DBController.locationName].compareTo(b[DBController.locationName]);
      case SearchOrder.duration:
        return (b, a) => DateTime.parse(a[DBController.sessionTimeStart])
            .difference(DateTime.parse(a[DBController.sessionTimeEnd]))
            .inMinutes
            .abs()
            .compareTo(DateTime.parse(b[DBController.sessionTimeStart])
                .difference(DateTime.parse(b[DBController.sessionTimeEnd]))
                .inMinutes
                .abs());
      case SearchOrder.duration_reversed:
        return (a, b) => DateTime.parse(a[DBController.sessionTimeStart])
            .difference(DateTime.parse(a[DBController.sessionTimeEnd]))
            .inMinutes
            .abs()
            .compareTo(DateTime.parse(b[DBController.sessionTimeStart])
                .difference(DateTime.parse(b[DBController.sessionTimeEnd]))
                .inMinutes
                .abs());
    }
  }
}
