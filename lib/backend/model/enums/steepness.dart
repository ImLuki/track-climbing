import 'package:climbing_track/ui/util/climbing_tracker_icons_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

enum Steepness {
  SLAB(1 << 0),
  VERTICAL(1 << 1),
  LIGHT_OVERHANGING(1 << 2),
  STRONG_OVERHANGING(1 << 3),
  ROOF(1 << 4),
  CHIMNEY(1 << 5),
  CORNER(1 << 6),
  ARETE(1 << 7);

  const Steepness(this.value);

  final int value;

  /// Returns `true` if [steepness] has at least one member in [flag]
  static bool hasFlag(Steepness steepness, int flag) {
    return steepness.value & flag != 0;
  }

  static List<Steepness> getValuesFromFlag(int flag) {
    return Steepness.values.where((element) => Steepness.hasFlag(element, flag)).toList();
  }
}

extension SteepnessExtension on Steepness {
  /// Translates StyleType enum to translated String for UI
  String toTranslatedString(BuildContext context) {
    switch (this) {
      case Steepness.SLAB:
        return AppLocalizations.of(context)!.steepness_slab;
      case Steepness.VERTICAL:
        return AppLocalizations.of(context)!.steepness_vertical;
      case Steepness.LIGHT_OVERHANGING:
        return AppLocalizations.of(context)!.steepness_light_overhanging;
      case Steepness.STRONG_OVERHANGING:
        return AppLocalizations.of(context)!.steepness_strong_overhanging;
      case Steepness.ROOF:
        return AppLocalizations.of(context)!.steepness_roof;
      case Steepness.CHIMNEY:
        return AppLocalizations.of(context)!.steepness_chimney;
      case Steepness.CORNER:
        return AppLocalizations.of(context)!.steepness_corner;
      case Steepness.ARETE:
        return AppLocalizations.of(context)!.steepness_arete;
    }
  }

  IconData getIcon() {
    switch (this) {
      case Steepness.SLAB:
        return ClimbingTrackerIcons.slab;
      case Steepness.VERTICAL:
        return ClimbingTrackerIcons.vertical;
      case Steepness.LIGHT_OVERHANGING:
        return ClimbingTrackerIcons.slight_overhang;
      case Steepness.STRONG_OVERHANGING:
        return ClimbingTrackerIcons.strongly_overhang;
      case Steepness.ROOF:
        return ClimbingTrackerIcons.roof;
      case Steepness.CHIMNEY:
        return ClimbingTrackerIcons.chimney;
      case Steepness.CORNER:
        return ClimbingTrackerIcons.corner;
      case Steepness.ARETE:
        return ClimbingTrackerIcons.arete;
    }
  }

  Widget getIconWidget(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(4.0),
      decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.surface,
          shape: BoxShape.circle,
          border: Border.all(color: Colors.black12)),
      child: Icon(getIcon(), size: 16),
    );
  }
}
