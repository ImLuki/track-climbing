import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/util/extensions.dart';
import 'package:latlong2/latlong.dart';

class Location {
  int? locationId;
  String _name;
  LatLng? _coordinates;
  double _standardHeight;
  String _locationComment;
  bool _outdoor;
  ClimbingType? _standardClimbingType;
  int? _standardDifficulty;

  /// Attention: This value can be deprecated because it will only be updated on app init phase!
  DateTime lastVisited;

  Location(
    this.locationId,
    this._name,
    this._coordinates,
    this._standardHeight,
    this._locationComment,
    this._outdoor, {
    DateTime? lastVisited,
    ClimbingType? standardClimbingType,
    int? standardDifficulty,
  })  : lastVisited = lastVisited ?? DateTime(0),
        _standardClimbingType = standardClimbingType,
        _standardDifficulty = standardDifficulty;

  static Location fromMap(Map<String, dynamic> data) {
    LatLng? coordinates;
    if (data[DBController.longitude] != null && data[DBController.latitude] != null) {
      coordinates = LatLng(data[DBController.latitude], data[DBController.longitude]);
    }

    return Location(
      data[DBController.locationId],
      data[DBController.locationName],
      coordinates,
      data[DBController.standardLocationHeight] ?? 0,
      data[DBController.locationComment] ?? "",
      data[DBController.locationOutdoor] == 1,
      lastVisited: data["last_visited"] == null ? null : DateTime.parse(data["last_visited"]),
      standardClimbingType: data[DBController.locationStandardAscendType] == null
          ? null
          : ClimbingType.values.byName(data[DBController.locationStandardAscendType]),
      standardDifficulty: data[DBController.locationStandardDifficulty],
    );
  }

  Location copy() {
    return Location(
      locationId,
      _name,
      _coordinates == null ? _coordinates : LatLng(_coordinates!.latitude, _coordinates!.longitude),
      _standardHeight,
      _locationComment,
      _outdoor,
      lastVisited: lastVisited.copy(),
      standardClimbingType: _standardClimbingType,
      standardDifficulty: _standardDifficulty,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      DBController.locationId: locationId,
      DBController.locationName: _name,
      DBController.longitude: _coordinates?.longitude,
      DBController.latitude: _coordinates?.latitude,
      DBController.standardLocationHeight: _standardHeight,
      DBController.locationComment: _locationComment,
      DBController.locationOutdoor: _outdoor ? 1 : 0,
      DBController.locationStandardAscendType: _standardClimbingType?.name,
      DBController.locationStandardDifficulty: _standardDifficulty,
    };
  }

  String get locationComment => _locationComment;

  double get standardHeight => _standardHeight;

  LatLng? get coordinates => _coordinates;

  String get name => _name;

  bool get outdoor => _outdoor;

  int? get standardDifficulty => _standardDifficulty;

  ClimbingType? get standardClimbingType => _standardClimbingType;

  set locationComment(String value) {
    if (_locationComment == value) return;
    _locationComment = value;
    if (locationId != null) DBController().updateLocation(this);
  }

  updateStandardHeight(double value, bool updateOld) {
    if (value == _standardHeight) return;
    double oldValue = _standardHeight;
    _standardHeight = value;

    if (locationId == null) return;

    DBController().updateLocation(this);
    if (updateOld) {
      DBController().updateAscendHeights(
        oldValue,
        value,
        locationId!,
      );
    }
  }

  set coordinates(LatLng? value) {
    _coordinates = value;
    if (locationId != null) DBController().updateLocation(this);
  }

  set outdoor(bool value) {
    // DO not update if locationId is not null because this will brake data consistency
    assert(locationId == null);
    _outdoor = value;
  }

  set name(String value) {
    // DO not update if locationId is not null because this will brake data consistency
    assert(locationId == null);
    _name = value;
  }

  set standardDifficulty(int? value) {
    _standardDifficulty = value;
    if (locationId != null) DBController().updateLocation(this);
  }

  set standardClimbingType(ClimbingType? value) {
    _standardClimbingType = value;
    if (locationId != null) DBController().updateLocation(this);
  }
}
