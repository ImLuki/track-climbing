import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/util/extensions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Session with ChangeNotifier {
  static const String SESSION_ACTIVE_STATE = "active";
  static const String SESSION_CLOSED_STATE = "end";

  int? sessionId;
  DateTime startDate;
  DateTime? endDate;
  int locationId;
  String comment;

  Session({
    required this.sessionId,
    required this.startDate,
    required this.locationId,
    this.endDate,
    this.comment = "",
  });

  static Session fromMap(Map<String, dynamic> data) {
    return Session(
      sessionId: data[DBController.sessionId] ?? -1,
      startDate: DateTime.parse(data[DBController.sessionTimeStart]),
      endDate: data[DBController.sessionTimeEnd] == null ? null : DateTime.parse(data[DBController.sessionTimeEnd]),
      locationId: data[DBController.locationId],
      comment: data[DBController.sessionComment] ?? "",
    );
  }

  void stop(DateTime end) {
    endDate = end;
  }

  String getDuration() {
    if (endDate == null) return "00:00";
    return getDurationDifference(endDate!);
  }

  String getDurationDifference(DateTime end) {
    final int difference = end.difference(startDate).inMinutes.abs();
    final int hours = difference ~/ 60;
    final int minutes = difference % 60;
    return "${hours.toString().padLeft(2, '0')}:${minutes.toString().padLeft(2, '0')}";
  }

  int getSecondsSinceStart() {
    return DateTime.now().difference(startDate).inSeconds.abs();
  }

  Map<String, dynamic> toMap(String status) {
    var map = {
      DBController.sessionStatus: status,
      DBController.sessionTimeStart: startDate.toIso8601String(),
      DBController.sessionTimeEnd: endDate?.toIso8601String(),
      DBController.locationId: locationId,
      DBController.sessionComment: comment,
    };
    if (sessionId != null) map[DBController.sessionId] = sessionId;
    return map;
  }

  Map<String, dynamic> toJson(int sessionId) {
    return {
      DBController.sessionId: sessionId,
      DBController.sessionTimeStart: startDate.toIso8601String(),
      DBController.sessionTimeEnd: endDate?.toIso8601String(),
      DBController.locationId: locationId,
      DBController.sessionComment: comment,
    };
  }

  Session copy() {
    return Session(
      sessionId: sessionId,
      startDate: startDate.copy(),
      endDate: endDate?.copy(),
      locationId: locationId,
      comment: comment,
    );
  }
}
