import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:flutter/material.dart';

class SessionState with ChangeNotifier {
  SessionState._privateConstructor();

  static final SessionState _instance = SessionState._privateConstructor();

  SessionStateEnum _currentState = SessionStateEnum.none;

  factory SessionState() {
    return _instance;
  }

  Future<void> init() async {
    List<Map<String, dynamic>> result = await DBController().queryRows(
      DBController.sessionsTable,
      where: "${DBController.sessionStatus} == (?)",
      whereArgs: [Session.SESSION_ACTIVE_STATE],
    );
    bool activeSession = result.isNotEmpty;
    _currentState = activeSession ? SessionStateEnum.active : SessionStateEnum.none;
  }

  void setState(SessionStateEnum state) {
    _currentState = state;
    notifyListeners();
  }

  void nextState() {
    _currentState = nextSessionStateEnum(_currentState);
    notifyListeners();
  }

  SessionStateEnum get currentState => _currentState;
}

enum SessionStateEnum { none, start, active, end }

SessionStateEnum nextSessionStateEnum(SessionStateEnum state) {
  switch (state) {
    case SessionStateEnum.none:
      return SessionStateEnum.start;
    case SessionStateEnum.start:
      return SessionStateEnum.active;
    case SessionStateEnum.active:
      return SessionStateEnum.end;
    case SessionStateEnum.end:
      return SessionStateEnum.none;
  }
}
