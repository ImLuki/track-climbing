import 'dart:io';

import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/enums/date_format.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

/// Class for formatting dates to different representations
class CustomDateFormatter {
  /// Get the current set date format
  static DateFormat _getDateFormat(BuildContext context) {
    if (DataStore().settingsDataStore.currentDateFormat == CustomDateFormat.SYSTEM) {
      // fix one digit date
      if (DateFormat.yMd(Platform.localeName).pattern == "d.M.y") {
        return DateFormat("dd.MM.y");
      }
      return DateFormat.yMd(Platform.localeName);
    } else {
      return DateFormat(DataStore().settingsDataStore.currentDateFormat.formatString);
    }
  }

  /// Format given date to the currently defined date format
  ///
  /// Returns String
  static String formatDateString(BuildContext context, DateTime dateTime) {
    return _getDateFormat(context).format(dateTime);
  }

  /// Format given date to the currently defined date format
  ///
  /// Returns String
  static String formatDateStringWithJM(BuildContext context, DateTime dateTime) {
    return "${_getDateFormat(context).format(dateTime)} - ${DateFormat.jm(Platform.localeName).format(dateTime)}";
  }
}
