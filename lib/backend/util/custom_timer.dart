import 'dart:async';

class CustomTimer {
  Stream<int>? _timerStream;
  StreamSubscription<int>? _timerSubscription;
  DateTime? _startTime;

  void startTimer(Function(int tick) callBack) {
    _timerStream = _stopWatchStream();
    _startTime = DateTime.now();
    callBack(0);

    _timerSubscription = _timerStream?.listen((int newTick) {
      callBack(newTick);
    });
  }

  void stopTimer() {
    if (_timerSubscription != null) {
      _timerSubscription?.cancel();
      _timerStream = null;
    }
  }

  Stream<int> _stopWatchStream() {
    StreamController<int>? streamController;
    Timer? timer;
    Duration timerInterval = const Duration(seconds: 1);
    int counter = 0;

    void stopTimer() {
      if (timer != null) {
        timer?.cancel();
        timer = null;
        counter = 0;
        _startTime = null;
        streamController?.close();
      }
    }

    void tick(_) {
      counter++;
      streamController?.add(counter);
    }

    void startTimer() {
      timer = Timer.periodic(timerInterval, tick);
    }

    streamController = StreamController<int>(
      onListen: startTimer,
      onCancel: stopTimer,
      onResume: startTimer,
      onPause: stopTimer,
    );

    return streamController.stream;
  }

  DateTime get startTime => _startTime ?? DateTime.now();
}
