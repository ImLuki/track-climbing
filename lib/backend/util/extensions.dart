import 'dart:io';
import 'dart:math';

import 'package:latlong2/latlong.dart';

extension StringCasingExtension on String {
  String toCapitalized() => length > 0 ? '${this[0].toUpperCase()}${substring(1).toLowerCase()}' : '';

  String toTitleCase() => replaceAll(RegExp(' +'), ' ').split(' ').map((str) => str.toCapitalized()).join(' ');
}

extension DateTimeExtension on DateTime {
  DateTime copy() => DateTime(year, month, day, hour, minute, second, millisecond, microsecond);
}

extension LatLngExtension on LatLng {
  LatLng copy() => LatLng(latitude, longitude);
}

/// An extension to get the tail of the string. Something like syntaxic sugar
/// to avoid substring every time.
extension StringPartExtension on String {
  String get tail {
    return substring(1);
  }

  String prefix(int length) {
    return substring(0, min(this.length, length));
  }
}

extension FileExtention on FileSystemEntity {
  String get directory {
    return path.substring(0, path.lastIndexOf(Platform.pathSeparator));
  }
}

extension DoubleExtension on double {
  double toPrecision(int n) => double.parse(toStringAsFixed(n));
}
