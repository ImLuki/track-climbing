import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/enums/height_measurement.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:climbing_track/backend/util/custom_date_formatter.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Util {
  static const String NULL_REPLACEMENT = "\u{200B}";

  static String getAscendSummary(Ascend ascend, BuildContext context) {
    return _getSummary(
      ascend.gradeID,
      ascend.styleID,
      ascend.toprope,
      ascend.speedTime,
      ascend.type,
      ascend.originalGradeSystem,
      context: context,
    );
  }

  static String getDataSummary(Map<String, dynamic> data, BuildContext context) {
    return _getSummary(
      data[DBController.ascendGradeId],
      data[DBController.ascendStyleId],
      data[DBController.routeTopRope] ?? 0,
      data[DBController.speedTime],
      ClimbingType.values.byName(data[DBController.ascendType]),
      data[DBController.ascendOriginalGradeSystem],
      context: context,
    );
  }

  static String _getSummary(
    int gradeID,
    int styleID,
    int toprope,
    int speedTime,
    ClimbingType type,
    String? gradingSystem, {
    required BuildContext context,
  }) {
    switch (type) {
      case ClimbingType.BOULDER:
        return "${getGrade(gradeID, type, gradingSystem: gradingSystem)} | ${getStyle(styleID, context)} | ${AppLocalizations.of(context)!.style_boulder}";
      case ClimbingType.SPEED_CLIMBING:
        return "${(speedTime / 1000).toStringAsFixed(2)} ${AppLocalizations.of(context)!.util_seconds}";
      case ClimbingType.FREE_SOLO:
        return "${getGrade(gradeID, type, gradingSystem: gradingSystem)} | ${AppLocalizations.of(context)!.style_free_solo}";
      case ClimbingType.DEEP_WATER_SOLO:
        return "${getGrade(gradeID, type, gradingSystem: gradingSystem)} | ${getStyle(styleID, context)} | ${AppLocalizations.of(context)!.style_dws}";
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.MULTI_PITCH:
      case ClimbingType.TRAD_CLIMBING:
        return "${getGrade(gradeID, type, gradingSystem: gradingSystem)} | ${getStyle(styleID, context)} | ${getTopOrLead(toprope, context, type.shouldRenameTopropeToFollowing())}";
    }
  }

  static String getStyle(int styleID, BuildContext context, {int? tries}) {
    ClimbingStyle climbingStyle = ClimbingStyle.getById(styleID);
    String styleString = climbingStyle.toTranslatedString(context);

    if (tries == null) {
      return styleString;
    }

    switch (climbingStyle) {
      case ClimbingStyle.ONSIGHT:
      case ClimbingStyle.FLASH:
      case ClimbingStyle.FLASH_BOULDERING:
        return ClimbingStyle.getById(styleID).toTranslatedString(context);
      case ClimbingStyle.REDPOINT:
      case ClimbingStyle.TOP:
      case ClimbingStyle.PROJECT:
        return "$styleString (${tries.toString()}. ${AppLocalizations.of(context)!.ascend_attempts(1)})";
      case ClimbingStyle.REPEAT:
      case ClimbingStyle.REPEAT_BOULDER:
        return "$styleString (${tries.toString()}. ${AppLocalizations.of(context)!.general_repetition_short})";
      case ClimbingStyle.AF:
      case ClimbingStyle.HANGDOGGING:
      case ClimbingStyle.THREE_QUARTERS:
      case ClimbingStyle.HALF:
      case ClimbingStyle.ONE_QUARTER:
      case ClimbingStyle.AID:
        return "$styleString (${tries.toString()} ${AppLocalizations.of(context)!.ascend_hangs(tries)})";
    }
  }

  static String getGrade(int gradeID, ClimbingType type, {String? gradingSystem}) {
    return GradeManager().getGradeMapper(type: type).gradeFromId(gradeID, gradingSystem: gradingSystem?.toLowerCase());
  }

  static String getOriginalGradeAndCurrent(int gradeID, ClimbingType type, {String? gradingSystem}) {
    // original Grade
    String gradeText = GradeManager().getGradeMapper(type: type).gradeFromId(
          gradeID,
          gradingSystem: gradingSystem?.toLowerCase(),
        );

    if (gradingSystem != null &&
        GradeManager().getGradeMapper(type: type).currentGradingSystem.toLowerCase() != gradingSystem.toLowerCase()) {
      gradeText += " $gradingSystem\n(";
      gradeText += GradeManager().getGradeMapper(type: type).gradeFromId(
            gradeID,
          );
      gradeText += " ${GradeManager().getGradeMapper(type: type).currentGradingSystem})";
    }

    return gradeText;
  }

  static String getTopOrLead(int toprope, BuildContext context, bool shouldRenameTopropeToFollowing) {
    return shouldRenameTopropeToFollowing
        ? [
            AppLocalizations.of(context)!.style_following,
            AppLocalizations.of(context)!.style_leading,
            AppLocalizations.of(context)!.style_alternate_leading,
          ][toprope]
        : toprope == 1
            ? AppLocalizations.of(context)!.style_toprope
            : AppLocalizations.of(context)!.style_lead;
  }

  static Widget getRating(int rating) {
    List<Widget> stars = [];
    for (int i = 0; i < 5; i++) {
      if (i < rating) {
        stars.add(Constants.FULL_STAR);
      } else {
        stars.add(Constants.EMPTY_STAR);
      }
    }
    return Wrap(children: stars);
  }

  static String getComment(String? comment) {
    if (comment == null || comment == '') {
      return '-';
    } else {
      return comment;
    }
  }

  static String getAscendNameWithoutPrefix(
    String name,
    BuildContext context, {
    required int speedType,
    required ClimbingType type,
  }) {
    if (type == ClimbingType.SPEED_CLIMBING && speedType == 0) {
      return AppLocalizations.of(context)!.style_speed_ascend_comp.replaceAll("", NULL_REPLACEMENT);
    }
    return replaceStringWithBlankSpaces(
      name == "" ? AppLocalizations.of(context)!.util_name_missing : name,
    );
  }

  static String getAscendName(
    BuildContext context,
    String name, {
    required ClimbingType type,
    required int speedType,
  }) {
    String prefix = "";
    switch (type) {
      case ClimbingType.BOULDER:
        prefix = "[${AppLocalizations.of(context)!.style_boulder_prefix}] ";
        break;
      case ClimbingType.SPEED_CLIMBING:
        if (speedType == 0) {
          return AppLocalizations.of(context)!.style_speed_ascend_comp.replaceAll("", NULL_REPLACEMENT);
        }
        if (name == "") return AppLocalizations.of(context)!.style_speed_ascend.replaceAll("", NULL_REPLACEMENT);
        prefix = "[${AppLocalizations.of(context)!.style_speed_prefix.toUpperCase()}] ";
        break;
      case ClimbingType.FREE_SOLO:
        prefix = "[${AppLocalizations.of(context)!.style_free_solo_prefix}] ";
        break;
      case ClimbingType.DEEP_WATER_SOLO:
        prefix = "[${AppLocalizations.of(context)!.style_dws}] ";
        break;
      case ClimbingType.ICE_CLIMBING:
        prefix = "[${AppLocalizations.of(context)!.style_ice_prefix}] ";
        break;
      case ClimbingType.MULTI_PITCH:
        prefix = "[${AppLocalizations.of(context)!.style_multi_pitch_prefix}] ";
        break;
      case ClimbingType.SPORT_CLIMBING:
        prefix = "";
      case ClimbingType.TRAD_CLIMBING:
        prefix = "[${AppLocalizations.of(context)!.style_trad_prefix}] ";
        break;
    }
    return replaceStringWithBlankSpaces(
      '$prefix${name == "" ? AppLocalizations.of(context)!.util_name_missing : name}',
    );
  }

  static String getRouteNameForFiltering(String name, BuildContext context) {
    return name == "" ? AppLocalizations.of(context)!.util_name_missing : name;
  }

  static String getLocationDateString(BuildContext context, DateTime date, String location) {
    return replaceStringWithBlankSpaces("${CustomDateFormatter.formatDateString(context, date)} | $location");
  }

  static String getLocation(String location) {
    return replaceStringWithBlankSpaces(location);
  }

  static String getHeightWithShortUnit(double meters, {String zeroFilling = "- - -"}) {
    String unit = DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET ? "ft" : "m";
    return meters <= 0 ? zeroFilling : "${heightFromMeters(meters)} $unit";
  }

  static String getHeightString(double meters) {
    return meters <= 0 ? "- - -" : "${heightFromMeters(meters)}";
  }

  /// Calculates height in meters from in app selected length unit (meters or feet)
  /// Returns height in meters as double
  static double heightToMeters(double height) {
    if (DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET) {
      return height * 0.3048;
    }
    return height;
  }

  /// Calculates meters to in app selected length unit
  /// Possible units so far: meters & feet
  static dynamic heightFromMeters(double meters) {
    if (DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET) {
      return (meters * 3.280839895).round();
    }
    return roundToNearestPointFive(meters);
  }

  /// rounding double to nearest .5 number
  ///
  /// Returns rounded value as double
  static double roundToNearestPointFive(double value) {
    return ((value * 2).round() / 2);
  }

  /// Replaces blank spaces with special character
  /// Text overflow splitting is no longer limited to words
  /// Splits are possible after each letter
  static String replaceStringWithBlankSpaces(String string) {
    return string.replaceAll("", NULL_REPLACEMENT);
  }

  /// Returns list of all previous ascents of this route or boulder
  static Future<List<Ascend>> findExistingAscents(Ascend ascend) async {
    if (ascend.name == "") return [];

    var result = await DBController().rawQuery(
      "SELECT * FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionsTable} using(${DBController.sessionId}) "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE LOWER(${DBController.ascendName}) == (?) "
      "AND ${DBController.ascendType} == (?) "
      "AND LOWER(${DBController.locationName}) == (?) "
      "AND ${DBController.ascendGradeId} == (?) "
      "AND ${DBController.ascendId} != (?) "
      "ORDER BY ${DBController.sessionTimeStart} DESC",
      [
        ascend.name.toLowerCase(),
        ascend.type.name,
        ascend.location!.toLowerCase(),
        ascend.gradeID,
        ascend.ascendId,
      ],
    );

    if (result.isEmpty) return [];

    return List.generate(result.length, (index) => Ascend.fromMap(result[index]));
  }
}
