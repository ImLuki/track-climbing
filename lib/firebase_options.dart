// File generated by FlutterFire CLI.
// ignore_for_file: type=lint
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      throw UnsupportedError(
        'DefaultFirebaseOptions have not been configured for web - '
        'you can reconfigure this by running the FlutterFire CLI again.',
      );
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for macos - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyBb76CTqcdf4YHHMmCo1Txky08WN0JDtc8',
    appId: '1:1080528496215:android:15a17d9e2a56932320e512',
    messagingSenderId: '1080528496215',
    projectId: 'track-climbing',
    storageBucket: 'track-climbing.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyAFz9_M89d1iA03XjHR7IU_coID3uG--es',
    appId: '1:1080528496215:ios:8dbeb39481078cc320e512',
    messagingSenderId: '1080528496215',
    projectId: 'track-climbing',
    storageBucket: 'track-climbing.appspot.com',
    androidClientId: '1080528496215-07nu1bgn1kudc41b4pn5r1f54vvdlvo8.apps.googleusercontent.com',
    iosClientId: '1080528496215-haiv24h482hjdnpuh1n7qks27mpel8fj.apps.googleusercontent.com',
    iosBundleId: 'de.JonasFrey.ClimbingTrack',
  );
}
