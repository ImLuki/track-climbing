import 'dart:io';

import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/data_store/tooltips_data_store.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/handler/location_handler.dart';
import 'package:climbing_track/backend/model/active_session.dart';
import 'package:climbing_track/backend/model/charts/statistic_charts.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'firebase_options.dart';

class Init {
  static const appVersionKey = "app_version";

  static Future initialize() async {
    await _onUpgrade();
    await _loadSettings();
  }

  static _loadSettings() async {
    debugPrint("starting loading settings");
    await DataStore().init();
    await GradeManager().init();
    await SessionState().init();
    await ActiveSession().init();
    await LocationHandler().init();
    await StatisticCharts.init();

    // Firebase not working with
    // ios so only activate if on android
    if (Platform.isAndroid) {
      try {
        await Firebase.initializeApp(
          options: DefaultFirebaseOptions.currentPlatform,
        );
      } catch (e) {
        debugPrint(e.toString());
      }
    }
    debugPrint("finished loading settings");
  }

  static _onUpgrade() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    int oldVersion = int.parse(prefs.getString(appVersionKey) ?? "0");
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    int currentVersion = int.parse(packageInfo.buildNumber);

    if (oldVersion < currentVersion) {
      // 2.10.0
      if (oldVersion < 105) {
        ToolTipsDataStore().reset();
      }
      prefs.setString(appVersionKey, currentVersion.toString());
    }
  }
}
