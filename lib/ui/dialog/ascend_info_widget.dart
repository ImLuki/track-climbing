import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums/ascend_characteristics.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/enums/steepness.dart';
import 'package:climbing_track/backend/util/custom_date_formatter.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AscendInfoWidget extends StatelessWidget {
  final Ascend ascend;
  final TextStyle? textStyle;

  const AscendInfoWidget({super.key, required this.ascend, this.textStyle});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: FocusScope.of(context).unfocus,
      child: SingleChildScrollView(
        child: Wrap(
          runSpacing: 10.0,
          children: _createRouteInfo(ascend, context),
        ),
      ),
    );
  }

  List<Widget> _createRouteInfo(Ascend ascend, BuildContext context) {
    return <Widget>[
      _location(ascend, context),
      _date(ascend, context),
      _name(ascend, context),
      ..._speed(ascend, context),
      _difficulty(ascend, context),
      ..._style(ascend, context),
      ..._height(ascend, context),
      _rating(ascend, context),
      _comment(ascend, context),
    ];
  }

  Widget _location(Ascend ascend, BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "${AppLocalizations.of(context)!.general_location}:",
          style: textStyle?.copyWith(fontWeight: FontWeight.w500) ?? const TextStyle(fontWeight: FontWeight.w500),
        ),
        const SizedBox(width: Constants.STANDARD_PADDING),
        Flexible(
          child: SelectableText(
            ascend.location!,
            textAlign: TextAlign.end,
            style: textStyle?.copyWith(overflow: TextOverflow.ellipsis),
          ),
        ),
      ],
    );
  }

  Widget _date(Ascend ascend, BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          "${AppLocalizations.of(context)!.general_date}:",
          style: textStyle?.copyWith(fontWeight: FontWeight.w500) ?? const TextStyle(fontWeight: FontWeight.w500),
        ),
        const SizedBox(width: Constants.STANDARD_PADDING),
        Flexible(
          child: SelectableText(
            maxLines: 1,
            CustomDateFormatter.formatDateString(context, ascend.dateTime!),
            style: textStyle?.copyWith(overflow: TextOverflow.ellipsis),
          ),
        ),
      ],
    );
  }

  Widget _name(Ascend ascend, BuildContext context) {
    return Visibility(
      visible: !(ascend.type.isSpeed() && ascend.speedType == 0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "${AppLocalizations.of(context)!.general_name}:",
            style: textStyle?.copyWith(fontWeight: FontWeight.w500) ?? const TextStyle(fontWeight: FontWeight.w500),
          ),
          const SizedBox(width: Constants.STANDARD_PADDING),
          Flexible(
            child: SelectableText(
              ascend.name.isEmpty
                  ? "-"
                  : Util.getAscendNameWithoutPrefix(
                      ascend.name,
                      context,
                      speedType: ascend.speedType,
                      type: ascend.type,
                    ),
              textAlign: TextAlign.end,
              style: textStyle?.copyWith(overflow: TextOverflow.ellipsis),
            ),
          )
        ],
      ),
    );
  }

  Widget _difficulty(Ascend ascend, BuildContext context) {
    return Visibility(
      visible: !(ascend.type.isSpeed() && ascend.speedType == 0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SelectableText(
            "${AppLocalizations.of(context)!.general_difficulty}:",
            maxLines: 1,
            style: textStyle?.copyWith(fontWeight: FontWeight.w500) ?? const TextStyle(fontWeight: FontWeight.w500),
          ),
          const SizedBox(width: 10.0),
          Flexible(
            flex: 2,
            child: SelectableText(
              Util.getOriginalGradeAndCurrent(ascend.gradeID, ascend.type, gradingSystem: ascend.originalGradeSystem),
              textAlign: TextAlign.end,
              style: textStyle?.copyWith(overflow: TextOverflow.ellipsis),
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> _style(Ascend ascend, BuildContext context) {
    String first = "";
    String second = "";

    switch (ascend.type) {
      case ClimbingType.SPEED_CLIMBING:
        if (ascend.speedType == 0) return [];
        first = Util.getTopOrLead(ascend.toprope, context, ascend.type.shouldRenameTopropeToFollowing());
        break;
      case ClimbingType.FREE_SOLO:
        return [];
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.MULTI_PITCH:
      case ClimbingType.TRAD_CLIMBING:
        first = Util.getStyle(ascend.styleID, context, tries: ascend.tries);
        second = Util.getTopOrLead(ascend.toprope, context, ascend.type.shouldRenameTopropeToFollowing());
        break;
      case ClimbingType.DEEP_WATER_SOLO:
      case ClimbingType.BOULDER:
        first = Util.getStyle(ascend.styleID, context, tries: ascend.tries);
    }

    final List<AscendCharacteristics> routeStyleList = AscendCharacteristics.values
        .where((element) => AscendCharacteristics.hasFlag(element, ascend.characteristic))
        .toList();

    return [
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "${AppLocalizations.of(context)!.style}:",
            style: textStyle?.copyWith(fontWeight: FontWeight.w500) ?? const TextStyle(fontWeight: FontWeight.w500),
          ),
          const SizedBox(width: Constants.STANDARD_PADDING),
          Flexible(
            child: SelectableText(
              first,
              textAlign: TextAlign.end,
              style: textStyle?.copyWith(overflow: TextOverflow.ellipsis),
            ),
          ),
        ],
      ),
      if (second != "")
        Align(
          alignment: Alignment.centerRight,
          child: SelectableText(
            second,
            style: textStyle?.copyWith(overflow: TextOverflow.ellipsis),
          ),
        ),
      Align(
        alignment: Alignment.centerRight,
        child: Wrap(
          spacing: 6.0,
          runSpacing: 6.0,
          children: List.generate(routeStyleList.length, (index) => routeStyleList[index].getIconWidget(context)),
        ),
      ),
    ];
  }

  List<Widget> _speed(Ascend ascend, BuildContext context) {
    return [
      Visibility(
        visible: ascend.type.isSpeed(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "${AppLocalizations.of(context)!.general_time}:",
              style: textStyle?.copyWith(fontWeight: FontWeight.w500) ?? const TextStyle(fontWeight: FontWeight.w500),
            ),
            SelectableText(
              "${(ascend.speedTime / 1000).toStringAsFixed(2)} ${AppLocalizations.of(context)!.util_seconds}",
              style: textStyle?.copyWith(overflow: TextOverflow.ellipsis),
            ),
          ],
        ),
      ),
      Visibility(
        visible: ascend.type.isSpeed(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "${AppLocalizations.of(context)!.general_type}:",
              style: textStyle?.copyWith(fontWeight: FontWeight.w500) ?? const TextStyle(fontWeight: FontWeight.w500),
            ),
            SelectableText(
              ascend.speedType == 0
                  ? AppLocalizations.of(context)!.general_comp_route
                  : AppLocalizations.of(context)!.general_normal_route,
              style: textStyle?.copyWith(overflow: TextOverflow.ellipsis),
            ),
          ],
        ),
      ),
    ];
  }

  List<Widget> _height(Ascend ascend, BuildContext context) {
    final List<Steepness> steepnessList =
        Steepness.values.where((element) => Steepness.hasFlag(element, ascend.steepness)).toList();

    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "${AppLocalizations.of(context)!.general_height}:",
            style: textStyle?.copyWith(fontWeight: FontWeight.w500) ?? const TextStyle(fontWeight: FontWeight.w500),
          ),
          SelectableText(
            Util.getHeightWithShortUnit(ascend.height),
            style: textStyle?.copyWith(overflow: TextOverflow.ellipsis),
          )
        ],
      ),
      Align(
        alignment: Alignment.centerRight,
        child: Wrap(
          spacing: 6.0,
          runSpacing: 6.0,
          children: List.generate(steepnessList.length, (index) => steepnessList[index].getIconWidget(context)),
        ),
      ),
    ];
  }

  Widget _rating(Ascend ascend, BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          "${AppLocalizations.of(context)!.general_rating}:",
          style: textStyle?.copyWith(fontWeight: FontWeight.w500) ?? const TextStyle(fontWeight: FontWeight.w500),
        ),
        const SizedBox(width: 12.0),
        Flexible(
          child: Util.getRating(ascend.rating),
        ),
      ],
    );
  }

  Widget _comment(Ascend ascend, BuildContext context) {
    String comment = Util.getComment(ascend.comment);
    if (comment == "-") {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "${AppLocalizations.of(context)!.general_comment(1)}:",
            style: textStyle?.copyWith(fontWeight: FontWeight.w500) ?? const TextStyle(fontWeight: FontWeight.w500),
          ),
          const SizedBox(width: 12.0),
          Flexible(
            child: SelectableText(
              comment,
              textAlign: TextAlign.end,
              style: textStyle?.copyWith(overflow: TextOverflow.clip),
            ),
          )
        ],
      );
    } else {
      return Wrap(
        alignment: WrapAlignment.spaceBetween,
        runAlignment: WrapAlignment.spaceBetween,
        runSpacing: 4.0,
        spacing: 20.0,
        children: [
          Text(
            "${AppLocalizations.of(context)!.general_comment(1)}:",
            style: textStyle?.copyWith(fontWeight: FontWeight.w500) ?? const TextStyle(fontWeight: FontWeight.w500),
          ),
          SelectableText(
            comment,
            textAlign: TextAlign.start,
            style: textStyle?.copyWith(overflow: TextOverflow.clip),
          ),
        ],
      );
    }
  }
}
