import 'dart:math';

import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:climbing_track/view_model/home_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';

class ConfirmAscendDialog {
  bool warningRoutesVisible = false;

  Future<bool?> showConfirmAscendDialog(
    BuildContext context, {
    required HomeViewModel homeViewModel,
    required Ascend ascend,
  }) async {
    List<Ascend> existingAscents = [];
    if (ClimbingStyle.getById(ascend.styleID).isFirstTry()) {
      existingAscents = await Util.findExistingAscents(ascend);
    }

    if (context.mounted) {
      return showDialog<bool>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("${ascend.type.toTitle(context)}:"),
            titleTextStyle: Theme.of(context).textTheme.titleLarge,
            contentTextStyle: Theme.of(context).textTheme.bodyLarge,
            actionsOverflowDirection: VerticalDirection.up,
            content: SingleChildScrollView(
              child: Wrap(
                runSpacing: 10.0,
                children: _createAscendInfo(ascend, existingAscents, context),
              ),
            ),
            actions: <Widget>[
              Wrap(
                alignment: WrapAlignment.end,
                spacing: 10,
                children: [
                  _cancelButton(context),
                  _saveOriginAscendButton(
                    context,
                    existingAscents: existingAscents,
                    homeViewModel: homeViewModel,
                  ),
                  if (existingAscents.isNotEmpty)
                    _changeAscendButton(
                      context,
                      ascend: ascend,
                      existingAscents: existingAscents,
                      homeViewModel: homeViewModel,
                    )
                ],
              ),
            ],
          );
        },
      );
    } else {
      return false;
    }
  }

  Widget _cancelButton(BuildContext context) {
    return TextButton(
      onPressed: () {
        FocusScope.of(context).unfocus();
        Navigator.of(context).pop();
      },
      child: Text(AppLocalizations.of(context)!.general_cancel),
    );
  }

  Widget _saveOriginAscendButton(
    BuildContext context, {
    required List<Ascend> existingAscents,
    required HomeViewModel homeViewModel,
  }) {
    return TextButton(
      onPressed: () => _saveAscend(context, homeViewModel: homeViewModel),
      child: Text(
        existingAscents.isEmpty
            ? AppLocalizations.of(context)!.general_save
            : AppLocalizations.of(context)!.general_dont_change,
      ),
    );
  }

  Widget _changeAscendButton(
    BuildContext context, {
    required Ascend ascend,
    required List<Ascend> existingAscents,
    required HomeViewModel homeViewModel,
  }) {
    return TextButton(
      onPressed: () {
        homeViewModel.currentAscend.styleID =
            StyleController.getDefaultClimbingStyle(homeViewModel.currentAscend.type).id;

        homeViewModel.currentAscend.tries = max(ascend.tries ?? 2, 2);
        _saveAscend(context, homeViewModel: homeViewModel);
      },
      child: Text(AppLocalizations.of(context)!.general_change),
    );
  }

  void _saveAscend(BuildContext context, {required HomeViewModel homeViewModel}) {
    FocusScope.of(context).unfocus();
    Navigator.of(context).pop(true);
    homeViewModel.addCurrentRoute();
  }

  List<Widget> _createAscendInfo(Ascend ascend, List<Ascend> existingAscents, BuildContext context) {
    return <Widget>[
      if (existingAscents.isNotEmpty) ...[
        _getExistingWarning(ascend, existingAscents, context),
        Container(height: 2.0), // has to be container, sizedBox not working here, don't know why
      ],
      ..._speed(ascend, context),
      _name(ascend, context),
      _difficulty(ascend, context),
      _style(ascend, existingAscents, context),
      ..._leadOrTop(ascend, context),
      _height(ascend, context),
      _rating(ascend, context),
      _comment(ascend, context),
    ];
  }

  Widget _getExistingWarning(Ascend ascend, List<Ascend> existingAscents, BuildContext context) {
    String languageCode = Localizations.localeOf(context).languageCode;
    return StatefulBuilder(
      builder: (context, setState) {
        return Container(
          padding: const EdgeInsets.all(8.0),
          decoration: const BoxDecoration(
            color: Color(0xFFe5e5e5),
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  const Icon(Icons.warning_amber_outlined, color: Colors.orange, size: 32),
                  const SizedBox(width: 12.0),
                  Flexible(
                    child: Text(
                      ascend.isBoulder()
                          ? AppLocalizations.of(context)!.confirm_existing_warning_boulder(
                              DateFormat.yMMMd(languageCode).format(existingAscents.first.dateTime!),
                              Util.getStyle(ascend.styleID, context, tries: ascend.tries),
                              AppLocalizations.of(context)!.style_redpoint,
                            )
                          : AppLocalizations.of(context)!.confirm_existing_warning_route(
                              DateFormat.yMMMd(languageCode).format(existingAscents.first.dateTime!),
                              Util.getStyle(ascend.styleID, context, tries: ascend.tries),
                              AppLocalizations.of(context)!.style_redpoint,
                            ),
                      overflow: TextOverflow.clip,
                      style: Theme.of(context).textTheme.bodyMedium,
                    ),
                  ),
                ],
              ),
              AnimatedSize(
                duration: const Duration(milliseconds: 200),
                child: Visibility(
                  visible: warningRoutesVisible,
                  child: Container(
                    padding: const EdgeInsets.only(top: 8.0, left: 10.0),
                    child: _getExistingWarningAscendListing(context, existingAscents),
                  ),
                ),
              ),
              const SizedBox(height: 6.0),
              Row(
                children: [
                  const Spacer(),
                  OutlinedButton(
                    style: OutlinedButton.styleFrom(
                      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      minimumSize: const Size(0, 0),
                      padding: const EdgeInsets.all(10.0),
                      backgroundColor: const Color(0xFFe5e5e5),
                      foregroundColor: Colors.black,
                      visualDensity: VisualDensity.compact,
                    ),
                    onPressed: () {
                      setState(() => warningRoutesVisible = !warningRoutesVisible);
                    },
                    child: Text(
                      warningRoutesVisible
                          ? AppLocalizations.of(context)!.less_info
                          : AppLocalizations.of(context)!.more_info,
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _getExistingWarningAscendListing(BuildContext context, List<Ascend> existingAscents) {
    String languageCode = Localizations.localeOf(context).languageCode;
    List<Widget> children = [];

    for (Ascend ascend in existingAscents) {
      children.add(
        Text(
          "${DateFormat.yMMMd(languageCode).format(ascend.dateTime!)}:",
          style: Theme.of(context).textTheme.bodyMedium?.copyWith(fontWeight: FontWeight.bold),
        ),
      );
      children.add(const SizedBox(width: 8.0));
      children.add(
        Padding(
          padding: const EdgeInsets.only(left: 12.0),
          child: Text(
            Util.getAscendNameWithoutPrefix(
              ascend.name,
              context,
              speedType: ascend.speedType,
              type: ascend.type,
            ),
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.bodyMedium,
          ),
        ),
      );
      children.add(
        Padding(
          padding: const EdgeInsets.only(left: 12.0),
          child: Text(
            "(${Util.getAscendSummary(ascend, context)})",
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.bodyMedium,
          ),
        ),
      );
      children.add(const SizedBox(height: 10.0));
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: children,
    );
  }

  Widget _name(Ascend ascend, BuildContext context) {
    return Visibility(
      visible: !(ascend.type.isSpeed() && ascend.speedType == 0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text("${AppLocalizations.of(context)!.general_name}:", style: const TextStyle(fontWeight: FontWeight.w500)),
          const SizedBox(width: Constants.STANDARD_PADDING),
          Flexible(
            child: Text(
              Util.getAscendNameWithoutPrefix(
                ascend.name,
                context,
                speedType: ascend.speedType,
                type: ascend.type,
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 3,
            ),
          ),
        ],
      ),
    );
  }

  Widget _difficulty(Ascend ascend, BuildContext context) {
    return Visibility(
      visible: !(ascend.type.isSpeed() && ascend.speedType == 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text("${AppLocalizations.of(context)!.general_difficulty}:",
              style: const TextStyle(fontWeight: FontWeight.w500)),
          const SizedBox(width: 4.0),
          const Spacer(flex: 1),
          Flexible(
            flex: 2,
            child: Text(
              Util.getGrade(ascend.gradeID, ascend.type, gradingSystem: ascend.originalGradeSystem),
              textAlign: TextAlign.end,
            ),
          ),
        ],
      ),
    );
  }

  Widget _style(Ascend ascend, List<Ascend> existingAscents, BuildContext context) {
    return Visibility(
      visible: !(ascend.type.isFreeSolo() || ascend.type.isSpeed()),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text("${AppLocalizations.of(context)!.style}:", style: const TextStyle(fontWeight: FontWeight.w500)),
          existingAscents.isEmpty
              ? Flexible(
                  child: Text(
                    Util.getStyle(ascend.styleID, context, tries: ascend.tries),
                    textAlign: TextAlign.end,
                  ),
                )
              : Flexible(
                  child: Wrap(
                    children: [
                      Text(
                        Util.getStyle(ascend.styleID, context, tries: ascend.tries),
                        style: const TextStyle(color: Colors.red),
                      ),
                      const Text(" -> "),
                      Text(
                        ascend.isBoulder()
                            ? AppLocalizations.of(context)!.style_top
                            : AppLocalizations.of(context)!.style_redpoint,
                        style: const TextStyle(color: Colors.green),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
        ],
      ),
    );
  }

  List<Widget> _leadOrTop(Ascend ascend, BuildContext context) {
    switch (ascend.type) {
      case ClimbingType.SPEED_CLIMBING:
        if (ascend.speedType == 0) return [];
        return [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("${AppLocalizations.of(context)!.style}:", style: const TextStyle(fontWeight: FontWeight.w500)),
              Flexible(
                child: Text(
                  Util.getTopOrLead(ascend.toprope, context, ascend.type.shouldRenameTopropeToFollowing()),
                ),
              ),
            ],
          ),
        ];
      case ClimbingType.BOULDER:
      case ClimbingType.FREE_SOLO:
      case ClimbingType.DEEP_WATER_SOLO:
        return [];
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.MULTI_PITCH:
      case ClimbingType.TRAD_CLIMBING:
        return [
          Align(
            alignment: Alignment.centerRight,
            child: Text(Util.getTopOrLead(ascend.toprope, context, ascend.type.shouldRenameTopropeToFollowing())),
          ),
        ];
    }
  }

  Widget _rating(Ascend ascend, BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          "${AppLocalizations.of(context)!.general_rating}:",
          style: const TextStyle(fontWeight: FontWeight.w500),
        ),
        const SizedBox(width: 12.0),
        Flexible(child: Util.getRating(ascend.rating)),
      ],
    );
  }

  Widget _comment(Ascend ascend, BuildContext context) {
    String comment = Util.getComment(ascend.comment);
    if (comment == "-") {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "${AppLocalizations.of(context)!.general_comment(1)}:",
            style: const TextStyle(fontWeight: FontWeight.w500),
          ),
          const SizedBox(width: 12.0),
          Flexible(
            child: Text(
              comment,
              overflow: TextOverflow.clip,
              textAlign: TextAlign.end,
            ),
          )
        ],
      );
    } else {
      return Wrap(
        alignment: WrapAlignment.spaceBetween,
        runAlignment: WrapAlignment.spaceBetween,
        runSpacing: 4.0,
        spacing: 20.0,
        children: [
          Text(
            "${AppLocalizations.of(context)!.general_comment(1)}:",
            style: const TextStyle(fontWeight: FontWeight.w500),
          ),
          Text(
            comment,
            overflow: TextOverflow.clip,
            textAlign: TextAlign.start,
          ),
        ],
      );
    }
  }

  List<Widget> _speed(Ascend ascend, BuildContext context) {
    return [
      Visibility(
        visible: ascend.type.isSpeed(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "${AppLocalizations.of(context)!.general_time}:",
              style: const TextStyle(fontWeight: FontWeight.w500),
            ),
            Text("${(ascend.speedTime / 1000).toStringAsFixed(2)}s"),
          ],
        ),
      ),
      Visibility(
        visible: ascend.type.isSpeed(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "${AppLocalizations.of(context)!.general_route}:",
              style: const TextStyle(fontWeight: FontWeight.w500),
            ),
            Text(ascend.speedType == 0
                ? AppLocalizations.of(context)!.general_comp_route
                : AppLocalizations.of(context)!.general_normal_route),
          ],
        ),
      ),
    ];
  }

  Widget _height(Ascend ascend, context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          "${AppLocalizations.of(context)!.general_height}:",
          style: const TextStyle(fontWeight: FontWeight.w500),
        ),
        Text(Util.getHeightWithShortUnit(ascend.height))
      ],
    );
  }
}
