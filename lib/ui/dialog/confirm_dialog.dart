import 'dart:io';
import 'dart:ui';

import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:climbing_track/ui/dialog/end_session_dialog.dart';
import 'package:climbing_track/view_model/home_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ConfirmDialog {
  static const double SMALL_FONT_SIZE = 14;
  static const double MATERIAL_BUTTON_HEIGHT = 38;
  static final ImageFilter blurFilter = ImageFilter.blur(sigmaX: 6, sigmaY: 6);

  Future<void> showEndSessionDialog({
    required BuildContext context,
    required HomeViewModel homeViewModel,
    required Session session,
    required int routeCount,
  }) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // This is handled by the PopScope below
      builder: (BuildContext context) {
        return PopScope(
          canPop: false, // Context cant pop
          onPopInvoked: (_) => FocusScope.of(context).unfocus(),
          child: StatefulBuilder(builder: (context, setState) {
            return EndSessionDialog(
              homeViewModel: homeViewModel,
              session: session,
              routeCount: routeCount,
            );
          }),
        );
      },
    );
  }

  Future<bool?> showDeleteDatabaseDialog(BuildContext context, Function onAccept) async {
    return showDialog<bool?>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 24.0),
          titleTextStyle: Theme.of(context).textTheme.titleLarge,
          contentTextStyle: Theme.of(context).textTheme.bodyMedium,
          scrollable: true,
          content: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(top: 12.0),
                width: 50.0,
                height: 50.0,
                decoration: const BoxDecoration(
                  color: Colors.red,
                  shape: BoxShape.circle,
                ),
                child: const Icon(Icons.delete_forever_rounded, color: Colors.white, size: 42),
              ),
              const SizedBox(width: 14.0),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: Constants.SMALL_PADDING),
                    Text(
                      AppLocalizations.of(context)!.confirm_delete_data,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(height: Constants.SMALL_PADDING),
                    Text(
                      AppLocalizations.of(context)!.confirm_delete_permanently_lose_data,
                      overflow: TextOverflow.clip,
                    ),
                    const SizedBox(height: Constants.SMALL_PADDING),
                    Text(
                      AppLocalizations.of(context)!.confirm_delete_sessions,
                      overflow: TextOverflow.clip,
                    ),
                    const SizedBox(height: 3.0),
                    Text(
                      AppLocalizations.of(context)!.confirm_delete_ascends,
                      overflow: TextOverflow.clip,
                    ),
                    const SizedBox(height: 3.0),
                    Text(
                      AppLocalizations.of(context)!.confirm_delete_statistics,
                      overflow: TextOverflow.clip,
                    )
                  ],
                ),
              ),
            ],
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () => {FocusScope.of(context).unfocus(), Navigator.of(context).pop()},
              child: Text(
                AppLocalizations.of(context)!.general_cancel,
                style: TextStyle(color: Colors.red.shade700),
              ),
            ),
            TextButton(
              style: TextButton.styleFrom(
                padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 8),
                backgroundColor: Colors.red.shade700,
                foregroundColor: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
              onPressed: () => {onAccept(), FocusScope.of(context).unfocus(), Navigator.of(context).pop()},
              child: Text(AppLocalizations.of(context)!.general_delete),
            ),
          ],
        );
      },
    );
  }

  Future<bool?> showLoadBackupDialog(BuildContext context, Function onAccept) async {
    return showDialog<bool?>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 24.0),
          titleTextStyle: Theme.of(context).textTheme.titleMedium,
          scrollable: true,
          content: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Padding(
                padding: EdgeInsets.only(top: 8),
                child: Icon(Icons.warning_amber_rounded, color: Colors.orange, size: 42),
              ),
              const SizedBox(width: 14.0),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: Constants.SMALL_PADDING),
                    Text(
                      "${AppLocalizations.of(context)!.confirm_load_backup}?",
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(height: Constants.SMALL_PADDING),
                    Text(
                      AppLocalizations.of(context)!.confirm_load_backup_warning,
                      style: Theme.of(context).textTheme.bodyMedium,
                      overflow: TextOverflow.clip,
                    ),
                  ],
                ),
              ),
            ],
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () => {
                FocusScope.of(context).unfocus(),
                Navigator.of(context).pop(),
              },
              child: Text(
                AppLocalizations.of(context)!.general_cancel,
              ),
            ),
            TextButton(
              onPressed: () => {FocusScope.of(context).unfocus(), Navigator.pop(context), onAccept()},
              child: Text(AppLocalizations.of(context)!.confirm_load_backup),
            ),
          ],
        );
      },
    );
  }

  Future<bool?> showChooseBackupDialog(
    BuildContext context,
    Function(String) onAccept,
    List<FileSystemEntity> backupList,
  ) async {
    return showDialog<bool?>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 24.0),
          titleTextStyle: Theme.of(context).textTheme.titleMedium,
          contentTextStyle: Theme.of(context).textTheme.bodyMedium,
          content: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(top: 8),
                      child: Icon(Icons.warning_amber_rounded, color: Colors.orange, size: 42),
                    ),
                    const SizedBox(width: 14.0),
                    Flexible(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: Constants.SMALL_PADDING),
                          Text(
                            "${AppLocalizations.of(context)!.confirm_load_backup}?",
                            style: Theme.of(context).textTheme.titleLarge,
                          ),
                          const SizedBox(height: Constants.SMALL_PADDING),
                          Text(
                            AppLocalizations.of(context)!.confirm_choose_backup_warning,
                            overflow: TextOverflow.clip,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: Constants.STANDARD_PADDING),
                ...List.generate(
                  backupList.length,
                  (index) => InkWell(
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: const BorderSide(color: Colors.black26),
                              top: index == 0
                                  ? const BorderSide(color: Colors.black26)
                                  : const BorderSide(color: Colors.transparent))),
                      child: ListTile(
                        trailing: const Icon(Icons.north_west, size: 18),
                        visualDensity: VisualDensity.compact,
                        title: Text(
                          backupList[index].path.split("/").last.replaceAll("-climbing-tracker.json", ""),
                          style: Theme.of(context).textTheme.bodyLarge,
                        ),
                        onTap: () {
                          FocusScope.of(context).unfocus();
                          Navigator.pop(context);
                          onAccept(backupList[index].path);
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
              child: Text(
                AppLocalizations.of(context)!.general_cancel,
              ),
            ),
          ],
        );
      },
    );
  }

  Future<bool?> showHeightUpdateDialog(BuildContext context) async {
    return showDialog<bool?>(
      context: context,
      barrierDismissible: false, // user must tap button
      builder: (BuildContext context) {
        return AlertDialog(
          titleTextStyle: Theme.of(context).textTheme.titleMedium,
          contentTextStyle: Theme.of(context).textTheme.bodyMedium,
          actionsPadding: const EdgeInsets.only(
            bottom: Constants.STANDARD_PADDING,
            left: Constants.STANDARD_PADDING,
            right: Constants.STANDARD_PADDING,
          ),
          title: Text(AppLocalizations.of(context)!.confirm_update_ascends),
          content: Text(AppLocalizations.of(context)!.confirm_update_ascends_info),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: Text(
                AppLocalizations.of(context)!.general_no,
              ),
            ),
            TextButton(
              onPressed: () => Navigator.of(context).pop(true),
              child: Text(
                AppLocalizations.of(context)!.general_yes,
              ),
            ),
          ],
        );
      },
    );
  }
}
