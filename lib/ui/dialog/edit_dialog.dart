import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/enums/date_format.dart';
import 'package:climbing_track/backend/model/enums/height_measurement.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';

class EditDialog {
  int tichyCounter = 0;

  Future<void> showGradingSystemsDialog(BuildContext context, ClimbingType routeType) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text(
            routeType.isBoulder()
                ? AppLocalizations.of(context)!.edit_dialog_boulder_system
                : AppLocalizations.of(context)!.edit_dialog_climbing_system,
            style: Theme.of(context).textTheme.titleLarge,
          ),
          children: List.generate(
            GradeManager().getGradeMapper(type: routeType).getGradeSystems().length,
            (index) => InkWell(
                child: ListTile(
                  title: Text(
                    GradeManager().getGradeMapper(type: routeType).getGradeSystems()[index],
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                  leading: Icon(
                    GradeManager().getGradeMapper(type: routeType).getGradeSystems()[index] ==
                            GradeManager().getGradeMapper(type: routeType).currentGradingSystem
                        ? Icons.radio_button_checked
                        : Icons.radio_button_off,
                  ),
                ),
                onTap: () {
                  GradeManager().getGradeMapper(type: routeType).currentGradingSystem =
                      GradeManager().getGradeMapper(type: routeType).getGradeSystems()[index];
                  Navigator.of(context).pop();
                }),
          ),
        );
      },
    );
  }

  Future<void> showMeasurementDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text(
            AppLocalizations.of(context)!.edit_dialog_measurement_unit,
            style: Theme.of(context).textTheme.titleLarge,
          ),
          children: [
            InkWell(
              child: ListTile(
                title: Text(
                  AppLocalizations.of(context)!.general_meter(1),
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                leading: Icon(
                  DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.METER
                      ? Icons.radio_button_checked
                      : Icons.radio_button_off,
                ),
              ),
              onTap: () {
                DataStore().settingsDataStore.heightMeasurement = HeightMeasurement.METER;
                Navigator.of(context).pop();
              },
            ),
            InkWell(
              child: ListTile(
                title: Text(
                  AppLocalizations.of(context)!.general_feet(2),
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                leading: Icon(
                  DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET
                      ? Icons.radio_button_checked
                      : Icons.radio_button_off,
                ),
              ),
              onTap: () {
                DataStore().settingsDataStore.heightMeasurement = HeightMeasurement.FEET;
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<bool?> showLanguageDialog(BuildContext context) async {
    return showDialog<bool?>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            focusColor: Colors.transparent,
            hoverColor: Colors.transparent,
            child: Text(
              AppLocalizations.of(context)!.edit_dialog_language,
              style: Theme.of(context).textTheme.titleLarge,
            ),
            onTap: () => tichyMode(context),
          ),
          children: [
            InkWell(
              child: ListTile(
                title: Text(
                  AppLocalizations.of(context)!.general_english,
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                leading: Icon(
                  DataStore().settingsDataStore.language == "en" ? Icons.radio_button_checked : Icons.radio_button_off,
                ),
              ),
              onTap: () {
                DataStore().settingsDataStore.language = "en";
                Navigator.of(context).pop(true);
              },
            ),
            InkWell(
              child: ListTile(
                title: Text(
                  AppLocalizations.of(context)!.general_german,
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                leading: Icon(
                  DataStore().settingsDataStore.language == "de" ? Icons.radio_button_checked : Icons.radio_button_off,
                ),
              ),
              onTap: () {
                DataStore().settingsDataStore.language = "de";
                Navigator.of(context).pop(true);
              },
            ),
            if (DataStore().settingsDataStore.tichyMode)
              InkWell(
                child: ListTile(
                  title: Text(
                    AppLocalizations.of(context)!.tichy_mode_german,
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                  leading: Icon(
                    DataStore().settingsDataStore.language == "de_TY"
                        ? Icons.radio_button_checked
                        : Icons.radio_button_off,
                  ),
                ),
                onTap: () {
                  DataStore().settingsDataStore.language = "de_TY";
                  Navigator.of(context).pop(true);
                },
              )
          ],
        );
      },
    );
  }

  void tichyMode(BuildContext context) {
    if (DataStore().settingsDataStore.tichyMode) return;
    tichyCounter++;
    if (tichyCounter == 10) {
      DataStore().settingsDataStore.tichyMode = true;
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            behavior: SnackBarBehavior.floating,
            content: Text(AppLocalizations.of(context)!.tichy_mode_activated),
          ),
        );
      return;
    }

    if (tichyCounter >= 5) {
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            behavior: SnackBarBehavior.floating,
            content: Text(AppLocalizations.of(context)!.tichy_mode_countdown(10 - tichyCounter)),
          ),
        );
    }
  }

  Future<void> showDateFormatDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text(
            AppLocalizations.of(context)!.edit_dialog_measurement_unit,
            style: Theme.of(context).textTheme.titleLarge,
          ),
          children: List.generate(
            CustomDateFormat.values.length,
            (index) => InkWell(
              child: ListTile(
                title: Text(
                  CustomDateFormat.values[index] == CustomDateFormat.SYSTEM
                      ? AppLocalizations.of(context)!.date_format_system
                      : "${CustomDateFormat.values[index].formatString.toUpperCase()} "
                          "(${DateFormat(CustomDateFormat.values[index].formatString).format(DateTime.now())})",
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                leading: Icon(
                  DataStore().settingsDataStore.currentDateFormat == CustomDateFormat.values[index]
                      ? Icons.radio_button_checked
                      : Icons.radio_button_off,
                ),
              ),
              onTap: () {
                DataStore().settingsDataStore.currentDateFormat = CustomDateFormat.values[index];
                Navigator.of(context).pop();
              },
            ),
          ),
        );
      },
    );
  }
}
