import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/view_model/home_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class EndSessionDialog extends StatefulWidget {
  final HomeViewModel homeViewModel;
  final Session session;
  final int routeCount;

  const EndSessionDialog({
    super.key,
    required this.homeViewModel,
    required this.session,
    required this.routeCount,
  });

  @override
  _EndSessionDialogState createState() => _EndSessionDialogState(DateTime.now());
}

class _EndSessionDialogState extends State<EndSessionDialog> {
  final FocusNode _commentTextFieldFocus = FocusNode();
  late final TextEditingController _textEditingController;
  DateTime endTime;

  _EndSessionDialogState(this.endTime);

  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController(text: widget.session.comment);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onVerticalDragUpdate: (dragDetails) {
        FocusScope.of(context).unfocus();
      },
      onTapDown: (tapDownDetails) {
        FocusScope.of(context).unfocus();
      },
      child: AlertDialog(
        scrollable: false,
        titleTextStyle: Theme.of(context).textTheme.titleMedium,
        content: createContent(context),
        contentTextStyle: Theme.of(context).textTheme.bodyLarge,
        actions: <Widget>[
          _cancelButton(),
          _actionButton(),
        ],
      ),
    );
  }

  Widget _cancelButton() {
    return TextButton(
      onPressed: () => widget.homeViewModel.onCancelEndSession(context, widget.session),
      child: Text(AppLocalizations.of(context)!.general_cancel),
    );
  }

  Widget _actionButton() {
    return TextButton(
      onPressed: () => widget.homeViewModel.onSubmitEndSession(
        context,
        routeCount: widget.routeCount,
        session: widget.session,
        endTime: endTime,
      ),
      child: widget.routeCount <= 0
          ? Text(AppLocalizations.of(context)!.general_delete)
          : Text(AppLocalizations.of(context)!.general_end),
    );
  }

  Widget createContent(BuildContext context) {
    if (widget.routeCount <= 0) {
      return Text(AppLocalizations.of(context)!.end_session_dialog_no_route_added);
    }

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(width: MediaQuery.of(context).size.width), // This means that the dialog is as wide as possible
          Align(
            alignment: Alignment.center,
            child: Text(
              "${AppLocalizations.of(context)!.home_page_close_session}?",
              style: Theme.of(context).textTheme.headlineSmall?.copyWith(color: Colors.black),
            ),
          ),
          ..._sessionTime(),
          ..._comment(),
        ],
      ),
    );
  }

  List<Widget> _comment() {
    return [
      const SizedBox(height: 20.0),
      Text(AppLocalizations.of(context)!.end_session_session_comment),
      SizedBox(height: _commentTextFieldFocus.hasFocus ? 12.0 : 16.0),
      Theme(
        data: ThemeData.from(
          colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor),
          useMaterial3: false,
        ),
        child: TextFormField(
          maxLines: null,
          minLines: 3,
          controller: _textEditingController,
          textAlign: TextAlign.start,
          textAlignVertical: TextAlignVertical.center,
          onChanged: (String value) => widget.session.comment = value.trim(),
          style: const TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            isDense: true,
            fillColor: AppColors.accentColor,
            label: Text(
              AppLocalizations.of(context)!.end_session_session_comment_info,
              style: const TextStyle(fontSize: 18, color: AppColors.accentColor2, overflow: TextOverflow.ellipsis),
              maxLines: widget.homeViewModel.commentEditingController.text.isNotEmpty || _commentTextFieldFocus.hasFocus
                  ? 1
                  : 2,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(12.0),
              borderSide: const BorderSide(),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: AppColors.accentColor, width: 1.0),
              borderRadius: BorderRadius.circular(12.0),
            ),
          ),
        ),
      ),
    ];
  }

  List<Widget> _sessionTime() {
    return [
      const SizedBox(height: 12.0),
      Text("${AppLocalizations.of(context)!.sessions_page_duration}:"),
      const SizedBox(height: 6.0),
      InkWell(
        onTap: _onChooseTime,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(width: Theme.of(context).iconTheme.size ?? 24.0), // match icon size
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: Text(
                "${widget.session.getDurationDifference(endTime)} h",
                style: Theme.of(context).textTheme.headlineSmall?.copyWith(
                      color: AppColors.accentColor,
                      height: 0.0,
                    ),
              ),
            ),
            const Icon(
              Icons.more_time,
              color: Colors.black,
              size: 24.0,
            ),
          ],
        ),
      ),
    ];
  }

  void _onChooseTime() {
    Duration diff = endTime.difference(widget.session.startDate);
    showTimePicker(
      builder: (BuildContext context, Widget? child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child!,
        );
      },
      helpText: AppLocalizations.of(context)!.end_session_dialog_change_duration,
      context: context,
      initialTime: TimeOfDay(
        hour: diff.inHours,
        minute: diff.inMinutes % 60,
      ),
    ).then(
      (time) {
        if (time != null) {
          endTime = widget.session.startDate.add(
            Duration(hours: time.hour, minutes: time.minute),
          );
          setState(() {});
        }
      },
    );
  }
}
