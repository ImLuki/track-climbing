import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/enums/enums.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/dialog/ascend_info_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher_string.dart';

class InfoDialog {
  Future<bool?> showLeaderboardInfo(BuildContext context) async {
    List<StyleType> styles = List.from(StyleType.values)..remove(StyleType.NONE);

    return showDialog<bool?>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(AppLocalizations.of(context)!.overview_icon_legend),
          titleTextStyle: Theme.of(context).textTheme.titleLarge,
          contentTextStyle: Theme.of(context).textTheme.bodyLarge,
          content: SingleChildScrollView(
            child: Column(
              children: List.generate(
                styles.length,
                (index) => Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 25.0,
                        height: 25.0,
                        child: Icon(styles[index].getIcon(), size: styles[index].getIconSize()),
                      ),
                      const SizedBox(width: 20.0),
                      Flexible(
                        child: Text(
                          styles[index].toTranslatedString(context),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
              child: Text(AppLocalizations.of(context)!.general_back),
            )
          ],
        );
      },
    );
  }

  Future<bool?> showExistingRoutesDialog({
    required BuildContext context,
    required String title,
    required List<Ascend> existingAscents,
  }) async {
    return showDialog<bool?>(
      context: context,
      barrierDismissible: true, // user must tap button
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          titleTextStyle: Theme.of(context).textTheme.titleLarge,
          contentTextStyle: Theme.of(context).textTheme.bodyLarge,
          content: SingleChildScrollView(
            child: _getExistingWarningAscendListing(context, existingAscents),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
              child: Text(AppLocalizations.of(context)!.general_back),
            )
          ],
        );
      },
    );
  }

  Future<bool?> showAscendDialog(BuildContext context, Ascend ascend) async {
    return showDialog<bool?>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("${ascend.type.toTitle(context)}:"),
          titleTextStyle: Theme.of(context).textTheme.titleLarge,
          contentTextStyle: Theme.of(context).textTheme.bodyLarge,
          content: AscendInfoWidget(ascend: ascend),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
              child: Text(AppLocalizations.of(context)!.general_back),
            )
          ],
        );
      },
    );
  }

  Future<void> showStyleInfoDialog(BuildContext context, ClimbingType type) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(type.getClimbingStyleText(context)),
          titleTextStyle: Theme.of(context).textTheme.titleLarge,
          contentTextStyle: Theme.of(context).textTheme.bodyLarge,
          content: SingleChildScrollView(
            child: Wrap(
              runSpacing: 10.0,
              children: type.isBoulder() ? _getBoulderStyleInfo(context) : _getStyleInfo(context),
            ),
          ),
          contentPadding: const EdgeInsets.only(
            left: 24.0,
            right: 24.0,
            top: 20.0,
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.general_back),
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> showLeadTopRopeInfoDialog(BuildContext context, {bool shouldRenameTopropeToFollowing = false}) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: true,
          contentTextStyle: Theme.of(context).textTheme.bodyLarge,
          contentPadding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 20.0),
          content: SingleChildScrollView(
            child: Wrap(
              runSpacing: 10.0,
              children: shouldRenameTopropeToFollowing ? _getLeadingFollowingInfo(context) : _getLeadTopInfo(context),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.general_back),
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> showSpeedInfoDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          contentTextStyle: Theme.of(context).textTheme.bodyLarge,
          contentPadding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 20.0),
          content: SingleChildScrollView(
            child: Wrap(
              runSpacing: 10.0,
              children: _getSpeedInfo(context),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.general_back),
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> showClimbingTypeInfoDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: false,
          contentTextStyle: Theme.of(context).textTheme.bodyLarge?.copyWith(fontWeight: FontWeight.w400),
          contentPadding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 20.0),
          content: SingleChildScrollView(
            child: Wrap(
              runSpacing: 10.0,
              children: _getBoulderRouteInfo(context),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.general_back),
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  Future<void> showIceGradingInfo(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: false,
          contentTextStyle: Theme.of(context).textTheme.bodyLarge,
          contentPadding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 20.0),
          content: SingleChildScrollView(
            child: Wrap(
              runSpacing: 10.0,
              children: _getIceGradingInfo(context),
            ),
          ),
          actions: <Widget>[
            TextButton(
                child: Text(AppLocalizations.of(context)!.general_back),
                onPressed: () {
                  FocusScope.of(context).unfocus();
                  Navigator.of(context).pop();
                })
          ],
        );
      },
    );
  }

  List<Widget> _getStyleInfo(BuildContext context) {
    return <Widget>[
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.style_onsight,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_onsight_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.style_flash,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_flash_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.style_redpoint,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_redpoint_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.style_repeat,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_repeat_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.style_af,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_af_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.style_hangdogging,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_hang_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.style_aid,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_aid_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          '3/4 | 1/2 | 1/4',
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_half_info),
    ];
  }

  List<Widget> _getBoulderStyleInfo(BuildContext context) {
    return <Widget>[
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.style_flash,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_flash_boulder_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.style_top,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_top_boulder_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.style_repeat,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_repeat_boulder_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.style_project,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_project_boulder_info),
    ];
  }

  List<Widget> _getLeadTopInfo(BuildContext context) {
    return <Widget>[
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.style_lead,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_lead_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.style_toprope,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_toprope_info),
    ];
  }

  List<Widget> _getLeadingFollowingInfo(BuildContext context) {
    return <Widget>[
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.style_leading,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_leading_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.style_following,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_following_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.style_alternate_leading,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_following_info),
    ];
  }

  List<Widget> _getSpeedInfo(BuildContext context) {
    return <Widget>[
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.general_comp_route,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_speed_comp_route_info),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.general_normal_route,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_speed_normal_route_info),
    ];
  }

  List<Widget> _getBoulderRouteInfo(BuildContext context) {
    List<Widget> content = [];

    for (ClimbingType climbingType in ClimbingType.values) {
      content.addAll([
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            climbingType.toName(context),
            style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
          ),
        ),
        Text(climbingType.getDescription(context)),
        const Divider(thickness: 2.0),
      ]);
    }

    content.removeLast();
    return content;
  }

  List<Widget> _getIceGradingInfo(BuildContext context) {
    return <Widget>[
      Text(
        AppLocalizations.of(context)!.info_dialog_further_information,
        style: Theme.of(context).textTheme.titleLarge,
      ),
      SelectableLinkify(
        onOpen: (link) async {
          if (await canLaunchUrlString(link.url)) {
            await launchUrlString(link.url, mode: LaunchMode.externalApplication);
          } else {
            throw 'Could not launch $link';
          }
        },
        options: const LinkifyOptions(humanize: false),
        text: "https://ascentionism.com/ice-climbing-grades-a-complete-guide",
      ),
      const Divider(thickness: 2.0),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'WI-1:',
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_ice_climbing_wi1),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'WI-2:',
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_ice_climbing_wi2),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'WI-3:',
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_ice_climbing_wi3),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'WI-4:',
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_ice_climbing_wi4),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'WI-5:',
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_ice_climbing_wi5),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'WI-6:',
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_ice_climbing_wi6),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'WI-7:',
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_ice_climbing_wi7),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          'WI-8:',
          style: Theme.of(context).textTheme.titleMedium?.copyWith(color: AppColors.accentColor),
        ),
      ),
      Text(AppLocalizations.of(context)!.info_dialog_ice_climbing_wi8),
    ];
  }

  Widget _getExistingWarningAscendListing(BuildContext context, List<Ascend> existingAscents) {
    String languageCode = Localizations.localeOf(context).languageCode;
    List<Widget> children = [];

    for (Ascend ascend in existingAscents) {
      children.add(
        Text(
          "${DateFormat.yMMMd(languageCode).format(ascend.dateTime!)}:",
          style: Theme.of(context).textTheme.bodyMedium?.copyWith(fontWeight: FontWeight.bold),
        ),
      );
      children.add(const SizedBox(width: 8.0));
      /*
      route name -> unnecessary because always same
      children.add(

        Padding(
          padding: const EdgeInsets.only(left: 12.0),
          child: Text(
            Util.getAscendNameWithoutPrefix(
              ascend.name,
              context,
              speedType: ascend.speedType,
              type: ascend.type,
            ),
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.bodyMedium,
          ),
        ),
      );
       */
      children.add(
        Padding(
          padding: const EdgeInsets.only(left: 12.0),
          child: Text(
            ascend.type.hasLeadAndToprope(speedType: ascend.speedType)
                ? "${Util.getTopOrLead(ascend.toprope, context, ascend.type.shouldRenameTopropeToFollowing())} | "
                    "${Util.getStyle(ascend.styleID, context, tries: ascend.tries)}"
                : Util.getStyle(ascend.styleID, context, tries: ascend.tries),
            overflow: TextOverflow.clip,
            style: Theme.of(context).textTheme.bodyMedium,
          ),
        ),
      );

      children.add(const SizedBox(height: 10.0));
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: children,
    );
  }
}
