import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/ui/pages/settings/donation_page.dart';
import 'package:climbing_track/ui/util/custom_page_route.dart';
import 'package:climbing_track/view_model/settings_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:climbing_track/app/climbing_track_app.dart';

class RateMyAppDialog extends StatefulWidget {
  const RateMyAppDialog({super.key});

  @override
  State<RateMyAppDialog> createState() => _RateMyAppDialogState();
}

class _RateMyAppDialogState extends State<RateMyAppDialog> {
  RateMyAppState state = RateMyAppState.ENTRY;
  final TextEditingController _textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        title(),
        Flexible(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: content(),
          ),
        ),
        const SizedBox(height: 8.0),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
          child: OverflowBar(
            alignment: MainAxisAlignment.end,
            spacing: 12.0,
            overflowAlignment: OverflowBarAlignment.end,
            overflowDirection: VerticalDirection.down,
            overflowSpacing: 0,
            children: actions(),
          ),
        ),
      ],
    );
  }

  Widget title() {
    String text;
    switch (state) {
      case RateMyAppState.ENTRY:
      case RateMyAppState.GOOD_REVIEW:
      case RateMyAppState.GOOD_REVIEW_DONATE:
      case RateMyAppState.GOOD_REVIEW_RATE:
        text = AppLocalizations.of(context)!.feedback_dialog_enjoying_the_app;
        break;
      case RateMyAppState.BAD_REVIEW:
        text = AppLocalizations.of(context)!.feedback_dialog_how_to_improve;
        break;
    }

    return Padding(
      padding: const EdgeInsets.all(24.0),
      child: Text(
        text,
        style: Theme.of(context).textTheme.titleMedium,
      ),
    );
  }

  Widget content() {
    switch (state) {
      case RateMyAppState.ENTRY:
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            imageContainer(
              imagePath: "assets/images/sad-face.png",
              onPressed: () => setState(() => state = RateMyAppState.BAD_REVIEW),
            ),
            imageContainer(
              imagePath: "assets/images/happy-face.png",
              onPressed: () => setState(() => state = RateMyAppState.GOOD_REVIEW),
            ),
          ],
        );
      case RateMyAppState.GOOD_REVIEW:
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            imageContainer(
              imagePath: "assets/images/comment.png",
              sizeMultiplier: 0.14,
              onPressed: () async {
                final InAppReview inAppReview = InAppReview.instance;
                if (await inAppReview.isAvailable()) {
                  inAppReview.requestReview();
                }
                setState(() => state = RateMyAppState.GOOD_REVIEW_DONATE);
              },
              text: "${AppLocalizations.of(context)!.screen_about_feedback_button_rate}!",
              border: false,
            ),
            imageContainer(
              imagePath: "assets/images/donation.png",
              sizeMultiplier: 0.14,
              onPressed: () {
                Navigator.pop(context);
                setState(() => state = RateMyAppState.GOOD_REVIEW_RATE);
                doNotShowAgain();
                Navigator.push(
                  context,
                  CustomPageRoute.build(builder: (BuildContext context) => const DonationPage()),
                );
              },
              text: "${AppLocalizations.of(context)!.screen_about_support_button}!",
              border: false,
            ),
          ],
        );
      case RateMyAppState.GOOD_REVIEW_DONATE:
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            imageContainer(
              imagePath: "assets/images/donation.png",
              sizeMultiplier: 0.14,
              onPressed: () {
                Navigator.pop(context);
                doNotShowAgain();
                Navigator.push(
                  context,
                  CustomPageRoute.build(builder: (BuildContext context) => const DonationPage()),
                );
              },
              text: "${AppLocalizations.of(context)!.screen_about_support_button}!",
              border: false,
            ),
          ],
        );
      case RateMyAppState.GOOD_REVIEW_RATE:
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            imageContainer(
              imagePath: "assets/images/comment.png",
              sizeMultiplier: 0.14,
              onPressed: () async {
                final InAppReview inAppReview = InAppReview.instance;
                if (await inAppReview.isAvailable()) {
                  inAppReview.requestReview();
                }
                doNotShowAgain();
                if (mounted) Navigator.pop(context);
              },
              text: "${AppLocalizations.of(context)!.screen_about_feedback_button_rate}!",
              border: false,
            ),
          ],
        );
      case RateMyAppState.BAD_REVIEW:
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.asset(
                    "assets/images/writing.png",
                    width: MediaQuery.of(context).size.width * 0.12,
                    height: MediaQuery.of(context).size.width * 0.12,
                  ),
                  const SizedBox(width: 12.0),
                  Flexible(
                    child: Text(AppLocalizations.of(context)!.feedback_dialog_feedback_info),
                  ),
                ],
              ),
              const SizedBox(height: 20.0),
              Flexible(
                child: Theme(
                  data: ThemeData.from(
                    colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor),
                    useMaterial3: false,
                  ),
                  child: TextFormField(
                    maxLines: null,
                    minLines: 6,
                    textAlign: TextAlign.start,
                    textAlignVertical: TextAlignVertical.center,
                    controller: _textEditingController,
                    decoration: InputDecoration(
                      isDense: true,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(16.0),
                        borderSide: const BorderSide(),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
    }
  }

  List<Widget> actions() {
    switch (state) {
      case RateMyAppState.ENTRY:
        return [
          TextButton(
            onPressed: () async {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              int counter = prefs.getInt(ClimbingTrackApp.FEEDBACK_DISMISS_COUNTER) ?? 0;
              counter++;
              prefs.setInt(ClimbingTrackApp.FEEDBACK_DISMISS_COUNTER, counter);
              if (mounted) Navigator.pop(context);
            },
            child: Text(AppLocalizations.of(context)!.general_dismiss),
          ),
        ];
      case RateMyAppState.GOOD_REVIEW:
      case RateMyAppState.GOOD_REVIEW_DONATE:
      case RateMyAppState.GOOD_REVIEW_RATE:
        return [
          TextButton(
            onPressed: () {
              doNotShowAgain();
              Navigator.pop(context);
            },
            child: Text(AppLocalizations.of(context)!.general_do_not_show_again),
          ),
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: Text(AppLocalizations.of(context)!.remind_me_later),
          ),
        ];
      case RateMyAppState.BAD_REVIEW:
        return [
          TextButton(
            onPressed: () {
              doNotShowAgain();
              Navigator.pop(context);
            },
            child: Text(AppLocalizations.of(context)!.general_do_not_show_again),
          ),
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: Text(AppLocalizations.of(context)!.remind_me_later),
          ),
          TextButton(
            onPressed: () {
              if (_textEditingController.text.isEmpty) {
                ScaffoldMessenger.of(context)
                  ..removeCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(
                      behavior: SnackBarBehavior.floating,
                      content: Text(AppLocalizations.of(context)!.feedback_dialog_feedback_info_snackbar),
                      duration: const Duration(seconds: 2),
                    ),
                  );
                return;
              }
              SettingsViewModel.sendFeedBack(feedback: _textEditingController.text.trim());
              doNotShowAgain();
              Navigator.pop(context);
            },
            child: Text(AppLocalizations.of(context)!.feedback_dialog_submit),
          ),
        ];
    }
  }

  Widget imageContainer({
    required String imagePath,
    String? text,
    Function()? onPressed,
    double sizeMultiplier = 0.12,
    bool border = true,
  }) {
    return TextButton(
      onPressed: onPressed,
      style: IconButton.styleFrom(
        shape: RoundedRectangleBorder(
          side: BorderSide(color: border ? Colors.black26 : Colors.transparent),
          borderRadius: BorderRadius.circular(24),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            imagePath,
            width: MediaQuery.of(context).size.width * sizeMultiplier,
            height: MediaQuery.of(context).size.width * sizeMultiplier,
          ),
          if (text != null) const SizedBox(height: 20.0),
          if (text != null) Text(text, style: Theme.of(context).textTheme.bodyMedium),
          if (text != null) const SizedBox(height: 4.0),
        ],
      ),
    );
  }

  Future<void> doNotShowAgain() async {
    // TODO only if really rated or donated
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(ClimbingTrackApp.NOT_SHOW_RATE_DIALOG, true);
  }
}

enum RateMyAppState {
  ENTRY,
  GOOD_REVIEW,
  GOOD_REVIEW_DONATE,
  GOOD_REVIEW_RATE,
  BAD_REVIEW,
}
