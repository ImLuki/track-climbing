import 'dart:math';

import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/location.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:climbing_track/ui/dialog/ascend_info_widget.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class StartSessionInfoDialog {
  final Location location;

  StartSessionInfoDialog({required this.location});

  Future<bool?> showInfoDialog(BuildContext context) async {
    return showDialog<bool?>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          insetPadding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 36.0),
          contentPadding: const EdgeInsets.symmetric(vertical: 12.0),
          scrollable: false,
          content: StartSessionDialogContent(location: location),
        );
      },
    );
  }
}

class StartSessionDialogContent extends StatefulWidget {
  final Location? location;

  const StartSessionDialogContent({super.key, this.location});

  @override
  State<StartSessionDialogContent> createState() => _StartSessionDialogContentState();
}

class _StartSessionDialogContentState extends State<StartSessionDialogContent> {
  final PageController _controller = PageController(initialPage: 0);

  List<Ascend> reminder = [];
  int elements = 0;
  int currentPage = 0;
  bool hasLocationComment = false;

  @override
  void initState() {
    super.initState();
    hasLocationComment = widget.location?.locationId != null && widget.location!.locationComment.isNotEmpty;
    reminder.clear();
    reminder.addAll(DataStore().ascendReminderDataStore.getAscends(locationId: widget.location?.locationId));
    elements = reminder.length;
    if (hasLocationComment) elements++;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(24.0, 12.0, 24.0, 0.0),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.75,
      child: Column(
        children: [
          Flexible(
            child: PageView(
              onPageChanged: (index) => setState(() {
                currentPage = index;
              }),
              controller: _controller,
              children: List.generate(
                elements,
                (index) => _content(index),
              ),
            ),
          ),
          const SizedBox(height: 24.0),
          _bottomRow(),
        ],
      ),
    );
  }

  Widget _content(int index) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        ..._title(),
        Flexible(
          child: ShaderMask(
            shaderCallback: (Rect rect) {
              return const LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: <Color>[Colors.black, Colors.transparent, Colors.transparent, Colors.black],
                stops: [0.0, 0.04, 0.95, 1.0],
              ).createShader(rect);
            },
            blendMode: BlendMode.dstOut,
            child: ListView(
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 8.0),
              if (hasLocationComment && index == 0) _locationInfo(),
              if ((hasLocationComment && index != 0) || !hasLocationComment) _ascendInfo(index),
              const SizedBox(height: 12.0),
            ],
          ),
          ),
        ),
        if (elements > 1) ...[
          const SizedBox(height: 12.0),
          Align(
            alignment: Alignment.centerRight,
            child: TextButton(
              onPressed: _doNotShowAgain,
              child: Text(AppLocalizations.of(context)!.general_do_not_show_again),
            ),
          ),
        ],
      ],
    );
  }

  Widget _ascendInfo(int index) {
    Ascend ascend = hasLocationComment ? reminder[index - 1] : reminder[index];
    ascend.location = widget.location!.name;
    return AscendInfoWidget(
      ascend: ascend,
      textStyle: Theme.of(context).textTheme.bodyLarge,
    );
  }

  Widget _locationInfo() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          AppLocalizations.of(context)!.location_comment_title,
          overflow: TextOverflow.clip,
          style: Theme.of(context).textTheme.headlineSmall,
        ),
        const SizedBox(height: Constants.STANDARD_PADDING),
        Flexible(
          child: Scrollbar(
            child: ShaderMask(
              shaderCallback: (Rect rect) {
                return const LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.transparent, Colors.black],
                  stops: [0.95, 1.0],
                ).createShader(rect);
              },
              blendMode: BlendMode.dstOut,
              child: SingleChildScrollView(
                padding: const EdgeInsets.only(left: 12.0),
                child: Text(widget.location!.locationComment),
              ),
            ),
          ),
        ),
      ],
    );
  }

  List<Widget> _title() {
    return [
      Align(
        alignment: Alignment.center,
        child: Text(
          (hasLocationComment && currentPage == 0)
              ? AppLocalizations.of(context)!.general_hints
              : AppLocalizations.of(context)!.general_reminder,
          style: Theme.of(context).textTheme.headlineLarge,
          textAlign: TextAlign.center,
        ),
      ),
      const SizedBox(height: 12.0),
      const Divider(),
      const SizedBox(height: 10.0),
    ];
  }

  Widget _bottomRow() {
    if (elements <= 1) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          TextButton(
            onPressed: _doNotShowAgain,
            child: Text(AppLocalizations.of(context)!.general_do_not_show_again),
          ),
          TextButton(
            onPressed: () {
              FocusScope.of(context).unfocus();
              Navigator.of(context).pop();
            },
            child: Text(AppLocalizations.of(context)!.general_dismiss),
          ),
        ],
      );
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Visibility(
          visible: currentPage != elements - 1,
          maintainSize: true,
          maintainState: true,
          maintainAnimation: true,
          child: IconButton(
            onPressed: Navigator.of(context).pop,
            icon: const Icon(Icons.close),
          ),
        ),
        const SizedBox(width: 8.0),
        Flexible(
          child: ShaderMask(
            shaderCallback: (Rect rect) {
              return const LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [Colors.black, Colors.transparent, Colors.transparent, Colors.black],
                stops: [0.0, 0.05, 0.95, 1.0],
              ).createShader(rect);
            },
            blendMode: BlendMode.dstOut,
            child: SingleChildScrollView(
              padding: EdgeInsets.zero,
              scrollDirection: Axis.horizontal,
              child: DotsIndicator(
                onTap: (int index) => setState(() {
                  currentPage = index;
                  _controller.animateToPage(
                    currentPage,
                    curve: Curves.easeIn,
                    duration: const Duration(milliseconds: 200),
                  );
                }),
                dotsCount: elements,
                position: currentPage,
                decorator: DotsDecorator(
                  size: const Size.square(9.0),
                  activeSize: const Size(18.0, 9.0),
                  activeShape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(width: 8.0),
        IconButton(
          onPressed: _nextElement,
          icon: Icon(currentPage != elements - 1 ? Icons.east : Icons.close),
        ),
      ],
    );
  }

  void _doNotShowAgain() {
    if (hasLocationComment) {
      if (currentPage == 0) {
        hasLocationComment = false;
        DataStore().settingsDataStore.doNotShowLocationInfoAgain =
            DataStore().settingsDataStore.doNotShowLocationInfoAgain..add(widget.location!.locationId!);
      } else {
        DataStore().ascendReminderDataStore.deleteEntry(ascendId: reminder[currentPage - 1].ascendId);
        reminder.removeAt(currentPage - 1);
      }
    } else {
      DataStore().ascendReminderDataStore.deleteEntry(ascendId: reminder[currentPage].ascendId);
      reminder.removeAt(currentPage);
    }

    elements--;
    currentPage = min(currentPage, elements - 1);
    FocusScope.of(context).unfocus();
    if (currentPage >= elements || (!hasLocationComment && elements <= 0)) {
      Navigator.of(context).pop();
      return;
    }
    setState(() {});
  }

  void _nextElement() {
    currentPage++;
    if (currentPage >= elements) {
      Navigator.of(context).pop();
      return;
    }
    _controller.animateToPage(currentPage, curve: Curves.easeIn, duration: const Duration(milliseconds: 200));
  }
}
