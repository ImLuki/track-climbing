import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/ui/widgets/edit_ascend/edit_ascend_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AscendEditPage extends StatelessWidget {
  final Ascend ascend;
  final int sessionID;
  final String title;
  final Function(Ascend)? onChanged;

  final bool updateDatabase;
  final bool hasSaveButton;
  final bool hasAppBarSaveButton;

  const AscendEditPage({
    super.key,
    required this.ascend,
    required this.sessionID,
    required this.title,
    this.onChanged,
    this.updateDatabase = true,
    this.hasSaveButton = true,
    this.hasAppBarSaveButton = false,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        leading: hasSaveButton || hasAppBarSaveButton
            ? IconButton(
                onPressed: () => Navigator.of(context).pop(false),
                icon: const Icon(Icons.close),
                tooltip: AppLocalizations.of(context)!.general_cancel,
              )
            : null,
        actions: [
          if (hasAppBarSaveButton)
            IconButton(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              tooltip: AppLocalizations.of(context)!.general_save,
              icon: const Icon(Icons.done),
              onPressed: () => _onSave(context),
            )
        ],
      ),
      floatingActionButton: hasSaveButton ? _floatingActionButton(context) : null,
      body: GestureDetector(
        onVerticalDragUpdate: (dragDetails) {
          FocusScope.of(context).unfocus();
        },
        onTapDown: (tapDownDetails) {
          FocusScope.of(context).unfocus();
        },
        child: SingleChildScrollView(
          child: EditAscendWidget(ascend: ascend),
        ),
      ),
    );
  }

  Future<void> updateAscendInDatabase(Ascend data) async {
    if (!updateDatabase) return;
    DBController dbController = DBController();
    await dbController.updateAscend(data.toMap(sessionID), data.ascendId);
  }

  Widget _floatingActionButton(BuildContext context) {
    return FloatingActionButton(
      tooltip: AppLocalizations.of(context)!.general_save,
      backgroundColor: Colors.black,
      onPressed: () => _onSave(context),
      child: const Icon(
        Icons.save,
        size: 32.0,
        color: Colors.white,
      ),
    );
  }

  Future<void> _onSave(BuildContext context) async {
    await updateAscendInDatabase(ascend);
    onChanged?.call(ascend);
    if (context.mounted) Navigator.of(context).pop(true);
  }
}
