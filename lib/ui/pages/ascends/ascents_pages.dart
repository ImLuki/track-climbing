import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/data_store/search_settings.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:climbing_track/backend/model/active_session.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/util/custom_date_formatter.dart';
import 'package:climbing_track/ui/pages/ascends/expandable_ascend.dart';
import 'package:climbing_track/ui/pages/ascends/search_settings_page.dart';
import 'package:climbing_track/ui/util/custom_page_route.dart';
import 'package:climbing_track/ui/widgets/empty_list_placeholder.dart';
import 'package:climbing_track/ui/widgets/search_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:showcaseview/showcaseview.dart';

class AscendsPage extends StatefulWidget {
  const AscendsPage({super.key});

  @override
  _AscendsPageState createState() => _AscendsPageState();
}

class _AscendsPageState extends State<AscendsPage> {
  static const String PLACEHOLDER_IMAGE_NAME = 'assets/images/3407006.png';
  static const String PLACEHOLDER_IMAGE_ROUTES = 'assets/images/2640193.png';

  final DBController dbController = DBController();
  final ItemScrollController _scrollController = ItemScrollController();
  final GlobalKey _globalKey = GlobalKey();
  final SearchSettingsDataStore searchSettings = DataStore().searchSettingsDataStore;

  late Future<List> data;

  int _expandedIndex = -1;
  String _searchString = "";

  @override
  void initState() {
    super.initState();
    data = fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            Flexible(
              child: SearchAppBar(
                title: AppLocalizations.of(context)!.ascent_page_ascent_list.toUpperCase(),
                hintText: AppLocalizations.of(context)!.ascent_page_search_info,
                onPressedSettings: () {
                  Navigator.push(
                    context,
                    CustomPageRoute.build(
                      builder: (BuildContext context) => const SearchSettingsPage(),
                    ),
                  ).then(
                    (value) => setState(() {
                      data = fetchData();
                    }),
                  );
                },
                onSearchStringChanged: (value) {
                  setState(() => _searchString = value.trim().toLowerCase());
                },
              ),
            ),
            _dropDownSearchOrder(),
          ],
        ),
      ),
      body: GestureDetector(
        onVerticalDragUpdate: (dragDetails) {
          FocusScope.of(context).unfocus();
        },
        onTapDown: (tapDownDetails) {
          FocusScope.of(context).unfocus();
        },
        child: Center(
          child: createListView(),
        ),
      ),
    );
  }

  Widget createListView() {
    return FutureBuilder(
      future: data,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Align(
            alignment: Alignment.topCenter,
            child: PreferredSize(
              preferredSize: Size.fromHeight(6.0),
              child: LinearProgressIndicator(),
            ),
          );
        }

        List<Map<String, dynamic>> filteredData = [];
        Map<String, List<dynamic>>? sameAscents;
        List<Map<String, dynamic>> allAscents = [];
        if (snapshot.data != null) {
          allAscents = snapshot.data[0];
          sameAscents = snapshot.data[1];

          filteredData = allAscents
              .where((item) => item[DBController.ascendName].toString().toLowerCase().contains(_searchString))
              .toList();
        }

        if (filteredData.length > 2) {
          WidgetsBinding.instance.addPostFrameCallback((_) => _showToolTip());
        }

        return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (searchSettings.hasCustomSetting()) _settingsBar(),
            if (searchSettings.hasCustomSetting()) const Divider(height: 0.0),
            filteredData.isEmpty
                ? Expanded(
                    child: Column(
                      children: [
                        const Spacer(),
                        allAscents.isNotEmpty
                            ? EmptyListPlaceHolder(
                                text: AppLocalizations.of(context)!.ascent_page_placeholder_name,
                                image: PLACEHOLDER_IMAGE_NAME,
                              )
                            : EmptyListPlaceHolder(
                                text: AppLocalizations.of(context)!.ascent_page_placeholder_routes,
                                image: PLACEHOLDER_IMAGE_ROUTES,
                              ),
                        const Spacer(),
                      ],
                    ),
                  )
                : Flexible(
                    child: _createAscents(filteredData, sameAscents),
                  ),
          ],
        );
      },
    );
  }

  Widget _createAscents(List<Map<String, dynamic>> data, Map<String, List<dynamic>>? sameAscents) {
    return Scrollbar(
      child: ScrollablePositionedList.separated(
        itemScrollController: _scrollController,
        itemCount: data.length,
        itemBuilder: (context, index) {
          return index == 0
              ? Showcase(
                  key: _globalKey,
                  description: AppLocalizations.of(context)!.ascend_page_tooltip,
                  targetBorderRadius: const BorderRadius.all(Radius.circular(12)),
                  tooltipPadding: const EdgeInsets.all(12.0),
                  child: ExpandableAscend(
                    key: Key(data[index][DBController.ascendId].toString()),
                    ascend: Ascend.fromMap(data[index]),
                    refresh: refresh,
                    index: index,
                    expandedIndex: _expandedIndex,
                    sessionId: data[index][DBController.sessionId],
                    sameAscents: sameAscents,
                  ),
                )
              : ExpandableAscend(
                  key: Key(data[index][DBController.ascendId].toString()),
                  ascend: Ascend.fromMap(data[index]),
                  refresh: refresh,
                  index: index,
                  expandedIndex: _expandedIndex,
                  sessionId: data[index][DBController.sessionId],
                  sameAscents: sameAscents,
                );
        },
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 10.0),
        separatorBuilder: (BuildContext context, int index) => const Divider(
          thickness: 2.0,
          height: 2.0,
        ),
      ),
    );
  }

  void refresh(int index, int expandedIndex) async {
    setState(() {
      data = fetchData();
      _expandedIndex = expandedIndex;
    });

    // hack because we have to wait for setState
    await Future.delayed(const Duration(milliseconds: 100));
    _scrollController.jumpTo(index: index);
  }

  Future<List> fetchData() async {
    List<Map<String, dynamic>> data = await dbController.rawQuery("SELECT * FROM ${DBController.ascendsTable} "
        "JOIN ${DBController.sessionsTable} using (${DBController.sessionId}) "
        "JOIN ${DBController.locationsTable} using (${DBController.locationId})");

    Map<String, List<dynamic>>? allAscentsMap;
    if (searchSettings.mapByName) {
      dynamic results = searchSettings.applyMapByName(data);
      data = results[0];
      allAscentsMap = results[2];
    }

    data = searchSettings.filterData(data);
    searchSettings.sortData(data);
    return [data, allAscentsMap];
  }

  void _showToolTip() async {
    if (!DataStore().toolTipsDataStore.tooltipAscendPage) return;

    await Future.delayed(const Duration(milliseconds: 750));

    if (mounted) {
      DataStore().toolTipsDataStore.tooltipAscendPage = false;
      ShowCaseWidget.of(context).startShowCase([_globalKey]);
    }
  }

  Widget _settingsBar() {
    return SizedBox(
      height: 60.0,
      child: Row(
        children: [
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  const SizedBox(width: 9.0),
                  if (searchSettings.indoor && !searchSettings.outdoor)
                    _chipItem(
                      text: AppLocalizations.of(context)!.general_indoor,
                      activated: searchSettings.indoor,
                      onTap: () {
                        searchSettings.indoor = true;
                        searchSettings.outdoor = true;
                      },
                    ),
                  if (!searchSettings.indoor && searchSettings.outdoor)
                    _chipItem(
                      text: AppLocalizations.of(context)!.general_outdoor,
                      activated: searchSettings.outdoor,
                      onTap: () {
                        searchSettings.indoor = true;
                        searchSettings.outdoor = true;
                      },
                    ),
                  if (searchSettings.currentLocation != SearchSettingsDataStore.ALL_LOCATIONS_KEY)
                    _chipItem(
                      text: searchSettings.currentLocation,
                      activated: true,
                      onTap: () => searchSettings.currentLocation = SearchSettingsDataStore.ALL_LOCATIONS_KEY,
                    ),
                  if (searchSettings.onlyCurrentLocation && ActiveSession().activeSession != null)
                    _chipItem(
                      text: AppLocalizations.of(context)!.search_settings_current_location_only,
                      activated: true,
                      onTap: () => searchSettings.onlyCurrentLocation = false,
                    ),
                  if (searchSettings.mapByName)
                    _chipItem(
                      text: AppLocalizations.of(context)!.search_settings_map_by_name,
                      activated: true,
                      onTap: () => searchSettings.mapByName = false,
                    ),
                  if (searchSettings.onlyNotAtLeastRedpoint)
                    _chipItem(
                      text: AppLocalizations.of(context)!.search_settings_not_at_least_redpoint_only,
                      activated: true,
                      onTap: () => searchSettings.onlyNotAtLeastRedpoint = false,
                    ),
                  if (searchSettings.onlyNotFinished)
                    _chipItem(
                      text: AppLocalizations.of(context)!.search_settings_not_finished_only,
                      activated: true,
                      onTap: () => searchSettings.onlyNotFinished = false,
                    ),
                  if (searchSettings.leadTopropeSelection != -1)
                    _chipItem(
                      text:
                          SearchSettingsDataStore.climbingMethodNames(context)[searchSettings.leadTopropeSelection + 1],
                      activated: true,
                      onTap: () => searchSettings.leadTopropeSelection = -1,
                    ),
                  if (searchSettings.onlyMarked)
                    _chipItem(
                      text: AppLocalizations.of(context)!.search_settings_marked_only,
                      activated: true,
                      onTap: () => searchSettings.onlyMarked = false,
                    ),
                  if (searchSettings.onlyWithName)
                    _chipItem(
                      text: AppLocalizations.of(context)!.search_settings_no_name,
                      activated: true,
                      onTap: () => searchSettings.onlyWithName = false,
                    ),
                  if (searchSettings.type != SearchSettingsDataStore.ALL_TYPES_KEY)
                    _chipItem(
                      text: ClimbingType.values.byName(searchSettings.type).toName(context),
                      activated: true,
                      onTap: () => searchSettings.type = SearchSettingsDataStore.ALL_TYPES_KEY,
                    ),
                  if (searchSettings.climbingStyle != SearchSettingsDataStore.ALL_STYLES_KEY)
                    _chipItem(
                      text: ClimbingStyle.values.byName(searchSettings.climbingStyle).toTranslatedString(context),
                      activated: true,
                      onTap: () => searchSettings.climbingStyle = SearchSettingsDataStore.ALL_STYLES_KEY,
                    ),
                  if (searchSettings.dateTime != null)
                    _chipItem(
                      text: (searchSettings.isBeforeDate ? '< ' : '> ') +
                          CustomDateFormatter.formatDateString(context, searchSettings.dateTime!),
                      activated: true,
                      onTap: () => searchSettings.dateTime = null,
                    ),
                  const SizedBox(width: 9.0),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _chipItem({required String text, bool activated = false, Icon? icon, Function()? onTap}) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 3.0),
      decoration: BoxDecoration(
        color: activated ? Colors.amber[800]!.withOpacity(0.1) : Colors.transparent,
        borderRadius: BorderRadius.circular(12.0),
        border: Border.all(
          width: 1.0,
          color: activated ? Colors.amber[800]! : Colors.black,
        ),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(11.0),
          onTap: () {
            onTap?.call();
            data = fetchData();
            setState(() {});
          },
          child: Padding(
            padding: EdgeInsets.only(left: activated ? 8.0 : 12.0, right: 12.0, top: 8.0, bottom: 8.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (activated) const Icon(Icons.close, size: 18, color: Colors.black),
                if (activated) const SizedBox(width: 4.0),
                Text(text),
                if (icon != null) const SizedBox(width: 4.0),
                if (icon != null) icon,
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _dropDownSearchOrder() {
    return PopupMenuButton<Sorting>(
      color: Theme.of(context).colorScheme.surface,
      icon: const Icon(Icons.sort),
      onSelected: (value) => setState(() {
        data = fetchData();
        searchSettings.currentSorting = value;
      }),
      itemBuilder: (BuildContext context) => List.generate(
        Sorting.values.length,
        (index) => PopupMenuItem(
          value: Sorting.values[index],
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(
                  Sorting.values[index].toName(context),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
              ),
              const SizedBox(width: 20.0),
              Icon(
                Icons.check,
                color: Sorting.values[index] == searchSettings.currentSorting ? Colors.black : Colors.transparent,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
