import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/data_store/search_settings.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/enums/ascend_characteristics.dart';
import 'package:climbing_track/backend/model/enums/steepness.dart';
import 'package:climbing_track/backend/util/custom_date_formatter.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/dialog/info_dialogs.dart';
import 'package:climbing_track/ui/pages/ascends/ascend_edit_page.dart';
import 'package:climbing_track/ui/util/custom_page_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ExpandableAscend extends StatefulWidget {
  final Ascend ascend;
  final int sessionId;
  final Function(int, int) refresh;
  final int index;
  final int expandedIndex;
  final Map<String, List<dynamic>>? sameAscents;

  const ExpandableAscend({
    super.key,
    required this.ascend,
    required this.refresh,
    required this.index,
    required this.expandedIndex,
    required this.sessionId,
    this.sameAscents,
  });

  @override
  _ExpandableAscendState createState() => _ExpandableAscendState(ascend);
}

class _ExpandableAscendState extends State<ExpandableAscend> {
  bool expanded = false;
  Ascend _ascend;

  _ExpandableAscendState(this._ascend);

  @override
  void initState() {
    super.initState();
    expanded = widget.expandedIndex == widget.index;
  }

  @override
  Widget build(BuildContext context) {
    return getDataItemWidget();
  }

  Widget getDataItemWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2.0),
      child: InkWell(
        onTap: () => setState(() => expanded = !expanded),
        borderRadius: BorderRadius.circular(18.0),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 10.0),
          child: AnimatedSize(
            duration: const Duration(milliseconds: 200),
            child: expanded ? _expandedWidget() : _collapsedWidget(),
          ),
        ),
      ),
    );
  }

  Widget _expandedWidget() {
    String k = SearchSettingsDataStore.toKeyV(widget.ascend.location ?? "", widget.ascend.name, widget.ascend.toprope);
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 4.0),
        _name(
          maxLines: 2,
          style: Theme.of(context).textTheme.titleMedium!.copyWith(fontSize: 22.0),
        ),
        if (widget.sameAscents != null &&
            widget.ascend.name != "" &&
            widget.sameAscents!.containsKey(k) &&
            widget.sameAscents![k]!.length > 1) ...[
          const SizedBox(height: 16.0),
          _createExistingAscentsInfo(),
        ],
        const SizedBox(height: 20),
        ..._dateAndLocation(),
        if (_ascend.type == ClimbingType.SPEED_CLIMBING) ..._speed(),
        _info(),
        const SizedBox(height: 10),
        ..._height(),
        Row(
          children: [
            const Icon(Icons.stars),
            const SizedBox(width: 20),
            Util.getRating(_ascend.rating),
          ],
        ),
        const SizedBox(height: 10),
        _comment(),
        const SizedBox(height: 20),
        _buttons(),
      ],
    );
  }

  Widget _collapsedWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          fit: FlexFit.tight,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _name(
                maxLines: 1,
                style: Theme.of(context).textTheme.titleMedium!,
              ),
              const SizedBox(height: 4.0),
              Text(
                Util.getLocationDateString(
                  context,
                  _ascend.dateTime!,
                  _ascend.location!,
                ),
                style: Theme.of(context).textTheme.bodyMedium,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
            ],
          ),
        ),
        const SizedBox(width: 14.0),
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              Util.getAscendSummary(_ascend, context),
              style: Theme.of(context).textTheme.bodyMedium,
            ),
            const SizedBox(height: 4.0),
            Util.getRating(_ascend.rating),
          ],
        )
      ],
    );
  }

  void toggleMarked() {
    setState(() {
      _ascend.marked = !_ascend.marked;
    });
    final DBController dbController = DBController();
    dbController.updateAscend({DBController.ascendMarked: _ascend.marked ? 1 : 0}, _ascend.ascendId);
  }

  List<Widget> _numberOfSameAscentsWidget() {
    String k = SearchSettingsDataStore.toKeyV(widget.ascend.location ?? "", widget.ascend.name, widget.ascend.toprope);

    if (widget.sameAscents == null ||
        widget.ascend.name == "" ||
        !widget.sameAscents!.containsKey(k) ||
        widget.sameAscents![k]!.length <= 1) {
      return [];
    }

    return [
      Container(
        padding: const EdgeInsets.all(3.0),
        decoration: const BoxDecoration(
          color: Colors.black26,
          borderRadius: BorderRadius.all(Radius.circular(100)),
        ),
        child: Text(
          "+${widget.sameAscents![k]!.length - 1}",
          style: const TextStyle(letterSpacing: -0.5),
        ),
      ),
      const SizedBox(width: 6),
    ];
  }

  Widget _name({required int maxLines, TextStyle? style}) {
    return Row(
      children: [
        ..._numberOfSameAscentsWidget(),
        Visibility(visible: _ascend.marked, child: const Icon(Icons.favorite, color: Colors.red)),
        Visibility(visible: _ascend.marked, child: const SizedBox(width: 4)),
        Flexible(
          child: Text(
            Util.getAscendName(
              context,
              _ascend.name,
              type: _ascend.type,
              speedType: _ascend.speedType,
            ),
            style: style,
            overflow: TextOverflow.ellipsis,
            maxLines: maxLines,
          ),
        ),
      ],
    );
  }

  List<Widget> _dateAndLocation() {
    return [
      Row(
        children: [
          const Icon(Icons.calendar_month),
          const SizedBox(width: 20),
          Text(
            CustomDateFormatter.formatDateString(context, _ascend.dateTime!),
            style: Theme.of(context).textTheme.bodyMedium,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
        ],
      ),
      const SizedBox(height: 10.0),
      Row(
        children: [
          const Icon(Icons.location_on_outlined),
          const SizedBox(width: 20),
          Text(
            Util.getLocation(_ascend.location!),
            style: Theme.of(context).textTheme.bodyMedium,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
        ],
      ),
      const SizedBox(height: 10),
    ];
  }

  Widget _info() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Icon(Icons.info_outline),
        const SizedBox(width: 20),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              _ascend.type.toTitle(context),
              style: Theme.of(context).textTheme.bodyMedium,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
            ),
            const SizedBox(height: 4.0),
            ..._grade(),
            ..._style(),
            _leadOrTop(),
            _climbingStyles(),
          ],
        ),
      ],
    );
  }

  List<Widget> _grade() {
    if (_ascend.type == ClimbingType.SPEED_CLIMBING && _ascend.speedType == 0) return [];
    return [
      Text(
        Util.getOriginalGradeAndCurrent(
          _ascend.gradeID,
          _ascend.type,
          gradingSystem: _ascend.originalGradeSystem,
        ),
        style: Theme.of(context).textTheme.bodyMedium,
        overflow: TextOverflow.clip,
      ),
      const SizedBox(height: 4.0),
    ];
  }

  List<Widget> _style() {
    if (_ascend.type == ClimbingType.FREE_SOLO || _ascend.type == ClimbingType.SPEED_CLIMBING) return [];
    return [
      Text(
        Util.getStyle(
          _ascend.styleID,
          context,
          tries: _ascend.tries,
        ),
        style: Theme.of(context).textTheme.bodyMedium,
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
      ),
      const SizedBox(height: 4.0),
    ];
  }

  Widget _leadOrTop() {
    return Visibility(
      visible: _ascend.type.hasLeadAndToprope(speedType: _ascend.speedType),
      child: Text(
        Util.getTopOrLead(_ascend.toprope, context, _ascend.type.shouldRenameTopropeToFollowing()),
        style: Theme.of(context).textTheme.bodyMedium,
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
      ),
    );
  }

  Widget _climbingStyles() {
    final List<AscendCharacteristics> routeStyleList = AscendCharacteristics.values
        .where((element) => AscendCharacteristics.hasFlag(element, _ascend.characteristic))
        .toList();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (routeStyleList.isNotEmpty) const SizedBox(height: 6.0),
        Wrap(
          spacing: 6.0,
          runSpacing: 6.0,
          children: List.generate(
            routeStyleList.length,
            (index) => routeStyleList[index].getIconWidget(context),
          ),
        ),
        if (routeStyleList.isNotEmpty) const SizedBox(height: 6.0),
      ],
    );
  }

  Widget _comment() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Icon(Icons.comment_outlined),
        const SizedBox(width: 20),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(width: 20),
              Text(
                Util.getComment(_ascend.comment),
                style: Theme.of(context).textTheme.bodyMedium,
                overflow: TextOverflow.clip,
              ),
            ],
          ),
        ),
      ],
    );
  }

  List<Widget> _speed() {
    return [
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Icon(Icons.timer_outlined),
          const SizedBox(width: 20),
          Text(
            "${(_ascend.speedTime / 1000).toStringAsFixed(2)} ${AppLocalizations.of(context)!.util_seconds}",
            style: Theme.of(context).textTheme.bodyMedium,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
        ],
      ),
      const SizedBox(height: 10),
    ];
  }

  List<Widget> _height() {
    final List<Steepness> steepnessList =
        Steepness.values.where((element) => Steepness.hasFlag(element, _ascend.steepness)).toList();
    return [
      Row(
        children: [
          const Icon(Icons.height),
          const SizedBox(width: 20),
          Text(
            Util.getHeightWithShortUnit(_ascend.height),
            style: Theme.of(context).textTheme.bodyMedium,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
          const SizedBox(width: 12.0),
          Wrap(
            spacing: 6.0,
            runSpacing: 6.0,
            children: List.generate(
              steepnessList.length,
              (index) => steepnessList[index].getIconWidget(context),
            ),
          ),
        ],
      ),
      const SizedBox(height: 10),
    ];
  }

  Widget _buttons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        _editButton(),
        _markButton(),
        _remindButton(),
      ],
    );
  }

  Widget _editButton() {
    return Tooltip(
      message: AppLocalizations.of(context)!.edit_ascend_tooltip,
      child: TextButton(
        style: TextButton.styleFrom(
          foregroundColor: Colors.black,
          backgroundColor: Colors.transparent,
        ),
        onPressed: _pushAscendEditPage,
        child: Column(
          children: <Widget>[
            const Icon(Icons.edit),
            const Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
            Text(AppLocalizations.of(context)!.general_edit, style: const TextStyle(color: Colors.black87)),
          ],
        ),
      ),
    );
  }

  Widget _markButton() {
    return Tooltip(
      message: AppLocalizations.of(context)!.mark_ascend_tooltip,
      child: TextButton(
        style: TextButton.styleFrom(
          foregroundColor: Colors.black,
          backgroundColor: Colors.transparent,
        ),
        onPressed: () => toggleMarked(),
        child: Column(
          children: <Widget>[
            Icon(
              _ascend.marked ? Icons.favorite : Icons.favorite_border,
              color: _ascend.marked ? Colors.red : Colors.black87,
            ),
            const Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
            Text(AppLocalizations.of(context)!.general_mark, style: const TextStyle(color: Colors.black87)),
          ],
        ),
      ),
    );
  }

  Widget _remindButton() {
    bool hasReminder = DataStore().ascendReminderDataStore.hasReminder(ascendId: _ascend.ascendId);
    return Tooltip(
      message: AppLocalizations.of(context)!.ascend_reminder_tooltip,
      child: TextButton(
        style: TextButton.styleFrom(
          foregroundColor: Colors.black,
          backgroundColor: Colors.transparent,
        ),
        onPressed: _toggleReminder,
        child: Column(
          children: <Widget>[
            Icon(
              hasReminder ? Icons.notifications_active : Icons.notifications_none,
              color: hasReminder ? Colors.amber.shade700 : Colors.black87,
            ),
            const Padding(padding: EdgeInsets.symmetric(vertical: 2.0)),
            Text(AppLocalizations.of(context)!.ascend_reminder_remind, style: const TextStyle(color: Colors.black87)),
          ],
        ),
      ),
    );
  }

  void _pushAscendEditPage() {
    Navigator.push(
      context,
      CustomPageRoute.build(
        builder: (BuildContext context) => AscendEditPage(
          ascend: _ascend.copy(),
          title: "${AppLocalizations.of(context)!.general_edit} ${_ascend.type.toTitle(context)}",
          onChanged: (Ascend newAscend) => setState(() {
            _ascend = newAscend;
          }),
          sessionID: widget.sessionId,
        ),
      ),
    );
  }

  void _toggleReminder() {
    DataStore().ascendReminderDataStore.toggleReminder(ascend: _ascend);

    setState(() {});
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(
            DataStore().ascendReminderDataStore.hasReminder(ascendId: _ascend.ascendId)
                ? AppLocalizations.of(context)!.ascend_reminder_add_reminder
                : AppLocalizations.of(context)!.ascend_reminder_remove_reminder,
          ),
        ),
      );
  }

  Widget _createExistingAscentsInfo() {
    String k = SearchSettingsDataStore.toKeyV(widget.ascend.location!, widget.ascend.name, widget.ascend.toprope);
    return Tooltip(
      message: AppLocalizations.of(context)!.further_ascents_tooltip,
      child: InkWell(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onTap: () {
          if (widget.sameAscents![k]!.isEmpty) return;

          widget.sameAscents![k]!.sort((b, a) => a[DBController.ascendOrder].compareTo(b[DBController.ascendOrder]));
          widget.sameAscents![k]!
              .sort((b, a) => a[DBController.sessionTimeStart].compareTo(b[DBController.sessionTimeStart]));

          InfoDialog().showExistingRoutesDialog(
            context: context,
            title: "${AppLocalizations.of(context)!.further_ascents_dialog}:\n${widget.ascend.name}",
            existingAscents: widget.sameAscents![k]!
                .where((element) => element[DBController.ascendId] != widget.ascend.ascendId)
                .map((e) => Ascend.fromMap(e))
                .toList(),
          );
        },
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            const Icon(Icons.info_outline, size: 32, color: Color(0xffefc775)),
            const SizedBox(width: 4.0),
            Expanded(
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                decoration: BoxDecoration(
                  color: Colors.orange.withOpacity(0.2),
                  borderRadius: const BorderRadius.all(Radius.circular(20)),
                ),
                child: RichText(
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                  text: TextSpan(
                    text: (widget.sameAscents![k]!.length - 1).toString(),
                    style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
                    children: <TextSpan>[
                      TextSpan(
                        text: AppLocalizations.of(context)!.further_ascends(widget.sameAscents![k]!.length - 1),
                        style: const TextStyle(fontWeight: FontWeight.normal),
                      )
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(width: 20.0),
          ],
        ),
      ),
    );
  }
}
