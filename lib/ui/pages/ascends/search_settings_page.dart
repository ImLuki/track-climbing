import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/data_store/search_settings.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:climbing_track/backend/util/custom_date_formatter.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SearchSettingsPage extends StatefulWidget {
  const SearchSettingsPage({super.key});

  @override
  _SearchSettingsPageState createState() => _SearchSettingsPageState();
}

class _SearchSettingsPageState extends State<SearchSettingsPage> {
  SearchSettingsDataStore settings = DataStore().searchSettingsDataStore;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context)!.general_search,
          // style: const TextStyle(color: Colors.white, fontSize: 22),
        ),
        actions: [
          IconButton(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            tooltip: AppLocalizations.of(context)!.general_reset,
            icon: const Icon(Icons.refresh),
            onPressed: () {
              settings.reset();
              setState(() {});
            },
          )
        ],
      ),
      body: Center(
        child: createSettings(),
      ),
    );
  }

  Widget createSettings() {
    return ListView(
      children: [
        _header(text: AppLocalizations.of(context)!.general_location),
        _settingsContainer([
          _indoorSetting(),
          _outdoorSetting(),
          _locationsSetting(),
          _currentLocationSetting(),
        ]),
        const Divider(thickness: 2.0),
        _header(text: AppLocalizations.of(context)!.style),
        _settingsContainer([
          _mapByName(),
          _redpointSetting(),
          _finishedSetting(),
          _leadTopRopeSelection(),
          _climbingStyle(),
        ]),
        const Divider(thickness: 2.0),
        _header(text: AppLocalizations.of(context)!.general_others),
        _settingsContainer(
          [
            _markedSetting(),
            _noNameSetting(),
            _typesSetting(),
            _sortingSetting(),
            _dateTime(),
          ],
        ),
        const SizedBox(height: 40.0),
      ],
    );
  }

  String getNameFromType(String name) {
    if (name == SearchSettingsDataStore.ALL_TYPES_KEY) return AppLocalizations.of(context)!.search_settings_all;

    return ClimbingType.values.byName(name).toName(context);
  }

  String getNameFromClimbingStyle(String name) {
    if (name == SearchSettingsDataStore.ALL_STYLES_KEY) return AppLocalizations.of(context)!.search_settings_all;

    return ClimbingStyle.values.byName(name).toTranslatedString(context, boulderExtra: true);
  }

  Widget _header({required String text}) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.only(
        left: Constants.STANDARD_PADDING,
        right: Constants.STANDARD_PADDING,
        top: 20.0,
        bottom: 10.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Text(
              text.toUpperCase(),
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.normal,
                letterSpacing: 1.5,
                color: Colors.red.shade700,
              ),
              overflow: TextOverflow.visible,
              maxLines: 1,
            ),
          ),
        ],
      ),
    );
  }

  Widget _settingsContainer(List<Widget> settings) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0 + Constants.STANDARD_PADDING),
      child: Column(
        children: settings,
      ),
    );
  }

  Widget _indoorSetting() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          AppLocalizations.of(context)!.general_indoor,
          style: Theme.of(context).textTheme.bodyLarge,
        ),
        Switch(
          activeColor: Colors.blue,
          value: settings.indoor,
          onChanged: (value) {
            settings.indoor = value;
            if (!settings.indoor && !settings.outdoor) {
              settings.outdoor = true;
            }
            setState(() {});
          },
        ),
      ],
    );
  }

  Widget _outdoorSetting() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          AppLocalizations.of(context)!.general_outdoor,
          style: Theme.of(context).textTheme.bodyLarge,
        ),
        Switch(
          activeColor: Colors.blue,
          value: settings.outdoor,
          onChanged: (value) {
            settings.outdoor = value;
            if (!settings.indoor && !settings.outdoor) {
              settings.indoor = true;
            }
            setState(() {});
          },
        ),
      ],
    );
  }

  Widget _redpointSetting() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          AppLocalizations.of(context)!.search_settings_not_at_least_redpoint_only,
          style: Theme.of(context).textTheme.bodyLarge,
        ),
        Switch(
          activeColor: Colors.blue,
          value: settings.onlyNotAtLeastRedpoint,
          onChanged: (value) {
            settings.onlyNotAtLeastRedpoint = value;
            settings.onlyNotFinished = false;
            setState(() {});
          },
        ),
      ],
    );
  }

  Widget _finishedSetting() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          AppLocalizations.of(context)!.search_settings_not_finished_only,
          style: Theme.of(context).textTheme.bodyLarge,
        ),
        Switch(
          activeColor: Colors.blue,
          value: settings.onlyNotFinished,
          onChanged: (value) {
            settings.onlyNotFinished = value;
            settings.onlyNotAtLeastRedpoint = false;
            setState(() {});
          },
        ),
      ],
    );
  }

  Widget _currentLocationSetting() {
    return Visibility(
      visible: SessionState().currentState == SessionStateEnum.active,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            AppLocalizations.of(context)!.search_settings_current_location_only,
            style: Theme.of(context).textTheme.bodyLarge,
          ),
          Switch(
            activeColor: Colors.blue,
            value: settings.onlyCurrentLocation,
            onChanged: (value) {
              settings.onlyCurrentLocation = value;
              settings.currentLocation = SearchSettingsDataStore.ALL_LOCATIONS_KEY;
              setState(() {});
            },
          ),
        ],
      ),
    );
  }

  Widget _mapByName() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                AppLocalizations.of(context)!.search_settings_map_by_name,
                style: Theme.of(context).textTheme.bodyLarge,
              ),
              Text(
                AppLocalizations.of(context)!.search_settings_map_by_name_info,
                style: Theme.of(context).textTheme.bodySmall,
                overflow: TextOverflow.clip,
              ),
            ],
          ),
        ),
        const SizedBox(width: 20.0),
        Switch(
          activeColor: Colors.blue,
          value: settings.mapByName,
          onChanged: (value) {
            settings.mapByName = value;
            setState(() {});
          },
        ),
      ],
    );
  }

  Widget _markedSetting() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          AppLocalizations.of(context)!.search_settings_marked_only,
          style: Theme.of(context).textTheme.bodyLarge,
        ),
        Switch(
          activeColor: Colors.blue,
          value: settings.onlyMarked,
          onChanged: (value) {
            settings.onlyMarked = value;
            setState(() {});
          },
        ),
      ],
    );
  }

  Widget _noNameSetting() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          AppLocalizations.of(context)!.search_settings_no_name,
          style: Theme.of(context).textTheme.bodyLarge,
        ),
        Switch(
          activeColor: Colors.blue,
          value: settings.onlyWithName,
          onChanged: (value) {
            settings.onlyWithName = value;
            setState(() {});
          },
        ),
      ],
    );
  }

  Widget _sortingSetting() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          AppLocalizations.of(context)!.search_settings_sorting,
          style: Theme.of(context).textTheme.bodyLarge,
        ),
        DropdownButton<Sorting>(
          borderRadius: BorderRadius.circular(20.0),
          alignment: Alignment.centerRight,
          value: settings.currentSorting,
          underline: Container(),
          style: Theme.of(context).textTheme.bodyLarge,
          onChanged: (Sorting? value) {
            setState(() {
              settings.currentSorting = value ?? Sorting.NEWEST;
            });
          },
          items: Sorting.values.map((Sorting item) {
            return DropdownMenuItem<Sorting>(
              value: item,
              child: Text(item.toName(context)),
            );
          }).toList(),
        ),
      ],
    );
  }

  Widget _typesSetting() {
    List<String> typeList = [
      SearchSettingsDataStore.ALL_TYPES_KEY,
      ...ClimbingType.values.map((e) => e.name),
    ];

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          AppLocalizations.of(context)!.search_settings_types,
          style: Theme.of(context).textTheme.bodyLarge,
        ),
        DropdownButton(
          borderRadius: BorderRadius.circular(20.0),
          alignment: Alignment.centerRight,
          value: settings.type,
          underline: Container(),
          style: Theme.of(context).textTheme.bodyLarge,
          onChanged: (value) {
            setState(() {
              settings.type = value;
            });
          },
          items: List<DropdownMenuItem>.generate(
            typeList.length,
            (i) => DropdownMenuItem(
              value: typeList[i],
              child: Text(
                getNameFromType(typeList[i]),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _locationsSetting() {
    List<String> locations = DataStore().locationsDataStore.locations.map((e) => e.name).toList();
    locations.sort();

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          AppLocalizations.of(context)!.general_location,
          style: Theme.of(context).textTheme.bodyLarge,
        ),
        const SizedBox(width: 24.0),
        Flexible(
          child: DropdownButton(
            isExpanded: true,
            borderRadius: BorderRadius.circular(20.0),
            alignment: Alignment.centerRight,
            value: settings.currentLocation,
            underline: Container(),
            style: Theme.of(context).textTheme.bodyLarge,
            onChanged: (value) {
              setState(() {
                settings.onlyCurrentLocation = false;
                settings.currentLocation = value;
              });
            },
            selectedItemBuilder: (context) => List<DropdownMenuItem>.generate(
              locations.length,
              (i) => DropdownMenuItem(
                value: locations[i],
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      Util.replaceStringWithBlankSpaces(locations[i]),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
            )..insert(
                0,
                DropdownMenuItem(
                  value: SearchSettingsDataStore.ALL_LOCATIONS_KEY,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        AppLocalizations.of(context)!.search_settings_all_locations,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
              ),
            items: List<DropdownMenuItem>.generate(
              locations.length,
              (i) => DropdownMenuItem(
                value: locations[i],
                child: Text(
                  Util.replaceStringWithBlankSpaces(locations[i]),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            )..insert(
                0,
                DropdownMenuItem(
                  value: SearchSettingsDataStore.ALL_LOCATIONS_KEY,
                  child: Text(
                    AppLocalizations.of(context)!.search_settings_all_locations,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
          ),
        ),
      ],
    );
  }

  Widget _leadTopRopeSelection() {
    List<String> climbingMethodNames = SearchSettingsDataStore.climbingMethodNames(context);

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          AppLocalizations.of(context)!.climbing_method,
          style: Theme.of(context).textTheme.bodyLarge,
        ),
        DropdownButton(
          borderRadius: BorderRadius.circular(20.0),
          alignment: Alignment.centerRight,
          value: settings.leadTopropeSelection + 1,
          underline: Container(),
          style: Theme.of(context).textTheme.bodyLarge,
          onChanged: (value) {
            setState(() {
              settings.leadTopropeSelection = value - 1;
            });
          },
          items: List<DropdownMenuItem>.generate(
            climbingMethodNames.length,
            (i) => DropdownMenuItem(
              value: i,
              child: Text(
                climbingMethodNames[i],
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _climbingStyle() {
    List<String> styleList = [
      SearchSettingsDataStore.ALL_STYLES_KEY,
      ...ClimbingStyle.values.map((e) => e.name),
    ];

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          AppLocalizations.of(context)!.climbing_styles(1),
          style: Theme.of(context).textTheme.bodyLarge,
        ),
        DropdownButton(
          borderRadius: BorderRadius.circular(20.0),
          alignment: Alignment.centerRight,
          value: settings.climbingStyle,
          underline: Container(),
          style: Theme.of(context).textTheme.bodyLarge,
          onChanged: (value) {
            setState(() {
              settings.climbingStyle = value;
            });
          },
          items: List<DropdownMenuItem>.generate(
            styleList.length,
            (i) => DropdownMenuItem(
              value: styleList[i],
              child: Text(
                getNameFromClimbingStyle(styleList[i]),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _dateTime() {
    String dateString = "---";
    if (settings.dateTime != null) {
      dateString = CustomDateFormatter.formatDateString(context, settings.dateTime!);
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Flexible(
          child: Text(
            AppLocalizations.of(context)!.general_date,
            style: Theme.of(context).textTheme.bodyLarge,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        const Spacer(),
        const Spacer(),
        if (settings.dateTime != null) ...[
          Material(
            borderRadius: BorderRadius.circular(20),
            color: const Color(0xfff3f3fa),
            elevation: 2,
            child: InkWell(
              onTap: () {
                settings.isBeforeDate = !settings.isBeforeDate;
                setState(() {});
              },
              borderRadius: BorderRadius.circular(20),
              child: Container(
                padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
                height: 32,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 4.0),
                  child: Icon(settings.isBeforeDate ? Icons.navigate_before : Icons.navigate_next, size: 22.0),
                ),
              ),
            ),
          ),
          //const SizedBox(width: 6.0),
        ],
        Material(
          borderRadius: BorderRadius.circular(20),
          color: const Color(0xfff3f3fa),
          elevation: 2,
          child: InkWell(
            borderRadius: BorderRadius.circular(20),
            onTap: () => _selectDate(
              context,
              settings.dateTime ?? DateTime.now(),
            ),
            child: Container(
              padding: settings.dateTime != null
                  ? const EdgeInsets.fromLTRB(8.0, 0.0, 2.0, 0.0)
                  : const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 0.0),
              height: 32,
              child: Row(
                children: [
                  Text(dateString, style: const TextStyle(fontWeight: FontWeight.w500)),
                  const SizedBox(width: 2.0),
                  if (settings.dateTime != null)
                    InkWell(
                      onTap: () {
                        settings.dateTime = null;
                        setState(() {});
                      },
                      child: const Padding(
                        padding: EdgeInsets.symmetric(vertical: 1.0, horizontal: 4.0),
                        child: Icon(Icons.close, size: 22.0),
                      ),
                    ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Future<void> _selectDate(BuildContext context, DateTime selectedDate) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
    );
    if (picked != null) {
      settings.dateTime = picked.copyWith(hour: 0);
      setState(() {});
    }
  }
}
