import 'dart:io';

import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/active_session.dart';
import 'package:climbing_track/backend/model/location.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:climbing_track/backend/util/extensions.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:climbing_track/ui/dialog/confirm_dialog.dart';
import 'package:climbing_track/ui/dialog/start_session_info_dialog.dart';
import 'package:climbing_track/ui/pages/home/pages/active_session_view.dart';
import 'package:climbing_track/ui/pages/home/pages/end_session_view.dart';
import 'package:climbing_track/ui/pages/home/pages/start_session_view.dart';
import 'package:climbing_track/ui/pages/home/widgets/save_button.dart';
import 'package:climbing_track/ui/pages/home/widgets/session_button.dart';
import 'package:climbing_track/ui/pages/home/widgets/session_timer_widget.dart';
import 'package:climbing_track/ui/util/screenshot.dart';
import 'package:climbing_track/view_model/home_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:intl/intl.dart';
import 'package:share_plus/share_plus.dart';
import 'package:showcaseview/showcaseview.dart';

class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final HomeViewModel homeViewModel = HomeViewModel();

  final SessionState state = SessionState();
  final ConfirmDialog confirmDialog = ConfirmDialog();
  final ScrollController controller = ScrollController();
  final ScreenshotController _screenshotController = ScreenshotController();
  final GlobalKey _globalKey = GlobalKey();
  late Function() listener;
  bool _startSessionInfoDialogShown = false;

  @override
  void initState() {
    super.initState();
    listener = () {
      if (mounted) {
        setState(() {});
        if (state.currentState == SessionStateEnum.active) {
          WidgetsBinding.instance.addPostFrameCallback((_) => _onStartSession());
        }
        if (controller.hasClients) controller.jumpTo(0.0);
      }
    };
    state.addListener(listener);

    // unfocus TextFields when keyboard isn't visible anymore
    var keyboardVisibilityController = KeyboardVisibilityController();
    keyboardVisibilityController.onChange.listen((bool visible) {
      if (!visible && mounted) {
        FocusScope.of(context).unfocus();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final bool isKeyboardVisible = KeyboardVisibilityProvider.isKeyboardVisible(context);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      // choose dark overlay on bright background, light overlay otherwise
      value: state.currentState == SessionStateEnum.none ? SystemUiOverlayStyle.dark : SystemUiOverlayStyle.light,
      child: _getContent(isKeyboardVisible),
    );
  }

  Widget _getContent(bool isKeyboardVisible) {
    Widget pageWidget;
    EdgeInsetsGeometry padding = const EdgeInsets.all(Constants.STANDARD_PADDING);
    Widget secondWidget = Container();
    switch (state.currentState) {
      case SessionStateEnum.none:
        return SafeArea(
          child: Stack(
            children: <Widget>[
              Align(alignment: Alignment.topCenter, child: _buildTitle()),
              SessionButton(homeViewModel: homeViewModel),
            ],
          ),
        );
      case SessionStateEnum.start:
        pageWidget = StartSessionWidget(homeViewModel: homeViewModel);
        break;
      case SessionStateEnum.active:
        padding = EdgeInsets.zero;
        pageWidget = ActiveSessionWidget(
          scrollController: controller,
          homeViewModel: homeViewModel,
        );
        if (!isKeyboardVisible) {
          secondWidget = Padding(
            padding: const EdgeInsets.only(
              bottom: Constants.STANDARD_PADDING,
            ),
            child: SaveButton(scrollController: controller, homeViewModel: homeViewModel),
          );
        }
        break;
      case SessionStateEnum.end:
        pageWidget = EndSessionWidget(
          _screenshotController,
          homeViewModel: homeViewModel,
        );
        padding = EdgeInsets.zero;
        break;
    }

    return Scaffold(
      appBar: _createTopBar(),
      body: SafeArea(
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: GestureDetector(
            onVerticalDragUpdate: (dragDetails) {
              FocusScope.of(context).unfocus();
            },
            onTapDown: (tapDownDetails) {
              FocusScope.of(context).unfocus();
            },
            child: Stack(
              children: [
                Scrollbar(
                  child: SingleChildScrollView(
                    controller: controller,
                    padding: padding,
                    child: pageWidget,
                  ),
                ),
                secondWidget
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTitle() {
    return Container(
      padding: const EdgeInsets.fromLTRB(40, 20, 40, 20),
      child: Text(
        AppLocalizations.of(context)!.home_page_title,
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.titleLarge,
      ),
    );
  }

  PreferredSizeWidget _createTopBar() {
    switch (state.currentState) {
      case SessionStateEnum.none:
        break;
      case SessionStateEnum.start:
        return _createSessionAppBar();
      case SessionStateEnum.active:
        return _activeSessionAppBar();
      case SessionStateEnum.end:
        return _endSessionAppBar();
    }
    return AppBar();
  }

  PreferredSizeWidget _createSessionAppBar() {
    return AppBar(
      centerTitle: true,
      title: Text(
        AppLocalizations.of(context)!.home_page_create_session.toTitleCase(),
        style: Theme.of(context).textTheme.headlineMedium!,
      ),
    );
  }

  PreferredSizeWidget _activeSessionAppBar() {
    return AppBar(
      toolbarHeight: 64,
      centerTitle: true,
      title: InkWell(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onTap: _endSession,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              AppLocalizations.of(context)!.session_time,
              style: Theme.of(context).textTheme.headlineSmall,
            ),
            SessionTimerWidget(homeViewModel: homeViewModel),
          ],
        ),
      ),
      actions: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Showcase(
              key: _globalKey,
              description: AppLocalizations.of(context)!.home_page_close_session_hint,
              targetBorderRadius: const BorderRadius.all(Radius.circular(12)),
              tooltipPadding: const EdgeInsets.all(12.0),
              child: IconButton(
                tooltip: AppLocalizations.of(context)!.home_page_close_session,
                onPressed: _endSession,
                icon: const Icon(
                  Icons.wrong_location_outlined,
                  size: 42,
                ),
              ),
            ),
            const SizedBox(width: 6.0),
          ],
        ),
      ],
    );
  }

  PreferredSizeWidget _endSessionAppBar() {
    return AppBar(
      title: Text(
        AppLocalizations.of(context)!.session_summary,
        style: Theme.of(context).textTheme.headlineSmall!.copyWith(height: 1.0),
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
      ),
      actions: [
        IconButton(
          padding: const EdgeInsets.all(12.0),
          tooltip: AppLocalizations.of(context)!.general_share,
          onPressed: () {
            _screenshotController
                .capture(
              fileName: "${DateFormat("yyyy-MM-dd").format(ActiveSession().activeSession!.startDate)}-session-summary",
            )
                .then((File image) {
              Share.shareXFiles([XFile(image.path)]);
            }).catchError((onError) {
              debugPrint(onError);
            });
          },
          icon: const Icon(Icons.share),
        ),
        const SizedBox(width: 8.0),
      ],
    );
  }

  void _endSession() {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    confirmDialog.showEndSessionDialog(
      context: context,
      homeViewModel: homeViewModel,
      session: ActiveSession().activeSession!,
      routeCount: ActiveSession().ascents.length,
    );
  }

  void _onStartSession() async {
    // only start dialog once. This can happen because of listener on session state
    if (_startSessionInfoDialogShown) return;
    await _showLocationInfoDialog();
    _startSessionInfoDialogShown = false;
    _showToolTip();
  }

  Future<void> _showToolTip() async {
    if (!DataStore().toolTipsDataStore.tooltipCloseSession) return;

    await Future.delayed(const Duration(milliseconds: 1000));
    if (mounted) {
      DataStore().toolTipsDataStore.tooltipCloseSession = false;
      ShowCaseWidget.of(context).startShowCase([_globalKey]);
    }
  }

  Future<void> _showLocationInfoDialog() async {
    _startSessionInfoDialogShown = true;

    // return if no active session
    if (ActiveSession().activeSession == null) return;

    // Only if new session (= start ten seconds ago)
    DateTime now = DateTime.now();
    DateTime tenSecondsAgo = DateTime(now.year, now.month, now.day, now.hour, now.minute, now.second - 10);
    if (ActiveSession().activeSession!.startDate.isBefore(tenSecondsAgo)) return;

    // delay
    await Future.delayed(const Duration(milliseconds: 500));

    // show dialog
    Location location = DataStore().locationsDataStore.locationById(ActiveSession().activeSession!.locationId);

    // If no reminder or do not show reminder again AND info to show -> return
    if ((homeViewModel.locationComment.isEmpty ||
            DataStore().settingsDataStore.doNotShowLocationInfoAgain.contains(location.locationId)) &&
        DataStore().ascendReminderDataStore.getAscends(locationId: location.locationId).isEmpty) {
      return;
    }

    if (mounted) await StartSessionInfoDialog(location: location).showInfoDialog(context);
  }
}
