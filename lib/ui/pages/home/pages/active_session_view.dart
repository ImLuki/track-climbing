import 'package:climbing_track/backend/model/active_session.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:climbing_track/ui/pages/home/widgets/save_button.dart';
import 'package:climbing_track/ui/widgets/edit_ascend/edit_ascend_widget.dart';
import 'package:climbing_track/ui/widgets/reorderable_ascents_list.dart';
import 'package:climbing_track/view_model/home_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';

class ActiveSessionWidget extends StatefulWidget {
  final ScrollController scrollController;
  final HomeViewModel homeViewModel;

  const ActiveSessionWidget({super.key, required this.scrollController, required this.homeViewModel});

  @override
  _ActiveSessionWidgetState createState() => _ActiveSessionWidgetState();
}

class _ActiveSessionWidgetState extends State<ActiveSessionWidget> {
  late Function() listener;
  Session? sessionWithListener;

  @override
  void initState() {
    super.initState();
    listener = () {
      if (mounted) {
        setState(() {});
      }
    };
    sessionWithListener = ActiveSession().activeSession;
    sessionWithListener?.addListener(listener);
    widget.homeViewModel.addListener(listener);
  }

  @override
  void dispose() {
    super.dispose();

    sessionWithListener?.removeListener(listener);
    widget.homeViewModel.removeListener(listener);
  }

  @override
  Widget build(BuildContext context) {
    final bool isKeyboardVisible = KeyboardVisibilityProvider.isKeyboardVisible(context);

    return Column(
      children: [
        EditAscendWidget(
          ascend: widget.homeViewModel.currentAscend,
          currentSession: ActiveSession().activeSession,
          addBottomPadding: false,
        ),
        if (isKeyboardVisible) ...[
          const SizedBox(height: Constants.STANDARD_PADDING),
          SaveButton(scrollController: widget.scrollController, homeViewModel: widget.homeViewModel),
          const SizedBox(height: Constants.STANDARD_PADDING),
          const Padding(
            padding: EdgeInsets.all(Constants.STANDARD_PADDING),
            child: Divider(thickness: 2.0),
          ),
        ],
        Visibility(
          visible: ActiveSession().ascents.isNotEmpty,
          child: Theme(
            data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
            child: ExpansionTile(
              initiallyExpanded: true,
              iconColor: Colors.black54,
              title: Text(
                AppLocalizations.of(context)!.home_page_ascents,
                style: Theme.of(context).textTheme.titleLarge!.copyWith(height: 0.0),
              ),
              children: [ascendList()],
            ),
          ),
        ),
        const SizedBox(height: 120.0),
      ],
    );
  }

  Widget ascendList() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: Constants.STANDARD_PADDING),
      child: ReorderableAscentsList(
        key: UniqueKey(),
        data: ActiveSession().ascents,
        onDelete: (Ascend ascend) => widget.homeViewModel.deleteAscend(ascend),
        onReAdd: (int index, Ascend ascend) => widget.homeViewModel.onReAdd(index, ascend),
        onReorder: (int oldIndex, int newIndex) => widget.homeViewModel.updateAscendsReorder(),
        session: ActiveSession().activeSession!,
      ),
    );
  }
}
