import 'dart:async';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/enums/height_measurement.dart';
import 'package:climbing_track/backend/model/location.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:climbing_track/backend/util/custom_date_formatter.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:climbing_track/ui/pages/home/widgets/filtered_text_field_with_list.dart';
import 'package:climbing_track/view_model/home_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_picker_plus/Picker.dart';
import 'package:toggle_switch/toggle_switch.dart';

class StartSessionWidget extends StatefulWidget {
  final HomeViewModel homeViewModel;

  const StartSessionWidget({super.key, required this.homeViewModel});

  @override
  _StartSessionWidgetState createState() => _StartSessionWidgetState();
}

class _StartSessionWidgetState extends State<StartSessionWidget> {
  final FocusNode _commentTextFieldFocus = FocusNode();
  final FocusNode _locationTextFieldFocus = FocusNode();
  final GlobalKey _commentTextFieldKey = GlobalKey();

  late StreamSubscription<bool> keyboardSubscription;

  bool editingComment = false;

  @override
  void initState() {
    super.initState();
    editingComment = false;
    widget.homeViewModel.dateTime = null; // Ensure start time is now (equals null)

    // The following lines are a bit hacky
    // These will prevent the text fields from autofocussing, which is a current flutter bug
    _locationTextFieldFocus.canRequestFocus = false;
    _commentTextFieldFocus.canRequestFocus = false;

    // keyboard visibility listener
    var keyboardVisibilityController = KeyboardVisibilityController();
    keyboardSubscription = keyboardVisibilityController.onChange.listen((bool visible) {
      if (!visible) {
        _locationTextFieldFocus.canRequestFocus = false;
        _commentTextFieldFocus.canRequestFocus = false;
      }
    });
  }

  @override
  void dispose() {
    keyboardSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _createIndoorOutdoorSwitch(context),
        _createLocationInput(),
        _createDatePickerWidget(context),
        _createHeightPicker(context),
        _createCommentsWidget(context),
        _createButtons(context),
        const SizedBox(height: 40.0),
      ],
    );
  }

  Widget _createCommentsWidget(BuildContext context) {
    return Column(
      key: _commentTextFieldKey,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        const SizedBox(height: 10.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: Text(
                AppLocalizations.of(context)!.location_comment_title,
                style: Theme.of(context).textTheme.bodyLarge,
                overflow: TextOverflow.clip,
              ),
            ),
            const SizedBox(width: 20.0),
            IconButton(
              onPressed: () => setState(() {
                if ((editingComment && widget.homeViewModel.locationComment.isNotEmpty) ||
                    _commentTextFieldFocus.hasFocus) {
                  if (widget.homeViewModel.locationComment.isEmpty) {
                    ScaffoldMessenger.of(context)
                      ..removeCurrentSnackBar()
                      ..showSnackBar(
                        SnackBar(
                          behavior: SnackBarBehavior.floating,
                          content: Text(AppLocalizations.of(context)!.location_comment_missing_warning),
                        ),
                      );
                    return;
                  }

                  widget.homeViewModel.updateLocationComment();
                } else {
                  if (widget.homeViewModel.commentEditingController.text == "") {
                    widget.homeViewModel.commentEditingController.text = "● ";
                  }
                  _commentTextFieldFocus.canRequestFocus = true;
                  setState(() {});
                  _commentTextFieldFocus.requestFocus();
                }
                editingComment = !editingComment;
              }),
              icon: Icon(
                  (editingComment && widget.homeViewModel.locationComment.isNotEmpty) || _commentTextFieldFocus.hasFocus
                      ? Icons.done
                      : Icons.edit),
            ),
          ],
        ),
        const SizedBox(height: 12.0),
        ..._createCommentTextField(),
      ],
    );
  }

  Widget _createIndoorOutdoorSwitch(BuildContext context) {
    return Column(
      children: [
        ToggleSwitch(
          initialLabelIndex: widget.homeViewModel.outdoor ? 1 : 0,
          minWidth: MediaQuery.of(context).size.width * 0.35,
          minHeight: 45.0,
          cornerRadius: 20.0,
          activeBgColor: const [Colors.green],
          activeFgColor: Colors.white,
          inactiveBgColor: AppColors.accentColor2,
          inactiveFgColor: Colors.white,
          labels: [
            AppLocalizations.of(context)!.general_indoor.toUpperCase(),
            AppLocalizations.of(context)!.general_outdoor.toUpperCase()
          ],
          onToggle: (index) {
            widget.homeViewModel.outdoor = index == 1;
          },
          totalSwitches: 2,
        ),
        const SizedBox(height: Constants.STANDARD_PADDING),
        const Divider(thickness: 2.0),
      ],
    );
  }

  Widget _createHeightPicker(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 14.0),
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            AppLocalizations.of(context)!.home_page_standard_route_length,
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        const SizedBox(height: 10),
        InkWell(
          borderRadius: BorderRadius.circular(10.0),
          onTap: () => _showPickerNumber(context),
          child: Padding(
            padding: const EdgeInsets.all(6.0),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 2.0, left: 20.0, right: 20.0),
                    decoration: const BoxDecoration(
                      border: Border(
                        bottom: BorderSide(color: Colors.black26),
                      ),
                    ),
                    child: Text(
                      Util.getHeightString(widget.homeViewModel.standardHeight),
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ),
                  const SizedBox(width: 25),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3.0),
                    child: Text(
                      DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET
                          ? AppLocalizations.of(context)!.general_feet(
                              Util.heightFromMeters(widget.homeViewModel.standardHeight).round(),
                            )
                          : AppLocalizations.of(context)!.general_meter(
                              Util.heightFromMeters(widget.homeViewModel.standardHeight).round(),
                            ),
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        const SizedBox(height: 12),
        const Divider(thickness: 2.0),
      ],
    );
  }

  Widget _createLocationInput() {
    return Column(
      children: [
        const SizedBox(height: Constants.STANDARD_PADDING),
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "${AppLocalizations.of(context)!.general_location}:",
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        const SizedBox(height: Constants.STANDARD_PADDING),
        LocationSearchInputField(
          onChanged: () => setState(() => editingComment = widget.homeViewModel.locationComment.isEmpty),
          homeViewModel: widget.homeViewModel,
          focusNode: _locationTextFieldFocus,
        ),
        const SizedBox(height: 12.0),
        const Divider(thickness: 2.0),
      ],
    );
  }

  Widget _createButtons(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const SizedBox(height: Constants.STANDARD_PADDING),
        MaterialButton(
          minWidth: 160,
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 14.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ),
          elevation: 0.0,
          color: AppColors.accentColor,
          textColor: Colors.white,
          onPressed: () => widget.homeViewModel.onStartSession(context),
          child: Text(
            AppLocalizations.of(context)!.general_start.toUpperCase(),
            style: Theme.of(context).textTheme.bodyLarge!.copyWith(fontSize: 24, color: AppColors.textSecondary),
          ),
        ),
        const SizedBox(height: 10.0),
        MaterialButton(
          minWidth: 160,
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 14.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
            side: const BorderSide(color: Colors.black26),
          ),
          highlightColor: AppColors.accentColor.withOpacity(0.3),
          elevation: 0.0,
          onPressed: () => SessionState().setState(SessionStateEnum.none),
          child: Text(
            AppLocalizations.of(context)!.general_cancel.toUpperCase(),
            style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                  fontSize: 24,
                  color: AppColors.accentColor,
                ),
          ),
        ),
      ],
    );
  }

  void _showPickerNumber(BuildContext context) {
    dynamic height = Util.heightFromMeters(widget.homeViewModel.standardHeight);
    int meters = height.floor();
    int cm = ((height % 1) * 10).floor();
    if (DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET) {
      Picker(
        cancelTextStyle: const TextStyle(
          color: AppColors.accentColor,
          fontSize: 18,
        ),
        confirmTextStyle: const TextStyle(
          color: AppColors.accentColor,
          fontSize: 18,
        ),
        adapter: NumberPickerAdapter(
          data: [
            const NumberPickerColumn(begin: 0, end: 999),
          ],
        ),
        delimiter: [
          PickerDelimiter(
            column: 1,
            child: Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.only(left: 12.0, right: 20.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    AppLocalizations.of(context)!.general_feet(2),
                    style: const TextStyle(fontWeight: FontWeight.normal),
                  ),
                ),
              ),
            ),
          ),
        ],
        selecteds: [height],
        hideHeader: false,
        onConfirm: (Picker picker, List value) {
          setState(() {
            widget.homeViewModel.standardHeight = Util.heightToMeters(value[0].toDouble());
          });
        },
      ).showModal(context);
    } else {
      Picker(
        cancelTextStyle: const TextStyle(
          color: AppColors.accentColor,
          fontSize: 18,
        ),
        confirmTextStyle: const TextStyle(
          color: AppColors.accentColor,
          fontSize: 18,
        ),
        adapter: NumberPickerAdapter(
          data: [
            const NumberPickerColumn(begin: 0, end: 999),
            NumberPickerColumn(items: [0, 5], onFormatValue: (val) => val.toString().padRight(2, "0")),
          ],
        ),
        delimiter: [
          PickerDelimiter(
            column: 1,
            child: Container(
              color: Colors.white,
              child: const Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text("m", style: TextStyle(fontWeight: FontWeight.normal)),
                ),
              ),
            ),
          ),
          PickerDelimiter(
            column: 3,
            child: Container(
              color: Colors.white,
              child: const Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text("cm", style: TextStyle(fontWeight: FontWeight.normal)),
                ),
              ),
            ),
          ),
        ],
        selecteds: [meters, cm],
        hideHeader: false,
        onConfirm: (Picker picker, List value) {
          setState(() {
            double result = value[0] + (value[1] * 0.5);
            widget.homeViewModel.standardHeight = result;
          });
        },
      ).showModal(context);
    }
  }

  List<Widget> _createCommentTextField() {
    return [
      Theme(
        data: ThemeData.from(
          colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor),
          useMaterial3: false,
        ),
        child: !editingComment && widget.homeViewModel.locationComment.isNotEmpty
            ? Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0),
                child: Text(widget.homeViewModel.locationComment, overflow: TextOverflow.clip),
              )
            : TextFormField(
                canRequestFocus: _commentTextFieldFocus.canRequestFocus,
                focusNode: _commentTextFieldFocus,
                onTap: () {
                  _commentTextFieldFocus.canRequestFocus = true;
                  setState(() {});
                  _commentTextFieldFocus.requestFocus();
                  editingComment = true;
                  Scrollable.ensureVisible(
                    _commentTextFieldKey.currentContext!,
                    duration: const Duration(milliseconds: 400),
                    curve: Curves.easeInOut,
                  );
                  if (widget.homeViewModel.commentEditingController.text == "") {
                    widget.homeViewModel.commentEditingController.text = "● ";
                  }
                  setState(() {});
                },
                autofocus: false,
                maxLines: null,
                minLines: 3,
                textAlign: TextAlign.start,
                textAlignVertical: TextAlignVertical.center,
                controller: widget.homeViewModel.commentEditingController,
                style: const TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
                onChanged: (value) {
                  if (value.endsWith('\n') && value.length > widget.homeViewModel.locationComment.length) {
                    value += "● ";
                    widget.homeViewModel.commentEditingController.text = value;
                  }
                  widget.homeViewModel.locationComment = value.trim();
                },
                textCapitalization: TextCapitalization.sentences,
                decoration: InputDecoration(
                  alignLabelWithHint: true,
                  isDense: true,
                  label: Text(
                    AppLocalizations.of(context)!.location_comment_hint,
                    style:
                        const TextStyle(fontSize: 18, color: AppColors.accentColor2, overflow: TextOverflow.ellipsis),
                    maxLines:
                        widget.homeViewModel.commentEditingController.text.isNotEmpty || _commentTextFieldFocus.hasFocus
                            ? 1
                            : 2,
                  ),
                  fillColor: AppColors.accentColor,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
                    borderSide: const BorderSide(),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
                    borderRadius: BorderRadius.circular(
                      Constants.STANDARD_BORDER_RADIUS,
                    ),
                  ),
                ),
              ),
      ),
      ..._locationCommentCheckBox(),
      const SizedBox(height: 30.0),
    ];
  }

  List<Widget> _locationCommentCheckBox() {
    Location? location = DataStore().locationsDataStore.findExistingLocation(
          widget.homeViewModel.location,
          widget.homeViewModel.outdoor,
        );
    if (location == null || location.locationId == null || location.locationComment.isEmpty) return [];
    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          const Icon(Icons.east, size: 20.0),
          const SizedBox(width: 4.0),
          Flexible(
            child: Text(
              AppLocalizations.of(context)!.home_page_don_not_show_comments_again,
              style: Theme.of(context).textTheme.bodySmall?.copyWith(height: 0.0),
            ),
          ),
          Checkbox(
            value: !DataStore().settingsDataStore.doNotShowLocationInfoAgain.contains(location.locationId),
            onChanged: (value) {
              if (value ?? false) {
                DataStore().settingsDataStore.doNotShowLocationInfoAgain =
                    DataStore().settingsDataStore.doNotShowLocationInfoAgain..remove(location.locationId);
              } else {
                DataStore().settingsDataStore.doNotShowLocationInfoAgain =
                    DataStore().settingsDataStore.doNotShowLocationInfoAgain..add(location.locationId!);
              }
              setState(() {});
            },
          )
        ],
      ),
    ];
  }

  Widget _createDatePickerWidget(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 14.0),
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "${AppLocalizations.of(context)!.general_date}:",
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        InkWell(
          borderRadius: BorderRadius.circular(10.0),
          onTap: () => _onChooseTime(widget.homeViewModel.dateTime ?? DateTime.now()),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 6.0, horizontal: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(),
                Flexible(
                  child: Text(
                    "${CustomDateFormatter.formatDateStringWithJM(
                      context,
                      widget.homeViewModel.dateTime ?? DateTime.now(),
                    )}${widget.homeViewModel.dateTime == null ? ' (${AppLocalizations.of(context)!.home_page_date_now})' : ''}",
                    style: Theme.of(context).textTheme.bodyLarge,
                    overflow: TextOverflow.clip,
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.only(left: 20.0),
                  child: Icon(Icons.more_time, color: Colors.black54),
                ),
              ],
            ),
          ),
        ),
        const SizedBox(height: 8),
        const Divider(thickness: 2.0),
      ],
    );
  }

  Future<void> _onChooseTime(DateTime initialTime) async {
    DateTime? dateTimeResult = await showDatePicker(
      context: context,
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
      initialDate: initialTime,
    );

    if (dateTimeResult == null || !context.mounted) return;

    if (!mounted) return;

    TimeOfDay? timeOfDayResult = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.fromDateTime(initialTime),
    );
    if (timeOfDayResult == null) return;

    DateTime finalDateTimeResult = dateTimeResult.copyWith(hour: timeOfDayResult.hour, minute: timeOfDayResult.minute);
    if (DateTime.now().isBefore(finalDateTimeResult)) {
      if (mounted) {
        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              content: Text(AppLocalizations.of(context)!.home_page_date_in_future_warning),
            ),
          );
      }
      widget.homeViewModel.dateTime = null;
    } else {
      widget.homeViewModel.dateTime = finalDateTimeResult;
    }
    setState(() {});
  }
}
