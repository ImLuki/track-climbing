import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/handler/location_handler.dart';
import 'package:climbing_track/backend/model/location.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:climbing_track/view_model/home_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:geolocator/geolocator.dart';

class LocationSearchInputField extends StatefulWidget {
  final Function? onChanged;
  final HomeViewModel homeViewModel;
  final FocusNode focusNode;

  const LocationSearchInputField({super.key, this.onChanged, required this.homeViewModel, required this.focusNode});

  @override
  _LocationSearchInputFieldState createState() => _LocationSearchInputFieldState();
}

class _LocationSearchInputFieldState extends State<LocationSearchInputField> {
  static const int MAX_ITEM_AMOUNT = 4;
  final LocationHandler _locationHandler = LocationHandler();
  final TextEditingController editingController = TextEditingController();
  late List<Location> locations;
  List<Location> items = [];
  bool searchingLocation = false;

  @override
  void initState() {
    super.initState();
    widget.homeViewModel.addListener(_updateLocations);
    _updateLocations();
  }

  @override
  void dispose() {
    super.dispose();
    widget.homeViewModel.removeListener(_updateLocations);
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> entries = [];
    for (Location location in items.take(MAX_ITEM_AMOUNT)) {
      entries.add(_getRowEntry(location));
    }
    return Column(
      children: <Widget>[
        _createTextField(),
        const SizedBox(height: 12.0),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Column(children: entries),
        ),
      ],
    );
  }

  void _updateLocations() {
    (widget.homeViewModel.outdoor)
        ? locations = DataStore().locationsDataStore.getOutdoorLocations()
        : locations = DataStore().locationsDataStore.getIndoorLocations();
    items = [];
    locations.sort((a, b) => b.lastVisited.compareTo(a.lastVisited));
    filterSearchResults(editingController.text.trim());
  }

  void filterSearchResults(String query) {
    List<Location> dummySearchList = [];
    dummySearchList.addAll(locations);
    if (query.isNotEmpty) {
      List<Location> dummyListData = [];
      for (Location item in dummySearchList) {
        if (item.name.toLowerCase().contains(query.toLowerCase())) {
          dummyListData.add(item);
        }
      }
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(locations);
      });
    }
  }

  Widget _getRowEntry(Location location) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(12),
      child: Material(
        child: InkWell(
          onTap: () {
            editingController.text = location.name;
            widget.homeViewModel.location = location.name;
            widget.onChanged?.call();
            FocusScope.of(context).unfocus();
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 6.0, vertical: 4.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const Icon(Icons.query_builder, color: AppColors.accentColor2),
                const SizedBox(width: 12.0),
                Flexible(
                  fit: FlexFit.loose,
                  child: Text(
                    location.name,
                    style: Theme.of(context).textTheme.bodyMedium,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.clip,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _createTextField() {
    return Theme(
      data: ThemeData.from(
        colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor),
        useMaterial3: false,
      ),
      child: TextFormField(
        canRequestFocus: widget.focusNode.canRequestFocus,
        focusNode: widget.focusNode,
        onTap: () {
          widget.focusNode.canRequestFocus = true;
          setState(() {});
          widget.focusNode.requestFocus();
        },
        autofocus: false,
        textAlign: TextAlign.start,
        textAlignVertical: TextAlignVertical.center,
        controller: editingController,
        style: const TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
        onChanged: (value) {
          widget.homeViewModel.location = value.trim();
          widget.onChanged?.call();
        },
        textCapitalization: TextCapitalization.words,
        decoration: InputDecoration(
          suffixIconColor: searchingLocation ? Colors.blueAccent : Colors.black54,
          suffixIcon: _locationHandler.saveLocation
              ? Tooltip(
                  message: AppLocalizations.of(context)!.home_page_find_nearest_location,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(100.0),
                    onTap: () => _findNearestLocation(context),
                    child: const Icon(Icons.my_location),
                  ),
                )
              : null,
          isDense: true,
          labelText: AppLocalizations.of(context)!.home_page_enter_location,
          labelStyle: const TextStyle(fontSize: 18, color: AppColors.accentColor2),
          fillColor: AppColors.accentColor,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
            borderSide: const BorderSide(),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
            borderRadius: BorderRadius.circular(
              Constants.STANDARD_BORDER_RADIUS,
            ),
          ),
        ),
      ),
    );
  }

  void _findNearestLocation(BuildContext context) async {
    if (searchingLocation) return;
    searchingLocation = true;
    setState(() {});

    // show loading with snack bar
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(AppLocalizations.of(context)!.home_page_find_searching_location),
              ),
              const SizedBox(width: 20.0),
              const SizedBox(
                width: 18.0,
                height: 18.0,
                child: CircularProgressIndicator(
                  color: Colors.blueAccent,
                  strokeWidth: 1.5,
                ),
              ),
            ],
          ),
          duration: const Duration(seconds: 25),
        ),
      );

    // calculate nearest location

    late Position currentLocation;
    try {
      currentLocation = await _locationHandler.determinePosition();
    } catch (e) {
      if (context.mounted) {
        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              content: Text(AppLocalizations.of(context)!.location_warning_permission),
              duration: const Duration(seconds: 2),
            ),
          );
      }
      return;
    }

    if (context.mounted) {
      // if location not found or nearest location is more than 2.5 km away
      Location? location = DataStore().locationsDataStore.findNearestLocation(currentLocation);
      if (location == null) {
        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              content: Text(AppLocalizations.of(context)!.home_page_find_not_found_location),
            ),
          );
        searchingLocation = false;
        setState(() {});
        return;
      }

      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            behavior: SnackBarBehavior.floating,
            content: Text(AppLocalizations.of(context)!.home_page_find_found_location(location.name)),
          ),
        );

      // set location name
      editingController.text = location.name;
      widget.homeViewModel.setLocation(location);
      widget.onChanged?.call();
    }

    searchingLocation = false;
    setState(() {});
  }
}
