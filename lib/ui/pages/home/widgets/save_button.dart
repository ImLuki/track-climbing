import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/dialog/confirm_ascend_dialog.dart';
import 'package:climbing_track/view_model/home_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SaveButton extends StatelessWidget {
  final HomeViewModel homeViewModel;
  final ConfirmAscendDialog confirmDialog = ConfirmAscendDialog();
  final ScrollController scrollController;

  SaveButton({
    super.key,
    required this.scrollController,
    required this.homeViewModel,
  });

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: ElevatedButton.icon(
        style: ElevatedButton.styleFrom(
          elevation: 6.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50.0),
          ),
          padding: const EdgeInsets.only(left: 24.0, top: 12.0, right: 24.0, bottom: 12.0),
          backgroundColor: Colors.black,
          foregroundColor: Colors.white,
        ),
        onPressed: () async {
          if (context.mounted) FocusScope.of(context).unfocus();
          String ascendName = Util.getAscendName(
            context,
            homeViewModel.currentAscend.name,
            type: homeViewModel.currentAscend.type,
            speedType: homeViewModel.currentAscend.speedType,
          );

          bool? result = await confirmDialog.showConfirmAscendDialog(
            context,
            ascend: homeViewModel.currentAscend.copy(),
            homeViewModel: homeViewModel,
          );

          if (result != null && result) {
            scrollController.animateTo(0.0, duration: const Duration(milliseconds: 500), curve: Curves.ease);
            if (context.mounted) {
              ScaffoldMessenger.of(context)
                ..removeCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    behavior: SnackBarBehavior.floating,
                    content: homeViewModel.currentAscend.isBoulder()
                        ? Text(AppLocalizations.of(context)!.home_page_added_boulder(ascendName))
                        : Text(AppLocalizations.of(context)!.home_page_added_route(ascendName)),
                    duration: const Duration(seconds: 2),
                  ),
                );
            }
          }
        },
        icon: const Icon(Icons.save, color: Colors.white, size: 26),
        label: Text(
          AppLocalizations.of(context)!.general_save.toUpperCase(),
          style: Theme.of(context).textTheme.titleLarge!.copyWith(
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
        ),
      ),
    );
  }
}
