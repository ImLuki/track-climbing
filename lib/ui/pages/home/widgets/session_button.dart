import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:climbing_track/view_model/home_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

abstract class LocalConstants {
  static const double CIRCLE_WIDTH_VALUE = 220;
  static const double CIRCLE_HEIGHT_VALUE = 220;
  static const double CIRCLE_OPACITY = 0.8;
  static const double CIRCLE_END_OPACITY = 0.2;
  static const double CIRCLE_BORDER_RADIUS = 1000.0;
}

class SessionButton extends StatefulWidget {
  final HomeViewModel homeViewModel;

  const SessionButton({super.key, required this.homeViewModel});

  @override
  _SessionButtonState createState() => _SessionButtonState();
}

class _SessionButtonState extends State<SessionButton> with SingleTickerProviderStateMixin {
  final Duration animatedDuration = const Duration(milliseconds: Constants.STANDARD_ANIMATION_TIME);
  double _width = LocalConstants.CIRCLE_WIDTH_VALUE;
  double _height = LocalConstants.CIRCLE_HEIGHT_VALUE;
  BoxShape animatedShape = BoxShape.rectangle;
  Alignment animatedAlign = Alignment.center;
  double opacity = LocalConstants.CIRCLE_OPACITY;
  BorderRadius _borderRadius = const BorderRadius.all(Radius.circular(LocalConstants.CIRCLE_BORDER_RADIUS));
  List<BoxShadow> _boxShadow = [
    const BoxShadow(
      color: Colors.black26,
      blurRadius: 5.0,
      offset: Offset(5.0, 5.0),
      spreadRadius: 3.0,
    )
  ];

  @override
  Widget build(BuildContext context) => AnimatedAlign(
        duration: animatedDuration,
        alignment: animatedAlign,
        child: AnimatedContainer(
          width: _width == 0 ? MediaQuery.of(context).size.width : _width,
          height: _height,
          decoration: BoxDecoration(
            shape: animatedShape,
            color: AppColors.accentColor.withOpacity(opacity),
            boxShadow: _boxShadow,
            borderRadius: _borderRadius,
          ),
          duration: animatedDuration,
          curve: Curves.easeInBack,
          onEnd: widget.homeViewModel.onInitSession,
          child: InkWell(
            onTap: _startAnimation,
            child: Center(
              child: getStartSessionText(),
            ),
          ),
        ),
      );

  Widget getStartSessionText() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          AppLocalizations.of(context)!.home_page_session_start.toUpperCase(),
          style: Theme.of(context).textTheme.displaySmall!.copyWith(
                color: Colors.white,
                letterSpacing: 0.0,
                wordSpacing: 0.0,
                height: 0.0,
              ),
        ),
        Text(
          AppLocalizations.of(context)!.home_page_session.toUpperCase(),
          style: Theme.of(context).textTheme.titleLarge!.copyWith(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: AppColors.textSecondary,
                letterSpacing: 0.0,
                wordSpacing: 0.0,
                height: 0.0,
              ),
        )
      ],
    );
  }

  void _startAnimation() {
    setState(
      () {
        _width = MediaQuery.of(context).size.width;
        _height = MediaQuery.of(context).size.height;
        animatedShape = BoxShape.rectangle;
        animatedAlign = Alignment.topCenter;
        opacity = LocalConstants.CIRCLE_END_OPACITY;
        _borderRadius = BorderRadius.zero;
        _boxShadow = [];
      },
    );
  }
}
