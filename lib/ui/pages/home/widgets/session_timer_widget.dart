import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/active_session.dart';
import 'package:climbing_track/ui/pages/home/widgets/timer_widget.dart';
import 'package:climbing_track/view_model/home_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SessionTimerWidget extends StatefulWidget {
  final HomeViewModel homeViewModel;

  const SessionTimerWidget({super.key, required this.homeViewModel});

  @override
  State<SessionTimerWidget> createState() => _SessionTimerWidgetState();
}

class _SessionTimerWidgetState extends State<SessionTimerWidget> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<HomeViewModel>(
      create: (context) => widget.homeViewModel,
      builder: (context, child) => buildTimerRow(context, child),
      child: TimerWidget(
        getStartTime: () => ActiveSession().activeSession?.startDate ?? DateTime.now(),
      ),
    );
  }

  Widget buildTimerRow(BuildContext context, Widget? child) {
    DateTime? lastGoTime = context.select<HomeViewModel, DateTime?>((HomeViewModel h) => h.lastGoTime);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        if (child != null) child,
        if (DataStore().settingsDataStore.showTimerBetweenGoes) ...[
          const SizedBox(width: 6.0),
          Visibility(
            visible: lastGoTime == null,
            child: const Text("(---)"),
          ),
          Visibility(
            visible: lastGoTime != null,
            maintainState: true,
            child: TimerWidget(
              prefix: "(",
              suffix: ")",
              textStyle: Theme.of(context).textTheme.bodyMedium,
              getStartTime: () => lastGoTime ?? DateTime.now(),
            ),
          ),
        ],
      ],
    );
  }
}
