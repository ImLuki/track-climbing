import 'dart:math';

import 'package:climbing_track/backend/util/custom_timer.dart';
import 'package:flutter/material.dart';

class TimerWidget extends StatefulWidget {
  const TimerWidget({
    super.key,
    this.suffix = "",
    this.prefix = "",
    this.textStyle,
    required this.getStartTime,
  });

  final String suffix;
  final String prefix;
  final TextStyle? textStyle;
  final DateTime Function() getStartTime;

  @override
  _TimerWidgetState createState() => _TimerWidgetState();
}

class _TimerWidgetState extends State<TimerWidget> with WidgetsBindingObserver {
  CustomTimer customTimer = CustomTimer();
  String timeString = "";

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    customTimer.startTimer(callBack);
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
    customTimer.stopTimer();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      customTimer.stopTimer();
      setState(() {
        customTimer = CustomTimer();
        customTimer.startTimer(callBack);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    String timeString = calculateTimeString(DateTime.now().difference(widget.getStartTime()).inSeconds);

    return Text(
      widget.prefix + timeString + widget.suffix,
      style: widget.textStyle ?? Theme.of(context).textTheme.bodyLarge,
    );
  }

  void callBack(int tick) {
    calculateTimeString(tick);
    setState(() {});
  }

  String calculateTimeString(secondsSinceStart) {
    int tick = max(secondsSinceStart, 0);

    String dayStr = "00";
    String hoursStr = "00";
    String minutesStr = "00";
    String secondsStr = "00";

    if ((tick / (60 * 60)) > 100) {
      dayStr = ((tick / (60 * 60 * 24))).floor().toString().padLeft(2, '0');
      hoursStr = ((tick / (60 * 60)) % 24).floor().toString().padLeft(2, '0');
    } else {
      hoursStr = ((tick / (60 * 60)) % 100).floor().toString().padLeft(2, '0');
    }
    minutesStr = ((tick / 60) % 60).floor().toString().padLeft(2, '0');
    secondsStr = (tick % 60).floor().toString().padLeft(2, '0');

    String timeString =
        dayStr != "00" ? "$dayStr:$hoursStr:$minutesStr:$secondsStr" : "$hoursStr:$minutesStr:$secondsStr";

    return timeString;
  }
}
