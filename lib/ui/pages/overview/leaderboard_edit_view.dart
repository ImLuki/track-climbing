import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/enums/enums.dart';
import 'package:flutter/material.dart';
import 'package:showcaseview/showcaseview.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LeaderboardEditPageWrapper extends StatelessWidget {
  final bool isIndoor;

  const LeaderboardEditPageWrapper({super.key, required this.isIndoor});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ShowCaseWidget(
        builder: (context) => LeaderboardEditPage(isIndoor: isIndoor),
      ),
    );
  }
}

class LeaderboardEditPage extends StatefulWidget {
  final bool isIndoor;

  const LeaderboardEditPage({super.key, required this.isIndoor});

  @override
  State<LeaderboardEditPage> createState() => _LeaderboardEditPageState();
}

class _LeaderboardEditPageState extends State<LeaderboardEditPage> {
  late List<Classes> _items;
  late List<bool> _active;
  final GlobalKey _tipKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    _items = widget.isIndoor
        ? List.from(DataStore().settingsDataStore.overviewLeaderboardSortingIndoor)
        : List.from(DataStore().settingsDataStore.overviewLeaderboardSortingOutdoor);

    _active = widget.isIndoor
        ? List.from(DataStore().settingsDataStore.overviewLeaderboardActiveIndoor)
        : List.from(DataStore().settingsDataStore.overviewLeaderboardActiveOutdoor);
  }

  @override
  Widget build(BuildContext context) {
    //showing tooltip
    WidgetsBinding.instance.addPostFrameCallback((_) => _showToolTip());

    return PopScope(
      onPopInvoked: (_) async {
        if (widget.isIndoor) {
          DataStore().settingsDataStore.overviewLeaderboardSortingIndoor = _items;
          DataStore().settingsDataStore.overviewLeaderboardActiveIndoor = _active;
        } else {
          DataStore().settingsDataStore.overviewLeaderboardSortingOutdoor = _items;
          DataStore().settingsDataStore.overviewLeaderboardActiveOutdoor = _active;
        }
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.accentColor,
          leading: const BackButton(color: Colors.white),
          title: Text(
            AppLocalizations.of(context)!.overview_edit_leaderboard,
            style: const TextStyle(color: Colors.white, fontSize: 22),
          ),
          actions: [
            IconButton(
              color: Colors.white,
              tooltip: AppLocalizations.of(context)!.general_reset,
              onPressed: () {
                _items = Classes.values.toList();
                setState(() {});
                ScaffoldMessenger.of(context)
                  ..removeCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(
                      behavior: SnackBarBehavior.floating,
                      content: Text(AppLocalizations.of(context)!.chart_page_reset_chart_order),
                    ),
                  );
              },
              icon: const Icon(Icons.refresh),
            ),
          ],
        ),
        body: _createReorderableList(),
      ),
    );
  }

  Widget _createReorderableList() {
    return ReorderableListView(
      children: <Widget>[
        for (int index = 0; index < _items.length; index++)
          index == 0
              ? Showcase(
                  key: _tipKey,
                  description: AppLocalizations.of(context)!.chart_page_settings_tooltip,
                  tooltipPadding: const EdgeInsets.all(12.0),
                  child: _buildListTile(index),
                )
              : _buildListTile(index),
      ],
      onReorder: (int oldIndex, int newIndex) {
        setState(() {
          if (oldIndex < newIndex) {
            newIndex -= 1;
          }
          final Classes item = _items.removeAt(oldIndex);
          _items.insert(newIndex, item);
        });
      },
    );
  }

  Widget _buildListTile(int index) {
    return ListTile(
      key: Key('$index'),
      leading: Checkbox(
          value: _active[_items[index].index],
          onChanged: (bool? val) {
            _active[_items[index].index] = val ?? false;
            setState(() {});
          }),
      title: Row(
        children: [
          _items[index].getIcon(),
          const SizedBox(width: 20.0),
          Flexible(
            child: Text(
              _items[index].getClimbName(context),
              style: Theme.of(context).textTheme.titleMedium,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
          ),
          //  const SizedBox(width: 20.0),
        ],
      ),
      trailing: const Icon(Icons.menu),
    );
  }

  void _showToolTip() async {
    if (!DataStore().toolTipsDataStore.tooltipOverViewLeaderboardEditPage) return;

    await Future.delayed(const Duration(milliseconds: 750));

    if (mounted) {
      DataStore().toolTipsDataStore.tooltipOverViewLeaderboardEditPage = false;
      ShowCaseWidget.of(context).startShowCase([_tipKey]);
    }
  }
}
