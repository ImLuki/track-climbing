import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/overview_data_loader.dart';
import 'package:climbing_track/ui/pages/overview/overview_stats.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:showcaseview/showcaseview.dart';

class OverviewPage extends StatefulWidget {
  const OverviewPage({super.key});

  @override
  _OverviewPageState createState() => _OverviewPageState();
}

class _OverviewPageState extends State<OverviewPage> {
  static const int INITIALIZE_PAGE = 0;
  final PageController _controller = PageController(initialPage: INITIALIZE_PAGE);
  final GlobalKey _globalKey = GlobalKey();
  int currentPage = INITIALIZE_PAGE;
  late OverviewDataLoader _overviewDataLoader;
  bool loading = true;

  @override
  void initState() {
    super.initState();
    _overviewDataLoader = OverviewDataLoader(
      onCompleted: () {
        if (mounted) {
          loading = false;
          setState(() {});
        }
      },
    );
    _overviewDataLoader.init();
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => _showToolTip());

    return Scaffold(
      appBar: AppBar(
        title: getTitle(),
      ),
      body: _createPageView(),
    );
  }

  Widget _createPageView() {
    return PageView(
      onPageChanged: (index) => setState(() {
        currentPage = index;
      }),
      controller: _controller,
      children: [
        OverViewStats(
          key: UniqueKey(),
          dataMap: _overviewDataLoader.calculatedData[0],
          overAllData: _overviewDataLoader.overAllData[0],
          speedData: _overviewDataLoader.speedData,
          outdoor: false,
          loading: loading,
        ),
        OverViewStats(
          key: UniqueKey(),
          dataMap: _overviewDataLoader.calculatedData[1],
          overAllData: _overviewDataLoader.overAllData[1],
          speedData: _overviewDataLoader.speedData,
          outdoor: true,
          loading: loading,
        ),
      ],
    );
  }

  Widget getTitle() {
    switch (currentPage) {
      case 0:
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Flexible(fit: FlexFit.tight, child: Text("")),
            Flexible(
              fit: FlexFit.tight,
              child: Text(
                AppLocalizations.of(context)!.general_indoor.toUpperCase(),
                textAlign: TextAlign.center,
              ),
            ),
            Flexible(
              fit: FlexFit.tight,
              child: InkWell(
                borderRadius: BorderRadius.circular(20.0),
                onTap: () => _controller.animateToPage(
                  1,
                  curve: Curves.easeIn,
                  duration: const Duration(milliseconds: 200),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(right: 4.0, top: 4.0, bottom: 4.0),
                  child: Showcase(
                    key: _globalKey,
                    description: AppLocalizations.of(context)!.overview_page_tooltip,
                    targetPadding: const EdgeInsets.fromLTRB(0, 6, 8, 6),
                    targetBorderRadius: const BorderRadius.all(Radius.circular(12)),
                    tooltipPadding: const EdgeInsets.all(12.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          AppLocalizations.of(context)!.general_outdoor.toLowerCase(),
                          style: const TextStyle(fontSize: 16),
                          textAlign: TextAlign.end,
                        ),
                        const Icon(Icons.arrow_right)
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      case 1:
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              fit: FlexFit.tight,
              child: InkWell(
                borderRadius: BorderRadius.circular(20.0),
                onTap: () =>
                    _controller.animateToPage(0, curve: Curves.easeIn, duration: const Duration(milliseconds: 200)),
                child: Padding(
                  padding: const EdgeInsets.only(left: 4.0, top: 4.0, bottom: 4.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const Icon(Icons.arrow_left),
                      Text(
                        AppLocalizations.of(context)!.general_indoor.toLowerCase(),
                        style: const TextStyle(fontSize: 16),
                        textAlign: TextAlign.start,
                      )
                    ],
                  ),
                ),
              ),
            ),
            Flexible(
              fit: FlexFit.tight,
              child: Text(
                AppLocalizations.of(context)!.general_outdoor.toUpperCase(),
                style: const TextStyle(fontSize: 22),
                textAlign: TextAlign.center,
              ),
            ),
            const Flexible(
              fit: FlexFit.tight,
              child: Text(""),
            ),
          ],
        );
    }
    return Container();
  }

  void _showToolTip() async {
    if (!DataStore().toolTipsDataStore.tooltipOverviewPage) return;

    await await Future.delayed(const Duration(milliseconds: 500));
    if (mounted) {
      DataStore().toolTipsDataStore.tooltipOverviewPage = false;
      ShowCaseWidget.of(context).startShowCase([_globalKey]);
    }
  }
}
