import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums/enums.dart';
import 'package:climbing_track/backend/model/enums/height_measurement.dart';
import 'package:climbing_track/backend/overview_data_loader.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:climbing_track/ui/dialog/info_dialogs.dart';
import 'package:climbing_track/ui/pages/overview/leaderboard_edit_view.dart';
import 'package:climbing_track/ui/util/custom_page_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OverViewStats extends StatefulWidget {
  final Map<Classes, Map<StyleType, Map<String, dynamic>?>> dataMap;
  final Map<String, dynamic> overAllData;
  final List<Map<String, dynamic>> speedData;
  final bool outdoor;
  final bool loading;

  const OverViewStats({
    super.key,
    required this.dataMap,
    required this.overAllData,
    required this.speedData,
    required this.outdoor,
    this.loading = false,
  });

  @override
  State<OverViewStats> createState() => _OverViewStatsState();
}

class _OverViewStatsState extends State<OverViewStats> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.zero,
      child: _createOverview(context),
    );
  }

  Widget _createOverview(BuildContext context) {
    List<Classes> classList = widget.outdoor
        ? DataStore().settingsDataStore.overviewLeaderboardSortingOutdoor
        : DataStore().settingsDataStore.overviewLeaderboardSortingIndoor;

    List<bool> activeClasses = widget.outdoor
        ? DataStore().settingsDataStore.overviewLeaderboardActiveOutdoor
        : DataStore().settingsDataStore.overviewLeaderboardActiveIndoor;

    return Column(
      children: [
        if (widget.loading) const LinearProgressIndicator(),
        if (widget.loading) const SizedBox(height: Constants.STANDARD_PADDING - 4),
        if (!widget.loading) const SizedBox(height: 8.0),
        _paddingWidget(_createSummaryWidget(context)),
        const SizedBox(height: 16),
        _divider(),
        _bestAscendsHeader(context),
        ...List.generate(
          classList.length,
          (index) => _bestAscendsExpansionTile(
            context: context,
            type: classList[index],
            visible: activeClasses[classList[index].index],
          ),
        ),
        const SizedBox(height: 60),
      ],
    );
  }

  Widget _bestAscendsHeader(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 14.0, 20.0, 2.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Row(
              children: [
                IconButton(
                  onPressed: () => InfoDialog().showLeaderboardInfo(context),
                  icon: const Icon(Icons.info_outline),
                ),
                Flexible(
                  child: Text(
                    AppLocalizations.of(context)!.overview_leaderboard,
                    style: const TextStyle(fontSize: 20, color: Colors.black87),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(width: 20.0),
          IconButton(onPressed: () => _pushLeaderboardPageEditDialog(context), icon: const Icon(Icons.edit))
        ],
      ),
    );
  }

  Widget _divider() {
    return const Divider(
      height: 2,
      thickness: 2.0,
      indent: Constants.STANDARD_PADDING,
      endIndent: Constants.STANDARD_PADDING,
    );
  }

  Widget _paddingWidget(Widget child) {
    return Container(
      padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
      alignment: Alignment.center,
      child: child,
    );
  }

  Widget _createSummaryWidget(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          opacity: widget.outdoor ? 0.4 : 0.2,
          image: widget.outdoor
              ? const AssetImage('assets/images/background2.jpg')
              : const AssetImage('assets/images/background_indoor2.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        children: [
          SizedBox(
            height: 170,
            child: Row(
              children: [
                Expanded(
                  flex: 3,
                  child: _createSummaryContainer(
                    context: context,
                    title: AppLocalizations.of(context)!.overview_total_ascents,
                    maxLines: 2,
                    content: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.overAllData[OverviewDataLoader.TOTAL_ASCENDS_KEY].toString(),
                          style: const TextStyle(fontSize: 28),
                          overflow: TextOverflow.ellipsis,
                        ),
                        const SizedBox(height: 1.0),
                        _differenceIndicator(
                          widget.overAllData[OverviewDataLoader.ASCENDS_LAST_30_DAYS_KEY],
                          context,
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Column(
                    children: [
                      Flexible(
                        flex: 1,
                        child: _createSummaryContainer(
                          context: context,
                          title: AppLocalizations.of(context)!.sessions_title,
                          padding: const EdgeInsets.all(10.0),
                          content: Row(
                            children: [
                              Flexible(
                                child: Text(
                                  widget.overAllData[OverviewDataLoader.TOTAL_SESSIONS_KEY].toString(),
                                  style: const TextStyle(fontSize: 24),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: _createSummaryContainer(
                          context: context,
                          title: AppLocalizations.of(context)!.style_onsight,
                          padding: const EdgeInsets.all(10.0),
                          content: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const SizedBox(
                                height: 18.0,
                                width: 18.0,
                                child: CircularProgressIndicator(
                                  value: 0.42,
                                  color: Colors.green,
                                  backgroundColor: Colors.black26,
                                ),
                              ),
                              const SizedBox(width: 8.0),
                              Flexible(
                                child: Text(
                                  widget.overAllData[OverviewDataLoader.TOTAL_SPORT_ASCENDS_KEY] == 0
                                      ? "0%"
                                      : "${(widget.overAllData[OverviewDataLoader.TOTAL_ONSIGHT_KEY] / widget.overAllData[OverviewDataLoader.TOTAL_SPORT_ASCENDS_KEY] * 100).toStringAsFixed(1)}%",
                                  style: const TextStyle(fontSize: 22),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 88,
            child: _createSummaryContainer(
              context: context,
              padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 14.0),
              title: "${AppLocalizations.of(context)!.overview_total_meters} "
                  "${DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET ? AppLocalizations.of(context)!.general_feet(2) : AppLocalizations.of(context)!.general_meter(2)}",
              content: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      const Icon(Icons.landscape, color: AppColors.color1, size: 30),
                      const SizedBox(width: 8.0),
                      Text(
                        Util.getHeightWithShortUnit(widget.overAllData[OverviewDataLoader.TOTAL_HEIGHT_KEY]),
                        style: const TextStyle(fontSize: 22, height: 0),
                        overflow: TextOverflow.ellipsis,
                      ),
                      const SizedBox(width: 8.0),
                    ],
                  ),
                  Flexible(
                    child: _differenceIndicator(
                      widget.overAllData[OverviewDataLoader.HEIGHT_LAST_30_DAYS_KEY],
                      context,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _createSummaryContainer({
    required BuildContext context,
    required String title,
    required Widget content,
    EdgeInsets padding = const EdgeInsets.all(14.0),
    int maxLines = 1,
  }) {
    return Container(
      padding: padding,
      // white border to have white padding between container
      foregroundDecoration: BoxDecoration(
        border: Border.all(color: Theme.of(context).colorScheme.surfaceBright, width: 4.0),
      ),
      decoration: BoxDecoration(
        // color: Colors.black.withOpacity(0.05),
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(12.0),
        border: Border.all(color: Theme.of(context).colorScheme.surfaceBright, width: 6),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: TextStyle(color: Colors.black.withOpacity(0.65), fontSize: 18),
            overflow: TextOverflow.ellipsis,
            maxLines: maxLines,
          ),
          content,
        ],
      ),
    );
  }

  Widget _differenceIndicator(num value, BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          "+ ${(value * 10) % 1 == 0 ? value.toInt() : value} ",
          style: TextStyle(
            color: value == 0 ? Colors.red : Colors.green,
            fontSize: 14,
            fontWeight: FontWeight.w500,
          ),
        ),
        Flexible(
          child: Text(
            AppLocalizations.of(context)!.overview_last_30_days,
            style: const TextStyle(color: Colors.black54, fontSize: 14),
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ],
    );
  }

  Widget _createBestAscendContainer(BuildContext context, Classes type, StyleType style) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4.0),
      child: InkWell(
        splashColor: Colors.black12,
        highlightColor: Colors.black12,
        borderRadius: BorderRadius.circular(12.0),
        onTap: () {
          if (widget.dataMap[type]![style] != null) {
            InfoDialog().showAscendDialog(context, Ascend.fromMap(widget.dataMap[type]![style]!));
          }
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 3.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Tooltip(
                message: style.toTranslatedString(context),
                child: SizedBox(
                  width: 25.0,
                  height: 25.0,
                  child: Icon(style.getIcon(), size: style.getIconSize()),
                ),
              ),
              const SizedBox(width: 20.0),
              _createBestAscendListTile(context, type, style),
            ],
          ),
        ),
      ),
    );
  }

  Widget _createBestSpeedAscendContainer(BuildContext context, int index) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4.0),
      child: InkWell(
        splashColor: Colors.black12,
        highlightColor: Colors.black12,
        borderRadius: BorderRadius.circular(12.0),
        onTap: () {
          if (widget.speedData.length > index) {
            InfoDialog().showAscendDialog(context, Ascend.fromMap(widget.speedData[index]));
          }
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 3.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 25.0,
                height: 25.0,
                child: Text(
                  "${index + 1}.",
                  style: const TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500),
                  textAlign: TextAlign.center,
                ),
              ),
              const SizedBox(width: 20.0),
              _createBestAscendListTile(context, Classes.SPEED, StyleType.NONE, index: index),
            ],
          ),
        ),
      ),
    );
  }

  Widget _createBestAscendListTile(BuildContext context, Classes type, StyleType style, {index = 0}) {
    Map<String, dynamic>? ascendData = type == Classes.SPEED
        ? widget.speedData.length > index
            ? widget.speedData[index]
            : null
        : widget.dataMap[type]?[style];
    if (ascendData == null) {
      return Expanded(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text("---", style: Theme.of(context).textTheme.bodyLarge),
          ],
        ),
      );
    }

    TextStyle textStyle = Theme.of(context).textTheme.bodyMedium!.copyWith(
          fontWeight: FontWeight.w500,
          letterSpacing: 0.0,
          wordSpacing: 0.0,
          height: 0.0,
        );

    Ascend ascend = Ascend.fromMap(ascendData);
    String grade = GradeManager().getGradeMapper(type: ascend.type).gradeFromId(ascend.gradeID);

    return Flexible(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          type == Classes.SPEED
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Flexible(
                      child: Text(
                        "${(widget.speedData[index][DBController.speedTime] / 1000).toStringAsFixed(2)} "
                        "${AppLocalizations.of(context)!.util_seconds}",
                        style: textStyle,
                      ),
                    ),
                  ],
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Flexible(
                      child: Text(
                        Util.getAscendNameWithoutPrefix(
                          ascend.name,
                          context,
                          speedType: ascend.speedType,
                          type: ascend.type,
                        ),
                        overflow: TextOverflow.ellipsis,
                        style: textStyle,
                      ),
                    ),
                    Text(" ($grade)", style: textStyle),
                  ],
                ),
          Text(
            Util.getLocationDateString(
              context,
              DateTime.parse(ascendData[DBController.sessionTimeStart]),
              ascend.location!,
            ),
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                  fontSize: 12,
                  letterSpacing: 0.0,
                  wordSpacing: 0.0,
                  height: 0.0,
                ),
          ),
        ],
      ),
    );
  }

  Widget _bestAscendsExpansionTile({
    required BuildContext context,
    required Classes type,
    required bool visible,
  }) {
    return Visibility(
      visible: visible,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 4.0),
        child: Theme(
          data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
          child: ExpansionTile(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            collapsedShape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            collapsedBackgroundColor: Colors.black.withOpacity(0.05),
            backgroundColor: Colors.black.withOpacity(0.05),
            textColor: Colors.black,
            iconColor: Colors.black54,
            tilePadding: const EdgeInsets.symmetric(horizontal: 12.0),
            childrenPadding: const EdgeInsets.only(bottom: 15.0),
            initiallyExpanded: widget.overAllData[OverviewDataLoader.EXPANDED_VALUES_KEY][type],
            leading: type.getIcon(),
            title: Text(
              type.getClimbName(context),
              style: Theme.of(context).textTheme.titleMedium?.copyWith(fontSize: 18),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
            onExpansionChanged: (expanded) => SharedPreferences.getInstance().then(
              (prefs) => prefs.setBool(
                "expandend_value_${widget.outdoor ? "out" : "in"}_${type.name.toLowerCase()}",
                expanded,
              ),
            ),
            children: type == Classes.SPEED
                ? List.generate(
                    3,
                    (index) => _createBestSpeedAscendContainer(context, index),
                  )
                : List.generate(
                    type.getStyles().length,
                    (index) => _createBestAscendContainer(context, type, type.getStyles()[index]),
                  ),
          ),
        ),
      ),
    );
  }

  void _pushLeaderboardPageEditDialog(BuildContext context) {
    Navigator.push(
      context,
      CustomPageRoute.build(
        builder: (BuildContext context) => LeaderboardEditPageWrapper(isIndoor: !widget.outdoor),
      ),
    ).then((value) {
      if (context.mounted) setState(() {});
    });
  }
}
