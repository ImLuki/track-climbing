import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SessionEditChangesInfoDialog {
  final List<String> changedParams;
  final List<Ascend> editedAscents;
  final List<Ascend> addedAscents;
  final List<Ascend> deletedAscents;
  final Function()? onDiscard;

  SessionEditChangesInfoDialog({
    required this.changedParams,
    required this.editedAscents,
    required this.addedAscents,
    required this.deletedAscents,
    this.onDiscard,
  });

  Future<void> showInfoDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(AppLocalizations.of(context)!.session_edit_page_unsaved_changes),
          contentTextStyle: Theme.of(context).textTheme.bodyLarge,
          contentPadding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 20.0),
          content: SingleChildScrollView(
            child: _content(context),
          ),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.general_back),
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text(AppLocalizations.of(context)!.session_edit_page_discard_changes),
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
                onDiscard?.call();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _content(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(AppLocalizations.of(context)!.session_edit_page_unsaved_changes_info),
        const SizedBox(height: 8.0),
        const Divider(),
        if (changedParams.isNotEmpty) ..._changedParametersWidgets(context),
        if (editedAscents.isNotEmpty) ..._editedAscentsWidgets(context),
        if (addedAscents.isNotEmpty) ..._addedAscentsWidgets(context),
        if (deletedAscents.isNotEmpty) ..._deletedAscentsWidgets(context),
        const SizedBox(height: 20.0),
      ],
    );
  }

  List<Widget> _changedParametersWidgets(BuildContext context) {
    return [
      const SizedBox(height: 12.0),
      Text(
        AppLocalizations.of(context)!.session_edit_page_discard_changes_session,
        style: const TextStyle(fontWeight: FontWeight.w500),
      ),
      const SizedBox(height: 4.0),
      ...List.generate(
        changedParams.length,
        (index) => Text(
          "    ● ${changedParams[index]}",
          overflow: TextOverflow.ellipsis,
        ),
      ),
    ];
  }

  List<Widget> _editedAscentsWidgets(BuildContext context) {
    return [
      const SizedBox(height: 12.0),
      Text(
        AppLocalizations.of(context)!.session_edit_page_discard_changes_edited_ascents,
        style: const TextStyle(fontWeight: FontWeight.w500),
      ),
      const SizedBox(height: 4.0),
      ...List.generate(
        editedAscents.length,
        (index) => Text(
          "    ● ${Util.getAscendName(
            context,
            editedAscents[index].name,
            type: editedAscents[index].type,
            speedType: editedAscents[index].speedType,
          )}",
          overflow: TextOverflow.ellipsis,
        ),
      ),
    ];
  }

  List<Widget> _addedAscentsWidgets(BuildContext context) {
    return [
      const SizedBox(height: 12.0),
      Text(
        AppLocalizations.of(context)!.session_edit_page_discard_changes_added_ascents,
        style: const TextStyle(fontWeight: FontWeight.w500),
      ),
      const SizedBox(height: 4.0),
      ...List.generate(
        addedAscents.length,
        (index) => Text(
          "    ● ${Util.getAscendName(
            context,
            addedAscents[index].name,
            type: addedAscents[index].type,
            speedType: addedAscents[index].speedType,
          )}",
          overflow: TextOverflow.ellipsis,
        ),
      ),
    ];
  }

  List<Widget> _deletedAscentsWidgets(BuildContext context) {
    return [
      const SizedBox(height: 12.0),
      Text(
        AppLocalizations.of(context)!.session_edit_page_discard_changes_deleted_ascents,
        style: const TextStyle(fontWeight: FontWeight.w500),
      ),
      const SizedBox(height: 4.0),
      ...List.generate(
        deletedAscents.length,
        (index) => Text(
          "    ● ${Util.getAscendName(
            context,
            deletedAscents[index].name,
            type: deletedAscents[index].type,
            speedType: deletedAscents[index].speedType,
          )}",
          overflow: TextOverflow.ellipsis,
        ),
      ),
    ];
  }
}
