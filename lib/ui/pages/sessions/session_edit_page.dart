import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/location.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:climbing_track/ui/pages/sessions/session_edit_changes_info_dialog.dart';
import 'package:climbing_track/ui/widgets/reorderable_ascents_list.dart';
import 'package:climbing_track/view_model/session_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:latlong2/latlong.dart';
import 'package:toggle_switch/toggle_switch.dart';

class SessionEditPage extends StatefulWidget {
  final SessionViewModel sessionViewModel;

  const SessionEditPage({super.key, required this.sessionViewModel});

  @override
  _SessionEditPageState createState() => _SessionEditPageState();
}

class _SessionEditPageState extends State<SessionEditPage> {
  final GlobalKey _locationCommentTextFieldKey = GlobalKey();
  final GlobalKey _sessionCommentTextFieldKey = GlobalKey();
  final TextEditingController _locationInfoTextFieldController = TextEditingController();
  final TextEditingController _sessionCommentTextFieldController = TextEditingController();
  final FocusNode _locationInfoTextFieldFocus = FocusNode();
  final FocusNode _sessionCommentTextFieldFocus = FocusNode();
  late final Future<void> _future;

  @override
  void initState() {
    super.initState();
    widget.sessionViewModel.initEditFunctions();
    _future = widget.sessionViewModel.loadEditableAscentsList();
    _locationInfoTextFieldController.text = widget.sessionViewModel.editedLocation.locationComment;
    _sessionCommentTextFieldController.text = widget.sessionViewModel.editedSession.comment;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      floatingActionButton: FloatingActionButton(
        tooltip: AppLocalizations.of(context)!.general_save,
        backgroundColor: Colors.black,
        child: const Icon(
          Icons.save,
          size: 32,
          color: Colors.white,
        ),
        onPressed: () => widget.sessionViewModel.onSave(context),
      ),
      body: GestureDetector(
        onTapDown: (tapDownDetails) => FocusScope.of(context).unfocus(),
        child: _content(),
      ),
    );
  }

  PreferredSizeWidget _appBar() {
    return AppBar(
      title: Text(
        AppLocalizations.of(context)!.sessions_page_edit_session,
      ),
      actions: [
        IconButton(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          tooltip: AppLocalizations.of(context)!.general_delete,
          icon: const Icon(Icons.delete_outline),
          onPressed: () => _showDeleteConfirmation(context),
        ),
      ],
      leading: IconButton(
        icon: const Icon(Icons.close),
        tooltip: AppLocalizations.of(context)!.general_cancel,
        onPressed: _onAbort,
      ),
    );
  }

  Future<void> _showDeleteConfirmation(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          titleTextStyle: Theme.of(context).textTheme.titleLarge,
          scrollable: true,
          contentTextStyle: Theme.of(context).textTheme.bodyMedium,
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Icon(Icons.warning_amber_rounded, color: Colors.orange, size: 42),
              const SizedBox(width: 8.0),
              Flexible(child: Text(AppLocalizations.of(context)!.sessions_page_confirm_deletion)),
            ],
          ),
          content: SingleChildScrollView(
            child: Text(AppLocalizations.of(context)!.sessions_page_delete_info),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () => {
                Navigator.of(context).pop(false),
              },
              child: Text(
                AppLocalizations.of(context)!.general_cancel,
              ),
            ),
            TextButton(
              onPressed: () => widget.sessionViewModel.deleteSession(context),
              child: Text(AppLocalizations.of(context)!.general_confirm),
            ),
          ],
        );
      },
    );
  }

  Future<bool?> _showLastAscendDeleteConfirmation(BuildContext context) async {
    return showDialog<bool?>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          titleTextStyle: Theme.of(context).textTheme.titleLarge,
          scrollable: true,
          contentTextStyle: Theme.of(context).textTheme.bodyMedium,
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Icon(Icons.warning_amber_rounded, color: Colors.orange, size: 42),
              const SizedBox(width: 8.0),
              Text(AppLocalizations.of(context)!.sessions_page_delete_session),
            ],
          ),
          content: SingleChildScrollView(
            child: Text(
              AppLocalizations.of(context)!.sessions_page_delete_session_warning,
              overflow: TextOverflow.clip,
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: Text(
                AppLocalizations.of(context)!.general_cancel,
              ),
            ),
            TextButton(
              onPressed: () => widget.sessionViewModel.deleteSession(context),
              child: Text(AppLocalizations.of(context)!.general_confirm),
            ),
          ],
        );
      },
    );
  }

  Widget _content() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(Constants.STANDARD_PADDING),
        child: Column(
          children: [
            const SizedBox(height: 12.0),
            ..._indoorSwitch(),
            const SizedBox(height: Constants.STANDARD_PADDING),
            _date(),
            const SizedBox(height: Constants.STANDARD_PADDING),
            ..._duration(),
            ..._sessionComment(),
            const SizedBox(height: Constants.STANDARD_PADDING),
            _locationName(),
            const SizedBox(height: Constants.STANDARD_PADDING),
            _position(),
            const SizedBox(height: Constants.STANDARD_PADDING),
            ..._locationCommentField(),
            const Divider(thickness: 2.0),
            const SizedBox(height: 12.0),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                AppLocalizations.of(context)!.home_page_ascents,
                style: Theme.of(context).textTheme.titleLarge,
              ),
            ),
            const SizedBox(height: 6.0),
            _ascendList(),
            const SizedBox(height: 120.0),
          ],
        ),
      ),
    );
  }

  List<Widget> _indoorSwitch() {
    return [
      ToggleSwitch(
        initialLabelIndex: widget.sessionViewModel.editedLocation.outdoor ? 1 : 0,
        minWidth: MediaQuery.of(context).size.width * 0.35,
        minHeight: 45.0,
        cornerRadius: 20.0,
        activeBgColor: const [Colors.green],
        activeFgColor: Colors.white,
        inactiveBgColor: AppColors.accentColor2,
        inactiveFgColor: Colors.white,
        labels: [
          AppLocalizations.of(context)!.general_indoor.toUpperCase(),
          AppLocalizations.of(context)!.general_outdoor.toUpperCase(),
        ],
        onToggle: (index) {
          widget.sessionViewModel.editedLocation.outdoor = index == 1;
        },
        totalSwitches: 2,
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      const Divider(thickness: 2.0),
    ];
  }

  List<Widget> _locationCommentField() {
    return [
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          "${AppLocalizations.of(context)!.session_edit_page_location_info}:",
          style: Theme.of(context).textTheme.titleMedium,
          overflow: TextOverflow.ellipsis,
        ),
      ),
      const SizedBox(height: 12.0),
      Theme(
        data: ThemeData.from(
          colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor),
          useMaterial3: false,
        ),
        child: TextFormField(
          key: _locationCommentTextFieldKey,
          controller: _locationInfoTextFieldController,
          focusNode: _locationInfoTextFieldFocus,
          maxLines: null,
          minLines: 2,
          textAlign: TextAlign.start,
          textAlignVertical: TextAlignVertical.center,
          style: const TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
          onChanged: (value) {
            if (value.endsWith('\n') && value.length > widget.sessionViewModel.editedLocation.locationComment.length) {
              value += "● ";
              _locationInfoTextFieldController.text = value;
            }
            widget.sessionViewModel.editedLocation.locationComment = value.trim();
          },
          onTap: () {
            _locationInfoTextFieldController.text =
                _locationInfoTextFieldController.text == "" ? "● " : _locationInfoTextFieldController.text;
            Scrollable.ensureVisible(
              _locationCommentTextFieldKey.currentContext!,
              duration: const Duration(milliseconds: 400),
              curve: Curves.easeInOut,
            );
          },
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            isDense: true,
            label: Text(
              AppLocalizations.of(context)!.location_comment_hint,
              style: const TextStyle(fontSize: 18, color: AppColors.accentColor2, overflow: TextOverflow.ellipsis),
              maxLines:
                  _locationInfoTextFieldController.text.isNotEmpty || _locationInfoTextFieldFocus.hasFocus ? 1 : 2,
            ),
            fillColor: AppColors.accentColor,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
              borderSide: const BorderSide(),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
              borderRadius: BorderRadius.circular(
                Constants.STANDARD_BORDER_RADIUS,
              ),
            ),
          ),
        ),
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
    ];
  }

  Widget _date() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Icon(Icons.calendar_today_outlined),
        const SizedBox(width: 10.0),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "${AppLocalizations.of(context)!.general_date}:",
                  style: Theme.of(context).textTheme.titleMedium,
                ),
              ),
              const SizedBox(height: 6.0),
              InkWell(
                onTap: () => _selectDate(
                  context,
                  widget.sessionViewModel.editedSession.startDate,
                ),
                child: Text(
                  _dateFormatter(widget.sessionViewModel.editedSession.startDate),
                  style: Theme.of(context).textTheme.bodyLarge,
                  overflow: TextOverflow.clip,
                ),
              ),
              const SizedBox(height: 4),
              InkWell(
                onTap: () => _selectTime(
                  context,
                  TimeOfDay.fromDateTime(widget.sessionViewModel.editedSession.startDate),
                ),
                child: Text(
                  DateFormat.jm().format(widget.sessionViewModel.editedSession.startDate),
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
              ),
            ],
          ),
        ),
        Column(
          children: [
            ElevatedButton.icon(
              onPressed: () => _selectDate(
                context,
                widget.sessionViewModel.editedSession.startDate,
              ),
              icon: const Icon(Icons.edit, size: 22.0),
              label: Text(AppLocalizations.of(context)!.general_date),
              style: ElevatedButton.styleFrom(
                foregroundColor: Colors.black,
                minimumSize: const Size(48, 32),
                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
              ),
            ),
            const SizedBox(height: 8.0),
            ElevatedButton.icon(
              onPressed: () => _selectTime(
                context,
                TimeOfDay.fromDateTime(widget.sessionViewModel.editedSession.startDate),
              ),
              icon: const Icon(Icons.edit, size: 22.0),
              label: Text(AppLocalizations.of(context)!.sessions_page_edit_session_time),
              style: ElevatedButton.styleFrom(
                foregroundColor: Colors.black,
                minimumSize: const Size(48, 32),
                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
              ),
            ),
          ],
        ),
        const SizedBox(width: 4.0),
      ],
    );
  }

  List<Widget> _duration() {
    return [
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Icon(Icons.timer_outlined),
          const SizedBox(width: 10.0),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "${AppLocalizations.of(context)!.sessions_page_duration}:",
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                ),
                const SizedBox(height: 6.0),
                InkWell(
                  onTap: () => _selectDuration(context),
                  child: Text(
                    "${widget.sessionViewModel.editedSession.getDuration()} h",
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                ),
              ],
            ),
          ),
          Column(
            children: [
              const SizedBox(height: 8.0), // added so that button is bottom aligned
              ElevatedButton.icon(
                onPressed: () => _selectDuration(context).then((value) => {if (value) setState(() {})}),
                icon: const Icon(Icons.edit, size: 22.0),
                label: Text(AppLocalizations.of(context)!.sessions_page_duration),
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.black,
                  minimumSize: const Size(48, 40),
                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                ),
              ),
            ],
          ),
          const SizedBox(width: 4.0),
        ],
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      // const Divider(thickness: 2.0),
    ];
  }

  List<Widget> _sessionComment() {
    return [
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.end_session_session_comment,
          style: Theme.of(context).textTheme.titleMedium,
          overflow: TextOverflow.ellipsis,
        ),
      ),
      const SizedBox(height: 12.0),
      Theme(
        data: ThemeData.from(
          colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor),
          useMaterial3: false,
        ),
        child: TextFormField(
          key: _sessionCommentTextFieldKey,
          controller: _sessionCommentTextFieldController,
          focusNode: _sessionCommentTextFieldFocus,
          maxLines: null,
          minLines: 2,
          textAlign: TextAlign.start,
          textAlignVertical: TextAlignVertical.center,
          style: const TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
          onChanged: (value) => widget.sessionViewModel.editedSession.comment = value.trim(),
          onTap: () {
            Scrollable.ensureVisible(
              _sessionCommentTextFieldKey.currentContext!,
              duration: const Duration(milliseconds: 400),
              curve: Curves.easeInOut,
            );
          },
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            isDense: true,
            label: Text(
              AppLocalizations.of(context)!.end_session_session_comment_info,
              style: const TextStyle(fontSize: 18, color: AppColors.accentColor2, overflow: TextOverflow.ellipsis),
              maxLines:
                  _sessionCommentTextFieldController.text.isNotEmpty || _sessionCommentTextFieldFocus.hasFocus ? 1 : 2,
            ),
            fillColor: AppColors.accentColor,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
              borderSide: const BorderSide(),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
              borderRadius: BorderRadius.circular(
                Constants.STANDARD_BORDER_RADIUS,
              ),
            ),
          ),
        ),
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      const Divider(thickness: 2.0),
    ];
  }

  Widget _locationName() {
    return Theme(
      data: ThemeData.from(
        colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor),
        useMaterial3: false,
      ),
      child: TextFormField(
        initialValue: widget.sessionViewModel.editedLocation.name,
        textAlign: TextAlign.start,
        textAlignVertical: TextAlignVertical.center,
        style: const TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
        onChanged: (value) {
          widget.sessionViewModel.editedLocation.name = value;
        },
        textCapitalization: TextCapitalization.words,
        decoration: InputDecoration(
          isDense: true,
          labelText: AppLocalizations.of(context)!.home_page_enter_location,
          labelStyle: const TextStyle(fontSize: 18, color: AppColors.accentColor2),
          fillColor: AppColors.accentColor,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
            borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
            borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
          ),
        ),
      ),
    );
  }

  Widget _position() {
    LatLng? coordinates = widget.sessionViewModel.editedLocation.coordinates;
    if (coordinates == null) return _addLocationWidget();

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "${AppLocalizations.of(context)!.sessions_page_edit_session_coordinates}:",
                style: Theme.of(context).textTheme.titleMedium,
                overflow: TextOverflow.ellipsis,
              ),
              Text(
                "* ${AppLocalizations.of(context)!.sessions_page_edit_session_longitude}: "
                "${coordinates.longitude.toStringAsFixed(8)}",
                style: Theme.of(context).textTheme.bodyLarge!.copyWith(fontSize: 16.0),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              Text(
                "* ${AppLocalizations.of(context)!.sessions_page_edit_session_latitude}:"
                " ${coordinates.latitude.toStringAsFixed(8)}",
                style: Theme.of(context).textTheme.bodyLarge!.copyWith(fontSize: 16.0),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ],
          ),
        ),
        const SizedBox(width: 12.0),
        ElevatedButton(
          onPressed: () => widget.sessionViewModel.chooseLocationCoordinatesFromMap(context, setState),
          style: ElevatedButton.styleFrom(
            foregroundColor: Colors.black,
            minimumSize: const Size(48, 38),
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          ),
          child: const Icon(Icons.edit, size: 22.0),
        ),
        const SizedBox(width: 12.0),
        ElevatedButton(
          onPressed: _deleteLocationCoordinates,
          style: ElevatedButton.styleFrom(
            foregroundColor: Colors.black,
            minimumSize: const Size(48, 38),
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          ),
          child: const Icon(Icons.delete, size: 22.0),
        ),
        const SizedBox(width: 4.0),
      ],
    );
  }

  Widget _addLocationWidget() {
    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(foregroundColor: Colors.black),
      onPressed: () => widget.sessionViewModel.chooseLocationCoordinatesFromMap(context, setState),
      icon: const Icon(Icons.location_on_outlined),
      label: Text(
        AppLocalizations.of(context)!.sessions_page_edit_session_add_coordinates,
        style: Theme.of(context).textTheme.bodyLarge!.copyWith(fontSize: 16),
      ),
    );
  }

  String _dateFormatter(DateTime date) {
    String languageCode = Localizations.localeOf(context).languageCode;
    return DateFormat("EEEE, d. MMM y", languageCode).format(date);
  }

  /// updating session start time
  /// returning true if date changes, false otherwise
  Future<bool> _selectDate(BuildContext context, DateTime selectedDate) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
    );
    if (picked != null && picked != selectedDate) {
      DateTime endTime = widget.sessionViewModel.editedSession.endDate!;
      DateTime startTime = widget.sessionViewModel.editedSession.startDate;
      Duration delta = endTime.difference(startTime);
      DateTime newStartTime = DateTime(
        picked.year,
        picked.month,
        picked.day,
        startTime.hour,
        startTime.minute,
        startTime.second,
      );
      widget.sessionViewModel.editedSession.startDate = newStartTime;
      widget.sessionViewModel.editedSession.endDate = newStartTime.add(delta);
      setState(() {});
      return true;
    }
    return false;
  }

  /// updating session start time
  /// returning true if date changes, false otherwise
  Future<bool> _selectTime(BuildContext context, TimeOfDay selectedTime) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );
    if (picked != null && picked != selectedTime) {
      DateTime endTime = widget.sessionViewModel.editedSession.endDate!;
      DateTime startTime = widget.sessionViewModel.editedSession.startDate;
      Duration delta = endTime.difference(startTime);
      DateTime updatedTime = DateTime(startTime.year, startTime.month, startTime.day, picked.hour, picked.minute);
      widget.sessionViewModel.editedSession.startDate = updatedTime;
      widget.sessionViewModel.editedSession.endDate = updatedTime.add(delta);
      setState(() {});
      return true;
    }
    return false;
  }

  /// updating session duration
  /// returning true if date changes, false otherwise
  Future<bool> _selectDuration(BuildContext context) async {
    DateTime endTime = widget.sessionViewModel.editedSession.endDate!;
    DateTime startTime = widget.sessionViewModel.editedSession.startDate;
    Duration diff = endTime.difference(startTime);
    TimeOfDay selectedTime = TimeOfDay(hour: diff.inHours, minute: diff.inMinutes % 60);

    final TimeOfDay? picked = await showTimePicker(
      builder: (BuildContext context, Widget? child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child!,
        );
      },
      context: context,
      helpText: AppLocalizations.of(context)!.sessions_page_select_duration,
      initialTime: selectedTime,
    );
    if (picked != null && picked != selectedTime) {
      DateTime updatedTime = startTime.add(Duration(hours: picked.hour, minutes: picked.minute));
      widget.sessionViewModel.editedSession.endDate = updatedTime;
      setState(() {});
      return true;
    }
    return false;
  }

  Widget _ascendList() {
    return FutureBuilder(
      future: _future,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }

        return ReorderableAscentsList(
          data: widget.sessionViewModel.editedAscents,
          session: widget.sessionViewModel.session,
          showEditIcon: true,
          showNewAscendAddButton: true,
          onDelete: (Ascend ascend) => widget.sessionViewModel.deleteAscend(ascend),
          onReAdd: (int index, Ascend ascend) => widget.sessionViewModel.onReAdd(index, ascend),
          onDeleteLast: (Ascend ascend) => _showLastAscendDeleteConfirmation(context),
        );
      },
    );
  }

  void _deleteLocationCoordinates() {
    LatLng? coordinates = widget.sessionViewModel.editedLocation.coordinates;
    widget.sessionViewModel.editedLocation.coordinates = null;
    setState(() {});
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          duration: const Duration(milliseconds: 2000),
          content: Text(AppLocalizations.of(context)!.sessions_page_edit_session_coordinates_deleted),
          action: SnackBarAction(
            textColor: Colors.lightBlue,
            label: AppLocalizations.of(context)!.dismissible_route_list_undo,
            onPressed: () => setState(() {
              widget.sessionViewModel.editedLocation.coordinates = coordinates;
            }),
          ),
        ),
      );
  }

  void _onAbort() async {
    List<String> changedParams = [];

    Location editedLocation = widget.sessionViewModel.editedLocation;
    Location originLocation = widget.sessionViewModel.location;
    Session editedSession = widget.sessionViewModel.editedSession;
    Session originSession = widget.sessionViewModel.session;

    // check differences in session
    if (editedLocation.outdoor != originLocation.outdoor) {
      changedParams.add(AppLocalizations.of(context)!.session_edit_page_indoor_outdoor);
    }

    if (!originSession.startDate.isAtSameMomentAs(editedSession.startDate)) {
      changedParams.add(AppLocalizations.of(context)!.general_date);
    }

    if (originSession.getDuration() != editedSession.getDuration()) {
      changedParams.add(AppLocalizations.of(context)!.sessions_page_duration);
    }

    if (editedLocation.name != originLocation.name) {
      changedParams.add(AppLocalizations.of(context)!.session_edit_page_location_name);
    }

    if ((editedLocation.coordinates == null && originLocation.coordinates != null) ||
        (editedLocation.coordinates != null && originLocation.coordinates == null)) {
      changedParams.add(AppLocalizations.of(context)!.sessions_page_edit_session_coordinates);
    } else if (editedLocation.coordinates != null && originLocation.coordinates != null) {
      double dist = Geolocator.distanceBetween(
          editedLocation.coordinates!.latitude,
          editedLocation.coordinates!.longitude,
          originLocation.coordinates!.latitude,
          originLocation.coordinates!.longitude);
      if (dist.abs() > 0.1) {
        changedParams.add(AppLocalizations.of(context)!.sessions_page_edit_session_coordinates);
      }
    }

    if (editedLocation.locationComment != originLocation.locationComment) {
      changedParams.add(AppLocalizations.of(context)!.session_edit_page_location_info);
    }

    // check differences in ascents
    List<Ascend> originAscents = (await widget.sessionViewModel.ascents).map((e) => Ascend.fromMap(e)).toList();
    List<Ascend> editedAscents = [];
    List<Ascend> addedAscents = [];

    for (Ascend ascend in widget.sessionViewModel.editedAscents) {
      List tmp = originAscents.where((element) => element.ascendId == ascend.ascendId).toList();

      if (tmp.isEmpty) {
        addedAscents.add(ascend);
      } else {
        Ascend originAscend = tmp.first;
        if (!ascend.isSame(originAscend)) {
          editedAscents.add(ascend);
        }
      }
    }

    if (changedParams.isNotEmpty ||
        editedAscents.isNotEmpty ||
        widget.sessionViewModel.toDelete.isNotEmpty ||
        addedAscents.isNotEmpty) {
      SessionEditChangesInfoDialog infoDialog = SessionEditChangesInfoDialog(
          changedParams: changedParams,
          editedAscents: editedAscents,
          deletedAscents: widget.sessionViewModel.toDelete,
          addedAscents: addedAscents,
          onDiscard: () {
            ScaffoldMessenger.of(context).removeCurrentSnackBar();
            Navigator.of(context).pop(false);
          });
      if (mounted) infoDialog.showInfoDialog(context);
    } else {
      if (mounted) {
        ScaffoldMessenger.of(context).removeCurrentSnackBar();
        Navigator.of(context).pop(false);
      }
    }
  }
}
