import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/enums/search_order_enum.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:climbing_track/ui/pages/sessions/session_tile.dart';
import 'package:climbing_track/ui/util/screenshot.dart';
import 'package:climbing_track/ui/widgets/empty_list_placeholder.dart';
import 'package:climbing_track/ui/widgets/search_app_bar.dart';
import 'package:climbing_track/view_model/session_view_model.dart';
import 'package:climbing_track/view_model/sessions_overview_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:showcaseview/showcaseview.dart';

class SessionsOverview extends StatefulWidget {
  const SessionsOverview({super.key});

  @override
  State<SessionsOverview> createState() => _SessionsOverviewState();
}

class _SessionsOverviewState extends State<SessionsOverview> {
  final SessionsOverviewViewModel _sessionsOverviewViewModel = SessionsOverviewViewModel();
  final GlobalKey _tooltipGlobalKey = GlobalKey();

  late Function() _listener;

  @override
  void initState() {
    super.initState();

    _listener = () async {
      if (mounted) setState(() {});
    };
    _sessionsOverviewViewModel.addListener(_listener);
  }

  @override
  void dispose() {
    _sessionsOverviewViewModel.removeListener(_listener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: SearchAppBar(
          title: AppLocalizations.of(context)!.sessions_title.toUpperCase(),
          hintText: AppLocalizations.of(context)!.sessions_page_search_bar_hint_text,
          onPressedSettings: _sessionsOverviewViewModel.toggleSettingsExpanded,
          onSearchStringChanged: (value) => _sessionsOverviewViewModel.searchString = value.trim().toLowerCase(),
        ),
      ),
      body: GestureDetector(
        onVerticalDragUpdate: (dragDetails) {
          FocusScope.of(context).unfocus();
        },
        onTapDown: (tapDownDetails) {
          FocusScope.of(context).unfocus();
        },
        child: FutureBuilder(
          future: _sessionsOverviewViewModel.futureData,
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const LinearProgressIndicator();
            }

            List<Map<String, dynamic>>? filteredData = _sessionsOverviewViewModel.filterAndSort(snapshot.data);

            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (_sessionsOverviewViewModel.settingsExpanded) _settingsBar(),
                if (_sessionsOverviewViewModel.settingsExpanded) const Divider(height: 0.0),
                Flexible(
                  child: _createSessions(filteredData),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _settingsBar() {
    return SizedBox(
      height: 60.0,
      child: Row(
        children: [
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  const SizedBox(width: 30.0),
                  _chipItem(
                    text: AppLocalizations.of(context)!.general_indoor,
                    activated: _sessionsOverviewViewModel.indoor,
                    onTap: _sessionsOverviewViewModel.toggleIndoor,
                  ),
                  const SizedBox(width: 12.0),
                  _chipItem(
                    text: AppLocalizations.of(context)!.general_outdoor,
                    activated: _sessionsOverviewViewModel.outdoor,
                    onTap: _sessionsOverviewViewModel.toggleOutdoor,
                  ),
                  const SizedBox(width: 12.0),
                  Showcase(
                    key: _tooltipGlobalKey,
                    description: AppLocalizations.of(context)!.sessions_page_comments_logbook_tooltip,
                    targetBorderRadius: BorderRadius.circular(12),
                    targetPadding: const EdgeInsets.all(3.0),
                    tooltipPadding: const EdgeInsets.all(12.0),
                    child: _chipItem(
                      text: AppLocalizations.of(context)!.general_comment(2),
                      activated: _sessionsOverviewViewModel.onlyShowComments,
                      onTap: _sessionsOverviewViewModel.toggleOnlyShowSessionComments,
                    ),
                  ),
                  const SizedBox(width: 12.0),
                ],
              ),
            ),
          ),
          const SizedBox(width: 6.0),
          _dropDownSearchOrder(),
          const SizedBox(width: 12.0),
        ],
      ),
    );
  }

  Widget _dropDownSearchOrder() {
    return PopupMenuButton<SearchOrder>(
      icon: const Icon(Icons.sort),
      onSelected: (value) => setState(() => DataStore().settingsDataStore.sessionSearchOrder = value),
      itemBuilder: (BuildContext context) => List.generate(
        SearchOrder.values.length,
        (index) => PopupMenuItem(
          value: SearchOrder.values[index],
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(
                  SearchOrder.values[index].getText(context),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
              ),
              const SizedBox(width: 20.0),
              Icon(
                Icons.check,
                color: SearchOrder.values[index] == DataStore().settingsDataStore.sessionSearchOrder
                    ? Colors.black
                    : Colors.transparent,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _chipItem({required String text, bool activated = false, Icon? icon, Function()? onTap}) {
    return Container(
      decoration: BoxDecoration(
        color: activated ? Colors.amber[800]!.withOpacity(0.1) : Colors.transparent,
        borderRadius: BorderRadius.circular(12.0),
        border: Border.all(
          width: 1.0,
          color: activated ? Colors.amber[800]! : Colors.black,
        ),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(11.0),
          onTap: onTap,
          child: Padding(
            padding: EdgeInsets.only(left: activated ? 8.0 : 12.0, right: 12.0, top: 8.0, bottom: 8.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (activated) const Icon(Icons.done, size: 18, color: Colors.black),
                if (activated) const SizedBox(width: 4.0),
                Text(text),
                if (icon != null) const SizedBox(width: 4.0),
                if (icon != null) icon,
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _createSessions(List<Map<String, dynamic>>? data) {
    return data == null || data.isEmpty
        ? EmptyListPlaceHolder(
            text: AppLocalizations.of(context)!.sessions_no_sessions_info, image: SessionsOverviewViewModel.IMAGE_PATH)
        : _getSessionScroll(data);
  }

  Widget _getSessionScroll(var data) {
    return Scrollbar(
      child: ListView.separated(
        padding: const EdgeInsets.all(Constants.STANDARD_PADDING),
        itemCount: data.length,
        itemBuilder: (BuildContext context, int index) {
          // cache data
          if (!_sessionsOverviewViewModel.sessionViewModels.containsKey(data[index][DBController.sessionId])) {
            _sessionsOverviewViewModel.sessionViewModels[data[index][DBController.sessionId]] = SessionViewModel(
              session: Session.fromMap(data[index]),
              screenshotController: ScreenshotController(),
            );
          }

          return SessionTile(
            sessionViewModel: _sessionsOverviewViewModel.sessionViewModels[data[index][DBController.sessionId]]!,
            first: index == 0,
            callBack: _sessionsOverviewViewModel.updateData,
            onlyShowComments: _sessionsOverviewViewModel.onlyShowComments,
            logbookCommentTooltipKey: _tooltipGlobalKey,
            showLogbookSettings: () => _sessionsOverviewViewModel.toggleSettingsExpanded(value: true),
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(thickness: 2.0),
      ),
    );
  }
}
