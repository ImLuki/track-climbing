import 'package:climbing_track/backend/util/extensions.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/pages/sessions/session_edit_page.dart';
import 'package:climbing_track/ui/util/custom_page_route.dart';
import 'package:climbing_track/ui/widgets/session_summary.dart';
import 'package:climbing_track/view_model/session_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:showcaseview/showcaseview.dart';

class SessionSummaryContainerWrapper extends StatelessWidget {
  final SessionViewModel sessionViewModel;

  const SessionSummaryContainerWrapper({super.key, required this.sessionViewModel});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ShowCaseWidget(
        builder: (context) => SessionSummaryContainer(
          sessionViewModel: sessionViewModel,
        ),
      ),
    );
  }
}

class SessionSummaryContainer extends StatefulWidget {
  final SessionViewModel sessionViewModel;

  const SessionSummaryContainer({super.key, required this.sessionViewModel});

  @override
  State<SessionSummaryContainer> createState() =>
      _SessionSummaryContainerState(sessionStartTime: sessionViewModel.session.startDate.copy());
}

class _SessionSummaryContainerState extends State<SessionSummaryContainer> {
  final GlobalKey _editIconKey = GlobalKey();
  final DateTime sessionStartTime;

  _SessionSummaryContainerState({required this.sessionStartTime});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          onPressed: () => Navigator.pop(
            context,
            // check start time has changed. This will lead to a changing order in session overview.
            // Therefore return value has to be true
            widget.sessionViewModel.session.startDate != sessionStartTime,
          ),
        ),
        title: Text(
          Util.replaceStringWithBlankSpaces(AppLocalizations.of(context)!.session_summary.toUpperCase()),
        ),
        actions: [
          IconButton(
            tooltip: AppLocalizations.of(context)!.general_share,
            onPressed: widget.sessionViewModel.createScreenShot,
            icon: const Icon(Icons.share),
          ),
          Showcase(
            key: _editIconKey,
            description: AppLocalizations.of(context)!.session_edit_page_edit_hint,
            targetBorderRadius: const BorderRadius.all(Radius.circular(12)),
            tooltipPadding: const EdgeInsets.all(12.0),
            child: IconButton(
              tooltip: AppLocalizations.of(context)!.general_edit,
              onPressed: () => pushSessionEditPage(context),
              icon: const Icon(Icons.edit),
            ),
          ),
        ],
      ),
      body: SessionSummary(
        widget.sessionViewModel,
        showToolTip: true,
        editIconKey: _editIconKey,
      ),
    );
  }

  void pushSessionEditPage(BuildContext context) {
    Navigator.push(
      context,
      CustomPageRoute.build(
        builder: (BuildContext context) => SessionEditPage(sessionViewModel: widget.sessionViewModel),
      ),
    ).then((value) {
      if (mounted) setState(() {});
    });
  }
}
