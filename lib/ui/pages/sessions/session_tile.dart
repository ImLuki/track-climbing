import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/enums/height_measurement.dart';
import 'package:climbing_track/backend/model/location.dart';
import 'package:climbing_track/backend/util/custom_date_formatter.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/pages/sessions/session_summary_container.dart';
import 'package:climbing_track/ui/util/custom_page_route.dart';
import 'package:climbing_track/view_model/session_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:showcaseview/showcaseview.dart';

class SessionTile extends StatefulWidget {
  const SessionTile({
    super.key,
    required this.sessionViewModel,
    required this.first,
    required this.callBack,
    required this.onlyShowComments,
    required this.logbookCommentTooltipKey,
    this.showLogbookSettings,
  });

  final bool first;
  final bool onlyShowComments;
  final SessionViewModel sessionViewModel;
  final Function callBack;

  // logbook tooltip
  final GlobalKey logbookCommentTooltipKey;
  final Function()? showLogbookSettings;

  @override
  _SessionTileState createState() => _SessionTileState();
}

class _SessionTileState extends State<SessionTile> {
  final GlobalKey _one = GlobalKey();
  static const int animationTimeInMilliseconds = 200;
  bool extended = false;

  @override
  void initState() {
    super.initState();
    if (widget.first) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        _showToolTip();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget content = InkWell(
      borderRadius: const BorderRadius.only(
        topRight: Radius.circular(18.0),
        bottomRight: Radius.circular(18.0),
      ),
      onLongPress: _pushSessionSummaryPage,
      onTap: () => setState(() => extended = !extended),
      child: _createSessionContainer(context),
    );

    return widget.first
        ? Showcase(
            key: _one,
            description: AppLocalizations.of(context)!.sessions_tooltip_long_press,
            targetBorderRadius: const BorderRadius.only(
              topRight: Radius.circular(12),
              bottomRight: Radius.circular(12),
            ),
            tooltipPadding: const EdgeInsets.all(12.0),
            child: content,
          )
        : content;
  }

  Widget _createSessionContainer(BuildContext context) {
    return FutureBuilder(
      future: widget.sessionViewModel.getAscentsAndCalculateSessionSummary(),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return _createExpansionSessionTile(context, []);
        }
        List ascents;

        if (snapshot.data == null) {
          ascents = [];
        } else {
          ascents = snapshot.data;
        }
        return _createExpansionSessionTile(context, ascents);
      },
    );
  }

  Widget _createExpansionSessionTile(BuildContext context, List ascents) {
    Location location = DataStore().locationsDataStore.locationById(widget.sessionViewModel.session.locationId);
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(
            width: 4.0,
            color: location.outdoor ? Colors.green : Colors.orangeAccent,
          ),
        ),
        color: Colors.transparent,
      ),
      child: _getContent(context, ascents),
    );
  }

  Widget _getContent(BuildContext context, List ascents) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, bottom: 8.0, top: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _sessionInfo(),
          const SizedBox(height: 14.0),
          IntrinsicHeight(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                getSummary(context),
                IconButton(
                  onPressed: () => setState(() => extended = !extended),
                  icon: extended ? const Icon(Icons.keyboard_arrow_up) : const Icon(Icons.keyboard_arrow_down),
                  iconSize: 22,
                  color: Colors.black54,
                  padding: EdgeInsets.zero,
                  splashRadius: 24.0,
                  constraints: BoxConstraints(
                    minWidth: widget.onlyShowComments ? 24 : 32,
                    minHeight: widget.onlyShowComments ? 24 : 32,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: extended ? 16.0 : 0.0),
          AnimatedSize(
            alignment: Alignment.topCenter,
            duration: const Duration(milliseconds: animationTimeInMilliseconds),
            child: _createRouteChildren(context, extended ? ascents : []),
          ),
        ],
      ),
    );
  }

  Widget _sessionInfo() {
    String unit = DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET ? "ft" : "m";
    String height = Util.getHeightWithShortUnit(
      widget.sessionViewModel.sessionSummaryData[SessionViewModel.HEIGHT_KEY],
      zeroFilling: "0 $unit",
    );

    String start = DateFormat.jm(DataStore().settingsDataStore.language).format(
      widget.sessionViewModel.session.startDate,
    );
    String end = DateFormat.jm(DataStore().settingsDataStore.language).format(
      widget.sessionViewModel.session.endDate!,
    );
    Location location = DataStore().locationsDataStore.locationById(widget.sessionViewModel.session.locationId);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _getDateTitle(widget.sessionViewModel.session.startDate),
              _getLocationWidget(location.name),
              _getLocationWidget(
                Util.replaceStringWithBlankSpaces(
                  AppLocalizations.of(context)!.sessions_page_height_from_to(height, start, end),
                ),
              ),
            ],
          ),
        ),
        IconButton(
          onPressed: _pushSessionSummaryPage,
          icon: const Icon(Icons.info_outline),
          color: Colors.black54,
          splashRadius: 24.0,
          constraints: const BoxConstraints(
            minWidth: 32,
            minHeight: 32,
          ),
        ),
      ],
    );
  }

  Widget getSummary(BuildContext context) {
    return Expanded(
      child: widget.onlyShowComments
          ? _sessionComment()
          : Row(
              children: [
                _getSummaryPart(widget.sessionViewModel.count.toString(), AppLocalizations.of(context)!.ascends_title),
                const VerticalDivider(indent: 8.0, endIndent: 8.0, thickness: 2, width: 28.0, color: Colors.black45),
                _getSummaryPart(widget.sessionViewModel.avgGrade, AppLocalizations.of(context)!.sessions_page_average),
                const VerticalDivider(indent: 8.0, endIndent: 8.0, thickness: 2, width: 28.0, color: Colors.black45),
                _getSummaryPart(widget.sessionViewModel.maxGrade, AppLocalizations.of(context)!.sessions_page_max),
              ],
            ),
    );
  }

  Widget _sessionComment() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Icon(Icons.comment_outlined, size: 28),
        const SizedBox(width: 20.0),
        Flexible(
          child: Text(widget.sessionViewModel.session.comment),
        ),
      ],
    );
  }

  Widget _getSummaryPart(String text, String info) {
    return Flexible(
      child: Container(
        constraints: const BoxConstraints(minWidth: 60.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              text,
              style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                    fontSize: 22.0,
                    fontWeight: FontWeight.w500,
                    letterSpacing: 0.0,
                    wordSpacing: 0.0,
                    height: 0.0,
                  ),
              overflow: TextOverflow.visible,
              maxLines: 2,
            ),
            const SizedBox(height: 4.0),
            Text(
              info,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                    fontSize: 13,
                    color: Colors.black.withOpacity(0.7),
                    fontWeight: FontWeight.w400,
                    letterSpacing: 0.0,
                    wordSpacing: 0.0,
                    height: 0.0,
                  ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getDateTitle(DateTime date) {
    return Text(
      CustomDateFormatter.formatDateString(context, date),
      style: Theme.of(context).textTheme.headlineSmall!.copyWith(
            fontSize: 26.0,
            fontWeight: FontWeight.w500,
          ),
    );
  }

  Widget _getLocationWidget(String location) {
    return Text(
      Util.getLocation(location),
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
            fontSize: 15,
            color: Colors.black54,
            fontWeight: FontWeight.w400,
            letterSpacing: 0.0,
            wordSpacing: 0.0,
            height: 0.0,
          ),
    );
  }

  Widget _createRouteChildren(BuildContext context, List routes) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: List<Widget>.generate(
          routes.length,
          (index) => Padding(
            padding: const EdgeInsets.only(bottom: 1.5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Text(
                    Util.getAscendName(
                      context,
                      routes[index][DBController.ascendName],
                      type: ClimbingType.values.byName(routes[index][DBController.ascendType]),
                      speedType: routes[index][DBController.speedType],
                    ),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          letterSpacing: 0.0,
                          wordSpacing: 0.0,
                          height: 0,
                        ),
                  ),
                ),
                const SizedBox(width: 14.0),
                Text(
                  Util.getDataSummary(routes[index], context),
                  style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        letterSpacing: 0.0,
                        wordSpacing: 0.0,
                        height: 0,
                      ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _pushSessionSummaryPage() {
    Navigator.push(
      context,
      CustomPageRoute.build(
        builder: (BuildContext context) => SessionSummaryContainerWrapper(sessionViewModel: widget.sessionViewModel),
      ),
    ).then((value) {
      if (value == null || value) widget.callBack();
      extended = true;
      setState(() {});
    });
  }

  void _showToolTip() async {
    if (!DataStore().toolTipsDataStore.tooltipSessionSummary) return;

    await Future.delayed(const Duration(milliseconds: 500));

    if (mounted) {
      widget.showLogbookSettings?.call();
      DataStore().toolTipsDataStore.tooltipSessionSummary = false;
      ShowCaseWidget.of(context).startShowCase([widget.logbookCommentTooltipKey, _one]);
    }
  }
}
