import 'dart:io' show Directory, FileSystemEntity, Platform;

import 'package:app_settings/app_settings.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/handler/location_handler.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/dialog/confirm_dialog.dart';
import 'package:climbing_track/ui/dialog/edit_dialog.dart';
import 'package:climbing_track/ui/pages/settings/about_developer.dart';
import 'package:climbing_track/ui/pages/settings/donation_page.dart';
import 'package:climbing_track/ui/util/custom_page_route.dart';
import 'package:climbing_track/view_model/settings_view_model.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dash/flutter_dash.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_picker_plus/Picker.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final SettingsViewModel _settingsViewModel = SettingsViewModel();

  @override
  void initState() {
    super.initState();
    _settingsViewModel.addListener(() => setState(() {}));
    _settingsViewModel.init();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            child: _createSettings(),
          ),
        ),
      ),
    );
  }

  Widget _getTitle() {
    return Padding(
      padding: const EdgeInsets.only(top: 18.0),
      child: Text(
        AppLocalizations.of(context)!.main_title,
        textAlign: TextAlign.start,
        style: Theme
            .of(context)
            .textTheme
            .displaySmall
            ?.copyWith(
          letterSpacing: -1.0,
        ),
      ),
    );
  }

  Widget _createSettings() {
    return Column(
      children: [
        _getTitle(),
        ..._systemsSettings(),
        ..._backupSettings(),
        ..._locationSettings(),
        ..._notificationSettings(),
        ..._chartSettings(),
        ..._generalSettings(),
        ..._otherSettings(),
        Text(
          AppLocalizations.of(context)!.settings_app_version,
          style: Theme
              .of(context)
              .textTheme
              .bodyLarge
              ?.copyWith(
            fontWeight: FontWeight.normal,
            height: 0.9,
            fontSize: 18.0,
          ),
        ),
        Text(
          _settingsViewModel.getVersionString(),
          style: Theme
              .of(context)
              .textTheme
              .bodyMedium
              ?.copyWith(
            color: Colors.black54,
            fontWeight: FontWeight.normal,
          ),
        ),
        const SizedBox(height: 30.0),
      ],
    );
  }

  Widget _createSettingWidget({
    required String title,
    required String info,
    IconData? icon,
    Widget? setting,
    void Function()? onTapFunction,
    bool progressIndicator = false,
  }) {
    Widget container = Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          progressIndicator
              ? const SizedBox(
            width: 24,
            height: 24,
            child: CircularProgressIndicator(
              strokeWidth: 2,
              color: Colors.blue,
            ),
          )
              : icon == null
              ? Container()
              : Icon(icon, color: Colors.black),
          const SizedBox(width: 20.0),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 6.0),
                  child: Text(
                    title,
                    style: Theme
                        .of(context)
                        .textTheme
                        .bodyLarge
                        ?.copyWith(
                      fontWeight: FontWeight.normal,
                      height: 0.9,
                      fontSize: 20.0,
                    ),
                  ),
                ),
                Text(
                  info,
                  maxLines: 10,
                  overflow: TextOverflow.ellipsis,
                  style: Theme
                      .of(context)
                      .textTheme
                      .bodyMedium
                      ?.copyWith(
                    height: 1.2,
                    color: Colors.black.withOpacity(0.75),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(width: setting != null ? 12.0 : 0.0),
          if (setting != null) setting,
        ],
      ),
    );
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12.0),
      child: setting == null
          ? InkWell(
        onTap: onTapFunction,
        borderRadius: const BorderRadius.all(
          Radius.circular(24.0),
        ),
        child: container,
      )
          : container,
    );
  }

  Widget _header({required String text}) {
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      padding: const EdgeInsets.only(
        left: 24.0,
        right: 24.0,
        top: 20.0,
        bottom: 10.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Text(
              text.toUpperCase(),
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.normal,
                letterSpacing: 1.5,
                color: Colors.red.shade700,
              ),
              overflow: TextOverflow.visible,
              maxLines: 1,
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> _systemsSettings() {
    return [
      _header(text: AppLocalizations.of(context)!.settings_climbing_systems),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_boulder_system,
        info: AppLocalizations.of(context)!.settings_boulder_system_info,
        icon: null,
        onTapFunction: () => EditDialog().showGradingSystemsDialog(context, ClimbingType.BOULDER),
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_climbing_system,
        info: AppLocalizations.of(context)!.settings_climbing_system_info,
        icon: null,
        onTapFunction: () => EditDialog().showGradingSystemsDialog(context, ClimbingType.SPORT_CLIMBING),
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_expanded_system,
        info: AppLocalizations.of(context)!.settings_expanded_system_info,
        icon: null,
        setting: Switch(
          activeColor: Colors.blue,
          value: DataStore().settingsDataStore.isGradeSystemExpanded,
          onChanged: (value) {
            setState(() => DataStore().settingsDataStore.isGradeSystemExpanded = value);
          },
        ),
      ),
      const Divider(thickness: 2.0),
    ];
  }

  List<Widget> _backupSettings() {
    return [
      _header(text: AppLocalizations.of(context)!.settings_backup),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_create_backup,
        info: AppLocalizations.of(context)!.settings_create_backup_info,
        onTapFunction: () {
          _settingsViewModel.isCreatingBackup = true;
          setState(() {});
          _settingsViewModel.createBackupFile().then((value) => setState(() {}));
        },
        icon: Icons.upload_sharp,
        progressIndicator: _settingsViewModel.isCreatingBackup,
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_load_backup,
        info: AppLocalizations.of(context)!.settings_load_backup_info,
        onTapFunction: _loadBackupDialog,
        icon: Icons.download_sharp,
        progressIndicator: _settingsViewModel.isLoadingBackup,
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_delete_all,
        info: AppLocalizations.of(context)!.settings_delete_all_info,
        onTapFunction: _deleteDataDialog,
        icon: Icons.delete_outline,
      ),
      _dots(),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_automatic_backups,
        info: AppLocalizations.of(context)!.settings_automatic_backups_info,
        icon: _settingsViewModel.settings.automaticBackups ? Icons.save_alt : Icons.file_download_off_outlined,
        setting: Switch(
          activeColor: Colors.blue,
          onChanged: onAutomaticBackupsChanged,
          value: _settingsViewModel.settings.automaticBackups,
        ),
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_restore_backup,
        info: AppLocalizations.of(context)!.settings_restore_backup_info,
        onTapFunction: _loadAutomaticBackup,
        icon: Icons.restore_page_outlined,
        progressIndicator: _settingsViewModel.isLoadingAutomaticBackup,
      ),
      if (_settingsViewModel.settings.automaticBackups)
        _createSettingWidget(
          title: AppLocalizations.of(context)!.settings_delete_automatic_backup,
          info: AppLocalizations.of(context)!.settings_delete_automatic_backup_info,
          icon: Icons.auto_delete_outlined,
          setting: Switch(
            activeColor: Colors.blue,
            onChanged: (bool value) => setState(() => _settingsViewModel.settings.overwriteAutomaticBackups = value),
            value: _settingsViewModel.settings.overwriteAutomaticBackups,
          ),
        ),
      if (Platform.isAndroid) ..._googleDriveBackupSettings(),
      const Divider(thickness: 2.0),
    ];
  }

  List<Widget> _locationSettings() {
    return [
      _header(text: AppLocalizations.of(context)!.general_location),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_enable_location,
        info: AppLocalizations.of(context)!.settings_enable_location_info,
        icon: LocationHandler().saveLocation ? Icons.location_on_outlined : Icons.location_off_outlined,
        setting: Switch(
          activeColor: Colors.blue,
          onChanged: onLocationEnabledChanged,
          value: LocationHandler().saveLocation,
        ),
      ),
      const Divider(thickness: 2.0),
    ];
  }

  List<Widget> _chartSettings() {
    return [
      _header(text: AppLocalizations.of(context)!.statistics_title),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_limit_charts,
        info: AppLocalizations.of(context)!.settings_limit_charts_info,
        icon: DataStore().settingsDataStore.limitCharts ? Icons.bar_chart_outlined : Icons.stacked_bar_chart,
        setting: Switch(
          activeColor: Colors.blue,
          onChanged: (value) => setState(() => DataStore().settingsDataStore.limitCharts = value),
          value: DataStore().settingsDataStore.limitCharts,
        ),
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_chart_animation,
        info: AppLocalizations.of(context)!.settings_chart_animation_info,
        icon: Icons.animation,
        setting: Switch(
          activeColor: Colors.blue,
          onChanged: (value) => setState(() => DataStore().settingsDataStore.chartAnimation = value),
          value: DataStore().settingsDataStore.chartAnimation,
        ),
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_chart_y_axis,
        info: AppLocalizations.of(context)!.settings_chart_y_axis_info,
        icon: Icons.line_axis,
        setting: Switch(
          activeColor: Colors.blue,
          onChanged: (value) => setState(() => DataStore().settingsDataStore.isYAxisExpanded = value),
          value: DataStore().settingsDataStore.isYAxisExpanded,
        ),
      ),
      const Divider(thickness: 2.0),
    ];
  }

  List<Widget> _generalSettings() {
    return [
      _header(text: AppLocalizations.of(context)!.settings_general),
      if (Platform.localeName.startsWith("de") || DataStore().settingsDataStore.language.startsWith("de"))
        _createSettingWidget(
          title: AppLocalizations.of(context)!.setting_language,
          info: AppLocalizations.of(context)!.setting_language_info,
          onTapFunction: () =>
              EditDialog().showLanguageDialog(context).then((value) {
                if (value == true) {
                  ScaffoldMessenger.of(context)
                    ..removeCurrentSnackBar()
                    ..showSnackBar(
                      SnackBar(
                        behavior: SnackBarBehavior.floating,
                        content: Text(AppLocalizations.of(context)!.settings_language_change_info),
                      ),
                    );
                }
              }),
          icon: Icons.language,
        ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_length_measurement,
        info: AppLocalizations.of(context)!.settings_length_measurement_info,
        onTapFunction: () => EditDialog().showMeasurementDialog(context),
        icon: Icons.straighten,
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_date_format,
        info: AppLocalizations.of(context)!.settings_date_format_info,
        onTapFunction: () => EditDialog().showDateFormatDialog(context),
        icon: Icons.date_range_outlined,
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_reset_tooltips,
        info: AppLocalizations.of(context)!.settings_reset_tooltips_info,
        onTapFunction: _resetTooltips,
        icon: Icons.info_outline,
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_show_timer_between_goes,
        info: AppLocalizations.of(context)!.settings_show_timer_between_goes_info,
        icon: Icons.timer_sharp,
        setting: Switch(
          activeColor: Colors.blue,
          onChanged: (value) => setState(() => DataStore().settingsDataStore.showTimerBetweenGoes = value),
          value: DataStore().settingsDataStore.showTimerBetweenGoes,
        ),
      ),
      const Divider(thickness: 2.0),
    ];
  }

  List<Widget> _notificationSettings() {
    return [
      _header(text: AppLocalizations.of(context)!.settings_notifications),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_notifications_enable,
        info: AppLocalizations.of(context)!.settings_notifications_enable_info,
        icon: DataStore().settingsDataStore.notificationsActivated
            ? Icons.notifications_on_outlined
            : Icons.notifications_off_outlined,
        setting: Switch(
          activeColor: Colors.blue,
          onChanged: onNotificationEnabledChanged,
          value: DataStore().settingsDataStore.notificationsActivated,
        ),
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_session_reminder,
        info: AppLocalizations.of(context)!.settings_session_reminder_info,
        icon: Icons.access_alarm_outlined,
        onTapFunction: () => _changeSessionReminderDuration(context),
      ),
      const Divider(thickness: 2.0),
    ];
  }

  List<Widget> _otherSettings() {
    return [
      _header(text: AppLocalizations.of(context)!.general_others),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_about_dev,
        info: AppLocalizations.of(context)!.settings_about_developer_info,
        onTapFunction: _aboutPage,
        icon: Icons.person,
      ),
      if (Platform.isAndroid)
        _createSettingWidget(
          title: AppLocalizations.of(context)!.settings_donation,
          info: AppLocalizations.of(context)!.settings_donation_support_info,
          onTapFunction: _donate,
          icon: Icons.volunteer_activism_outlined,
        ),
      if (Platform.isAndroid)
        _createSettingWidget(
          title: AppLocalizations.of(context)!.general_share,
          info: AppLocalizations.of(context)!.settings_share_info,
          onTapFunction: () =>
              Share.share(
                AppLocalizations.of(context)!
                    .settings_share_text("https://play.google.com/store/apps/details?id=freudenmann.climbing_track"),
              ),
          icon: Icons.share,
        ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_rate,
        info: AppLocalizations.of(context)!.settings_rate_info,
        onTapFunction: _settingsViewModel.rateApp,
        icon: Icons.star_border,
      ),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.settings_feedback,
        info: AppLocalizations.of(context)!.settings_feedback_info,
        onTapFunction: SettingsViewModel.sendFeedBack,
        icon: Icons.local_post_office_outlined,
      ),
      const SizedBox(height: 20),
    ];
  }

  Widget _dots() {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
      child: Dash(
        length: MediaQuery
            .of(context)
            .size
            .width * 0.3,
        dashColor: Colors.black26,
        dashLength: 30,
        dashGap: 18,
        dashThickness: 1.4,
      ),
    );
  }

  List<Widget> _googleDriveBackupSettings() {
    return [
      _dots(),
      _createSettingWidget(
        title: AppLocalizations.of(context)!.google_drive_backup_activate,
        info: AppLocalizations.of(context)!.google_drive_backup_activate_info,
        icon: Icons.add_to_drive,
        setting: Switch(
          activeColor: Colors.blue,
          value: _settingsViewModel.settings.googleDriveActivated,
          onChanged: (value) {
            _settingsViewModel.settings.googleDriveActivated = true;
            setState(() {});
            _settingsViewModel.toggleGoogleCloudBackups(context, value).then((value) => setState(() {}));
          },
        ),
      ),
      if (_settingsViewModel.settings.googleDriveActivated) ...[
        if (_settingsViewModel.getLastDriveBackup() != null) _lastBackupWidget(),
        _createSettingWidget(
          title: AppLocalizations.of(context)!.google_drive_backup_upload,
          info: AppLocalizations.of(context)!.google_drive_backup_upload_info,
          onTapFunction: () {
            _settingsViewModel.isUploadingGoogleDrive = true;
            setState(() {});
            _settingsViewModel.uploadGoogleDriveBackup(context).then((value) => setState(() {}));
          },
          icon: Icons.cloud_upload_outlined,
          progressIndicator: _settingsViewModel.isUploadingGoogleDrive,
        ),
        _createSettingWidget(
          title: AppLocalizations.of(context)!.google_drive_backup_restore,
          info: AppLocalizations.of(context)!.google_drive_backup_restore_info,
          onTapFunction: () {
            ConfirmDialog().showLoadBackupDialog(context, () {
              _settingsViewModel.isDownloadingGoogleDrive = true;
              setState(() {});
              _settingsViewModel.restoreGoogleDriveBackup(context).then((value) => setState(() {}));
            });
          },
          icon: Icons.cloud_download_outlined,
          progressIndicator: _settingsViewModel.isDownloadingGoogleDrive,
        ),
      ],
    ];
  }

  void _aboutPage() {
    Navigator.push(
      context,
      CustomPageRoute.build(
        builder: (BuildContext context) =>
            AboutPage(
              versionString: _settingsViewModel.getVersionString(),
            ),
      ),
    );
  }

  void _deleteDataDialog() {
    ConfirmDialog().showDeleteDatabaseDialog(context, _deleteDataBase);
  }

  void _deleteDataBase() {
    _settingsViewModel.deleteDataBase();
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(AppLocalizations.of(context)!.settings_data_deleted_info),
        ),
      );
  }

  void _loadBackupDialog() {
    ConfirmDialog().showLoadBackupDialog(context, _loadBackup);
  }

  void _loadBackup() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(type: FileType.any);
    if (result == null && context.mounted) {
      if (!mounted) return;

      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            behavior: SnackBarBehavior.floating,
            content: Text(AppLocalizations.of(context)!.settings_backup_not_loaded_info),
          ),
        );
      return;
    }
    _settingsViewModel.isLoadingBackup = true;
    setState(() {});

    _settingsViewModel.loadBackup(result!.files.single.path).then((value) {
      _settingsViewModel.isLoadingBackup = false;

      if (mounted) {
        setState(() {});
        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              content: Text(
                value
                    ? AppLocalizations.of(context)!.settings_backup_loaded_info
                    : AppLocalizations.of(context)!.settings_backup_not_loaded_info,
              ),
            ),
          );
      }
    });
  }

  void _loadAutomaticBackupDialog(List<FileSystemEntity> backupList) {
    ConfirmDialog().showChooseBackupDialog(context, _loadFileWithResponse, backupList);
  }

  void _loadAutomaticBackup() async {
    String dir;
    if (Platform.isAndroid) {
      List<Directory>? path = await getExternalStorageDirectories();

      if (path == null) return;

      dir = path[0].path;
    } else if (Platform.isIOS) {
      Directory documentsDirectory = await getApplicationDocumentsDirectory();
      dir = join(documentsDirectory.path, "Climbing_Tracker");
    } else {
      return;
    }

    // create directory if non-existent
    Directory(dir).createSync();

    List<FileSystemEntity> files = Directory(dir).listSync();
    files.sort((a, b) => b.path.compareTo(a.path));
    if (files.isEmpty && mounted) {
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            behavior: SnackBarBehavior.floating,
            content: Text(AppLocalizations.of(context)!.settings_no_backup_info),
            duration: const Duration(seconds: 2),
          ),
        );
      return;
    }

    _loadAutomaticBackupDialog(files);
  }

  void _loadFileWithResponse(String path) {
    _settingsViewModel.isLoadingAutomaticBackup = true;
    setState(() {});
    _settingsViewModel.loadBackup(path).then((value) {
      _settingsViewModel.isLoadingAutomaticBackup = false;
      setState(() {});
      if (mounted) {
        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              content: Text(
                value
                    ? AppLocalizations.of(context)!.settings_backup_restored_info
                    : AppLocalizations.of(context)!.settings_backup_not_restored_info,
              ),
              duration: const Duration(seconds: 2),
            ),
          );
      }
    });
  }

  void _changeSessionReminderDuration(BuildContext context) async {
    Picker(
      backgroundColor: Theme
          .of(context)
          .colorScheme
          .surface,
      itemExtent: 42.0,
      adapter: NumberPickerAdapter(
        data: [
          NumberPickerColumn(
            initValue: (DataStore().settingsDataStore.sessionReminderDuration / 60 * 2).toInt(),
            begin: 1,
            end: 19,
            suffix: const Text("  h"),
            onFormatValue: (value) => (value / 2).toStringAsFixed(1),
          ),
        ],
      ),
      hideHeader: true,
      title: Text(
        AppLocalizations.of(context)!.settings_please_select,
        style: Theme
            .of(context)
            .textTheme
            .headlineMedium
            ?.copyWith(color: Colors.black),
      ),
      selectedTextStyle: const TextStyle(color: Colors.blue),
      onConfirm: (Picker picker, List value) {
        double result = (picker
            .getSelectedValues()
            .first / 2) * 60;
        DataStore().settingsDataStore.sessionReminderDuration = result.toInt();
      },
    ).showDialog(context);
  }

  void _resetTooltips() {
    _settingsViewModel.resetTooltips();
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(AppLocalizations.of(context)!.settings_reset_tooltips_reset_info),
          duration: const Duration(seconds: 2),
        ),
      );
  }

  void onLocationEnabledChanged(bool value) async {
    setState(() {
      LocationHandler().setLocation(value);
    });
    if (value) {
      String text = AppLocalizations.of(context)!.permissions_location_warning;
      if (!await _settingsViewModel.checkLocationPermission()) {
        setState(() {
          LocationHandler().setLocation(false);
        });
        showPermissionErrorModalSheet(
          text: text,
          onOpenSettings: () => AppSettings.openAppSettings(type: AppSettingsType.settings),
        );
      }
    }
  }

  void onNotificationEnabledChanged(bool value) async {
    DataStore().settingsDataStore.notificationsActivated = value;
    setState(() {});

    // check permission
    if (value) {
      String text = AppLocalizations.of(context)!.permission_notification_warning;
      if (!await _settingsViewModel.checkNotificationPermission()) {
        DataStore().settingsDataStore.notificationsActivated = false;
        setState(() {});
        showPermissionErrorModalSheet(
          text: text,
          onOpenSettings: () => AppSettings.openAppSettings(type: AppSettingsType.notification),
        );
      }
    }
  }

  void onAutomaticBackupsChanged(bool value) async {
    _settingsViewModel.settings.automaticBackups = value;
    setState(() {});
  }

  String _dateFormatter(DateTime? date) {
    if (date == null) return "---";
    String languageCode = Localizations
        .localeOf(context)
        .languageCode;
    return DateFormat.yMMMd(languageCode).add_jm().format(date);
  }

  Widget _lastBackupWidget() {
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      padding: const EdgeInsets.fromLTRB(24, 10, 0, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Flexible(
            child: Text(
              AppLocalizations.of(context)!.google_drive_last_backup,
              style: Theme
                  .of(context)
                  .textTheme
                  .titleMedium,
              overflow: TextOverflow.clip,
            ),
          ),
          const SizedBox(width: 8),
          Flexible(
            child: Text(
              _dateFormatter(_settingsViewModel.getLastDriveBackup()),
              overflow: TextOverflow.clip,
              style: Theme
                  .of(context)
                  .textTheme
                  .bodyLarge,
            ),
          ),
        ],
      ),
    );
  }

  void showPermissionErrorModalSheet({
    required String text,
    required VoidCallback? onOpenSettings,
  }) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                text,
                style: const TextStyle(fontSize: 16.0),
              ),
              const SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: FilledButton(
                      child: Text(AppLocalizations.of(context)!.device_settings),
                      onPressed: () {
                        onOpenSettings?.call();
                        Navigator.pop(context);
                      },
                    ),
                  ),
                  const SizedBox(width: 12.0),
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: Text(AppLocalizations.of(context)!.general_dismiss),
                  ),
                ],
              )
            ],
          ),
        );
      },
    );
  }

  void _donate() {
    Navigator.push(
      context,
      CustomPageRoute.build(builder: (BuildContext context) => const DonationPage()),
    );
  }
}
