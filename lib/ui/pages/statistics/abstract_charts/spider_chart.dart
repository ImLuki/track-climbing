import 'dart:ui';

import 'package:climbing_track/ui/pages/statistics/chart_constants.dart';
import 'package:flutter/material.dart';
import 'dart:math' show cos, max, min, pi, sin;

/// Displays a spider/radar chart
class SpiderChart extends StatefulWidget {
  /// The data points to be displayed
  final List<double> data;

  /// The color of the spider chart
  final Color color;

  /// Icon labels for the data points
  final List<IconData> icons;

  /// String labels for the data points
  final List<String> labels;

  /// The value represented by the chart perimeter
  final double? maxValue;
  final int decimalPrecision;

  /// number of circles in spider chart
  final int numberOfCircles;

  /// If not expanded only miniature chart is displayed
  final bool expanded;

  /// Show legend
  final bool showLegend;

  /// Delay after the animation should start
  final double animationDelay;

  /// Creates a widget that displays a spider chart
  SpiderChart({
    super.key,
    required this.data,
    this.color = Colors.red,
    this.maxValue,
    this.decimalPrecision = 0,
    this.numberOfCircles = 4,
    required this.expanded,
    required this.showLegend,
    required this.icons,
    required this.labels,
    required this.animationDelay,
  }) : assert(icons.isNotEmpty ? data.length == icons.length : true, 'Length of data and labels lists must be equal');

  @override
  State<SpiderChart> createState() => _SpiderChartState();
}

class _SpiderChartState extends State<SpiderChart> with TickerProviderStateMixin {
  /// animation variables
  late Animation<double> animation;
  late AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: ChartConstants.animationDuration.toInt()),
    );

    Tween<double> scaleTween = Tween(begin: 0.0, end: 3.0);

    animation = scaleTween.animate(CurvedAnimation(parent: controller, curve: Curves.easeOut))
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.stop();
        }
      });

    Future.delayed(Duration(milliseconds: widget.animationDelay.toInt())).then((value) {
      if (mounted) controller.forward();
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var calculatedMax = widget.maxValue ?? widget.data.reduce(max);

    Widget spiderChart = AnimatedBuilder(
      animation: animation,
      builder: (context, snapshot) {
        return CustomPaint(
          size: Size(
            MediaQuery.of(context).size.width,
            MediaQuery.of(context).size.height * (widget.expanded ? (3 / 5) : 1),
          ),
          painter: SpiderChartPainter(
            widget.data,
            calculatedMax,
            widget.color,
            widget.icons,
            widget.decimalPrecision,
            widget.numberOfCircles,
            !widget.expanded,
            animation.value,
          ),
        );
      },
    );

    return widget.expanded
        ? Column(
            children: [
              spiderChart,
              _legendWidget(),
            ],
          )
        : spiderChart;
  }

  Widget _legendWidget() {
    return Flexible(
      child: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Scrollbar(
          child: SingleChildScrollView(
            child: Wrap(
              direction: Axis.horizontal,
              spacing: 25.0,
              runSpacing: 6.0,
              children: List.generate(
                widget.data.length,
                (index) => Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(widget.icons[index], size: 20.0),
                    const SizedBox(width: 8.0),
                    Text(widget.labels[index]),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

/// Custom painter for the [SpiderChart] widget
class SpiderChartPainter extends CustomPainter {
  final List<double> data;
  final double maxNumber;
  final Color color;
  final List<IconData> labels;
  final int decimalPrecision;
  final int numberOfCircles;

  final bool miniatureChart;

  final Paint dataPolygonFillingPaint = Paint();
  final Paint dataPolygonBorderPaint = Paint();
  final Paint lines = Paint();
  final Paint spokes = Paint();

  /// animation value
  final double scaleValue;

  SpiderChartPainter(
    this.data,
    this.maxNumber,
    this.color,
    this.labels,
    this.decimalPrecision,
    this.numberOfCircles,
    this.miniatureChart,
    this.scaleValue,
  ) {
    dataPolygonFillingPaint.color = color.withOpacity(0.15);
    dataPolygonFillingPaint.style = PaintingStyle.fill;

    dataPolygonBorderPaint.color = color;
    dataPolygonBorderPaint.strokeWidth = miniatureChart ? 1.0 : 2.0;
    dataPolygonBorderPaint.style = PaintingStyle.stroke;

    lines.color = const Color.fromARGB(255, 30, 30, 30);
    lines.style = PaintingStyle.fill;

    spokes.color = const Color.fromARGB(255, 30, 30, 30);
    spokes.style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    // Auxiliary calculations
    Offset center = size.center(Offset.zero);
    double radius = min(center.dy, center.dx) - (miniatureChart ? 0.0 : 40);

    double angle = (2 * pi) / data.length;

    // calculate data points
    List<Offset> dataPoints = <Offset>[];

    // calculate all points positions
    for (int i = 0; i < data.length; i++) {

      // animation scale factor for realising staggered animation
      // animation value is from 1 to 3
      // removing (i * (2 / (data.length - 1)) leads to delay/staggering of animation
      // clamping to 0.0 keeps value within bounds
      double animationScaleFactor = clampDouble(scaleValue - (i * (2 / (data.length - 1))), 0.0, 1.0);

      // calculate position of each point

      // calculate distance to the center point
      double scaledRadius = (data[i] / maxNumber) * radius * animationScaleFactor;
      // calculate x and y position by angle and distance
      double x = scaledRadius * cos(angle * i - pi / 2);
      double y = scaledRadius * sin(angle * i - pi / 2);

      dataPoints.add(Offset(x, y) + center);
    }

    // calculate points for spider net
    List<List<Offset>> polygonsList = [];
    for (int n = numberOfCircles; n > 0; n--) {
      List<Offset> polygonPoints = [];

      for (int i = 0; i < data.length; i++) {
        double x = (radius * (n / numberOfCircles)) * cos(angle * i - pi / 2);
        double y = (radius * (n / numberOfCircles)) * sin(angle * i - pi / 2);

        polygonPoints.add(Offset(x, y) + center);
      }
      polygonsList.add(polygonPoints);
    }

    // calculate icon label pos
    List<Offset> labelPoints = [];
    const double margin = 20.0;
    for (int i = 0; i < data.length; i++) {
      double x = (radius + margin) * cos(angle * i - pi / 2);
      double y = (radius + margin) * sin(angle * i - pi / 2);

      labelPoints.add(Offset(x, y) + center);
    }

    if (labels.isNotEmpty && !miniatureChart) {
      paintLabels(canvas, center, labelPoints, labels);
    }

    if (!miniatureChart) {
      paintSpiderNet(canvas, center, polygonsList);
    }

    paintDataPolygon(canvas, dataPoints);
    paintDataPoints(canvas, dataPoints);
    // paintDataValues(canvas, center, dataPoints, data);
  }

  void paintDataPolygon(Canvas canvas, List<Offset> points) {
    Path path = Path()..addPolygon(points, true);

    // draw filling
    canvas.drawPath(path, dataPolygonFillingPaint);
    // draw border
    canvas.drawPath(path, dataPolygonBorderPaint);
  }

  void paintDataPoints(Canvas canvas, List<Offset> points) {
    for (var i = 0; i < points.length; i++) {
      canvas.drawCircle(points[i], miniatureChart ? 2.0 : 4.0, Paint()..color = color);
    }
  }

  void paintDataValues(Canvas canvas, Offset center, List<Offset> points, List<double> data) {
    TextPainter textPainter = TextPainter(textDirection: TextDirection.ltr);
    for (var i = 0; i < points.length; i++) {
      String s = data[i].toStringAsFixed(decimalPrecision);
      textPainter.text = TextSpan(text: s, style: const TextStyle(color: Colors.black));
      textPainter.layout();
      if (points[i].dx < center.dx) {
        textPainter.paint(canvas, points[i].translate(-(textPainter.size.width + 5.0), 0));
      } else if (points[i].dx > center.dx) {
        textPainter.paint(canvas, points[i].translate(5.0, 0));
      } else if (points[i].dy < center.dy) {
        textPainter.paint(canvas, points[i].translate(-(textPainter.size.width / 2), -20));
      } else {
        textPainter.paint(canvas, points[i].translate(-(textPainter.size.width / 2), 4));
      }
    }
  }

  void paintSpiderNet(Canvas canvas, Offset center, List<List<Offset>> pointList) {
    // draw polygons
    for (List<Offset> points in pointList) {
      canvas.drawPoints(PointMode.polygon, [...points, points.first], lines);
    }

    // draw spokes from outer points
    for (Offset point in pointList.first) {
      canvas.drawLine(center, point, spokes);
    }
  }

  void paintLabels(Canvas canvas, Offset center, List<Offset> points, List<IconData> labels) {
    TextPainter textPainter = TextPainter(textDirection: TextDirection.rtl);

    for (int i = 0; i < points.length; i++) {
      // textPainter.text = TextSpan(text: labels[i], style: textStyle);
      textPainter.text = TextSpan(
        text: String.fromCharCode(labels[i].codePoint),
        style: TextStyle(
          fontSize: 24.0,
          fontFamily: labels[i].fontFamily,
          color: Colors.black,
          package: labels[i].fontPackage,
        ),
      );

      textPainter.layout();

      textPainter.paint(
        canvas,
        points[i].translate(-textPainter.size.width / 2, -textPainter.size.height / 2),
      );
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
