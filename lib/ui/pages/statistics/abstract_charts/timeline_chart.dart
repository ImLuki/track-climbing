import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/enums/enums.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:climbing_track/backend/util/custom_date_formatter.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/dialog/info_dialogs.dart';
import 'package:climbing_track/ui/widgets/empty_list_placeholder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:timelines/timelines.dart';

class TimeLineChart extends StatefulWidget {
  final List<List<Map<String, dynamic>>> data;

  const TimeLineChart(
    this.data, {
    super.key,
  });

  @override
  _TimeLineChartState createState() => _TimeLineChartState();
}

class _TimeLineChartState extends State<TimeLineChart> {
  bool indoor = true;
  bool outdoor = true;
  bool toprope = true;
  bool lead = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        widget.data.isEmpty
            ? Padding(
                padding: const EdgeInsets.only(top: 40.0),
                child: EmptyListPlaceHolder(
                  text: AppLocalizations.of(context)!.charts_timeline_no_data,
                  image: "assets/images/3327053.png",
                ),
              )
            : Expanded(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 16.0, left: 12),
                    child: _OuterTimeline(
                      data: widget.data,
                    ),
                  ),
                ),
              ),
      ],
    );
  }
}

class _OuterTimeline extends StatelessWidget {
  final List<List<Map<String, dynamic>>> data;

  const _OuterTimeline({required this.data});

  @override
  Widget build(BuildContext context) {
    return FixedTimeline.tileBuilder(
      theme: TimelineThemeData(
        nodePosition: 0,
        color: const Color(0xff989898),
        indicatorTheme: const IndicatorThemeData(
          position: 0,
          size: 20.0,
        ),
        connectorTheme: const ConnectorThemeData(
          thickness: 1.7,
        ),
      ),
      builder: TimelineTileBuilder.connected(
        itemCount: data.length,
        contentsBuilder: (_, index) {
          return Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  _getSessionTitle(context, data[index].first),
                  style: DefaultTextStyle.of(context).style.copyWith(
                        fontSize: 18.0,
                      ),
                ),
                _InnerTimeline(ascents: data[index]),
              ],
            ),
          );
        },
        indicatorBuilder: (_, index) {
          if (data[index].first[DBController.locationOutdoor] == 1) {
            return const DotIndicator(
              size: 24.0,
              color: Colors.green,
              child: Icon(
                FontAwesomeIcons.mountainSun,
                color: Colors.white,
                size: 12.0,
              ),
            );
          } else {
            return const DotIndicator(
              size: 24.0,
              color: Colors.orangeAccent,
              child: Icon(
                Icons.home,
                color: Colors.white,
                size: 18.0,
              ),
            );
          }
        },
        connectorBuilder: (_, index, ___) => const SolidLineConnector(),
        lastConnectorBuilder: (_) => const SolidLineConnector(),
      ),
    );
  }

  String _getSessionTitle(BuildContext context, Map<String, dynamic> ascend) {
    String date = CustomDateFormatter.formatDateString(context, DateTime.parse(ascend[DBController.sessionTimeStart]));
    String location = ascend[DBController.locationName];
    return "$date | $location";
  }
}

class _InnerTimeline extends StatelessWidget {
  const _InnerTimeline({
    required this.ascents,
  });

  final List<Map<String, dynamic>> ascents;

  @override
  Widget build(BuildContext context) {
    bool isEdgeIndex(int index) {
      return index == 0 || index == ascents.length + 1;
    }

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: FixedTimeline.tileBuilder(
        theme: TimelineTheme.of(context).copyWith(
          nodePosition: 0,
          connectorTheme: TimelineTheme.of(context).connectorTheme.copyWith(
                thickness: 1.0,
              ),
          indicatorTheme: TimelineTheme.of(context).indicatorTheme.copyWith(
                size: 10.0,
                position: 0.5,
              ),
        ),
        builder: TimelineTileBuilder(
          indicatorBuilder: (_, index) => !isEdgeIndex(index)
              ? _getIndicator(
                  StyleController.mapClimbingStyleToStyleType(ascents[index - 1][DBController.ascendStyleId]),
                )
              : null,
          startConnectorBuilder: (_, index) => Connector.solidLine(space: 24.0),
          endConnectorBuilder: (_, index) => Connector.solidLine(space: 24.0),
          contentsBuilder: (_, index) {
            if (isEdgeIndex(index)) {
              return null;
            }

            return Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: InkWell(
                onTap: () => InfoDialog().showAscendDialog(context, Ascend.fromMap(ascents[index - 1])),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${_getHeadline(ascents[index - 1], context)}:",
                      style: const TextStyle(fontWeight: FontWeight.w500),
                      overflow: TextOverflow.ellipsis,
                    ),
                    Row(
                      children: [
                        Flexible(
                          child: Text(
                            Util.getAscendNameWithoutPrefix(
                              ascents[index - 1][DBController.ascendName],
                              context,
                              speedType: ascents[index - 1][DBController.speedType],
                              type: ClimbingType.values.byName(
                                ascents[index - 1][DBController.ascendType],
                              ),
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        const SizedBox(width: 6.0),
                        Text(_getSummary(ascents[index - 1], context)),
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
          itemExtentBuilder: (_, index) => isEdgeIndex(index) ? 10.0 : 40.0,
          nodeItemOverlapBuilder: (_, index) => isEdgeIndex(index) ? true : null,
          itemCount: ascents.length + 2,
        ),
      ),
    );
  }

  Widget _getIndicator(StyleType styleType) {
    double iconSize = 0.0;
    switch (styleType) {
      case StyleType.ONSIGHT:
        iconSize = 20.0;
        break;
      case StyleType.FLASH:
      case StyleType.REDPOINT:
      case StyleType.PROJECT:
        iconSize = 18.0;
        break;
      case StyleType.TOP:
        iconSize = 16.0;
        break;
      case StyleType.NONE:
        break;
    }
    return DotIndicator(
      size: 24.0,
      color: Colors.transparent,
      child: Icon(
        styleType.getIcon(),
        size: iconSize,
      ),
    );
  }

  String _getHeadline(Map<String, dynamic> ascend, BuildContext context) {
    ClimbingType type = ClimbingType.values.byName(ascend[DBController.ascendType]);

    switch (type) {
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.TRAD_CLIMBING:
        return "${StyleController.mapClimbingStyleToStyleType(ascend[DBController.ascendStyleId]).toTranslatedString(context)} "
            "(${Util.getTopOrLead(ascend[DBController.routeTopRope] ?? 0, context, type.shouldRenameTopropeToFollowing())})";
      case ClimbingType.BOULDER:
      case ClimbingType.FREE_SOLO:
      case ClimbingType.DEEP_WATER_SOLO:
      case ClimbingType.MULTI_PITCH:
        return StyleController.mapClimbingStyleToStyleType(ascend[DBController.ascendStyleId])
            .toTranslatedString(context);
      case ClimbingType.SPEED_CLIMBING:
        int speedTime = ascend[DBController.speedTime];
        return "${AppLocalizations.of(context)!.general_time}: ${(speedTime / 1000).toStringAsFixed(2)} "
            "${AppLocalizations.of(context)!.util_seconds}";
    }
  }

  String _getSummary(Map<String, dynamic> ascend, BuildContext context) {
    int gradeID = ascend[DBController.ascendGradeId];
    ClimbingType type = ClimbingType.values.byName(ascend[DBController.ascendType]);

    switch (type) {
      case ClimbingType.SPORT_CLIMBING:
      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.BOULDER:
      case ClimbingType.FREE_SOLO:
      case ClimbingType.DEEP_WATER_SOLO:
      case ClimbingType.MULTI_PITCH:
      case ClimbingType.TRAD_CLIMBING:
        return "(${Util.getGrade(gradeID, type)})";
      case ClimbingType.SPEED_CLIMBING:
        if (ascend[DBController.speedType] != 0) {
          return "(${Util.getGrade(gradeID, type)})";
        }
        return "";
    }
  }
}
