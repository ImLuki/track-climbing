import 'dart:math';

import 'package:climbing_track/backend/model/charts/statistic_charts.dart';
import 'package:flutter/material.dart';

class ZoomableFlChart extends StatefulWidget {
  const ZoomableFlChart({super.key, required this.maxX, required this.builder});

  final double maxX;
  final Widget Function(double, double) builder;

  @override
  State<ZoomableFlChart> createState() => _ZoomableFlChartState();
}

class _ZoomableFlChartState extends State<ZoomableFlChart> {
  late double minX;
  late double maxX;

  late double lastMaxXValue;
  late double lastMinXValue;

  @override
  void initState() {
    super.initState();
    maxX = widget.maxX;
    minX = max(0, maxX - StatisticCharts.MAX_SESSION_AMOUNT);
    lastMaxXValue = maxX;
    lastMinXValue = minX;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onDoubleTap: () {
        setState(() {
          maxX = widget.maxX;
          minX = max(0, maxX - StatisticCharts.MAX_SESSION_AMOUNT);
        });
      },
      onHorizontalDragStart: (details) {
        lastMinXValue = minX;
        lastMaxXValue = maxX;
      },
      onHorizontalDragUpdate: (details) {
        var horizontalDistance = details.primaryDelta ?? 0;
        if (horizontalDistance == 0) return;
        var lastMinMaxDistance = max(lastMaxXValue - lastMinXValue, 0.0);

        setState(() {
          minX -= lastMinMaxDistance * 0.005 * horizontalDistance;
          maxX -= lastMinMaxDistance * 0.005 * horizontalDistance;

          if (minX < 0) {
            minX = 0;
            maxX = lastMinMaxDistance;
          }
          if (maxX > widget.maxX) {
            maxX = widget.maxX;
            minX = maxX - lastMinMaxDistance;
          }
        });
      },
      onScaleStart: (details) {
        lastMinXValue = minX;
        lastMaxXValue = maxX;
      },
      onScaleUpdate: (details) {
        var horizontalScale = details.horizontalScale;
        if (horizontalScale == 0) return;
        var lastMinMaxDistance = max(lastMaxXValue - lastMinXValue, 0);
        var newMinMaxDistance = max(lastMinMaxDistance / horizontalScale, 10);
        var distanceDifference = newMinMaxDistance - lastMinMaxDistance;
        setState(() {
          final newMinX = max(
            lastMinXValue - distanceDifference,
            0.0,
          );
          final newMaxX = min(
            lastMaxXValue + distanceDifference,
            widget.maxX,
          );

          if (newMaxX - newMinX > 2) {
            minX = newMinX;
            maxX = newMaxX;
          }
        });
      },
      child: widget.builder(minX, maxX),
    );
  }
}
