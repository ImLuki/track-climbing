import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:showcaseview/showcaseview.dart';

class ChartEditPageWrapper extends StatelessWidget {
  final List<AbstractChart> elements;
  final List<int> order;

  const ChartEditPageWrapper({super.key, required this.elements, required this.order});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ShowCaseWidget(
        builder: (context) => ChartEditPage(
          elements: elements,
          order: order,
        ),
      ),
    );
  }
}

class ChartEditPage extends StatefulWidget {
  const ChartEditPage({super.key, required this.elements, required this.order});

  @override
  State<ChartEditPage> createState() => _ChartEditPageState(order.map((e) => Item(elements[e], e)).toList());

  final List<AbstractChart> elements;
  final List<int> order;
}

class Item {
  final AbstractChart chart;
  final int number;

  Item(this.chart, this.number);
}

class _ChartEditPageState extends State<ChartEditPage> {
  final List<Item> _items;
  final GlobalKey _tipKey = GlobalKey();

  _ChartEditPageState(this._items);

  @override
  Widget build(BuildContext context) {
    //showing tooltip
    WidgetsBinding.instance.addPostFrameCallback((_) => _showToolTip());

    return PopScope(
      onPopInvoked: (_) async {
        _storeNewValues();
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            AppLocalizations.of(context)!.chart_page_setting_title,
          ),
          actions: [
            IconButton(
              tooltip: AppLocalizations.of(context)!.general_reset,
              onPressed: () {
                _items.sort((a, b) => a.number.compareTo(b.number));
                setState(() {});
                ScaffoldMessenger.of(context)
                  ..removeCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(
                      behavior: SnackBarBehavior.floating,
                      content: Text(AppLocalizations.of(context)!.chart_page_reset_chart_order),
                    ),
                  );
              },
              icon: const Icon(Icons.refresh),
            ),
          ],
        ),
        body: _createReorderableList(),
      ),
    );
  }

  Widget _createReorderableList() {
    return ReorderableListView(
      children: <Widget>[
        for (int index = 0; index < _items.length; index++)
          index == 0
              ? Showcase(
                  key: _tipKey,
                  description: AppLocalizations.of(context)!.chart_page_settings_tooltip,
                  tooltipPadding: const EdgeInsets.all(12.0),
                  child: _buildListTile(index),
                )
              : _buildListTile(index),
      ],
      onReorder: (int oldIndex, int newIndex) {
        setState(() {
          if (oldIndex < newIndex) {
            newIndex -= 1;
          }
          final Item item = _items.removeAt(oldIndex);
          _items.insert(newIndex, item);
        });
      },
    );
  }

  Widget _buildListTile(int index) {
    return ListTile(
      key: Key('$index'),
      title: Text(
        _items[index].chart.getTitle(context).replaceAll(Util.NULL_REPLACEMENT, ""),
        overflow: TextOverflow.ellipsis,
        maxLines: 2,
        style: Theme.of(context).textTheme.bodyLarge,
      ),
      leading: Checkbox(
        activeColor: Colors.blue,
        value: _items[index].chart.visible,
        onChanged: (bool? value) {
          if (value == null) return;
          setState(() {
            _items[index].chart.setVisibility(value);
          });
        },
      ),
      trailing: const Icon(Icons.menu),
    );
  }

  void _storeNewValues() {
    String order = _items.map((e) => e.number).join("_");
    DataStore().settingsDataStore.chartOrder = order;
  }

  void _showToolTip() async {
    if (!DataStore().toolTipsDataStore.tooltipChartSettings) return;

    await Future.delayed(const Duration(milliseconds: 500));

    if (mounted) {
      DataStore().toolTipsDataStore.tooltipChartSettings = false;
      ShowCaseWidget.of(context).startShowCase([_tipKey]);
    }
  }
}
