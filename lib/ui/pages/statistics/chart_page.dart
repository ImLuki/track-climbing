import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/charts/statistic_charts.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/pages/statistics/chart_edit_page.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/util/custom_page_route.dart';
import 'package:climbing_track/ui/widgets/empty_list_placeholder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class ChartPage extends StatefulWidget {
  const ChartPage({super.key});

  @override
  _ChartPageState createState() => _ChartPageState();
}

class _ChartPageState extends State<ChartPage> {
  static const double animationDelay = 250.0;
  ChartType _currentChartType = ChartType.ALL;

  @override
  void dispose() {
    // reset charts
    for (AbstractChart abstractChart in StatisticCharts.chartList) {
      abstractChart.futureData = null;
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final List<int> orderList = [];

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: _getTitle(),
        actions: [
          if (_currentChartType == ChartType.ALL)
            IconButton(
              tooltip: AppLocalizations.of(context)!.general_edit,
              icon: const Icon(Icons.edit),
              onPressed: () => _pushChartEditingPage(orderList),
            ),
        ],
      ),
      body: _body(orderList),
    );
  }

  Widget _body(List<int> orderList) {
    String order = DataStore().settingsDataStore.chartOrder ?? buildStandardOrder();

    orderList.clear();
    orderList.addAll(order.split("_").map(int.parse));
    if (orderList.length != StatisticCharts.chartList.length) {
      DataStore().settingsDataStore.resetChartOrder();
      order = buildStandardOrder();
      orderList.clear();
      orderList.addAll(order.split("_").map(int.parse));
    }

    AbstractChart.loadYears();
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        if (DataStore().locationsDataStore.locations.isEmpty) return _createNoDataWarning();
        if (!StatisticCharts.chartList.any((element) => element.visible) && _currentChartType == ChartType.ALL) {
          return _createNoChartSelectedWarning();
        }
        return _createBody(context, orderList);
      },
    );
  }

  Widget _getTitle() {
    return Center(
      child: DropdownButton(
        borderRadius: BorderRadius.circular(20.0),
        isExpanded: true,
        alignment: Alignment.center,
        underline: Container(),
        onChanged: (ChartType? chart) {
          if (chart == null) return;
          setState(() {
            _currentChartType = chart;
            StatisticCharts.init(currentChartType: _currentChartType);
          });
        },
        value: _currentChartType,
        items: ChartType.values.map((ChartType item) {
          return DropdownMenuItem<ChartType>(
            alignment: Alignment.center,
            value: item,
            child: Text(
              Util.replaceStringWithBlankSpaces(getNameMapping(context, item)),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                    fontSize: 22,
                  ),
            ),
          );
        }).toList(),
      ),
    );
  }

  Widget _createNoDataWarning() {
    return EmptyListPlaceHolder(
      text: AppLocalizations.of(context)!.chart_page_no_data,
      image: "assets/images/2920349.png",
    );
  }

  Widget _createNoChartSelectedWarning() {
    return EmptyListPlaceHolder(
      text: AppLocalizations.of(context)!.chart_page_no_selected_chart,
      image: "assets/images/2920349.png",
    );
  }

  Widget _createBody(BuildContext context, List<int> indices) {
    List<Widget> list = _chartListWithContainer(indices);
    return ScrollablePositionedList.builder(
      itemCount: list.length,
      itemBuilder: (BuildContext context, int index) => list[index],
    );
  }

  List<Widget> _chartListWithContainer(List<int> indices) {
    int i = 0;
    List<Widget> list;
    if (_currentChartType == ChartType.ALL) {
      list = List.generate(
        indices.length,
        (index) => StatisticCharts.chartList[indices[index]]
            .getWidget(context, StatisticCharts.chartList[indices[index]].visible ? animationDelay * i++ : 0.0),
      );
    } else {
      list = StatisticCharts.chartList
          .where((element) => element.getType() == _currentChartType)
          .map((e) => e.getWidget(context, 200.0 * i++, force: true))
          .toList();
    }
    list.add(const SizedBox(height: 8.0));
    return list;
  }

  String buildStandardOrder() {
    return List<String>.generate(StatisticCharts.chartList.length, (index) => "$index").join("_");
  }

  String getNameMapping(BuildContext context, ChartType type) {
    switch (type) {
      case ChartType.ALL:
        return AppLocalizations.of(context)!.chart_page_overview;
      case ChartType.SPORT_CLIMBING:
        return AppLocalizations.of(context)!.style_sport_climbing;
      case ChartType.BOULDER:
        return AppLocalizations.of(context)!.style_bouldering;
      case ChartType.SPEED_CLIMBING:
        return AppLocalizations.of(context)!.style_speed_climbing;
      case ChartType.FREE_SOLO:
        return AppLocalizations.of(context)!.style_free_solo_climbing;
      case ChartType.DEEP_WATER_SOLO:
        return AppLocalizations.of(context)!.style_dws_climbing;
      case ChartType.ICE_CLIMBING:
        return AppLocalizations.of(context)!.style_ice_climbing;
      case ChartType.MULTI_PITCH:
        return AppLocalizations.of(context)!.style_multi_pitch_climbing;
      case ChartType.TRAD:
        return AppLocalizations.of(context)!.style_trad_climbing;
    }
  }

  void _pushChartEditingPage(List<int> orderList) {
    Navigator.push(
      context,
      CustomPageRoute.build(
        builder: (BuildContext context) => ChartEditPageWrapper(
          elements: StatisticCharts.chartList,
          order: orderList,
        ),
      ),
    ).then(
      (value) => setState(() {
        StatisticCharts.init(currentChartType: _currentChartType);
      }),
    );
  }
}
