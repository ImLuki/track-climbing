import 'dart:math';

import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/charts/statistic_charts.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:climbing_track/ui/pages/statistics/expanded_chart.dart';
import 'package:climbing_track/ui/util/custom_page_route.dart';
import 'package:climbing_track/ui/widgets/empty_list_placeholder.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum ChartType {
  ALL,
  SPORT_CLIMBING,
  BOULDER,
  SPEED_CLIMBING,
  MULTI_PITCH,
  TRAD,
  ICE_CLIMBING,
  DEEP_WATER_SOLO,
  FREE_SOLO,
}

abstract class AbstractChart {
  static const String ACTIVE_KEY = "chart_active";
  static const String NO_DATA_IMAGE_PATH = "assets/images/3327053.png";
  static const String NO_INTERNET_IMAGE_PATH = "assets/images/no-wifi.png";

  static List<String> yearList = [];
  late bool visible;
  Future? futureData;

  /// if true this kind of ascends should be displayed
  bool indoor = true;
  bool outdoor = true;
  bool toprope = true;
  bool lead = true;

  /// Initialize chart. visibility value
  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    visible = prefs.getBool(_visibilityKey) ?? getStandardVisibility();
  }

  ///Returns unique key
  String getKey();

  /// Returns name of chart as [String]
  ///
  /// Must be overwritten
  String getName(BuildContext context);

  /// Returns type of chart as instance of enum [ChartType]
  ///
  /// Must be overwritten to set chart type
  ChartType getType();

  /// Load data as future and writes future in variable [futureData]
  void loadData();

  /// Returns chart as instance of class [Widget] to be rendered
  /// Called in [getChart()]
  ///
  /// [preCalculatedData] The precalculated information to plot chart
  /// [extended] If true chart is expanded on whole page
  Widget chart(BuildContext context, dynamic preCalculatedData, bool extended, double animationDelay);

  /// Returns expanded chart as instance of class [Widget] to be rendered
  /// Called in [getExpandedWidget()]
  Widget expanded();

  /// Pre calculate raw data to be accessed by chart builder
  ///
  /// [input] the raw data from database
  dynamic calculateData(BuildContext context, dynamic input);

  /// Pre filter list of ascends
  ///
  /// [ascends] List of ascends, raw from database
  ///
  /// Returns list of ascends fitting to settings
  List<Map<String, dynamic>> filterData(List<Map<String, dynamic>> ascends) {
    return ascends
        .where((elem) =>
            (elem[DBController.locationOutdoor] == null
                ? true
                : ((elem[DBController.locationOutdoor] == 1 && outdoor) ||
                    (elem[DBController.locationOutdoor] == 0 && indoor))) &&
            (elem[DBController.routeTopRope] == null
                ? true
                : ((elem[DBController.routeTopRope] == 1 && toprope) ||
                    (elem[DBController.routeTopRope] == 0 && lead))))
        .toList();
  }

  /// Returns Widget as instance of class [Widget] containing chart container with chart to be rendered if visible
  /// Otherwise returning empty [Container]
  ///
  /// [context] : Instance current [BuildContext]
  /// [force] : If true returning chart container with chart regardless of [visible] value
  Widget getWidget(BuildContext context, double animationDelay, {bool force = false}) {
    return visible || force ? createChartContainer(context, animationDelay) : Container();
  }

  /// Returns title as [String] of chart
  /// Consists of name and type of the chart
  String getTitle(BuildContext context) {
    return Util.replaceStringWithBlankSpaces(getName(context) + typeSuffix(context));
  }

  /// Returns standard visible value, if no value of [visible] has been stored yet
  bool getStandardVisibility() {
    return getType() == ChartType.ALL || getType() == ChartType.SPORT_CLIMBING;
  }

  /// Returns expanded chart as instance of class [Widget] to be rendered
  Widget getExpandedWidget() {
    return Container(
      padding: const EdgeInsets.only(left: 10, top: 15),
      color: Colors.white,
      child: expanded(),
    );
  }

  /// Returns widget which is displayed when no data is available
  /// Returns instance of class [Widget]
  Widget noDataWidget(BuildContext context, {bool expanded = false, bool addPadding = false}) {
    if (expanded) {
      return EmptyListPlaceHolder(
        text: AppLocalizations.of(context)!.charts_no_data_placeholder_text,
        image: NO_DATA_IMAGE_PATH,
      );
    } else {
      return Padding(
        padding: EdgeInsets.only(top: addPadding ? 6.0 : 0.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Opacity(
              opacity: 0.8,
              child: Image.asset(
                NO_DATA_IMAGE_PATH,
                width: 40,
                height: 40,
                fit: BoxFit.cover,
              ),
            ),
            const SizedBox(width: Constants.STANDARD_PADDING),
            Padding(
              padding: const EdgeInsets.only(bottom: 2.0),
              child: Text(
                AppLocalizations.of(context)!.charts_no_data_placeholder_text,
                style: const TextStyle(color: Colors.black38, fontSize: 17),
              ),
            ),
          ],
        ),
      );
    }
  }

  /// Returns widget which is displayed when no internet is available
  /// Returns instance of class [Widget]
  Widget noInternetWidget(BuildContext context, {bool expanded = false, bool addPadding = false}) {
    if (expanded) {
      return EmptyListPlaceHolder(
        text: AppLocalizations.of(context)!.charts_no_internet_placeholder_text_extended,
        image: NO_INTERNET_IMAGE_PATH,
      );
    } else {
      return Padding(
        padding: EdgeInsets.only(top: addPadding ? 6.0 : 0.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Opacity(
              opacity: 0.8,
              child: Image.asset(
                NO_INTERNET_IMAGE_PATH,
                width: 40,
                height: 40,
                fit: BoxFit.cover,
              ),
            ),
            const SizedBox(width: Constants.STANDARD_PADDING),
            Padding(
              padding: const EdgeInsets.only(bottom: 2.0),
              child: Text(
                AppLocalizations.of(context)!.charts_no_internet_placeholder_text,
                style: const TextStyle(color: Colors.black38, fontSize: 17),
                overflow: TextOverflow.clip,
              ),
            ),
          ],
        ),
      );
    }
  }

  /// If there is a choice of different charts, override this method.
  /// Should return titles of all sub charts which can be selected
  List<String> getTitleSelection(BuildContext context) => [];

  /// Future builder which return chart in chart container if data are available
  /// Otherwise returns placeholder
  Widget getChart({
    double animationDelay = 0,
    bool extended = false,
    bool addPaddingIfNoData = false,
    bool checkInternet = false,
  }) {
    if (futureData == null) {
      loadData();
    }
    return FutureBuilder(
      future: Future.wait([futureData!, _checkInternetConnection(checkInternet)]),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Container();
        }
        var data = calculateData(context, snapshot.data[0]);

        // check internet
        if (!snapshot.data[1]) {
          return noInternetWidget(context, expanded: extended, addPadding: addPaddingIfNoData);
        }

        if (data.runtimeType is List) {
          if (data.first.runtimeType is List) {
            if (data.first.isEmpty) return noDataWidget(context, expanded: extended, addPadding: addPaddingIfNoData);
          } else {
            if (data.isEmpty) return noDataWidget(context, expanded: extended, addPadding: addPaddingIfNoData);
          }
        } else {
          if (data.isEmpty) return noDataWidget(context, expanded: extended, addPadding: addPaddingIfNoData);
        }

        return chart(context, data, extended, animationDelay);
      },
    );
  }

  /// Will be called if sub chart selection changes
  ///
  /// [value] The title of the selected sub chart
  ///
  /// Override this method to implement appropriate functionality
  void onChangeTitleSelection(BuildContext context, String value) {}

  /// limit data to [StatisticCharts.MAX_SESSION_AMOUNT] entries if corresponding attribute [limitCharts] is set
  List limitDate(List<dynamic> data) {
    // Data have to be sorted before copping
    return DataStore().settingsDataStore.limitCharts
        ? data.getRange(max(0, data.length - StatisticCharts.MAX_SESSION_AMOUNT), data.length).toList()
        : data;
  }

  /// Set visibility of chart
  /// Stores value locally with [SharedPreferences] library
  void setVisibility(bool visibility) async {
    visible = visibility;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(_visibilityKey, visible);
  }

  Widget createChartContainer(BuildContext context, double animationDelay) {
    return InkWell(
      onTap: () => showChart(context),
      focusColor: Colors.transparent,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        margin: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
        padding: const EdgeInsets.only(
          left: Constants.STANDARD_PADDING,
          right: Constants.STANDARD_PADDING,
          bottom: 8.0,
          top: 14,
        ),
        width: MediaQuery.of(context).size.width,
        height: 100,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black12, width: 2), // added
          borderRadius: BorderRadius.circular(25.0),
        ),
        child: Column(
          children: [
            Flexible(
              fit: FlexFit.tight,
              child: Padding(
                padding: const EdgeInsets.only(left: 30.0, right: 30.0),
                child: getChart(animationDelay: animationDelay),
              ),
            ),
            const SizedBox(height: 10.0),
            Text(
              getTitle(context),
              maxLines: 1,
              overflow: TextOverflow.visible,
              style: Theme.of(context).textTheme.bodyLarge,
            ),
          ],
        ),
      ),
    );
  }

  void showChart(BuildContext context) {
    Navigator.push(
      context,
      CustomPageRoute.build(
        builder: (BuildContext context) => ExpandedChart(
          expanded: this,
          title: getTitleSelection(context).isNotEmpty ? getTitleSelection(context) : [getTitle(context)],
          onChangeSelection: (val) => onChangeTitleSelection(context, val),
          appBarActions: getAppBarActions(context),
          floatingActionButton: getFAB(context),
        ),
      ),
    );
  }

  String typeSuffix(BuildContext context) {
    switch (getType()) {
      case ChartType.ALL:
        return "";
      case ChartType.SPORT_CLIMBING:
        return " [${AppLocalizations.of(context)!.style_sport_climbing}]";
      case ChartType.BOULDER:
        return " [${AppLocalizations.of(context)!.style_boulder}]";
      case ChartType.SPEED_CLIMBING:
        return " [${AppLocalizations.of(context)!.style_speed_prefix}]";
      case ChartType.FREE_SOLO:
        return " [${AppLocalizations.of(context)!.style_free_solo}]";
      case ChartType.DEEP_WATER_SOLO:
        return " [${AppLocalizations.of(context)!.style_dws}]";
      case ChartType.ICE_CLIMBING:
        return " [${AppLocalizations.of(context)!.style_ice_climbing}]";
      case ChartType.MULTI_PITCH:
        return " [${AppLocalizations.of(context)!.style_multi_pitch_climbing}]";
      case ChartType.TRAD:
        return " [${AppLocalizations.of(context)!.style_trad_climbing}]";
    }
  }

  String get _visibilityKey => "${ACTIVE_KEY}_${getKey()}_${getType().name.toLowerCase()}";

  /// Load list of years which contains each year since app is first used
  static void loadYears() async {
    int lastYear = DataStore().settingsDataStore.firstYear;

    DateTime now = DateTime.now();

    if (lastYear > now.year) lastYear = now.year;

    List<String> years = List.generate(now.year - lastYear + 1, (index) => "${lastYear + index}");
    AbstractChart.yearList = years.reversed.toList();
  }

  List<Widget> getAppBarActions(BuildContext context) => const [];

  Widget? getFAB(BuildContext context) => null;

  Future<void> showInfoDialog(BuildContext context, {required String title, required String text}) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: true,
          title: Text(title),
          titleTextStyle: Theme.of(context).textTheme.titleLarge,
          contentTextStyle: Theme.of(context).textTheme.bodyLarge,
          contentPadding: const EdgeInsets.only(left: 24.0, right: 24.0, top: 20.0),
          content: Text(text),
          actions: <Widget>[
            TextButton(
              child: Text(AppLocalizations.of(context)!.general_back),
              onPressed: () {
                FocusScope.of(context).unfocus();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<bool> _checkInternetConnection(bool checkInternet) async {
    if (!checkInternet) return true;
    List<ConnectivityResult> connectivityResults = await (Connectivity().checkConnectivity());
    return connectivityResults.any((e) => e != ConnectivityResult.none);
  }

  /// Whether the checkbox for lead should be visible or not
  bool leadCheckBoxActivated() => true;

  /// Whether the checkbox for toprope should be visible or not
  bool topropeCheckBoxActivated() => true;

  /// Whether the checkbox for indoor should be visible or not
  bool indoorCheckBoxActivated() => true;

  /// Whether the checkbox for outdoor should be visible or not
  bool outdoorCheckBoxActivated() => true;

  /// Will be called if checkbox selection changes
  void onCheckboxSelectionChanged() {}
}
