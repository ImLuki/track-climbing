import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/average_climbing_grade_chart.dart';

class AverageBoulderGradeChart extends AverageClimbingGradeChart {
  @override
  ChartType getType() => ChartType.BOULDER;

  @override
  ClimbingType getClimbingType() => ClimbingType.BOULDER;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;
}
