import 'dart:math';

import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/database/sql_query_builder.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/charts/data_points.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/best_grades_climbing.dart';
import 'package:climbing_track/ui/pages/statistics/syncfusion_charts/syncfusion_date_line_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BestGradesBoulderChart extends BestGradesClimbingChart {
  @override
  ClimbingType getClimbingType() => ClimbingType.BOULDER;

  @override
  Future<List<Map<String, dynamic>>> fetchData2() async {
    return DBController().rawQuery(
      "SELECT ${DBController.ascendGradeId}, ${DBController.sessionId} "
      "FROM ${DBController.ascendsTable} "
      "WHERE ${SQLQueryBuilder.getBoulderTopCondition()} AND "
      "${DBController.ascendType} == (?)",
      [getClimbingType().name],
    );
  }

  @override
  ChartType getType() => ChartType.BOULDER;

  @override
  Widget chart(BuildContext context, dynamic preCalculatedData, extended, double animationDelay) {
    int maxIndex = GradeManager().getGradeMapper(type: getClimbingType()).maxIndexOfNormalized();

    double? maximum = (preCalculatedData as List<DateTimeDataPoint>).map((e) => e.yPoint ?? 0.0).reduce(max);
    maximum += maximum.toInt().isEven ? 2 : 1;
    maximum = min(maximum, maxIndex.toDouble());
    maximum = maximum == 0.0 ? null : maximum;

    double? minimum = (preCalculatedData).map((e) => e.yPoint ?? double.infinity).reduce(min);
    minimum -= minimum.toInt().isEven ? 2 : 1;
    minimum = max(minimum, 0.0);
    minimum = minimum == double.infinity ? null : minimum;

    return SyncfusionDateTimeLineChart(
      preCalculatedData,
      extended,
      AppLocalizations.of(context)!.charts_grade,
      maxY: DataStore().settingsDataStore.isYAxisExpanded ? maxIndex.toDouble() : maximum,
      minY: DataStore().settingsDataStore.isYAxisExpanded ? 0.0 : minimum ?? 0.0,
      maximumLabels: DataStore().settingsDataStore.isYAxisExpanded ? null : maximum! - minimum!,
      measureFormatter: (double value) =>
          GradeManager().getGradeMapper(type: getClimbingType()).labelFromIndex(value.round()),
      yLabelFormatter: (double value) =>
          GradeManager().getGradeMapper(type: getClimbingType()).labelFromIndex(value.round()),
      animationDelay: animationDelay,
    );
  }

  @override
  dynamic calculateData(BuildContext context, dynamic input) {
    List<Map<String, dynamic>> sessions = input[0];
    List<Map<String, dynamic>> data = input[1];
    if (data.isEmpty) return [];

    Map<int, int> grades = {};
    Map<int, dynamic> dateIDMapper = {};
    for (Map<String, dynamic> session in sessions) {
      grades[session[DBController.sessionId]] = 0;
      dateIDMapper[session[DBController.sessionId]] = session;
    }

    for (Map<String, dynamic> element in data) {
      grades[element[DBController.sessionId]] = max(
        element[DBController.ascendGradeId],
        grades[element[DBController.sessionId]]!,
      );
    }

    List<DateTimeDataPoint> out = [];
    for (int sessionID in grades.keys) {
      if (!((dateIDMapper[sessionID][DBController.locationOutdoor] == 1 && outdoor) ||
          (dateIDMapper[sessionID][DBController.locationOutdoor] == 0 && indoor))) {
        continue;
      }

      double? bestGrade = grades[sessionID] == 0
          ? null
          : GradeManager().getGradeMapper(type: getClimbingType()).idToIndex(grades[sessionID]!).toDouble();
      DateTime start = DateTime.parse(dateIDMapper[sessionID][DBController.sessionTimeStart]);
      out.add(DateTimeDataPoint(start, bestGrade));
    }

    return out;
  }
}
