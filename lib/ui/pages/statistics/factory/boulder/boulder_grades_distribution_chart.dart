import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/climbing_grades_distribution_chart.dart';

class BoulderGradesDistributionChart extends ClimbingGradesDistributionChart {
  @override
  ChartType getType() => ChartType.BOULDER;

  @override
  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT "
      "${getClimbingStyleSQLString()}"
      "${DBController.ascendGradeId} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionsTable} using(${DBController.sessionId}) "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE ${DBController.ascendType} == (?) AND "
      "((${DBController.locationOutdoor} == (?) OR (?)) AND (?)) "
      "GROUP BY ${DBController.ascendGradeId} ",
      [
        getClimbingType().name,
        outdoor ? 1 : 0,
        (outdoor && indoor) ? 1 : 0,
        (outdoor || indoor) ? 1 : 0,
      ],
    );
  }

  @override
  ClimbingType getClimbingType() => ClimbingType.BOULDER;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;
}
