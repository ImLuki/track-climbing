import 'dart:math';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/climbing_last_year_chart.dart';
import 'package:climbing_track/ui/pages/statistics/syncfusion_charts/syncfusion_stacked_bar_line_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LastYearBoulderChart extends LastYearClimbingChart {
  @override
  ChartType getType() => ChartType.BOULDER;

  @override
  List<List<double>> calculateData(BuildContext context, input) {
    // Filter data
    List<Map<String, dynamic>> filteredData = filterData(input);

    DateTime now = DateTime.now();
    DateTime fromUpdated = from ?? DateTime(now.year - 1, now.month + 1, 1, 0, 0, 0, 0, 0);
    DateTime toUpdated = to ?? now;
    List data = filteredData
        .where((element) =>
            DateTime.parse(element[DBController.sessionTimeStart]).isAfter(fromUpdated) &&
            DateTime.parse(element[DBController.sessionTimeStart]).isBefore(toUpdated))
        .toList();

    List<List<double>> lastYear = [
      List.filled(12, 0.0),
      List.filled(12, 0.0),
    ];
    for (Map<String, dynamic> element in data) {
      lastYear[1][DateTime.parse(element[DBController.sessionTimeStart]).month - 1] += 1;
      if (StyleController.getBoulderTopIds().contains(element[DBController.ascendStyleId])) {
        lastYear[0][DateTime.parse(element[DBController.sessionTimeStart]).month - 1] = max(
          lastYear[0][DateTime.parse(element[DBController.sessionTimeStart]).month - 1],
          GradeManager()
              .getGradeMapper(type: getClimbingType())
              .idToIndex(element[DBController.ascendGradeId])
              .toDouble(),
        );
      }
    }
    return lastYear;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended, double animationDelay) {
    return SyncfusionStackedBarLineChart(
      data: preCalculatedData,
      expanded: extended,
      legends: [
        AppLocalizations.of(context)!.charts_amount,
        AppLocalizations.of(context)!.charts_best,
      ],
      chartColors: [
        AppColors.chart_colors[1],
        AppColors.chart_colors[3],
      ],
      reorderMonths: from == null,
      barMeasureFormatter: (double value) => value.round().toString(),
      lineMeasureFormatter: (double value) =>
          GradeManager().getGradeMapper(type: getClimbingType()).labelFromIndex(value.round()),
      lineYLabelFormatter: (double value) =>
          GradeManager().getGradeMapper(type: getClimbingType()).labelFromIndex(value.round()),
      maxY: GradeManager().getGradeMapper(type: getClimbingType()).maxIndexOfNormalized().toDouble(),
      animationDelay: animationDelay,
    );
  }

  @override
  ClimbingType getClimbingType() => ClimbingType.BOULDER;
}
