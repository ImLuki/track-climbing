import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/steepness_climbing_chart.dart';

class SteepnessBoulderChart extends SteepnessClimbingChart {
  @override
  ChartType getType() => ChartType.BOULDER;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  ClimbingType getClimbingType() => ClimbingType.BOULDER;
}
