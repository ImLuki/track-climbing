import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/enums/enums.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/timeline_climbing.dart';

class TimelineBoulderChart extends TimelineClimbingChart {
  @override
  ChartType getType() => ChartType.BOULDER;

  @override
  ClimbingType getClimbingType() => ClimbingType.BOULDER;

  @override
  String getKey() => "timeline_sport_boulder";

  @override
  Classes getClassType(Map<String, dynamic> ascend) => Classes.BOULDER;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;
}
