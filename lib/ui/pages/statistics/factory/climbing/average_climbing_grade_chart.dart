import 'dart:math';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/charts/data_points.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/syncfusion_charts/syncfusion_date_line_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AverageClimbingGradeChart extends AbstractChart {
  @override
  calculateData(BuildContext context, input) {
    if (input[0].length <= 1) return [];

    var sessionData = filterData(input[1]);
    var ascentsData = filterData(input[0]);

    Map<int, double> amountMap = {};
    Map<int, int> countMap = {};
    Map<int, DateTime> dateMap = {};
    for (Map<String, dynamic> session in sessionData) {
      amountMap[session[DBController.sessionId]] = 0;
      countMap[session[DBController.sessionId]] = 0;
      dateMap[session[DBController.sessionId]] = DateTime.parse(session[DBController.sessionTimeStart]);
    }

    for (Map<String, dynamic> element in ascentsData) {
      if (amountMap[element[DBController.sessionId]] != null) {
        amountMap[element[DBController.sessionId]] = (amountMap[element[DBController.sessionId]] ?? 0) +
            GradeManager().getGradeMapper(type: getClimbingType()).normalize(
                  element[DBController.ascendGradeId],
                );
        countMap[element[DBController.sessionId]] = countMap[element[DBController.sessionId]]! + 1;
      }
    }

    // remove empty entries
    amountMap.removeWhere((key, value) => countMap[key] == 0);
    dateMap.removeWhere((key, value) => !amountMap.containsKey(key));

    // calculate average
    List<double> averageNormalized = amountMap
        .map(
          (key, value) => MapEntry(key, countMap[key] == 0 ? 0.0 : value / (countMap[key] as int)),
        )
        .values
        .toList();

    List<DateTimeDataPoint> results = List.generate(
      dateMap.values.length,
      (index) => DateTimeDataPoint(
        dateMap.values.toList()[index],
        GradeManager().getGradeMapper(type: getClimbingType()).indexFromNormalized(averageNormalized[index]).toDouble(),
      ),
    );

    // chart only visible if contains more than 2 sessions
    // if (results.length <= 2) return [];

    return results;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended, double animationDelay) {
    int maxIndex = GradeManager().getGradeMapper(type: getClimbingType()).maxIndexOfNormalized();

    double? maximum = (preCalculatedData as List<DateTimeDataPoint>).map((e) => e.yPoint ?? 0.0).reduce(max);
    maximum += maximum.toInt().isEven ? 2 : 1;
    maximum = min(maximum, maxIndex.toDouble());
    maximum = maximum == 0.0 ? null : maximum;

    double? minimum = (preCalculatedData).map((e) => e.yPoint ?? double.infinity).reduce(min);
    minimum -= minimum.toInt().isEven ? 2 : 1;
    minimum = max(minimum, 0.0);
    minimum = minimum == double.infinity ? null : minimum;

    return SyncfusionDateTimeLineChart(
      preCalculatedData,
      extended,
      AppLocalizations.of(context)!.charts_average_grade_chart,
      maxY: DataStore().settingsDataStore.isYAxisExpanded ? maxIndex.toDouble() : maximum,
      minY: DataStore().settingsDataStore.isYAxisExpanded ? 0.0 : minimum ?? 0,
      maximumLabels: DataStore().settingsDataStore.isYAxisExpanded ? null : (maximum ?? 0) - (minimum ?? 0),
      measureFormatter: (double value) =>
          GradeManager().getGradeMapper(type: getClimbingType()).labelFromIndex(value.round()),
      yLabelFormatter: (double value) =>
          GradeManager().getGradeMapper(type: getClimbingType()).labelFromIndex(value.round()),
      useLegendNameForTrackball: false,
      fillArea: true,
      chartColors: [AppColors.chart_colors[2]],
      linearGradient: LinearGradient(
        colors: <Color>[AppColors.chart_colors[0].withOpacity(0.35), AppColors.chart_colors[3].withOpacity(0.35)],
        begin: Alignment.bottomCenter,
        end: Alignment.topCenter,
      ),
      // areaOpacity: 0.35,
      animationDelay: animationDelay,
    );
  }

  @override
  Widget expanded() {
    return Container(
      padding: const EdgeInsets.only(left: 0, right: 20, top: 10.0),
      child: getChart(extended: true),
    );
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context)!.charts_average_grade_chart;

  @override
  ChartType getType() => ChartType.SPORT_CLIMBING;

  @override
  void loadData() {
    futureData = Future.wait([fetchData1(), _fetchData2()]);
  }

  Future<List<Map<String, dynamic>>> fetchData1() async {
    return DBController().rawQuery(
      "SELECT ${DBController.sessionId}, ${DBController.ascendGradeId}, ${DBController.routeTopRope} "
      "FROM ${DBController.ascendsTable} "
      "WHERE ${DBController.ascendType} == (?)",
      [getClimbingType().name],
    );
  }

  Future<List<Map<String, dynamic>>> _fetchData2() async {
    return DBController().rawQuery(
      "SELECT ${DBController.sessionId}, ${DBController.sessionTimeStart}, ${DBController.locationOutdoor} "
      "FROM ${DBController.sessionsTable} "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE ${DBController.sessionStatus} != (?) "
      "ORDER BY ${DBController.sessionTimeStart} DESC",
      [Session.SESSION_ACTIVE_STATE],
    );
  }

  ClimbingType getClimbingType() => ClimbingType.SPORT_CLIMBING;

  @override
  String getKey() => "average_grade";

  @override
  void onCheckboxSelectionChanged() {
    loadData();
  }

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.charts_average_grade_chart_info_title,
          text: AppLocalizations.of(context)!.charts_average_grade_chart_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }
}
