import 'dart:math';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/charts/data_points.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/syncfusion_charts/syncfusion_date_line_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BestAndAverageGradesClimbingChart extends AbstractChart {
  @override
  String getName(BuildContext context) => AppLocalizations.of(context)!.charts_best_and_average_grades;

  @override
  ChartType getType() => ChartType.SPORT_CLIMBING;

  @override
  void loadData() {
    futureData = Future.wait([fetchSessions(), fetchAscends()]);
  }

  @override
  Widget chart(BuildContext context, dynamic preCalculatedData, extended, double animationDelay) {
    int maxIndex = GradeManager().getGradeMapper(type: getClimbingType()).maxIndexOfNormalized();
    double? maximum = max(
      (preCalculatedData[0] as List<DateTimeDataPoint>).map((e) => e.yPoint ?? 0.0).reduce(max),
      (preCalculatedData[1] as List<DateTimeDataPoint>).map((e) => e.yPoint ?? 0.0).reduce(max),
    );
    maximum += maximum.toInt().isEven ? 2 : 1;
    maximum = min(maximum, maxIndex.toDouble());
    maximum = maximum == 0.0 ? null : maximum;

    double? minimum = min(
      (preCalculatedData[0] as List<DateTimeDataPoint>).map((e) => e.yPoint ?? double.infinity).reduce(min),
      (preCalculatedData[1] as List<DateTimeDataPoint>).map((e) => e.yPoint ?? double.infinity).reduce(min),
    );
    minimum -= minimum.toInt().isEven ? 2 : 1;
    minimum = max(minimum, 0.0);
    minimum = minimum == double.infinity ? null : minimum;

    return SyncfusionDateTimeLineChart(
      preCalculatedData[0],
      extended,
      getLegendOne(context),
      legendTwo: getLegendTwo(context),
      data2: preCalculatedData[1],
      chartColors: [AppColors.chart_colors[2], AppColors.chart_colors[1]],
      maxY: DataStore().settingsDataStore.isYAxisExpanded ? maxIndex.toDouble() : maximum,
      minY: DataStore().settingsDataStore.isYAxisExpanded ? 0.0 : minimum ?? 0.0,
      maximumLabels: DataStore().settingsDataStore.isYAxisExpanded ? null : maximum! - minimum!,
      measureFormatter: (double value) =>
          GradeManager().getGradeMapper(type: getClimbingType()).labelFromIndex(value.round()),
      yLabelFormatter: (double value) =>
          GradeManager().getGradeMapper(type: getClimbingType()).labelFromIndex(value.round()),
      animationDelay: animationDelay,
    );
  }

  @override
  Widget expanded() => getChart(extended: true);

  @override
  dynamic calculateData(BuildContext context, dynamic input) {
    if (input[0].length <= 1) return [];

    var sessionData = filterData(input[0]);
    var ascentsData = filterData(input[1]);
    if (sessionData.isEmpty || ascentsData.isEmpty) return [];

    Map<int, double> amountMap = {};
    Map<int, int> countMap = {};
    Map<int, int> best = {};
    Map<int, dynamic> sessionIDMapper = {};

    for (Map<String, dynamic> session in sessionData) {
      best[session[DBController.sessionId]] = 0;
      amountMap[session[DBController.sessionId]] = 0;
      countMap[session[DBController.sessionId]] = 0;
      sessionIDMapper[session[DBController.sessionId]] = session;
    }

    List<int> topIDs = [
      ...StyleController.getRedpointIds(),
      ...StyleController.getBoulderTopIds(),
    ];

    for (Map<String, dynamic> element in ascentsData) {
      // does not contain session
      if (!sessionIDMapper.containsKey(element[DBController.sessionId])) continue;

      // best grades
      // is redpoint style full filled?
      if (topIDs.contains(element[DBController.ascendStyleId])) {
        best[element[DBController.sessionId]] = max(
          element[DBController.ascendGradeId],
          best[element[DBController.sessionId]]!,
        );
      }

      // average grades
      amountMap[element[DBController.sessionId]] = amountMap[element[DBController.sessionId]]! +
          GradeManager().getGradeMapper(type: getClimbingType()).normalize(
                element[DBController.ascendGradeId],
              );
      countMap[element[DBController.sessionId]] = countMap[element[DBController.sessionId]]! + 1;
    }

    // remove empty entries
    sessionIDMapper.removeWhere((key, value) => countMap[key] == 0);

    List<DateTimeDataPoint> data1 = [];
    List<DateTimeDataPoint> data2 = [];

    for (int sessionID in sessionIDMapper.keys) {
      double? bestTop = best[sessionID] == 0
          ? null
          : GradeManager().getGradeMapper(type: getClimbingType()).idToIndex(best[sessionID]!).toDouble();

      double normalized = countMap[sessionID] == 0 ? 0.0 : amountMap[sessionID]! / (countMap[sessionID] as int);

      DateTime start = DateTime.parse(sessionIDMapper[sessionID][DBController.sessionTimeStart]);
      data1.add(DateTimeDataPoint(start, bestTop));
      data2.add(
        DateTimeDataPoint(
          start,
          GradeManager().getGradeMapper(type: getClimbingType()).indexFromNormalized(normalized).toDouble(),
        ),
      );
    }

    if (data1.isEmpty && data2.isEmpty) return [];

    return [data1, data2];
  }

  Future<List<Map<String, dynamic>>> fetchSessions() async {
    return await DBController().rawQuery(
      "SELECT ${DBController.sessionTimeStart}, ${DBController.sessionId}, ${DBController.locationOutdoor} "
      "FROM ${DBController.sessionsTable} "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE ${DBController.sessionStatus} != (?) "
      "ORDER BY ${DBController.sessionTimeStart} DESC",
      [Session.SESSION_ACTIVE_STATE],
    );
  }

  Future<List<Map<String, dynamic>>> fetchAscends() async {
    return DBController().rawQuery(
      "SELECT ${DBController.ascendGradeId}, ${DBController.ascendStyleId}, "
      "${DBController.routeTopRope}, ${DBController.sessionId} "
      "FROM ${DBController.ascendsTable} "
      "WHERE ${DBController.ascendType} == (?)",
      [getClimbingType().name],
    );
  }

  ClimbingType getClimbingType() => ClimbingType.SPORT_CLIMBING;

  @override
  String getKey() => "best_and_average_grades";

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.charts_best_and_average_grades_info_title,
          text: AppLocalizations.of(context)!.charts_best_and_average_grades_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }

  String getLegendOne(BuildContext context) => AppLocalizations.of(context)!.charts_best_and_average_grades_best_grade;

  String getLegendTwo(BuildContext context) => AppLocalizations.of(context)!.charts_average_grade_chart;
}
