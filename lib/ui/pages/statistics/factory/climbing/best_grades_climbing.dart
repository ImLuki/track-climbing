import 'dart:math';

import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/database/sql_query_builder.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/charts/data_points.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/syncfusion_charts/syncfusion_date_line_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BestGradesClimbingChart extends AbstractChart {
  @override
  String getName(BuildContext context) => AppLocalizations.of(context)!.chart_best_grades;

  @override
  ChartType getType() => ChartType.SPORT_CLIMBING;

  @override
  void loadData() {
    futureData = Future.wait([fetchData1(), fetchData2()]);
  }

  @override
  Widget chart(BuildContext context, dynamic preCalculatedData, extended, double animationDelay) {
    int maxIndex = GradeManager().getGradeMapper(type: getClimbingType()).maxIndexOfNormalized();
    double? maximum = max(
      (preCalculatedData[0] as List<DateTimeDataPoint>).map((e) => e.yPoint ?? 0.0).reduce(max),
      (preCalculatedData[1] as List<DateTimeDataPoint>).map((e) => e.yPoint ?? 0.0).reduce(max),
    );
    maximum += maximum.toInt().isEven ? 2 : 1;
    maximum = min(maximum, maxIndex.toDouble());
    maximum = maximum == 0.0 ? null : maximum;

    double? minimum = min(
      (preCalculatedData[0] as List<DateTimeDataPoint>).map((e) => e.yPoint ?? double.infinity).reduce(min),
      (preCalculatedData[1] as List<DateTimeDataPoint>).map((e) => e.yPoint ?? double.infinity).reduce(min),
    );
    minimum -= minimum.toInt().isEven ? 2 : 1;
    minimum = max(minimum, 0.0);
    minimum = minimum == double.infinity ? null : minimum;

    return SyncfusionDateTimeLineChart(
      preCalculatedData[0],
      extended,
      getLegendOne(context),
      legendTwo: getLegendTwo(context),
      data2: preCalculatedData[1],
      maxY: DataStore().settingsDataStore.isYAxisExpanded ? maxIndex.toDouble() : maximum,
      minY: DataStore().settingsDataStore.isYAxisExpanded ? 0.0 : minimum ?? 0.0,
      maximumLabels: DataStore().settingsDataStore.isYAxisExpanded ? null : maximum! - minimum!,
      measureFormatter: (double value) =>
          GradeManager().getGradeMapper(type: getClimbingType()).labelFromIndex(value.round()),
      yLabelFormatter: (double value) =>
          GradeManager().getGradeMapper(type: getClimbingType()).labelFromIndex(value.round()),
      animationDelay: animationDelay,
    );
  }

  @override
  Widget expanded() => getChart(extended: true);

  @override
  dynamic calculateData(BuildContext context, dynamic input) {
    List<Map<String, dynamic>> sessions = input[0];
    List<Map<String, dynamic>> data = input[1];
    if (data.isEmpty) return [];

    Map<int, int> toprope = {};
    Map<int, int> lead = {};
    Map<int, dynamic> dateIDMapper = {};
    for (Map<String, dynamic> session in sessions) {
      toprope[session[DBController.sessionId]] = 0;
      lead[session[DBController.sessionId]] = 0;
      dateIDMapper[session[DBController.sessionId]] = session;
    }

    for (Map<String, dynamic> element in data) {
      if (element[DBController.routeTopRope] == 1) {
        toprope[element[DBController.sessionId]] = max(
          element[DBController.ascendGradeId],
          toprope[element[DBController.sessionId]]!,
        );
      } else {
        lead[element[DBController.sessionId]] = max(
          element[DBController.ascendGradeId],
          lead[element[DBController.sessionId]]!,
        );
      }
    }

    List<DateTimeDataPoint> data1 = [];
    List<DateTimeDataPoint> data2 = [];
    for (int sessionID in lead.keys) {
      if (!((dateIDMapper[sessionID][DBController.locationOutdoor] == 1 && outdoor) ||
          (dateIDMapper[sessionID][DBController.locationOutdoor] == 0 && indoor))) {
        continue;
      }

      double? bestTop = toprope[sessionID] == 0
          ? null
          : GradeManager().getGradeMapper(type: getClimbingType()).idToIndex(toprope[sessionID]!).toDouble();
      double? bestLead = lead[sessionID] == 0
          ? null
          : GradeManager().getGradeMapper(type: getClimbingType()).idToIndex(lead[sessionID]!).toDouble();
      DateTime start = DateTime.parse(dateIDMapper[sessionID][DBController.sessionTimeStart]);
      data1.add(DateTimeDataPoint(start, bestLead));
      data2.add(DateTimeDataPoint(start, bestTop));
    }

    data1.removeWhere((element) => element.yPoint == null || element.yPoint! <= 0);
    data2.removeWhere((element) => element.yPoint == null || element.yPoint! <= 0);

    if (data1.isEmpty && data2.isEmpty) return [];

    // add one null-element to list if list is empty
    if (data1.isEmpty) {
      data1.add(DateTimeDataPoint(data2.last.xPoint, null));
    }
    if (data2.isEmpty) {
      data2.add(DateTimeDataPoint(data1.last.xPoint, null));
    }

    // both must have same last element (date time wise)
    if (data1.last.xPoint.isBefore(data2.last.xPoint)) {
      data1.add(DateTimeDataPoint(data2.last.xPoint, null));
    }
    if (data2.last.xPoint.isBefore(data1.last.xPoint)) {
      data2.add(DateTimeDataPoint(data1.last.xPoint, null));
    }

    return [data1, data2];
  }

  Future<List<Map<String, dynamic>>> fetchData1() async {
    return await DBController().rawQuery(
      "SELECT ${DBController.sessionTimeStart}, ${DBController.sessionId}, ${DBController.locationOutdoor} "
      "FROM ${DBController.sessionsTable} "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE ${DBController.sessionId} IN "
      "(SELECT ${DBController.sessionId} FROM ${DBController.ascendsTable} "
      "WHERE ${DBController.ascendType} == (?)) "
      "ORDER BY ${DBController.sessionTimeStart}",
      [getClimbingType().name],
    );
  }

  Future<List<Map<String, dynamic>>> fetchData2() async {
    return DBController().rawQuery(
      "SELECT ${DBController.ascendGradeId}, ${DBController.routeTopRope}, ${DBController.sessionId} "
      "FROM ${DBController.ascendsTable} "
      "WHERE ${SQLQueryBuilder.getRedpointCondition()} AND "
      "${DBController.ascendType} == (?)",
      [getClimbingType().name],
    );
  }

  ClimbingType getClimbingType() => ClimbingType.SPORT_CLIMBING;

  @override
  String getKey() => "best_grades";

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.chart_best_grades_info_title,
          text: AppLocalizations.of(context)!.chart_best_grades_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }

  String getLegendOne(BuildContext context) => AppLocalizations.of(context)!.style_lead;

  String getLegendTwo(BuildContext context) => AppLocalizations.of(context)!.style_toprope;
}
