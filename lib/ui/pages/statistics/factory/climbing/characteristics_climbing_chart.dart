import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/charts/data_points.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/enums/ascend_characteristics.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/spider_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CharacteristicsClimbingChart extends AbstractChart {
  @override
  calculateData(BuildContext context, input) {
    List<Map<String, dynamic>> data = filterData(input);
    if (data.isEmpty) return [];

    Map<AscendCharacteristics, double> dataPoints = {};
    for (AscendCharacteristics element in AscendCharacteristics.values) {
      dataPoints[element] = 0.0;
    }
    for (Map<String, dynamic> element in data) {
      List<AscendCharacteristics> characteristics =
          AscendCharacteristics.getValuesFromFlag(element[DBController.ascendCharacteristic] ?? 0);

      for (AscendCharacteristics characteristic in characteristics) {
        dataPoints[characteristic] = (dataPoints[characteristic] ?? 0.0) + 1.0;
      }
    }

    return dataPoints.keys.map((e) => DataPoint<AscendCharacteristics>(e, dataPoints[e] ?? 0.0)).toList();
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended, double animationDelay) {
    List<double> dataPoints = (preCalculatedData as List<DataPoint>).map((e) => e.yPoint).toList();
    List<IconData> icons = (preCalculatedData as List<DataPoint<AscendCharacteristics>>)
        .map((DataPoint<AscendCharacteristics> e) => e.xPoint.getIcon())
        .toList();
    List<String> labels =
        (preCalculatedData).map((DataPoint<AscendCharacteristics> e) => e.xPoint.toTranslatedString(context)).toList();
    return SpiderChart(
      data: dataPoints,
      expanded: extended,
      color: AppColors.accentColor,
      icons: icons,
      showLegend: true,
      labels: labels,
      animationDelay: animationDelay,
    );
  }

  @override
  Widget expanded() {
    return getChart(extended: true);
  }

  @override
  String getKey() => "characteristics_chart";

  @override
  String getName(BuildContext context) {
    return AppLocalizations.of(context)!.chart_characteristics;
  }

  @override
  ChartType getType() => ChartType.SPORT_CLIMBING;

  @override
  void loadData() {
    futureData = fetchData();
  }

  @override
  bool getStandardVisibility() => false;

  ClimbingType getClimbingType() => ClimbingType.SPORT_CLIMBING;

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.chart_characteristics_info_title,
          text: AppLocalizations.of(context)!.chart_characteristics_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.ascendCharacteristic}, ${DBController.routeTopRope}, ${DBController.locationOutdoor} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionsTable} using(${DBController.sessionId}) "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE ${DBController.ascendType} == (?) AND "
      "${DBController.ascendCharacteristic} IS NOT NULL AND "
      "${DBController.ascendCharacteristic} != 0",
      [getClimbingType().name],
    );
  }
}
