import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/grade_mapper/abstract_grade_mapper.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/syncfusion_charts/syncfusion_bar_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ClimbingGradesDistributionChart extends AbstractChart {
  ClimbingGradesDistributionChart() {
    assert(getMapping().length == getStyles().length);
  }

  @override
  List<List<dynamic>> calculateData(BuildContext context, input) {
    if (input.isEmpty) return [];

    AbstractGradeMapper mapper = GradeManager().getGradeMapper(type: getClimbingType());
    List<List<dynamic>> out = [mapper.getLabels(expandedSystem: false)];

    List<Map<String, int>> results = [];

    for (dynamic _ in getStyles()) {
      results.add({});
    }

    for (String grade in out[0]) {
      for (int i = 0; i < results.length; i++) {
        results[i][grade] = 0;
      }
    }

    for (Map<String, dynamic> element in input) {
      for (int i = 0; i < results.length; i++) {
        int index = mapper.idToIndex(
          element[DBController.ascendGradeId],
          expandedSystem: false,
        );
        String label = mapper.labelFromIndex(
          index,
          expandedSystem: false,
        );

        results[i][label] = results[i][label]! + element[getMapping()[i]] as int;
      }
    }

    for (Map value in results) {
      out.add(value.values.toList());
    }
    return out;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended, double animationDelay) {
    return SyncfusionBarChart(
      preCalculatedData,
      extended,
      getStyles().map((e) => e.toTranslatedString(context)).toList(),
      measureFormatter: (double value) => value.round().toString(),
      animationDelay: animationDelay,
    );
  }

  @override
  Widget expanded() {
    return Container(
      padding: const EdgeInsets.only(right: 10, bottom: 20),
      child: getChart(extended: true),
    );
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context)!.chart_grade_distribution;

  @override
  ChartType getType() => ChartType.SPORT_CLIMBING;

  @override
  void loadData() {
    futureData = fetchData();
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT "
      "${getClimbingStyleSQLString()}"
      "${DBController.ascendGradeId} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionsTable} using(${DBController.sessionId}) "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE ${DBController.ascendType} == (?) AND "
      "((${DBController.locationOutdoor} == (?) OR (?)) AND (?)) AND "
      "((${DBController.routeTopRope} == (?) OR (?)) AND (?)) "
      "GROUP BY ${DBController.ascendGradeId} ",
      [
        getClimbingType().name,
        outdoor ? 1 : 0,
        (outdoor && indoor) ? 1 : 0,
        (outdoor || indoor) ? 1 : 0,
        toprope ? 1 : 0,
        (toprope && lead) ? 1 : 0,
        (toprope || lead) ? 1 : 0,
      ],
    );
  }

  String getClimbingStyleSQLString() {
    String query = "";

    for (ClimbingStyle style in getStyles()) {
      query += "COUNT(CASE WHEN ${DBController.ascendStyleId} = ${style.id} THEN 1 END) as ${style.name}, ";
    }

    return query;
  }

  ClimbingType getClimbingType() => ClimbingType.SPORT_CLIMBING;

  @override
  String getKey() => "grade_distribution";

  /// must match sql query
  List<String> getMapping() {
    return getStyles().map((e) => e.name).toList();
  }

  List<ClimbingStyle> getStyles() {
    List<int> climbingStyles = StyleController.getClimbingStyleIdsByClimbingType(getClimbingType());

    // remove climbing style which should not be displayed
    // For example 1/2, 1/4, ...
    climbingStyles.remove(6); // THREE_QUARTERS
    climbingStyles.remove(7); // HALF
    climbingStyles.remove(8); // ONE_QUARTER

    return climbingStyles.map((e) => ClimbingStyle.getById(e)).toList();
  }

  @override
  void onCheckboxSelectionChanged() {
    loadData();
  }

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.chart_grade_distribution_info_title,
          text: AppLocalizations.of(context)!.chart_grade_distribution_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }
}
