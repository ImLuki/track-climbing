import 'dart:math';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/syncfusion_charts/syncfusion_stacked_bar_line_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LastYearClimbingChart extends AbstractChart {
  DateTime? from;
  DateTime? to;

  @override
  List<String> getTitleSelection(BuildContext context) {
    return [getName(context), ...AbstractChart.yearList];
  }

  @override
  List<List<double>> calculateData(BuildContext context, input) {
    // Filter data
    List<Map<String, dynamic>> filteredData = filterData(input);

    DateTime now = DateTime.now();
    DateTime fromUpdated = from ?? DateTime(now.year - 1, now.month + 1, 1, 0, 0, 0, 0, 0);
    DateTime toUpdated = to ?? now;

    List data = filteredData
        .where((element) =>
            DateTime.parse(element[DBController.sessionTimeStart]).isAfter(fromUpdated) &&
            DateTime.parse(element[DBController.sessionTimeStart]).isBefore(toUpdated))
        .toList();

    List<List<double>> lastYear = [
      List.filled(12, 0.0),
      List.filled(12, 0.0),
      List.filled(12, 0.0),
      List.filled(12, 0.0)
    ];
    for (Map<String, dynamic> element in data) {
      if (element[DBController.routeTopRope] == 1) {
        lastYear[3][DateTime.parse(element[DBController.sessionTimeStart]).month - 1] += 1;
        if (StyleController.getRedpointIds().contains(element[DBController.ascendStyleId])) {
          lastYear[1][DateTime.parse(element[DBController.sessionTimeStart]).month - 1] = max(
            lastYear[1][DateTime.parse(element[DBController.sessionTimeStart]).month - 1],
            GradeManager()
                .getGradeMapper(type: getClimbingType())
                .idToIndex(element[DBController.ascendGradeId])
                .toDouble(),
          );
        }
      } else {
        lastYear[2][DateTime.parse(element[DBController.sessionTimeStart]).month - 1] += 1;
        if (StyleController.getRedpointIds().contains(element[DBController.ascendStyleId])) {
          lastYear[0][DateTime.parse(element[DBController.sessionTimeStart]).month - 1] = max(
            lastYear[0][DateTime.parse(element[DBController.sessionTimeStart]).month - 1],
            GradeManager()
                .getGradeMapper(type: getClimbingType())
                .idToIndex(element[DBController.ascendGradeId])
                .toDouble(),
          );
        }
      }
    }
    return lastYear;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended, double animationDelay) {
    return SyncfusionStackedBarLineChart(
      data: preCalculatedData,
      expanded: extended,
      legends: getLegends(context),
      chartColors: [
        AppColors.chart_colors[1],
        AppColors.chart_colors[3],
        AppColors.chart_colors[0],
        AppColors.chart_colors[2],
      ],
      reorderMonths: from == null,
      barMeasureFormatter: (double value) => value.round().toString(),
      lineMeasureFormatter: (double value) =>
          GradeManager().getGradeMapper(type: getClimbingType()).labelFromIndex(value.round()),
      lineYLabelFormatter: (double value) =>
          GradeManager().getGradeMapper(type: getClimbingType()).labelFromIndex(value.round()),
      maxY: GradeManager().getGradeMapper(type: getClimbingType()).maxIndexOfNormalized().toDouble(),
      animationDelay: animationDelay,
    );
  }

  @override
  Widget expanded() {
    return Container(
      key: UniqueKey(),
      padding: const EdgeInsets.only(right: 10, bottom: 20),
      child: getChart(extended: true),
    );
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context)!.chart_past_months;

  @override
  ChartType getType() => ChartType.SPORT_CLIMBING;

  @override
  void loadData() {
    futureData = fetchData();
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.sessionTimeStart}, ${DBController.routeTopRope}, "
      "${DBController.ascendGradeId}, ${DBController.ascendStyleId}, ${DBController.locationOutdoor} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionsTable} using(${DBController.sessionId}) "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE ${DBController.ascendType} == (?)",
      [getClimbingType().name],
    );
  }

  @override
  void onChangeTitleSelection(BuildContext context, String value) {
    from = value == getName(context) ? null : DateTime(int.parse(value), 1, 1, 0);
    to = value == getName(context) ? null : DateTime(int.parse(value), 12, 31, 23, 59);
  }

  ClimbingType getClimbingType() => ClimbingType.SPORT_CLIMBING;

  @override
  String getKey() => "past_months";

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.chart_past_months_info_title,
          text: AppLocalizations.of(context)!.chart_past_months_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }

  List<String> getLegends(BuildContext context) {
    return [
      "#${AppLocalizations.of(context)!.style_lead}",
      "Best ${AppLocalizations.of(context)!.style_lead}",
      "#${AppLocalizations.of(context)!.style_toprope}",
      "Best ${AppLocalizations.of(context)!.style_toprope}",
    ];
  }
}
