import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/syncfusion_charts/syncfusion_donut_chart.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LeadTopropeChart extends AbstractChart {
  @override
  calculateData(BuildContext context, input) {
    if (input[0].isEmpty) return;
    Map<String, double> data = <String, double>{};
    input[0].forEach((key, value) => data[replaceString(context, key)] = value.toDouble());
    if (data.values.sum <= 0.0) return {};
    return data;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended, double animationDelay) {
    return SyncfusionDonutChart(
      expanded: extended,
      data: preCalculatedData,
      colors: [
        AppColors.chart_colors[1],
        const Color(0xffc2e1e3),
        AppColors.chart_colors[0],
        const Color(0xFFFFC5CB),
      ],
      animationDelay: animationDelay,
    );
  }

  @override
  Widget expanded() {
    return Container(
      padding: const EdgeInsets.fromLTRB(10, 10.0, 10.0, 40.0),
      child: getChart(extended: true),
    );
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context)!.chart_lead_toprope;

  @override
  ChartType getType() => ChartType.SPORT_CLIMBING;

  @override
  void loadData() {
    futureData = fetchData();
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT "
      "COUNT(CASE WHEN ${DBController.routeTopRope} == 0 AND ${DBController.locationOutdoor} == 0 THEN 1 END) as '#+', "
      "COUNT(CASE WHEN ${DBController.routeTopRope} == 0 AND ${DBController.locationOutdoor} THEN 1 END) as '#-', "
      "COUNT(CASE WHEN ${DBController.routeTopRope} AND ${DBController.locationOutdoor} == 0 THEN 1 END) as ':+', "
      "COUNT(CASE WHEN ${DBController.routeTopRope} AND ${DBController.locationOutdoor} THEN 1 END) as ':-' "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionsTable} using(${DBController.sessionId}) "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE ${DBController.ascendType} == (?)",
      [getClimbingType().name],
    );
  }

  ClimbingType getClimbingType() => ClimbingType.SPORT_CLIMBING;

  @override
  String getKey() => "lead_toprope";

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.chart_lead_toprope_info_title,
          text: AppLocalizations.of(context)!.chart_lead_toprope_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }

  String replaceString(BuildContext context, String text) {
    text = text.replaceAll("#", AppLocalizations.of(context)!.style_lead);
    text = text.replaceAll(":", AppLocalizations.of(context)!.style_top);
    text = text.replaceAll("-", " (${AppLocalizations.of(context)!.general_outdoor})");
    text = text.replaceAll("+", " (${AppLocalizations.of(context)!.general_indoor})");

    return text;
  }
}
