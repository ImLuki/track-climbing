import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/enums/steepness.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/syncfusion_charts/syncfusion_donut_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class SteepnessClimbingChart extends AbstractChart {
  @override
  dynamic calculateData(BuildContext context, input) {
    // Filter data
    List<Map<String, dynamic>> data = filterData(input);

    // return if no data is left
    if (data.isEmpty) return [];

    Map<Steepness, double> dataMap = {};

    for (Map<String, dynamic> dataPoint in data) {
      List<Steepness> content = Steepness.getValuesFromFlag(dataPoint[DBController.ascendSteepness] ?? 0);
      for (Steepness value in content) {
        dataMap[value] = (dataMap[value] ?? 0.0) + (1 / content.length);
      }
    }

    return dataMap.map((key, value) => MapEntry(key.name, value));
  }

  @override
  Widget chart(BuildContext context, dynamic preCalculatedData, bool extended, double animationDelay) {
    // remove zero entries
    preCalculatedData.removeWhere((key, value) => value == 0);

    List<Color> colors = AppColors.chart_colors.take(preCalculatedData.length).toList();

    return SyncfusionDonutChart(
      expanded: extended,
      data: preCalculatedData,
      animationDelay: animationDelay,
      tooltipFormat: "point.y%",
      tooltipEnabled: false,
      colors: colors,
      legendItemBuilder: (String name, dynamic series, ChartPoint<dynamic> point, int index) {
        Steepness steepness = Steepness.values.byName(name);

        return Padding(
          padding: const EdgeInsets.all(4.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.all(2.0),
                decoration: BoxDecoration(
                  color: colors[index],
                  shape: BoxShape.circle,
                ),
                child: Icon(steepness.getIcon(), color: Colors.white),
              ),
              const SizedBox(width: 8.0),
              Text(steepness.toTranslatedString(context)),
              const SizedBox(width: 20.0),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget expanded() {
    return Container(
      padding: const EdgeInsets.fromLTRB(10, 10.0, 10.0, 40.0),
      child: getChart(extended: true),
    );
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context)!.chart_steepness;

  @override
  ChartType getType() => ChartType.SPORT_CLIMBING;

  @override
  void loadData() {
    futureData = fetchData();
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.ascendSteepness}, ${DBController.routeTopRope}, ${DBController.locationOutdoor} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionsTable} using(${DBController.sessionId}) "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE ${DBController.ascendType} == (?) AND "
      "${DBController.ascendSteepness} IS NOT NULL AND "
      "${DBController.ascendSteepness} != 0",
      [getClimbingType().name],
    );
  }

  ClimbingType getClimbingType() => ClimbingType.SPORT_CLIMBING;

  @override
  String getKey() => "steepness_chart";

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.chart_steepness_info_title,
          text: AppLocalizations.of(context)!.chart_steepness_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }

  @override
  bool getStandardVisibility() => false;
}
