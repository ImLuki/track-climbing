import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/syncfusion_charts/syncfusion_donut_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ClimbingStyleChart extends AbstractChart {
  @override
  dynamic calculateData(BuildContext context, input) {
    // Filter data
    List<Map<String, dynamic>> data = filterData(input);

    // return if no data is left
    if (data.isEmpty) return [];

    Map<String, double> styles = {};
    StyleController.getClimbingStylesByClimbingType(getClimbingType()).forEach((ClimbingStyle element) {
      styles[element.toTranslatedString(context)] = 0;
    });

    for (Map<String, dynamic> route in data) {
      styles[ClimbingStyle.getById(route[DBController.ascendStyleId]).toTranslatedString(context)] =
          styles[ClimbingStyle.getById(route[DBController.ascendStyleId]).toTranslatedString(context)]! + 1;
    }

    return styles;
  }

  @override
  Widget chart(BuildContext context, dynamic preCalculatedData, bool extended, double animationDelay) {
    // remove zero entries
    preCalculatedData.removeWhere((key, value) => value == 0);

    return SyncfusionDonutChart(
      expanded: extended,
      data: preCalculatedData,
      animationDelay: animationDelay,
    );
  }

  @override
  Widget expanded() {
    return Container(
      padding: const EdgeInsets.fromLTRB(10, 10.0, 10.0, 40.0),
      child: getChart(extended: true),
    );
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context)!.chart_styles;

  @override
  ChartType getType() => ChartType.SPORT_CLIMBING;

  @override
  void loadData() {
    futureData = fetchData();
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.ascendStyleId}, ${DBController.routeTopRope}, ${DBController.locationOutdoor} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionsTable} using(${DBController.sessionId}) "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE ${DBController.ascendType} == (?)",
      [getClimbingType().name],
    );
  }

  ClimbingType getClimbingType() => ClimbingType.SPORT_CLIMBING;

  @override
  String getKey() => "styles";

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.chart_styles_info_title,
          text: AppLocalizations.of(context)!.chart_styles_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }
}
