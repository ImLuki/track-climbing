import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/enums/enums.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:climbing_track/ui/pages/statistics/abstract_charts/timeline_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/widgets/fade_in_animation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:timelines/timelines.dart';

class TimelineClimbingChart extends AbstractChart {
  @override
  String getName(BuildContext context) => AppLocalizations.of(context)!.charts_timeline;

  @override
  ChartType getType() => ChartType.SPORT_CLIMBING;

  @override
  void loadData() {
    futureData = fetchData();
  }

  @override
  Widget chart(BuildContext context, dynamic preCalculatedData, extended, double animationDelay) {
    return extended ? TimeLineChart(preCalculatedData) : _chartPreview(animationDelay);
  }

  @override
  Widget expanded() => getChart(extended: true);

  @override
  Widget getExpandedWidget() {
    return Container(
      color: Colors.white,
      child: expanded(),
    );
  }

  @override
  dynamic calculateData(BuildContext context, dynamic input) {
    List<Map<String, dynamic>> filteredData = filterData(input);
    if (filteredData.isEmpty) return [];
    Map<int, Map<Classes, Map<StyleType, Map<String, dynamic>?>>> data = {1: getEmptyData(), 0: getEmptyData()};
    List<List<Map<String, dynamic>>> allAscents = [];
    List<Map<String, dynamic>> currentAscends = [];

    int sessionID = filteredData.first[DBController.sessionId];

    for (Map<String, dynamic> ascend in filteredData) {
      // add elements if new session
      if (sessionID != ascend[DBController.sessionId]) {
        sessionID = ascend[DBController.sessionId];

        currentAscends.sort((a, b) => a[DBController.ascendStyleId].compareTo(b[DBController.ascendStyleId]));
        if (currentAscends.isNotEmpty) allAscents.add(currentAscends);
        currentAscends = [];
      }

      bool add = false;
      Classes type = getClassType(ascend);
      int indoorOutdoorKey = ascend[DBController.locationOutdoor];

      switch (StyleController.mapClimbingStyleToStyleType(ascend[DBController.ascendStyleId])) {
        case StyleType.ONSIGHT:
          if (_check(
            data[indoorOutdoorKey]?[type]?[StyleType.ONSIGHT],
            ascend[DBController.ascendGradeId],
            ascend[DBController.sessionTimeStart],
          )) {
            data[indoorOutdoorKey]![type]![StyleType.ONSIGHT] = ascend;
            add = true;
            continue FLASH;
          }
          break;
        FLASH:
        case StyleType.FLASH:
          if (_check(
            data[indoorOutdoorKey]?[type]?[StyleType.FLASH],
            ascend[DBController.ascendGradeId],
            ascend[DBController.sessionTimeStart],
          )) {
            data[indoorOutdoorKey]![type]![StyleType.FLASH] = ascend;
            add = true;
            continue REDPOINT;
          }
          break;
        REDPOINT:
        case StyleType.REDPOINT:
          if (_check(
            data[indoorOutdoorKey]?[type]?[StyleType.REDPOINT],
            ascend[DBController.ascendGradeId],
            ascend[DBController.sessionTimeStart],
          )) {
            data[indoorOutdoorKey]![type]![StyleType.REDPOINT] = ascend;
            add = true;
            continue TOP;
          }
          break;
        TOP:
        case StyleType.TOP:
          if (_check(
            data[indoorOutdoorKey]?[type]?[StyleType.TOP],
            ascend[DBController.ascendGradeId],
            ascend[DBController.sessionTimeStart],
          )) {
            data[indoorOutdoorKey]![type]![StyleType.TOP] = ascend;
            add = true;
            continue PROJECT;
          }
          break;
        PROJECT:
        case StyleType.PROJECT:
          if (_check(
            data[indoorOutdoorKey]?[type]?[StyleType.PROJECT],
            ascend[DBController.ascendGradeId],
            ascend[DBController.sessionTimeStart],
          )) {
            data[indoorOutdoorKey]![type]![StyleType.PROJECT] = ascend;
            add = true;
          }
          break;
        case StyleType.NONE:
          break;
      }

      if (add) {
        currentAscends.removeWhere((element) =>
            element[DBController.ascendStyleId] >= ascend[DBController.ascendStyleId] &&
            element[DBController.routeTopRope] == ascend[DBController.routeTopRope] &&
            element[DBController.ascendGradeId] <= ascend[DBController.ascendGradeId]);
        currentAscends.add(ascend);
      }
    }

    currentAscends.sort((a, b) => a[DBController.ascendStyleId].compareTo(b[DBController.ascendStyleId]));
    if (currentAscends.isNotEmpty) allAscents.add(currentAscends);
    return allAscents;
  }

  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT * FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionsTable} using (${DBController.sessionId}) "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE ${DBController.ascendType} == (?) "
      "ORDER BY ${DBController.ascendOrder}, ${DBController.sessionTimeStart}",
      [getClimbingType().name],
    );
  }

  ClimbingType getClimbingType() => ClimbingType.SPORT_CLIMBING;

  @override
  String getKey() => "timeline_sport_climbing";

  Map<Classes, Map<StyleType, Map<String, dynamic>?>> getEmptyData() {
    Map<Classes, Map<StyleType, Map<String, dynamic>?>> data = {};

    // creating data map
    for (Classes x in Classes.values) {
      Map<StyleType, Map<String, dynamic>?> styles = {};
      for (StyleType styleType in StyleType.values) {
        styles[styleType] = null;
      }
      data[x] = styles;
    }
    return data;
  }

  bool _check(Map<String, dynamic>? ascend, int gradeId, String dateString) {
    if (ascend == null) {
      return true;
    }
    if (ascend[DBController.ascendGradeId] < gradeId) {
      return true;
    }

    if (ascend[DBController.ascendGradeId] == gradeId) {
      if (DateTime.parse(ascend[DBController.sessionTimeStart]).isAfter(DateTime.parse(dateString))) {
        return true;
      }
    }

    return false;
  }

  Classes getClassType(Map<String, dynamic> ascend) {
    return ascend[DBController.routeTopRope] == 0 ? Classes.LEAD : Classes.TOPROPE;
  }

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.charts_timeline_info_title,
          text: AppLocalizations.of(context)!.charts_timeline_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }

  Widget _chartPreview(double animationDelay) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        //calculating item count using max width
        double contentWidth = 60;
        int itemCount = (constraints.maxWidth / contentWidth).truncate();
        return Stack(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FixedTimeline.tileBuilder(
                  direction: Axis.horizontal,
                  builder: TimelineTileBuilder.connected(
                    itemCount: itemCount,
                    contentsBuilder: (_, index) => SizedBox(width: contentWidth),
                    connectorBuilder: (_, index, ___) => const SolidLineConnector(color: Color(0xff989898)),
                    lastConnectorBuilder: (_) => const SolidLineConnector(color: Color(0xff989898)),
                    firstConnectorBuilder: (_) => const SolidLineConnector(color: Color(0xff989898)),
                    indicatorBuilder: (_, index) {
                      if (index == 2) {
                        return const DotIndicator(
                          size: 24.0,
                          color: Colors.green,
                          child: Icon(
                            FontAwesomeIcons.mountainSun,
                            color: Colors.white,
                            size: 12.0,
                          ),
                        );
                      } else {
                        return const DotIndicator(
                          size: 24.0,
                          color: Colors.orangeAccent,
                          child: Icon(
                            Icons.home,
                            color: Colors.white,
                            size: 18.0,
                          ),
                        );
                      }
                    },
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                FadeOut(
                  width: constraints.maxWidth,
                  height: constraints.maxHeight,
                  color: Theme.of(context).colorScheme.surfaceBright,
                  delay: Duration(milliseconds: animationDelay.toInt()),
                ),
              ],
            ),
          ],
        );
      },
    );
  }
}
