import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/best_and_average_grades_climbing.dart';

class BestAndAverageGradesDwsChart extends BestAndAverageGradesClimbingChart {
  @override
  ClimbingType getClimbingType() => ClimbingType.DEEP_WATER_SOLO;

  @override
  ChartType getType() => ChartType.DEEP_WATER_SOLO;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;
}
