import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/database/sql_query_builder.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/best_grades_boulder.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';

class BestGradesDwsChart extends BestGradesBoulderChart {
  @override
  ChartType getType() => ChartType.DEEP_WATER_SOLO;

  @override
  ClimbingType getClimbingType() => ClimbingType.DEEP_WATER_SOLO;

  @override
  Future<List<Map<String, dynamic>>> fetchData2() async {
    return DBController().rawQuery(
      "SELECT ${DBController.ascendGradeId}, ${DBController.sessionId} "
      "FROM ${DBController.ascendsTable} "
      "WHERE ${SQLQueryBuilder.getRedpointCondition()} AND "
      "${DBController.ascendType} == (?)",
      [getClimbingType().name],
    );
  }

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;
}
