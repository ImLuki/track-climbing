import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/climbing_grades_distribution_chart.dart';

class DwsGradesDistributionChart extends ClimbingGradesDistributionChart {
  @override
  ChartType getType() => ChartType.DEEP_WATER_SOLO;

  @override
  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT "
      "${getClimbingStyleSQLString()}"
      "${DBController.ascendGradeId} "
      "FROM ${DBController.ascendsTable} "
      "WHERE ${DBController.ascendType} == (?) "
      "GROUP BY ${DBController.ascendGradeId} ",
      [getClimbingType().name],
    );
  }

  @override
  ClimbingType getClimbingType() => ClimbingType.DEEP_WATER_SOLO;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;
}
