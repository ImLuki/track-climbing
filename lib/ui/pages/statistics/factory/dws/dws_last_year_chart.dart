import 'dart:math';

import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/boulder/boulder_last_year_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/widgets.dart';

class LastYearDwsChart extends LastYearBoulderChart {
  @override
  List<List<double>> calculateData(BuildContext context, input) {
    // Filter data
    List<Map<String, dynamic>> filteredData = filterData(input);

    DateTime now = DateTime.now();
    DateTime fromUpdated = from ?? DateTime(now.year - 1, now.month + 1, 1, 0, 0, 0, 0, 0);
    DateTime toUpdated = to ?? now;
    List data = filteredData
        .where((element) =>
            DateTime.parse(element[DBController.sessionTimeStart]).isAfter(fromUpdated) &&
            DateTime.parse(element[DBController.sessionTimeStart]).isBefore(toUpdated))
        .toList();

    List<List<double>> lastYear = [
      List.filled(12, 0.0),
      List.filled(12, 0.0),
    ];
    for (Map<String, dynamic> element in data) {
      lastYear[1][DateTime.parse(element[DBController.sessionTimeStart]).month - 1] += 1;
      if (StyleController.getRedpointIds().contains(element[DBController.ascendStyleId])) {
        lastYear[0][DateTime.parse(element[DBController.sessionTimeStart]).month - 1] = max(
          lastYear[0][DateTime.parse(element[DBController.sessionTimeStart]).month - 1],
          GradeManager()
              .getGradeMapper(type: getClimbingType())
              .idToIndex(element[DBController.ascendGradeId])
              .toDouble(),
        );
      }
    }
    return lastYear;
  }

  @override
  ChartType getType() => ChartType.DEEP_WATER_SOLO;

  @override
  ClimbingType getClimbingType() => ClimbingType.DEEP_WATER_SOLO;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;
}
