import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/location_handler.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:geolocator/geolocator.dart';
import 'package:webview_flutter/webview_flutter.dart';

class HeatMapChart extends AbstractChart {
  static const String COUNT_KEY = "count";
  static const String DISTANCE_KEY = "distance";
  static const String LOCATION_KEY = "location";
  static const String HTML_HEATMAP_EXTENDED = 'assets/html/heatmap_expanded.html';
  static const String HTML_HEATMAP = 'assets/html/heatmap.html';

  late WebViewController _controller;
  String _locationString = "";
  String bounds = "";
  bool _loading = true;

  @override
  calculateData(BuildContext context, input) {
    // Filter data
    List<Map<String, dynamic>> data = filterData(input);

    _loading = true;
    if (data.isEmpty) {
      _locationString = "[]";
      return [];
    }
    _locationString = "";

    List sortedInput = List.generate(data.length, (index) => Map<String, dynamic>.of(data[index]));
    sortedInput.sort((a, b) => a[COUNT_KEY].compareTo(b[COUNT_KEY]));

    Map<String, dynamic> max = sortedInput.last;
    int lastValue = sortedInput.first[COUNT_KEY];
    int counter = 1;
    for (Map<String, dynamic> location in sortedInput) {
      if (location[COUNT_KEY] != lastValue) {
        lastValue = location[COUNT_KEY];
        counter++;
      }
      location[COUNT_KEY] = counter;

      _locationString +=
          '[${location[DBController.latitude]}, ${location[DBController.longitude]}, ${location[COUNT_KEY]}],';
    }

    // remove last ','
    _locationString = _locationString.substring(0, _locationString.length - 1);

    // find two nearest points
    List<Map<String, dynamic>> distances = List.generate(
        sortedInput.length,
        (index) => {
              DISTANCE_KEY: Geolocator.distanceBetween(
                sortedInput[index][DBController.latitude],
                sortedInput[index][DBController.longitude],
                max[DBController.latitude],
                max[DBController.longitude],
              ),
              LOCATION_KEY: sortedInput[index]
            });

    distances.sort((a, b) => a[DISTANCE_KEY].compareTo(b[DISTANCE_KEY]));
    distances = distances.take(3).toList();

    bounds = "";
    for (Map<String, dynamic> bound in distances.map((e) => e[LOCATION_KEY])) {
      bounds += "[${bound[DBController.latitude]}, ${bound[DBController.longitude]}],";
    }
    // remove last ','
    bounds = bounds.substring(0, bounds.length - 1);

    return sortedInput;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended, double animationDelay) {
    _controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {},
          onPageStarted: (String url) {},
          onPageFinished: (String url) {},
          onWebResourceError: (WebResourceError error) {},
          onNavigationRequest: (NavigationRequest request) {
            if (!request.url.startsWith("data:text/html")) {
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
        ),
      );

    _loadHtmlFromAssets(extended ? HTML_HEATMAP_EXTENDED : HTML_HEATMAP, context);
    return IgnorePointer(
      ignoring: !extended,
      child: Stack(
        children: [
          if (_loading && extended) const Center(child: CircularProgressIndicator()),
          WebViewWidget(
            controller: _controller,
          ),
        ],
      ),
    );
  }

  @override
  Widget getExpandedWidget() {
    return expanded();
  }

  @override
  Widget expanded() {
    return getChart(extended: true, checkInternet: true);
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context)!.charts_heatmap;

  @override
  ChartType getType() => ChartType.ALL;

  @override
  void loadData() {
    futureData = _fetchData();
  }

  Future<List<Map<String, dynamic>>> _fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.locationName}, COUNT(${DBController.locationName}) as $COUNT_KEY, "
      "${DBController.longitude}, ${DBController.latitude}, ${DBController.locationOutdoor} "
      "FROM ${DBController.sessionsTable} "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE ${DBController.longitude} IS NOT NULL AND "
      "${DBController.latitude} IS NOT NULL "
      "GROUP BY ${DBController.locationName}",
    );
  }

  @override
  String getKey() => "heatmap";

  @override
  Widget createChartContainer(BuildContext context, double animationDelay) {
    return InkWell(
      onTap: () => showChart(context),
      focusColor: Colors.transparent,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        margin: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
        padding: const EdgeInsets.all(8.0),
        width: MediaQuery.of(context).size.width,
        height: 100,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black12, width: 2), // added
          borderRadius: BorderRadius.circular(25.0),
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: getChart(addPaddingIfNoData: true, checkInternet: true, animationDelay: animationDelay),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12.0),
                    topRight: Radius.circular(12.0),
                  ),
                ),
                padding: const EdgeInsets.only(top: 2.0, left: 8.0, right: 8.0),
                child: Text(
                  getTitle(context),
                  maxLines: 1,
                  overflow: TextOverflow.visible,
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.charts_heatmap_info_title,
          text: AppLocalizations.of(context)!.charts_heatmap_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }

  @override
  Widget getFAB(BuildContext context) {
    return StatefulBuilder(
      builder: (BuildContext context, StateSetter setState) => FloatingActionButton(
        onPressed: () async {
          _loading = true;
          setState(() {});
          try {
            Position position = await LocationHandler().determinePosition();
            bounds = "[${position.latitude}, ${position.longitude}]";
            if (context.mounted) {
              _loadHtmlFromAssets(HTML_HEATMAP_EXTENDED, context);
            }
          } catch (_) {
            if (context.mounted) {
              ScaffoldMessenger.of(context)
                ..removeCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    behavior: SnackBarBehavior.floating,
                    content: Text(AppLocalizations.of(context)!.current_position_not_determined),
                  ),
                );
            }
          }
          _loading = false;
          setState(() {});
        },
        child: _loading ? const CircularProgressIndicator() : const Icon(Icons.my_location),
      ),
    );
  }

  _loadHtmlFromAssets(String htmlPath, BuildContext context) async {
    _loading = true;
    String fileText = await rootBundle.loadString(htmlPath);
    fileText = fileText.replaceAll("&&", _locationString);
    fileText = fileText.replaceAll("&bound", bounds);
    await _controller.loadHtmlString(fileText);
    _loading = false;
  }

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool outdoorCheckBoxActivated() => true;

  @override
  bool indoorCheckBoxActivated() => true;
}
