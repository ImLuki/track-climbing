import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/charts/data_points.dart';
import 'package:climbing_track/backend/model/enums/height_measurement.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/backend/overview_data_loader.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/syncfusion_charts/syncfusion_date_line_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HeightChart extends AbstractChart {
  @override
  calculateData(BuildContext context, input) {
    List<DateTimeDataPoint> data = [];
    for (Map<String, dynamic> element in input.reversed) {
      DateTime start = DateTime.parse(element[DBController.sessionTimeStart]);
      data.add(
        DateTimeDataPoint(
          start,
          Util.heightFromMeters(element[OverviewDataLoader.TOTAL_HEIGHT_KEY]).toDouble(),
        ),
      );
    }
    return data;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended, double animationDelay) {
    return SyncfusionDateTimeLineChart(
      preCalculatedData,
      extended,
      DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET
          ? AppLocalizations.of(context)!.general_feet(1)
          : AppLocalizations.of(context)!.general_meter(1),
      yLabelFormatter: (double value) => (value % 1) == 0 ? value.toInt().toString() : value.toStringAsFixed(1),
      measureFormatter: (double value) => DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET
          ? "$value ${AppLocalizations.of(context)!.general_feet(value)}"
          : "$value ${AppLocalizations.of(context)!.general_meter(value)}",
      useLegendNameForTrackball: false,
      fillArea: true,
      chartColors: [AppColors.chart_colors[0]],
      areaOpacity: 0.2,
      animationDelay: animationDelay,
    );
  }

  @override
  Widget expanded() {
    return getChart(extended: true);
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context)!.chart_climbed_meters;

  @override
  ChartType getType() => ChartType.ALL;

  @override
  void loadData() {
    futureData = _fetchData();
  }

  Future<List<Map<String, dynamic>>> _fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.sessionTimeStart}, "
      "SUM(${DBController.ascendHeight}) as '${OverviewDataLoader.TOTAL_HEIGHT_KEY}' "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionsTable} using(${DBController.sessionId}) "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE ${DBController.sessionStatus} != (?) AND "
      "((${DBController.locationOutdoor} == (?) OR (?)) AND (?)) "
      "GROUP BY ${DBController.sessionId} "
      "ORDER BY ${DBController.sessionTimeStart} DESC",
      [
        Session.SESSION_ACTIVE_STATE,
        outdoor ? 1 : 0,
        (outdoor && indoor) ? 1 : 0,
        (outdoor || indoor) ? 1 : 0,
      ],
    );
  }

  @override
  String getKey() => "climbed_meters";

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  void onCheckboxSelectionChanged() {
    loadData();
  }

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.chart_climbed_meters_info_title,
          text: AppLocalizations.of(context)!.chart_climbed_meters_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }
}
