import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/syncfusion_charts/syncfusion_donut_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LocationsChart extends AbstractChart {
  // should be AppColors.chart_colors.length
  static const int LOCATION_SEPARATE_LIMIT = 8;

  @override
  calculateData(BuildContext context, input) {
    // Filter data
    List<Map<String, dynamic>> filteredData = filterData(input);

    Map<String, double> locations = {};

    for (Map<String, dynamic> session in filteredData) {
      locations[session[DBController.locationName]] = (locations[session[DBController.locationName]] ?? 0) + 1;
    }

    return _stripData(context, locations, filteredData.length);
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended, double animationDelay) {
    return SyncfusionDonutChart(
      expanded: extended,
      data: preCalculatedData,
      animationDelay: animationDelay,
      colors:
          preCalculatedData.keys.any((val) => (val as String).contains(AppLocalizations.of(context)!.general_others))
              ? AppColors.chart_colors.take(preCalculatedData.length - 1).toList() + [Colors.grey]
              : AppColors.chart_colors.take(preCalculatedData.length).toList(),
    );
  }

  @override
  Widget expanded() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 10.0, 10.0, 40.0),
      child: getChart(extended: true),
    );
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context)!.chart_locations;

  @override
  ChartType getType() => ChartType.ALL;

  @override
  void loadData() {
    futureData = _fetchData();
  }

  Future<List<Map<String, dynamic>>> _fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.locationName}, ${DBController.locationOutdoor} "
      "FROM ${DBController.sessionsTable} "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE ${DBController.sessionStatus} != (?) ",
      [Session.SESSION_ACTIVE_STATE],
    );
  }

  Map<String, double> _stripData(BuildContext context, Map<String, double> map, int totalEntries) {
    double others = 0;
    List<String> remove = [];
    map.forEach((key, value) {
      if (value / totalEntries < 0.015) {
        others += value;
        remove.add(key);
      }
    });
    for (var element in remove) {
      map.remove(element);
    }

    List<MapEntry<String, double>> res = map.entries.toList();
    res.sort((a, b) => b.value.compareTo(a.value));

    if (map.length > LOCATION_SEPARATE_LIMIT) {
      while (map.length > LOCATION_SEPARATE_LIMIT) {
        map.removeWhere((String key, double val) => key == res.last.key);
        others += res.removeLast().value;
      }
    }

    Map<String, double> out = Map.fromEntries(
      res.toSet().map((e) => MapEntry("${e.key} (${e.value.toInt()})", e.value)).toList()
        ..sort((e1, e2) => e2.value.compareTo(e1.value)),
    );

    if (others > 0) {
      out["${AppLocalizations.of(context)!.general_others} (${others.toInt()})"] = others;
    }

    return out;
  }

  @override
  String getKey() => "locations";

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.chart_locations_info_title,
          text: AppLocalizations.of(context)!.chart_locations_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }
}
