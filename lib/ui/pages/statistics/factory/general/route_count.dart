import 'dart:math';

import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/charts/statistic_charts.dart';
import 'package:climbing_track/backend/model/charts/data_points.dart';
import 'package:climbing_track/backend/model/enums/enums.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/syncfusion_charts/syncfusion_date_bar_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class RouteCountChart extends AbstractChart {
  @override
  calculateData(BuildContext context, input) {
    if (input.isEmpty) return [];

    List<List<DateTimeDataPoint>> lastSessions = [];
    for (String element in Classes.values.map((e) => e.name)) {
      lastSessions.add(
        List.generate(
          input.length,
          (index) => DateTimeDataPoint(
            DateTime.parse(input[input.length - index - 1][DBController.sessionTimeStart]),
            input[input.length - index - 1][element]?.toDouble(),
          ),
        ),
      );
    }

    return lastSessions;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended, double animationDelay) {
    // removing empty entries
    List<int> indices = [];

    for (List<DateTimeDataPoint> data in preCalculatedData) {
      if (!data.any((element) => (element.yPoint ?? 0.0) > 0)) {
        indices.add(preCalculatedData.indexOf(data));
      }
    }

    indices.sort((a, b) => b.compareTo(a));

    List<String> titles = _getTitles(context);
    List<Color> colors = Classes.values.map((e) => e.getColor()).toList();

    for (int index in indices) {
      colors.removeAt(index);
      preCalculatedData.removeAt(index);
      titles.removeAt(index);
    }

    assert(colors.length == preCalculatedData.length);

    return SyncfusionDateTimeBarChart(
      extended
          ? preCalculatedData
          : (preCalculatedData as List<List<DateTimeDataPoint>>)
              .map((e) => e.getRange(max(0, e.length - StatisticCharts.MAX_SESSION_AMOUNT), e.length).toList())
              .toList(),
      extended,
      titles,
      chartColors: colors,
      measureFormatter: (double value) => value.round().toString(),
      yLabelFormatter: (double value) => (value % 1) == 0 ? value.toInt().toString() : "",
      animationDelay: animationDelay,
    );
  }

  @override
  Widget expanded() {
    return getChart(extended: true);
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context)!.chart_route_count;

  @override
  ChartType getType() => ChartType.ALL;

  @override
  void loadData() {
    futureData = _fetchData();
  }

  Future<List<Map<String, dynamic>>> _fetchData() async {
    String sqlQuery = "SELECT ${DBController.sessionTimeStart}, "
        "COUNT(CASE WHEN ${DBController.routeTopRope} AND ${DBController.ascendType} == (?) THEN 1 END) as ${Classes.TOPROPE.name}, "
        "COUNT(CASE WHEN ${DBController.routeTopRope} == 0 AND ${DBController.ascendType} == (?) THEN 1 END) as ${Classes.LEAD.name}";

    for (Classes cls in Classes.values.getRange(2, Classes.values.length)) {
      sqlQuery += ", COUNT(CASE WHEN ${DBController.ascendType} == (?) THEN 1 END) as ${cls.name}";
    }

    sqlQuery += " FROM ${DBController.ascendsTable} "
        "JOIN ${DBController.sessionsTable} using(${DBController.sessionId}) "
        "JOIN ${DBController.locationsTable} using(${DBController.locationId}) "
        "WHERE (${DBController.locationOutdoor} == (?) OR (?)) AND (?) "
        "GROUP BY ${DBController.sessionId} "
        "ORDER BY ${DBController.sessionTimeStart} DESC";

    return DBController().rawQuery(
      sqlQuery,
      [
        ...Classes.values.map((e) => e.getClimbingType().name),
        outdoor ? 1 : 0,
        (outdoor && indoor) ? 1 : 0,
        (outdoor || indoor) ? 1 : 0,
      ],
    );
  }

  @override
  String getKey() => "route_count";

  List<String> _getTitles(BuildContext context) {
    return Classes.values.map((e) => e.toTranslatedString(context)).toList();
  }

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  void onCheckboxSelectionChanged() {
    loadData();
  }

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.chart_route_count_info_title,
          text: AppLocalizations.of(context)!.chart_route_count_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }
}
