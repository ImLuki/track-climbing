import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/charts/data_points.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/syncfusion_charts/syncfusion_date_line_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SessionTimeChart extends AbstractChart {
  @override
  calculateData(BuildContext context, input) {
    // Filter data
    List<Map<String, dynamic>> filteredData = filterData(input);

    List<DateTimeDataPoint> dataPints = [];
    for (Map<String, dynamic> element in filteredData) {
      DateTime start = DateTime.parse(element[DBController.sessionTimeStart]);
      DateTime end = DateTime.parse(element[DBController.sessionTimeEnd]);
      dataPints.add(DateTimeDataPoint(start, end.difference(start).inMinutes.abs().toDouble()));
    }
    return dataPints;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended, double animationDelay) {
    return SyncfusionDateTimeLineChart(
      preCalculatedData,
      extended,
      AppLocalizations.of(context)!.general_minutes,
      yLabelFormatter: (double value) => value.round().toString(),
      measureFormatter: (double value) => "${value.round()} ${AppLocalizations.of(context)!.general_minutes}",
      chartColors: [AppColors.chart_colors[0]],
      useLegendNameForTrackball: false,
      fillArea: true,
      areaOpacity: 0.2,
      animationDelay: animationDelay,
    );
  }

  @override
  Widget expanded() {
    return getChart(extended: true);
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context)!.chart_session_time;

  @override
  ChartType getType() => ChartType.ALL;

  @override
  void loadData() {
    futureData = _fetchData();
  }

  Future<List<Map<String, dynamic>>> _fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.sessionTimeStart}, ${DBController.sessionTimeEnd}, ${DBController.locationOutdoor} "
      "FROM ${DBController.sessionsTable} "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE ${DBController.sessionStatus} != (?) "
      "ORDER BY ${DBController.sessionTimeStart}",
      [Session.SESSION_ACTIVE_STATE],
    );
  }

  @override
  String getKey() => "session_time";

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.chart_session_time_info_title,
          text: AppLocalizations.of(context)!.chart_session_time_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }
}
