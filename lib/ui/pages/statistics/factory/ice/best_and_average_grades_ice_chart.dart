import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/best_and_average_grades_climbing.dart';

class BestAndAverageGradesIceChart extends BestAndAverageGradesClimbingChart {
  @override
  ClimbingType getClimbingType() => ClimbingType.ICE_CLIMBING;

  @override
  ChartType getType() => ChartType.ICE_CLIMBING;
}
