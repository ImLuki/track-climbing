import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/best_grades_climbing.dart';

class BestGradesIceChart extends BestGradesClimbingChart {
  @override
  ChartType getType() => ChartType.ICE_CLIMBING;

  @override
  ClimbingType getClimbingType() => ClimbingType.ICE_CLIMBING;
}
