import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/timeline_climbing.dart';

class TimelineIceChart extends TimelineClimbingChart {
  @override
  ChartType getType() => ChartType.ICE_CLIMBING;

  @override
  ClimbingType getClimbingType() => ClimbingType.ICE_CLIMBING;

  @override
  String getKey() => "timeline_sport_ice";
}
