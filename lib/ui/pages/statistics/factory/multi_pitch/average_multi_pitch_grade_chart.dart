import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/average_climbing_grade_chart.dart';

class AverageMultiPitchGradeChart extends AverageClimbingGradeChart {
  @override
  ChartType getType() => ChartType.MULTI_PITCH;

  @override
  ClimbingType getClimbingType() => ClimbingType.MULTI_PITCH;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;

  @override
  List<Map<String, dynamic>> filterData(List<Map<String, dynamic>> ascends) {
    return ascends;
  }
}
