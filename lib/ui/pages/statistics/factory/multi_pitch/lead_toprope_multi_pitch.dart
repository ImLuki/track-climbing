import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/lead_toprope_chart.dart';
import 'package:climbing_track/ui/pages/statistics/syncfusion_charts/syncfusion_donut_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LeadTopropeMultiPitchChart extends LeadTopropeChart {
  @override
  ChartType getType() => ChartType.MULTI_PITCH;

  @override
  ClimbingType getClimbingType() => ClimbingType.MULTI_PITCH;

  @override
  String replaceString(BuildContext context, String text) {
    text = text.replaceAll("#", AppLocalizations.of(context)!.style_leading);
    text = text.replaceAll(":", AppLocalizations.of(context)!.style_following);
    text = text.replaceAll("+", AppLocalizations.of(context)!.style_alternate_leading);

    return text;
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context)!.chart_leading_following;

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.chart_leading_following_info_title,
          text: AppLocalizations.of(context)!.chart_leading_following_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;

  @override
  Future<List<Map<String, dynamic>>> fetchData() async {
    return DBController().rawQuery(
      "SELECT "
      "COUNT(CASE WHEN ${DBController.routeTopRope} == 0 THEN 1 END) as '#', "
      "COUNT(CASE WHEN ${DBController.routeTopRope} == 1 THEN 1 END) as ':', "
      "COUNT(CASE WHEN ${DBController.routeTopRope} == 2 THEN 1 END) as '+' "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionsTable} using(${DBController.sessionId}) "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE ${DBController.ascendType} == (?)",
      [getClimbingType().name],
    );
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended, double animationDelay) {
    return SyncfusionDonutChart(
      expanded: extended,
      data: preCalculatedData,
      colors: [
        AppColors.chart_colors.elementAt(1),
        AppColors.chart_colors.elementAt(0),
        AppColors.chart_colors.elementAt(4),
      ],
      animationDelay: animationDelay,
    );
  }

  @override
  List<Map<String, dynamic>> filterData(List<Map<String, dynamic>> ascends) {
    return ascends;
  }
}
