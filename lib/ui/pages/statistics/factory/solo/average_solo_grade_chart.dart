import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/average_climbing_grade_chart.dart';

class AverageFreeSoloGradeChart extends AverageClimbingGradeChart {
  @override
  ChartType getType() => ChartType.FREE_SOLO;

  @override
  ClimbingType getClimbingType() => ClimbingType.FREE_SOLO;

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;
}
