import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/enums/enums.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/timeline_climbing.dart';

class TimelineSoloChart extends TimelineClimbingChart {
  @override
  ChartType getType() => ChartType.FREE_SOLO;

  @override
  ClimbingType getClimbingType() => ClimbingType.FREE_SOLO;

  @override
  String getKey() => "timeline_sport_solo";

  @override
  Classes getClassType(Map<String, dynamic> ascend) => Classes.FREE;



  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;
}
