import 'dart:math';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/syncfusion_charts/syncfusion_stacked_bar_line_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LastYearSpeedChart extends AbstractChart {
  DateTime? from;
  DateTime? to;

  @override
  List<String> getTitleSelection(BuildContext context) {
    return [AppLocalizations.of(context)!.chart_past_months, ...AbstractChart.yearList];
  }

  @override
  List<List<double>> calculateData(BuildContext context, input) {
    DateTime now = DateTime.now();
    DateTime fromUpdated = from ?? DateTime(now.year - 1, now.month + 1, 1, 0, 0, 0, 0, 0);
    DateTime toUpdated = to ?? now;

    List data = input
        .where((element) =>
            DateTime.parse(element[DBController.sessionTimeStart]).isAfter(fromUpdated) &&
            DateTime.parse(element[DBController.sessionTimeStart]).isBefore(toUpdated))
        .toList();

    List<List<double>> lastYear = [
      List.filled(12, 0.0),
      List.filled(12, 0.0),
    ];
    for (Map<String, dynamic> element in data) {
      DateTime dateTime = DateTime.parse(element[DBController.sessionTimeStart]);
      lastYear[1][dateTime.month - 1] += element["tries"].toDouble();

      if (lastYear[0][dateTime.month - 1] == 0.0) {
        lastYear[0][dateTime.month - 1] = element["time"] / 1000;
      } else {
        lastYear[0][dateTime.month - 1] = min(lastYear[0][dateTime.month - 1], element["time"] / 1000);
      }
    }
    return lastYear;
  }

  @override
  Widget chart(BuildContext context, preCalculatedData, bool extended, double animationDelay) {
    return SyncfusionStackedBarLineChart(
      data: preCalculatedData,
      expanded: extended,
      legends: [
        AppLocalizations.of(context)!.charts_tries,
        AppLocalizations.of(context)!.charts_best_time,
      ],
      chartColors: [
        AppColors.chart_colors[1],
        AppColors.chart_colors[3],
      ],
      maxY: GradeManager().getGradeMapper(type: getClimbingType()).maxIndexOfNormalized().toDouble(),
      reorderMonths: from == null,
      barMeasureFormatter: (double value) => value.round().toString(),
      lineMeasureFormatter: (double value) => "${value.toString()} s",
      lineYLabelFormatter: (double value) => value.toString(),
      animationDelay: animationDelay,
    );
  }

  @override
  Widget expanded() {
    return Container(
      key: UniqueKey(),
      padding: const EdgeInsets.only(right: 10, bottom: 20),
      child: getChart(extended: true),
    );
  }

  @override
  String getName(BuildContext context) => AppLocalizations.of(context)!.chart_past_months;

  @override
  String getTitle(BuildContext context) {
    return Util.replaceStringWithBlankSpaces(
      "${getName(context)} (${AppLocalizations.of(context)!.chart_page_comp_wall})",
    );
  }

  @override
  ChartType getType() => ChartType.SPEED_CLIMBING;

  @override
  void loadData() {
    futureData = _fetchData();
  }

  Future<List<Map<String, dynamic>>> _fetchData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.sessionTimeStart}, "
      "MIN(${DBController.speedTime}) as time, "
      "COUNT(${DBController.ascendId}) as tries "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionsTable} using(${DBController.sessionId}) "
      "WHERE ${DBController.ascendType} == (?) AND "
      "${DBController.sessionStatus} != (?) AND "
      "${DBController.speedType} == (?) "
      "GROUP BY ${DBController.sessionId}",
      [getClimbingType().name, Session.SESSION_ACTIVE_STATE, 0],
    );
  }

  @override
  void onChangeTitleSelection(BuildContext context, String value) {
    from = value == getName(context) ? null : DateTime(int.parse(value), 1, 1, 0);
    to = value == getName(context) ? null : DateTime(int.parse(value), 12, 31, 23, 59);
  }

  ClimbingType getClimbingType() => ClimbingType.SPEED_CLIMBING;

  @override
  String getKey() => "past_moths_speed";

  @override
  bool topropeCheckBoxActivated() => false;

  @override
  bool leadCheckBoxActivated() => false;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;

  @override
  List<Widget> getAppBarActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => showInfoDialog(
          context,
          title: AppLocalizations.of(context)!.chart_past_months_info_title,
          text: AppLocalizations.of(context)!.chart_past_months_speed_info_content,
        ),
        icon: const Icon(Icons.info_outline_rounded),
      ),
    ];
  }
}
