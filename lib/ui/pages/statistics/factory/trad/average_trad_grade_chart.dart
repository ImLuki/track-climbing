import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/average_climbing_grade_chart.dart';

class AverageTradGradeChart extends AverageClimbingGradeChart {
  @override
  ChartType getType() => ChartType.TRAD;

  @override
  ClimbingType getClimbingType() => ClimbingType.TRAD_CLIMBING;

  @override
  bool topropeCheckBoxActivated() => true;

  @override
  bool leadCheckBoxActivated() => true;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;
}
