import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/climbing/best_and_average_grades_climbing.dart';

class BestAndAverageGradesTradChart extends BestAndAverageGradesClimbingChart {
  @override
  ClimbingType getClimbingType() => ClimbingType.TRAD_CLIMBING;

  @override
  ChartType getType() => ChartType.TRAD;

  @override
  bool topropeCheckBoxActivated() => true;

  @override
  bool leadCheckBoxActivated() => true;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;
}
