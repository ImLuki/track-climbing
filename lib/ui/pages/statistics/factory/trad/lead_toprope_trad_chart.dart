import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/ui/pages/statistics/factory/abstract_chart.dart';
import 'package:climbing_track/ui/pages/statistics/factory/ice/lead_toprope_ice_chart.dart';

class LeadTopropeTradChart extends LeadTopropeIceChart {
  @override
  ChartType getType() => ChartType.TRAD;

  @override
  ClimbingType getClimbingType() => ClimbingType.TRAD_CLIMBING;

  @override
  bool outdoorCheckBoxActivated() => false;

  @override
  bool indoorCheckBoxActivated() => false;
}
