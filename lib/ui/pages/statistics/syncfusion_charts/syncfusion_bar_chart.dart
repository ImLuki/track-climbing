import 'dart:math';
import 'dart:ui';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/charts/data_points.dart';
import 'package:climbing_track/ui/pages/statistics/chart_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class SyncfusionBarChart extends StatefulWidget {
  final List<List<dynamic>> data;
  final bool expanded;
  final List<String> legends;
  final double? maxY;
  final String Function(double value) measureFormatter;
  final String Function(double value)? yLabelFormatter;
  final List<Color> chartColors;
  final bool useLegendNameForTrackball;
  final bool fillArea;
  final LinearGradient? linearGradient;
  final double areaOpacity;
  final double animationDelay;

  const SyncfusionBarChart(
    this.data,
    this.expanded,
    this.legends, {
    super.key,
    required this.measureFormatter,
    required this.animationDelay,
    this.yLabelFormatter,
    this.maxY,
    this.chartColors = AppColors.chart_colors,
    this.useLegendNameForTrackball = true,
    this.fillArea = false,
    this.linearGradient,
    this.areaOpacity = 0.2,
  });

  @override
  _SyncfusionBarChartState createState() => _SyncfusionBarChartState();
}

class _SyncfusionBarChartState extends State<SyncfusionBarChart> {
  static const int STANDARD_LENGTH = 10;
  late List<List<DoubleDataPoint>> dataPoints;
  int maxHeight = 0;
  int indexOfHighestBar = 0;

  @override
  void initState() {
    super.initState();
    _calculateDataPoints();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        _createChart(),
        //This container makes graph touchable, so it can be expanded
        if (!widget.expanded) Container(color: Colors.transparent)
      ],
    );
  }

  Widget _createChart() {
    return SfCartesianChart(
      margin: widget.expanded ? const EdgeInsets.fromLTRB(0, 0, 10, 10) : EdgeInsets.zero,
      plotAreaBorderWidth: 0,
      primaryXAxis: _primaryXAxis(),
      primaryYAxis: _primaryYAxis(),
      legend: _legend(isVisible: widget.expanded),
      series: _series(),
      zoomPanBehavior: widget.expanded ? _zoomPanBehavior() : null,
      trackballBehavior: _trackballBehavior(),
    );
  }

  ChartAxis _primaryXAxis() {
    return CategoryAxis(
      isVisible: widget.expanded,
      initialZoomFactor: clampDouble(1.0 / widget.data.first.length * STANDARD_LENGTH, 0.0, 1.0),
      initialZoomPosition: clampDouble(
        1.0 / widget.data.first.length * (indexOfHighestBar - STANDARD_LENGTH / 2),
        0.0,
        1.0,
      ),
      majorGridLines: const MajorGridLines(width: 0),
      labelRotation: 90,
      interval: 1,
    );
  }

  ChartAxis _primaryYAxis() {
    return NumericAxis(
      rangePadding: widget.expanded ? ChartRangePadding.normal : ChartRangePadding.none,
      isVisible: widget.expanded,
      minimum: widget.expanded ? 0.0 : null,
      maximum: widget.expanded ? widget.maxY : null,
      axisLabelFormatter: (AxisLabelRenderDetails details) => ChartAxisLabel(
        widget.yLabelFormatter == null
            ? widget.measureFormatter(details.value.toDouble())
            : widget.yLabelFormatter!(details.value.toDouble()),
        null,
      ),
    );
  }

  dynamic _series() {
    return List.generate(
      widget.data.length - 1,
      (index) => StackedColumnSeries<DoubleDataPoint, String>(
        name: widget.legends[index],
        dataSource: dataPoints[index],
        xValueMapper: (DoubleDataPoint dataPoint, _) => widget.data[0][dataPoint.xPoint.toInt()],
        yValueMapper: (DoubleDataPoint dataPoint, _) => dataPoint.yPoint,
        color: widget.chartColors[index],
        animationDuration: DataStore().settingsDataStore.chartAnimation ? ChartConstants.animationDuration : 0,
        animationDelay: widget.animationDelay,
      ),
    ).reversed.toList();
  }

  Legend _legend({required bool isVisible}) {
    return Legend(
      isVisible: isVisible,
      overflowMode: LegendItemOverflowMode.wrap,
      orientation: LegendItemOrientation.horizontal,
      position: LegendPosition.top,
      alignment: ChartAlignment.center,
      shouldAlwaysShowScrollbar: true,
    );
  }

  ZoomPanBehavior _zoomPanBehavior() {
    return ZoomPanBehavior(
      enablePinching: false,
      enablePanning: true,
      enableMouseWheelZooming: false,
    );
  }

  TrackballBehavior _trackballBehavior() {
    return TrackballBehavior(
      enable: true,
      markerSettings: const TrackballMarkerSettings(
        markerVisibility: TrackballVisibilityMode.visible,
        height: 10,
        width: 10,
        borderWidth: 1,
      ),
      activationMode: ActivationMode.singleTap,
      tooltipAlignment: ChartAlignment.center,
      tooltipDisplayMode: TrackballDisplayMode.groupAllPoints,
      shouldAlwaysShow: false,
      builder: _trackBallBuilder,
    );
  }

  Widget _trackBallBuilder(BuildContext context, TrackballDetails trackballDetails) {
    return Container(
      height:
          54.0 + (trackballDetails.groupingModeInfo?.points.where((element) => (element.y ?? 0) > 0).length ?? 0) * 16,
      width: 180.0,
      decoration: const BoxDecoration(
        color: Color.fromRGBO(0, 8, 22, 0.75),
        borderRadius: BorderRadius.all(Radius.circular(6.0)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Text(
              "${AppLocalizations.of(context)!.charts_grade}: ${trackballDetails.groupingModeInfo!.points.first.x}",
              style: const TextStyle(color: Colors.white),
            ),
            const Divider(),
            ...List.generate(
              trackballDetails.groupingModeInfo!.points.length,
              (index) => (trackballDetails.groupingModeInfo!.points[index].y ?? 0) <= 0
                  ? Container()
                  : Row(
                      children: [
                        Container(
                          width: 12,
                          height: 12,
                          decoration: BoxDecoration(
                            color: trackballDetails.groupingModeInfo!.visibleSeriesList[index].color,
                            borderRadius: BorderRadius.circular(100),
                          ),
                        ),
                        if (widget.useLegendNameForTrackball)
                          Text(
                            "  ${trackballDetails.groupingModeInfo!.visibleSeriesList[index].name}:  ",
                            style: const TextStyle(color: Colors.white),
                          ),
                        if (!widget.useLegendNameForTrackball) const SizedBox(width: 6.0),
                        Text(
                          widget.measureFormatter(trackballDetails.groupingModeInfo!.points[index].y!.toDouble()),
                          style: const TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
            ),
          ],
        ),
      ),
    );
  }

  void _calculateDataPoints() {
    dataPoints = [];

    for (int j = 1; j < widget.data.length; j++) {
      dataPoints.add([]);
    }

    for (int i = 0; i < widget.data[0].length; i++) {
      int tmp = 0;
      for (int j = 1; j < widget.data.length; j++) {
        dataPoints[j - 1].add(DoubleDataPoint(i.toDouble(), widget.data[j][i].toDouble()));
        tmp += widget.data[j][i] as int;
      }
      if (maxHeight < tmp) {
        indexOfHighestBar = i;
      }
      maxHeight = max(maxHeight, tmp);
    }
  }
}
