import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/charts/data_points.dart';
import 'package:climbing_track/ui/pages/statistics/chart_constants.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class SyncfusionCategoryBarChart extends StatefulWidget {
  final List<StringDataPoint> data;
  final bool expanded;
  final double animationDelay;

  const SyncfusionCategoryBarChart(
    this.data,
    this.expanded, {
    super.key,
    required this.animationDelay,
  });

  @override
  _SyncfusionCategoryBarChartState createState() => _SyncfusionCategoryBarChartState();
}

class _SyncfusionCategoryBarChartState extends State<SyncfusionCategoryBarChart> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        _createChart(),
        //This container makes graph touchable, so it can be expanded
        if (!widget.expanded) Container(color: Colors.transparent)
      ],
    );
  }

  Widget _createChart() {
    return SfCartesianChart(
      margin: widget.expanded ? const EdgeInsets.fromLTRB(10, 20, 20, 16) : EdgeInsets.zero,
      plotAreaBorderWidth: 0,
      legend: const Legend(isVisible: false),
      primaryXAxis: _primaryXAxis(),
      primaryYAxis: _primaryYAxis(),
      series: _getSeries(),
      tooltipBehavior: TooltipBehavior(
        enable: widget.expanded,
        canShowMarker: false,
        header: '',
        format: 'point.y',
      ),
    );
  }

  ChartAxis _primaryXAxis() {
    return CategoryAxis(
      isVisible: widget.expanded,
      majorGridLines: const MajorGridLines(width: 0),
      labelRotation: 90,
      interval: 1,
    );
  }

  ChartAxis _primaryYAxis() {
    return NumericAxis(
      rangePadding: widget.expanded ? ChartRangePadding.normal : ChartRangePadding.none,
      isVisible: widget.expanded,
      minimum: 0,
      maximum: 100,
      interval: 10,
      labelFormat: widget.expanded ? "{value}%" : ' ',
    );
  }

  /// Get column series with tracker
  List<ColumnSeries<StringDataPoint, String>> _getSeries() {
    return <ColumnSeries<StringDataPoint, String>>[
      ColumnSeries<StringDataPoint, String>(
        dataSource: widget.data,

        /// We can enable the track for column here.
        isTrackVisible: true,
        trackColor: const Color.fromRGBO(198, 201, 207, 1),
        borderRadius: const BorderRadius.only(topRight: Radius.circular(8), topLeft: Radius.circular(8)),
        xValueMapper: (StringDataPoint sales, _) => sales.xPoint,
        yValueMapper: (StringDataPoint sales, _) => sales.yPoint,
        dataLabelSettings: const DataLabelSettings(
          isVisible: true,
          labelAlignment: ChartDataLabelAlignment.top,
          textStyle: TextStyle(fontSize: 10, color: Colors.white, fontWeight: FontWeight.w700),
        ),
        animationDuration: DataStore().settingsDataStore.chartAnimation ? ChartConstants.animationDuration : 0,
        animationDelay: widget.animationDelay,
      ),
    ];
  }
}
