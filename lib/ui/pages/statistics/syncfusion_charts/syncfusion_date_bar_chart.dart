import 'dart:math';
import 'dart:ui';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/charts/statistic_charts.dart';
import 'package:climbing_track/backend/model/charts/data_points.dart';
import 'package:climbing_track/ui/pages/statistics/chart_constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class SyncfusionDateTimeBarChart extends StatefulWidget {
  final List<List<DateTimeDataPoint>> data;
  final bool expanded;
  final List<String> legends;
  final double? maxY;
  final String Function(double value) measureFormatter;
  final String Function(double value)? yLabelFormatter;
  final List<Color> chartColors;
  final bool useLegendNameForTrackball;
  final bool fillArea;
  final LinearGradient? linearGradient;
  final double areaOpacity;
  final double animationDelay;

  const SyncfusionDateTimeBarChart(
    this.data,
    this.expanded,
    this.legends, {
    super.key,
    required this.measureFormatter,
    required this.animationDelay,
    this.yLabelFormatter,
    this.maxY,
    this.chartColors = AppColors.chart_colors,
    this.useLegendNameForTrackball = true,
    this.fillArea = false,
    this.linearGradient,
    this.areaOpacity = 0.2,
  });

  @override
  _SyncfusionDateTimeBarChartState createState() => _SyncfusionDateTimeBarChartState();
}

class _SyncfusionDateTimeBarChartState extends State<SyncfusionDateTimeBarChart> {
  late List<List<DateTimeDataPoint>> data;
  late String languageCode;

  @override
  void initState() {
    super.initState();
    limitData();
  }

  void limitData() {
    if (DataStore().settingsDataStore.limitCharts) {
      data = widget.data
          .map((e) => e.getRange(max(0, e.length - StatisticCharts.MAX_SESSION_AMOUNT), e.length).toList())
          .toList();
    } else {
      data = widget.data;
    }
  }

  @override
  Widget build(BuildContext context) {
    languageCode = Localizations.localeOf(context).languageCode;
    return Stack(
      children: [
        _createChart(),
        //This container makes graph touchable, so it can be expanded
        if (!widget.expanded) Container(color: Colors.transparent)
      ],
    );
  }

  Widget _createChart() {
    return SfCartesianChart(
      margin: widget.expanded ? const EdgeInsets.fromLTRB(0, 0, 10, 10) : EdgeInsets.zero,
      plotAreaBorderWidth: 0,
      primaryXAxis: _primaryXAxis(),
      primaryYAxis: _primaryYAxis(),
      onTooltipRender: _onTooltipRender,
      legend: _legend(isVisible: widget.expanded),
      series: _series(),
      zoomPanBehavior: widget.expanded && !DataStore().settingsDataStore.limitCharts ? _zoomPanBehavior() : null,
      trackballBehavior: _trackballBehavior(),
    );
  }

  ChartAxis _primaryXAxis() {
    return NumericAxis(
      isVisible: widget.expanded,
      initialZoomFactor: clampDouble(1.0 / data.first.length * StatisticCharts.MAX_SESSION_AMOUNT, 0.0, 1.0),
      initialZoomPosition: max(0.0, 1.0 - ((1.0 / data.first.length) * StatisticCharts.MAX_SESSION_AMOUNT)),
      majorGridLines: const MajorGridLines(width: 0),
      labelRotation: 75,
      axisLabelFormatter: (AxisLabelRenderDetails details) => ChartAxisLabel(
        "",
        null,
      ),
    );
  }

  ChartAxis _primaryYAxis() {
    return NumericAxis(
      isVisible: widget.expanded,
      rangePadding: widget.expanded ? ChartRangePadding.normal : ChartRangePadding.none,
      minimum: widget.expanded ? 0.0 : null,
      maximum: widget.expanded ? widget.maxY : null,
      axisLabelFormatter: (AxisLabelRenderDetails details) => ChartAxisLabel(
        widget.yLabelFormatter == null
            ? widget.measureFormatter(details.value.toDouble())
            : widget.yLabelFormatter!(details.value.toDouble()),
        null,
      ),
    );
  }

  dynamic _series() {
    return data
        .map(
          (dataPoint) => StackedColumnSeries<DateTimeDataPoint, int>(
            name: widget.legends[data.indexOf(dataPoint)],
            dataSource: dataPoint,
            xValueMapper: (DateTimeDataPoint dataPoint, int index) => index,
            yValueMapper: (DateTimeDataPoint dataPoint, _) => dataPoint.yPoint,
            color: widget.chartColors[data.indexOf(dataPoint)],
            animationDuration: DataStore().settingsDataStore.chartAnimation ? ChartConstants.animationDuration : 0,
            animationDelay: widget.animationDelay,
          ),
        )
        .toList()
        .reversed
        .toList();
  }

  Legend _legend({required bool isVisible}) {
    return Legend(
      isVisible: isVisible,
      overflowMode: LegendItemOverflowMode.wrap,
      orientation: LegendItemOrientation.horizontal,
      position: LegendPosition.top,
      alignment: ChartAlignment.center,
      shouldAlwaysShowScrollbar: true,
      toggleSeriesVisibility: true,
    );
  }

  void _onTooltipRender(TooltipArgs args) {
    String languageCode = Localizations.localeOf(context).languageCode;

    String value = widget.measureFormatter(args.dataPoints![args.pointIndex!.toInt()].y);
    args.text = '${DateFormat.yMMMd(languageCode).format(args.dataPoints![args.pointIndex!.toInt()].x)} : $value';
  }

  ZoomPanBehavior _zoomPanBehavior() {
    return ZoomPanBehavior(
      enablePinching: true,
      zoomMode: ZoomMode.x,
      enablePanning: true,
      enableMouseWheelZooming: false,
    );
  }

  TrackballBehavior _trackballBehavior() {
    return TrackballBehavior(
      enable: true,
      markerSettings: const TrackballMarkerSettings(
        markerVisibility: TrackballVisibilityMode.visible,
        height: 10,
        width: 10,
        borderWidth: 1,
      ),
      activationMode: ActivationMode.longPress,
      tooltipAlignment: ChartAlignment.center,
      tooltipDisplayMode: TrackballDisplayMode.groupAllPoints,
      shouldAlwaysShow: false,
      builder: _trackBallBuilder,
    );
  }

  Widget _trackBallBuilder(BuildContext context, TrackballDetails trackballDetails) {
    return Container(
      height: 54.0 + trackballDetails.groupingModeInfo!.points.where((element) => (element.y ?? 0) > 0).length * 16,
      width: 180.0,
      decoration: const BoxDecoration(
        color: Color.fromRGBO(0, 8, 22, 0.75),
        borderRadius: BorderRadius.all(Radius.circular(6.0)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Text(
              DateFormat.yMMMd(languageCode).format(
                data.first[trackballDetails.groupingModeInfo!.points.first.x].xPoint,
              ),
              style: const TextStyle(color: Colors.white),
            ),
            const Divider(),
            ...List.generate(
              trackballDetails.groupingModeInfo!.points.length,
              (index) => (trackballDetails.groupingModeInfo!.points[index].y ?? 0) <= 0
                  ? Container()
                  : Row(
                      children: [
                        Container(
                          width: 12,
                          height: 12,
                          decoration: BoxDecoration(
                            color: trackballDetails.groupingModeInfo!.visibleSeriesList[index].color,
                            borderRadius: BorderRadius.circular(100),
                          ),
                        ),
                        if (widget.useLegendNameForTrackball)
                          Flexible(
                            child: Text(
                              "  ${trackballDetails.groupingModeInfo!.visibleSeriesList[index].name}:  ",
                              style: const TextStyle(color: Colors.white),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        if (!widget.useLegendNameForTrackball) const SizedBox(width: 6.0),
                        Text(
                          widget.measureFormatter(trackballDetails.groupingModeInfo!.points[index].y!.toDouble()),
                          style: const TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
