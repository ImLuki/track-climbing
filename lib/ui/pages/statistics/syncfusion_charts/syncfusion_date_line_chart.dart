import 'dart:math';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/charts/statistic_charts.dart';
import 'package:climbing_track/backend/model/charts/data_points.dart';
import 'package:climbing_track/ui/pages/statistics/chart_constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SyncfusionDateTimeLineChart extends StatefulWidget {
  final List<DateTimeDataPoint> data1;
  final List<DateTimeDataPoint> data2;
  final bool expanded;
  final String legendOne;
  final String? legendTwo;
  final double? maxY;
  final double minY;
  final double? maximumLabels;
  final int maximumLabelsCap;
  final String Function(double value) measureFormatter;
  final String Function(double value)? yLabelFormatter;
  final List<Color> chartColors;
  final bool useLegendNameForTrackball;
  final bool fillArea;
  final LinearGradient? linearGradient;
  final double animationDelay;

  /// Warning: This also applies on border opacity
  final double areaOpacity;

  const SyncfusionDateTimeLineChart(
    this.data1,
    this.expanded,
    this.legendOne, {
    super.key,
    required this.measureFormatter,
    required this.animationDelay,
    this.yLabelFormatter,
    this.data2 = const [],
    this.legendTwo,
    this.maxY,
    this.minY = 0.0,
    this.chartColors = AppColors.chart_colors,
    this.useLegendNameForTrackball = true,
    this.fillArea = false,
    this.linearGradient,
    this.areaOpacity = 1.0,
    this.maximumLabels,
    this.maximumLabelsCap = 3,
  });

  @override
  _SyncfusionDateTimeLineChartState createState() => _SyncfusionDateTimeLineChartState();
}

class _SyncfusionDateTimeLineChartState extends State<SyncfusionDateTimeLineChart> {
  late List<DateTimeDataPoint> data1;
  late List<DateTimeDataPoint> data2;
  late List<DateTime> dateTimes;
  double? zoomFactor;

  @override
  void initState() {
    super.initState();
    dateTimes = [...widget.data1, ...widget.data2].map((DateTimeDataPoint e) => e.xPoint).toSet().toList();
    dateTimes.sort();
    limitData();
  }

  void limitData() {
    DateTime dateTime = dateTimes[max(0, dateTimes.length - StatisticCharts.MAX_SESSION_AMOUNT)];

    data1 = DataStore().settingsDataStore.limitCharts
        ? widget.data1.where((element) => !element.xPoint.isBefore(dateTime)).toList()
        : widget.data1;
    data2 = DataStore().settingsDataStore.limitCharts
        ? widget.data2.where((element) => !element.xPoint.isBefore(dateTime)).toList()
        : widget.data2;

    dateTimes = DataStore().settingsDataStore.limitCharts
        ? dateTimes.where((element) => !element.isBefore(dateTime)).toList()
        : dateTimes;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        _createChart(),
        //This container makes graph touchable, so it can be expanded
        if (!widget.expanded) Container(color: Colors.transparent)
      ],
    );
  }

  Widget _createChart() {
    return SfCartesianChart(
      margin: widget.expanded ? const EdgeInsets.fromLTRB(0, 0, 10, 10) : EdgeInsets.zero,
      plotAreaBorderWidth: 0,
      primaryXAxis: _primaryXAxis(),
      primaryYAxis: _primaryYAxis(),
      onTooltipRender: _onTooltipRender,
      //tooltipBehavior: widget.expanded ? TooltipBehavior(enable: true) : null,
      legend: _legend(isVisible: widget.expanded),
      series: _series(),
      trackballBehavior: _trackballBehavior(),
      zoomPanBehavior: widget.expanded && !DataStore().settingsDataStore.limitCharts ? _zoomPanBehavior() : null,
      onZooming: (args) {
        if (args.previousZoomFactor != args.currentZoomFactor) {
          zoomFactor = args.currentZoomFactor;
        }
      },
      onMarkerRender: (MarkerRenderArgs args) {
        if (zoomFactor != null && zoomFactor! > 1.0 / dateTimes.length * (StatisticCharts.MAX_SESSION_AMOUNT + 5)) {
          args.markerHeight = 0;
          args.markerWidth = 0;
        }
      },
    );
  }

  ChartAxis _primaryXAxis() {
    return DateTimeAxis(
      maximumLabels: 8,
      isVisible: widget.expanded,
      initialVisibleMinimum: dateTimes[max(0, dateTimes.length - StatisticCharts.MAX_SESSION_AMOUNT)],
      initialVisibleMaximum: dateTimes.last,
      majorGridLines: const MajorGridLines(width: 0),
      labelRotation: 75,
      intervalType: DateTimeIntervalType.auto,
      edgeLabelPlacement: EdgeLabelPlacement.shift,
      maximum: dateTimes.length > 1
          ? dateTimes.last.add(const Duration(seconds: 1))
          : dateTimes.last.add(const Duration(days: 10)),
      minimum: dateTimes.length == 1 ? dateTimes.first.add(const Duration(days: -10)) : null,
    );
  }

  ChartAxis _primaryYAxis() {
    return NumericAxis(
      isVisible: widget.expanded,
      rangePadding: widget.expanded ? ChartRangePadding.normal : ChartRangePadding.none,
      minimum: widget.expanded ? widget.minY : null,
      maximum: widget.expanded ? widget.maxY : null,
      axisLabelFormatter: (AxisLabelRenderDetails details) => ChartAxisLabel(
        widget.yLabelFormatter == null
            ? widget.measureFormatter(details.value.toDouble())
            : widget.yLabelFormatter!(details.value.toDouble()),
        null,
      ),
      maximumLabels: widget.maximumLabels != null
          ? min(
              widget.maximumLabelsCap,
              (widget.maximumLabels! / (MediaQuery.of(context).size.height / 100 / 2)).round(),
            )
          : 3,
    );
  }

  dynamic _series() {
    if (widget.fillArea && widget.expanded) {
      return _areaSeries();
    } else {
      return _lineSeries();
    }
  }

  dynamic _lineSeries() {
    if (data2.isEmpty) {
      return <FastLineSeries<DateTimeDataPoint, DateTime>>[
        FastLineSeries<DateTimeDataPoint, DateTime>(
          name: widget.legendOne,
          dataSource: data1,
          xValueMapper: (DateTimeDataPoint dataPoint, _) => dataPoint.xPoint,
          yValueMapper: (DateTimeDataPoint dataPoint, _) => dataPoint.yPoint,
          color: widget.chartColors[1 % widget.chartColors.length],
          markerSettings: _markerSettings(data: data1),
          width: 3.0,
          animationDuration: DataStore().settingsDataStore.chartAnimation ? ChartConstants.animationDuration : 0,
          animationDelay: widget.animationDelay,
        ),
      ];
    } else {
      return <LineSeries<DateTimeDataPoint, DateTime>>[
        // FastLineSeries with two lines is buggy, dates will be shuffled
        LineSeries<DateTimeDataPoint, DateTime>(
          name: widget.legendTwo,
          dataSource: data2,
          xValueMapper: (DateTimeDataPoint dataPoint, _) => dataPoint.xPoint,
          yValueMapper: (DateTimeDataPoint dataPoint, _) => dataPoint.yPoint,
          color: widget.chartColors[0 % widget.chartColors.length],
          markerSettings: _markerSettings(data: data2),
          width: 3.0,
          animationDuration: DataStore().settingsDataStore.chartAnimation ? ChartConstants.animationDuration : 0,
          animationDelay: widget.animationDelay,
        ),
        LineSeries<DateTimeDataPoint, DateTime>(
          name: widget.legendOne,
          dataSource: data1,
          xValueMapper: (DateTimeDataPoint dataPoint, _) => dataPoint.xPoint,
          yValueMapper: (DateTimeDataPoint dataPoint, _) => dataPoint.yPoint,
          color: widget.chartColors[1 % widget.chartColors.length],
          markerSettings: _markerSettings(data: data1),
          width: 3.0,
          animationDuration: DataStore().settingsDataStore.chartAnimation ? ChartConstants.animationDuration : 0,
          animationDelay: widget.animationDelay,
        ),
      ];
    }
  }

  dynamic _areaSeries() {
    if (data2.isEmpty) {
      return <AreaSeries<DateTimeDataPoint, DateTime>>[
        AreaSeries<DateTimeDataPoint, DateTime>(
          name: widget.legendOne,
          dataSource: data1,
          xValueMapper: (DateTimeDataPoint dataPoint, _) => dataPoint.xPoint,
          yValueMapper: (DateTimeDataPoint dataPoint, _) => dataPoint.yPoint,
          color: widget.chartColors[1 % widget.chartColors.length].withOpacity(widget.areaOpacity),
          borderColor: widget.chartColors[1 % widget.chartColors.length],
          markerSettings: _markerSettings(
            fillColor: widget.chartColors[1 % widget.chartColors.length],
            data: data1,
          ),
          borderWidth: 4.0,
          // opacity: widget.areaOpacity,
          gradient: widget.linearGradient,
          animationDuration: DataStore().settingsDataStore.chartAnimation ? ChartConstants.animationDuration : 0,
          animationDelay: widget.animationDelay,
          trendlines: _trendLines(),
        ),
      ];
    } else {
      return <AreaSeries<DateTimeDataPoint, DateTime>>[
        AreaSeries<DateTimeDataPoint, DateTime>(
          name: widget.legendTwo,
          dataSource: data2,
          xValueMapper: (DateTimeDataPoint dataPoint, _) => dataPoint.xPoint,
          yValueMapper: (DateTimeDataPoint dataPoint, _) => dataPoint.yPoint,
          color: widget.chartColors[0 % widget.chartColors.length].withOpacity(widget.areaOpacity),
          borderColor: widget.chartColors[0 % widget.chartColors.length],
          markerSettings: _markerSettings(
            fillColor: widget.chartColors[0 % widget.chartColors.length],
            data: data2,
          ),
          borderWidth: 4.0,
          //opacity: widget.areaOpacity,
          gradient: widget.linearGradient,
          animationDuration: DataStore().settingsDataStore.chartAnimation ? ChartConstants.animationDuration : 0,
          animationDelay: widget.animationDelay,
        ),
        AreaSeries<DateTimeDataPoint, DateTime>(
          name: widget.legendOne,
          dataSource: data1,
          xValueMapper: (DateTimeDataPoint dataPoint, _) => dataPoint.xPoint,
          yValueMapper: (DateTimeDataPoint dataPoint, _) => dataPoint.yPoint,
          color: widget.chartColors[1 % widget.chartColors.length].withOpacity(widget.areaOpacity),
          borderColor: widget.chartColors[1 % widget.chartColors.length],
          markerSettings: _markerSettings(
            fillColor: widget.chartColors[1 % widget.chartColors.length],
            data: data1,
          ),
          borderWidth: 4.0,
          //opacity: widget.areaOpacity,
          gradient: widget.linearGradient,
          animationDuration: DataStore().settingsDataStore.chartAnimation ? ChartConstants.animationDuration : 0,
          animationDelay: widget.animationDelay,
        ),
      ];
    }
  }

  MarkerSettings _markerSettings({Color? fillColor, required List<DateTimeDataPoint> data}) {
    return MarkerSettings(
      isVisible: widget.expanded || data.length <= 3,
      height: widget.expanded ? 10.0 : 8.0,
      width: widget.expanded ? 10.0 : 8.0,
      color: Colors.white,
      borderColor: fillColor,
    );
  }

  Legend _legend({required bool isVisible}) {
    return Legend(
      isVisible: isVisible,
      overflowMode: LegendItemOverflowMode.wrap,
      orientation: LegendItemOrientation.horizontal,
      position: LegendPosition.top,
      alignment: ChartAlignment.center,
      shouldAlwaysShowScrollbar: true,
    );
  }

  void _onTooltipRender(TooltipArgs args) {
    String languageCode = Localizations.localeOf(context).languageCode;

    String value = widget.measureFormatter(args.dataPoints![args.pointIndex!.toInt()].y);
    args.text = '${DateFormat.yMMMd(languageCode).format(args.dataPoints![args.pointIndex!.toInt()].x)} : $value';
  }

  ZoomPanBehavior _zoomPanBehavior() {
    return ZoomPanBehavior(
      enablePinching: true,
      zoomMode: ZoomMode.x,
      enablePanning: true,
      enableMouseWheelZooming: false,
    );
  }

  TrackballBehavior _trackballBehavior() {
    return TrackballBehavior(
      enable: true,
      markerSettings: const TrackballMarkerSettings(
        markerVisibility: TrackballVisibilityMode.visible,
        height: 10,
        width: 10,
        borderWidth: 1,
      ),
      activationMode: ActivationMode.longPress,
      tooltipAlignment: ChartAlignment.center,
      tooltipDisplayMode: TrackballDisplayMode.groupAllPoints,
      shouldAlwaysShow: false,
      builder: _trackBallBuilder,
    );
  }

  Widget _trackBallBuilder(BuildContext context, TrackballDetails trackballDetails) {
    String languageCode = Localizations.localeOf(context).languageCode;

    return Container(
      height: 54.0 + trackballDetails.groupingModeInfo!.points.length * 16,
      width: 180.0,
      decoration: const BoxDecoration(
        color: Color.fromRGBO(0, 8, 22, 0.75),
        borderRadius: BorderRadius.all(Radius.circular(6.0)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Text(
              DateFormat.yMMMd(languageCode).format(trackballDetails.groupingModeInfo!.points.first.x),
              style: const TextStyle(color: Colors.white),
            ),
            const Divider(),
            ...List.generate(
              trackballDetails.groupingModeInfo!.points.length,
              (index) => Row(
                children: [
                  Container(
                    width: 12,
                    height: 12,
                    decoration: BoxDecoration(
                      color: trackballDetails.groupingModeInfo!.visibleSeriesList[index].color,
                      borderRadius: BorderRadius.circular(100),
                    ),
                  ),
                  if (widget.useLegendNameForTrackball)
                    Text(
                      "  ${trackballDetails.groupingModeInfo!.visibleSeriesList[index].name}:  ",
                      style: const TextStyle(color: Colors.white),
                    ),
                  if (!widget.useLegendNameForTrackball) const SizedBox(width: 6.0),
                  Flexible(
                    child: Text(
                      widget.measureFormatter(trackballDetails.groupingModeInfo!.points[index].y!.toDouble()),
                      style: const TextStyle(color: Colors.white),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Trendline>? _trendLines() {
    return <Trendline>[
      Trendline(
        type: TrendlineType.linear,
        color: Colors.blue,
        name: AppLocalizations.of(context)!.charts_average_trendline,
        width: 4,
        isVisible: false,
      ),
    ];
  }
}
