import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/charts/data_points.dart';
import 'package:climbing_track/ui/pages/statistics/chart_constants.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class SyncfusionDonutChart extends StatefulWidget {
  final bool expanded;
  final Map<String, double> data;
  final List<Color>? colors;
  final double animationDelay;
  final LegendItemBuilder? legendItemBuilder;
  final String tooltipFormat;
  final bool tooltipEnabled;

  const SyncfusionDonutChart({
    super.key,
    required this.data,
    required this.expanded,
    required this.animationDelay,
    this.colors,
    this.legendItemBuilder,
    this.tooltipFormat = "point.x - point.y%",
    this.tooltipEnabled = true,
  });

  @override
  _SyncfusionDonutChartState createState() => _SyncfusionDonutChartState();
}

class _SyncfusionDonutChartState extends State<SyncfusionDonutChart> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        _createChart(),
        //This container makes graph touchable, so it can be expanded
        if (!widget.expanded) Container(color: Colors.transparent)
      ],
    );
  }

  Widget _createChart() {
    return SfCircularChart(
      margin: EdgeInsets.zero,
      legend: _legend(isVisible: widget.expanded),
      series: _series(),
      tooltipBehavior: _tooltipBehavior(),
      palette: widget.colors ?? AppColors.chart_colors.take(widget.data.length).toList(),
    );
  }

  List<DoughnutSeries> _series() {
    double valueSum = widget.data.values.sum;
    List<StringDataPoint> data = widget.data.entries
        .map((element) => StringDataPoint(
            element.key, widget.data.values.sum == 0.0 ? 0.0 : (element.value / valueSum * 1000).round() / 10))
        .toList();

    return <DoughnutSeries<StringDataPoint, String>>[
      DoughnutSeries<StringDataPoint, String>(
        dataSource: data,
        xValueMapper: (StringDataPoint data, _) => data.xPoint,
        yValueMapper: (StringDataPoint data, _) => data.yPoint,
        dataLabelMapper: (StringDataPoint data, _) => '${data.yPoint}%',
        dataLabelSettings: _dataLabelSettings(),
        startAngle: 0,
        endAngle: 360,
        radius: widget.expanded ? '70%' : '100%',
        innerRadius: widget.expanded ? "50%" : "30%",
        animationDuration: DataStore().settingsDataStore.chartAnimation ? ChartConstants.animationDuration : 0,
        animationDelay: widget.animationDelay,
        legendIconType: LegendIconType.diamond,
      ),
    ];
  }

  Legend _legend({required bool isVisible}) {
    return Legend(
      isVisible: isVisible,
      overflowMode: LegendItemOverflowMode.wrap,
      orientation: LegendItemOrientation.horizontal,
      position: LegendPosition.bottom,
      alignment: ChartAlignment.center,
      toggleSeriesVisibility: false,
      // shouldAlwaysShowScrollbar: true,
      legendItemBuilder: widget.legendItemBuilder,
    );
  }

  TooltipBehavior _tooltipBehavior() {
    return TooltipBehavior(
      enable: widget.tooltipEnabled,
      format: widget.tooltipFormat,
    );
  }

  DataLabelSettings _dataLabelSettings() {
    return DataLabelSettings(
      margin: EdgeInsets.zero,
      isVisible: widget.expanded,
      labelPosition: ChartDataLabelPosition.inside,
      connectorLineSettings: const ConnectorLineSettings(type: ConnectorType.line),
      labelIntersectAction: LabelIntersectAction.shift,
      builder: (dynamic data, ChartPoint<dynamic> point, dynamic series, int pointIndex, int seriesIndex) {
        return Container(
          padding: const EdgeInsets.symmetric(vertical: 3.0, horizontal: 5.0),
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
          ),
          child: Text("${point.y}%", style: const TextStyle(fontWeight: FontWeight.bold)),
        );
      },
    );
  }
}
