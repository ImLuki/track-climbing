import 'dart:math';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/model/charts/data_points.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/pages/statistics/chart_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class SyncfusionStackedBarLineChart extends StatefulWidget {
  final List<List<dynamic>> data;
  final bool expanded;
  final List<String> legends;
  final double? maxY;
  final double minY;
  final double? maximumLabels;
  final int maximumLabelsCap;
  final String Function(double value) barMeasureFormatter;
  final String Function(double value)? barYLabelFormatter;
  final String Function(double value) lineMeasureFormatter;
  final String Function(double value)? lineYLabelFormatter;
  final List<Color> chartColors;
  final bool useLegendNameForTrackball;
  final bool reorderMonths;
  final double animationDelay;

  const SyncfusionStackedBarLineChart({
    super.key,
    required this.data,
    required this.expanded,
    required this.legends,
    required this.barMeasureFormatter,
    required this.lineMeasureFormatter,
    required this.animationDelay,
    this.barYLabelFormatter,
    this.lineYLabelFormatter,
    this.maxY,
    this.minY = 0.0,
    this.chartColors = AppColors.chart_colors,
    this.useLegendNameForTrackball = true,
    this.reorderMonths = false,
    this.maximumLabels = 4,
    this.maximumLabelsCap = 3,
  })  : assert(data.length == legends.length),
        assert(data.length <= chartColors.length);

  @override
  _SyncfusionStackedBarLineChartState createState() => _SyncfusionStackedBarLineChartState();
}

class _SyncfusionStackedBarLineChartState extends State<SyncfusionStackedBarLineChart> {
  static const String lineAxisName = "line_axis_right";
  late List<DoubleDataPoint> lineDataOne;
  late List<DoubleDataPoint> lineDataTwo;
  late List<DoubleDataPoint> barDataOne;
  late List<DoubleDataPoint> barDataTwo;
  late int currentMonth;

  @override
  void initState() {
    super.initState();
    _calculateDataPoints();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        _createChart(),
        //This container makes graph touchable, so it can be expanded
        if (!widget.expanded) Container(color: Colors.transparent)
      ],
    );
  }

  Widget _createChart() {
    return SfCartesianChart(
      margin: widget.expanded ? const EdgeInsets.fromLTRB(0, 0, 10, 10) : EdgeInsets.zero,
      plotAreaBorderWidth: 0,
      primaryXAxis: _primaryXAxis(),
      primaryYAxis: _primaryYAxis(),
      axes: <ChartAxis>[_secondaryYAxis()],
      legend: _legend(isVisible: widget.expanded),
      series: _series(),
      zoomPanBehavior: widget.expanded ? _zoomPanBehavior() : null,
      trackballBehavior: _trackballBehavior(),
    );
  }

  ChartAxis _primaryXAxis() {
    return CategoryAxis(
      isVisible: widget.expanded,
      majorGridLines: const MajorGridLines(width: 0),
      labelRotation: 90,
      interval: 1.0,
      minimum: -0.5,
      maximum: 11.5,
      labelPlacement: LabelPlacement.onTicks,
      maximumLabels: 12,
    );
  }

  ChartAxis _primaryYAxis() {
    return NumericAxis(
      rangePadding: widget.expanded ? ChartRangePadding.normal : ChartRangePadding.none,
      isVisible: widget.expanded,
      axisLabelFormatter: (AxisLabelRenderDetails details) => ChartAxisLabel(
        widget.barYLabelFormatter == null
            ? widget.barMeasureFormatter(details.value.toDouble())
            : widget.barYLabelFormatter!(details.value.toDouble()),
        null,
      ),
    );
  }

  ChartAxis _secondaryYAxis() {
    return NumericAxis(
      isVisible: widget.expanded,
      opposedPosition: true,
      name: lineAxisName,
      majorGridLines: const MajorGridLines(width: 0),
      rangePadding: widget.expanded ? ChartRangePadding.normal : ChartRangePadding.none,
      minimum: widget.expanded ? widget.minY : null,
      maximum: widget.expanded ? widget.maxY : null,
      axisLabelFormatter: (AxisLabelRenderDetails details) => ChartAxisLabel(
        widget.lineYLabelFormatter == null
            ? widget.lineMeasureFormatter(details.value.toDouble())
            : widget.lineYLabelFormatter!(details.value.toDouble()),
        null,
      ),
      maximumLabels: widget.maximumLabels != null
          ? min(
              widget.maximumLabelsCap,
              (widget.maximumLabels! / (MediaQuery.of(context).size.height / 100 / 2)).round(),
            )
          : 3,
    );
  }

  List<CartesianSeries<dynamic, dynamic>> _series() {
    return [
      if (widget.data.length > 2)
        StackedColumnSeries<DoubleDataPoint, String>(
          name: widget.legends[2],
          dataSource: barDataTwo,
          xValueMapper: (DoubleDataPoint dataPoint, _) => getMonthFromIndex(context, dataPoint.xPoint.toInt()),
          yValueMapper: (DoubleDataPoint dataPoint, _) => dataPoint.yPoint,
          color: widget.chartColors[2],
          animationDuration: DataStore().settingsDataStore.chartAnimation ? ChartConstants.animationDuration : 0,
          animationDelay: widget.animationDelay,
        ),
      StackedColumnSeries<DoubleDataPoint, String>(
        name: widget.legends[0],
        dataSource: barDataOne,
        xValueMapper: (DoubleDataPoint dataPoint, _) => getMonthFromIndex(context, dataPoint.xPoint.toInt()),
        yValueMapper: (DoubleDataPoint dataPoint, _) => dataPoint.yPoint,
        color: widget.chartColors[0],
        animationDuration: DataStore().settingsDataStore.chartAnimation ? ChartConstants.animationDuration : 0,
        animationDelay: widget.animationDelay,
      ),
      if (widget.data.length > 2)
        LineSeries<DoubleDataPoint, String>(
          name: widget.legends[3],
          dataSource: lineDataTwo,
          yAxisName: lineAxisName,
          xValueMapper: (DoubleDataPoint dataPoint, _) => getMonthFromIndex(context, dataPoint.xPoint.toInt()),
          yValueMapper: (DoubleDataPoint dataPoint, _) => dataPoint.yPoint,
          color: widget.chartColors[3],
          markerSettings: _markerSettings(fillColor: widget.chartColors[3]),
          width: 3.0,
          animationDuration: DataStore().settingsDataStore.chartAnimation ? ChartConstants.animationDuration : 0,
          animationDelay: widget.animationDelay,
        ),
      LineSeries<DoubleDataPoint, String>(
        name: widget.legends[1],
        dataSource: lineDataOne,
        yAxisName: lineAxisName,
        xValueMapper: (DoubleDataPoint dataPoint, _) => getMonthFromIndex(context, dataPoint.xPoint.toInt()),
        yValueMapper: (DoubleDataPoint dataPoint, _) => dataPoint.yPoint,
        color: widget.chartColors[1],
        markerSettings: _markerSettings(fillColor: widget.chartColors[1]),
        width: 3.0,
        animationDuration: DataStore().settingsDataStore.chartAnimation ? ChartConstants.animationDuration : 0,
        animationDelay: widget.animationDelay,
      ),
    ];
  }

  Legend _legend({required bool isVisible}) {
    return Legend(
      padding: 12.0,
      isVisible: isVisible,
      overflowMode: LegendItemOverflowMode.wrap,
      orientation: LegendItemOrientation.horizontal,
      position: LegendPosition.top,
      alignment: ChartAlignment.center,
      shouldAlwaysShowScrollbar: true,
    );
  }

  ZoomPanBehavior _zoomPanBehavior() {
    return ZoomPanBehavior(
      enablePinching: false,
      enablePanning: true,
      enableMouseWheelZooming: false,
    );
  }

  TrackballBehavior _trackballBehavior() {
    return TrackballBehavior(
      enable: true,
      markerSettings: const TrackballMarkerSettings(
        markerVisibility: TrackballVisibilityMode.visible,
        height: 10,
        width: 10,
        borderWidth: 1,
      ),
      activationMode: ActivationMode.singleTap,
      tooltipAlignment: ChartAlignment.center,
      tooltipDisplayMode: TrackballDisplayMode.groupAllPoints,
      shouldAlwaysShow: false,
      builder: _trackBallBuilder,
    );
  }

  Widget _trackBallBuilder(BuildContext context, TrackballDetails trackballDetails) {
    return Container(
      height: 54.0 + trackballDetails.groupingModeInfo!.points.where((element) => (element.y ?? 0) > 0).length * 16,
      width: 180.0,
      decoration: const BoxDecoration(
        color: Color.fromRGBO(0, 8, 22, 0.75),
        borderRadius: BorderRadius.all(Radius.circular(6.0)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Text(
              trackballDetails.groupingModeInfo!.points.first.x,
              style: const TextStyle(color: Colors.white),
            ),
            const Divider(),
            ...List.generate(
              trackballDetails.groupingModeInfo!.points.length,
              (index) => (trackballDetails.groupingModeInfo!.points[index].y ?? 0) <= 0
                  ? Container()
                  : Row(
                      children: [
                        Container(
                          width: 12,
                          height: 12,
                          decoration: BoxDecoration(
                            color: trackballDetails.groupingModeInfo!.visibleSeriesList[index].color,
                            borderRadius: BorderRadius.circular(100),
                          ),
                        ),
                        if (widget.useLegendNameForTrackball)
                          Text(
                            "  ${trackballDetails.groupingModeInfo!.visibleSeriesList[index].name}:  ",
                            style: const TextStyle(color: Colors.white),
                          ),
                        if (!widget.useLegendNameForTrackball) const SizedBox(width: 6.0),
                        Flexible(
                          child: Text(
                            Util.replaceStringWithBlankSpaces(
                              trackballDetails.groupingModeInfo!.visibleSeriesList[index]
                              is StackedColumnSeriesRenderer ||
                                  trackballDetails.groupingModeInfo!.visibleSeriesList[index]
                                  is ColumnSeriesRenderer
                                  ? widget.barMeasureFormatter(
                                      trackballDetails.groupingModeInfo!.points[index].y!.toDouble(),
                                    )
                                  : widget.lineMeasureFormatter(
                                      trackballDetails.groupingModeInfo!.points[index].y!.toDouble(),
                                    ),
                            ),
                            style: const TextStyle(color: Colors.white),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
            ),
          ],
        ),
      ),
    );
  }

  String getMonthFromIndex(BuildContext context, int index) {
    return [
      AppLocalizations.of(context)!.month_january,
      AppLocalizations.of(context)!.month_february,
      AppLocalizations.of(context)!.month_march,
      AppLocalizations.of(context)!.month_april,
      AppLocalizations.of(context)!.month_may,
      AppLocalizations.of(context)!.month_june,
      AppLocalizations.of(context)!.month_july,
      AppLocalizations.of(context)!.month_august,
      AppLocalizations.of(context)!.month_september,
      AppLocalizations.of(context)!.month_october,
      AppLocalizations.of(context)!.month_november,
      AppLocalizations.of(context)!.month_december,
    ][index];
  }

  void _calculateDataPoints() {
    lineDataOne = [];
    lineDataTwo = [];
    barDataOne = [];
    barDataTwo = [];
    currentMonth = widget.reorderMonths ? DateTime.now().month : 12;
    for (int i = currentMonth; i < currentMonth + 12; i++) {
      int index = i % 12;

      if (widget.data.length > 2) {
        lineDataOne.add(DoubleDataPoint(index.toDouble(), widget.data[0].elementAt(index)));
        lineDataTwo.add(DoubleDataPoint(index.toDouble(), widget.data[1].elementAt(index)));
        barDataOne.add(DoubleDataPoint(index.toDouble(), widget.data[2].elementAt(index)));
        barDataTwo.add(DoubleDataPoint(index.toDouble(), widget.data[3].elementAt(index)));
      } else {
        lineDataOne.add(DoubleDataPoint(index.toDouble(), widget.data[0].elementAt(index)));
        barDataOne.add(DoubleDataPoint(index.toDouble(), widget.data[1].elementAt(index)));
      }
    }
  }

  MarkerSettings _markerSettings({Color? fillColor}) => MarkerSettings(
        isVisible: widget.expanded,
        height: widget.expanded ? 10.0 : 8.0,
        width: widget.expanded ? 10.0 : 8.0,
        color: fillColor,
        borderColor: fillColor,
      );
}
