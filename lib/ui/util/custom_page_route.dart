import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomPageRoute {
  static PageRoute<T> build<T>({required Widget Function(BuildContext) builder}) {
    if (Platform.isIOS) {
      return CupertinoPageRoute<T>(builder: builder);
    } else {
      return MaterialPageRoute<T>(builder: builder);
    }
  }
}
