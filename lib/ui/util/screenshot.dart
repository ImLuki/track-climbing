import 'dart:io';
import 'dart:ui' as ui;
import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:path_provider/path_provider.dart';

class ScreenshotController {
  late GlobalKey _containerKey;

  ScreenshotController() {
    _containerKey = GlobalKey();
  }

  Future<File> capture({
    String? fileName,
    double? pixelRatio,
    Duration delay = const Duration(milliseconds: 20),
  }) {
    return Future.delayed(delay, () async {
      try {
        ui.Image? image = await captureAsUiImage(
          delay: Duration.zero,
          pixelRatio: pixelRatio,
        );

        ByteData? byteData = await image?.toByteData(format: ui.ImageByteFormat.png);
        image?.dispose();

        Uint8List? pngBytes = byteData?.buffer.asUint8List();

        if (pngBytes == null) throw Exception();

        final directory = (await getApplicationDocumentsDirectory()).path;
        fileName ??= DateTime.now().toIso8601String();
        String path = '$directory/$fileName.png';

        File imgFile = File(path);
        await imgFile.writeAsBytes(pngBytes).then((onValue) {});
        return imgFile;
      } catch (exception) {
        throw (Exception);
      }
    });
  }

  Future<ui.Image?> captureAsUiImage({double? pixelRatio = 1, Duration delay = const Duration(milliseconds: 20)}) {
    return Future.delayed(delay, () async {
      try {
        var findRenderObject = _containerKey.currentContext?.findRenderObject();
        if (findRenderObject == null) {
          return null;
        }
        RenderRepaintBoundary boundary = findRenderObject as RenderRepaintBoundary;
        BuildContext? context = _containerKey.currentContext;
        if (pixelRatio == null) {
          if (context != null) pixelRatio = pixelRatio ?? MediaQuery.of(context).devicePixelRatio;
        }
        ui.Image image = await boundary.toImage(pixelRatio: pixelRatio ?? 1);
        return image;
      } on Exception {
        throw (Exception);
      }
    });
  }
}

class Screenshot extends StatefulWidget {
  final Widget? child;
  final ScreenshotController controller;

  const Screenshot({
    super.key,
    required this.child,
    required this.controller,
  });

  @override
  State<Screenshot> createState() {
    return ScreenshotState();
  }
}

class ScreenshotState extends State<Screenshot> with TickerProviderStateMixin {
  late ScreenshotController _controller;

  @override
  void initState() {
    super.initState();
    _controller = widget.controller;
  }

  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      key: _controller._containerKey,
      child: widget.child,
    );
  }
}
