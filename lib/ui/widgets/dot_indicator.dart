import 'package:flutter/material.dart';

class DotIndicator extends StatelessWidget {
  final double size;
  final Color color;
  final bool visible;

  const DotIndicator({super.key, this.size = 6.0, this.color = Colors.black, this.visible = true});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size,
      width: size,
      decoration: BoxDecoration(
        color: visible ? color : Colors.transparent,
        shape: BoxShape.circle,
      ),
    );
  }
}
