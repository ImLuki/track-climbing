import 'dart:math';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/backend/util/extensions.dart';
import 'package:flutter/material.dart';
import 'package:string_similarity/string_similarity.dart';

class AscendNameSuggestion extends StatefulWidget {
  final String location;
  final Ascend ascend;
  final Session? currentSession;
  final ValueChanged<String> onSelected;

  const AscendNameSuggestion({
    super.key,
    required this.location,
    required this.ascend,
    required this.onSelected,
    required this.currentSession,
  });

  @override
  State<AscendNameSuggestion> createState() => _AscendNameSuggestionState();
}

class _AscendNameSuggestionState extends State<AscendNameSuggestion> {
  static const int MAX_ITEM_AMOUNT = 4;
  static const String STRING_DIST_KEY = "string_dist_key";
  String previousName = "";

  List<Map<String, dynamic>> ascendNames = [];
  List<Map<String, dynamic>> queriedNames = [];
  late Function() ascendListener;
  late Function() sessionListener;

  @override
  void initState() {
    super.initState();

    ascendListener = () async {
      // If ascend name hasn't changed
      if (previousName == widget.ascend.name) return;

      _queryAscendNames();
      previousName = widget.ascend.name;
      if (mounted) setState(() {});
    };

    sessionListener = () {
      _loadAscendsNames();
    };

    widget.ascend.addListener(ascendListener);

    if (widget.currentSession != null) {
      widget.currentSession?.addListener(sessionListener);
    }

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _loadAscendsNames();
      if (mounted) _queryAscendNames();
    });
  }

  @override
  void dispose() {
    widget.ascend.removeListener(ascendListener);
    if (widget.currentSession != null) {
      widget.currentSession?.removeListener(sessionListener);
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (queriedNames.isEmpty) return Container();

    return Container(
      margin: const EdgeInsets.only(top: 8.0),
      padding: const EdgeInsets.symmetric(horizontal: 14.0),
      child: Column(
        children: List.generate(
          min(MAX_ITEM_AMOUNT, queriedNames.length),
          (index) => _nameRowEntry(queriedNames[index][DBController.ascendName]),
        ),
      ),
    );
  }

  /// Returns list of all previous ascend names from this location
  Future<List<Map<String, dynamic>>> _getAscendNamesByLocation(String location) async {
    if (location == "") return [];

    var result = await DBController().rawQuery(
      "SELECT ${DBController.ascendName}, ${DBController.sessionTimeStart} FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionsTable} using(${DBController.sessionId}) "
      "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
      "WHERE LOWER(${DBController.locationName}) == (?) "
      "GROUP BY ${DBController.ascendName} "
      "ORDER BY ${DBController.sessionTimeStart} DESC",
      [location.toLowerCase()],
    );

    if (result.isEmpty) return [];

    return List<Map<String, dynamic>>.generate(result.length, (index) => Map.of(result[index]));
  }

  void _loadAscendsNames() async {
    ascendNames = await _getAscendNamesByLocation(widget.location);
    if (mounted) setState(() {});
  }

  void _queryAscendNames() {
    final String query = widget.ascend.name.toLowerCase();
    if (query.isEmpty) {
      queriedNames = [];
      return;
    }

    queriedNames = ascendNames.where((element) => _checkSimilarity(element, query)).toList();
    queriedNames.sort((a, b) => b[STRING_DIST_KEY].compareTo(a[STRING_DIST_KEY]));
  }

  Widget _nameRowEntry(String ascendName) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(12),
      child: Material(
        child: InkWell(
          onTap: () {
            widget.onSelected(ascendName);
            queriedNames.clear();
            setState(() {});
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 6.0, horizontal: 6.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const Icon(Icons.query_builder, color: AppColors.accentColor2),
                const SizedBox(width: 12.0),
                Flexible(
                  fit: FlexFit.loose,
                  child: Text(
                    ascendName,
                    style: Theme.of(context).textTheme.bodyMedium,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool _checkSimilarity(Map<String, dynamic> element, String query) {
    final String ascendName = element[DBController.ascendName].toLowerCase();

    element[STRING_DIST_KEY] = max<double>(
      ascendName.prefix(query.length).similarityTo(query),
      ascendName.prefix(query.length + 1).similarityTo(query),
    );

    return element[STRING_DIST_KEY] > 0.4;
  }
}
