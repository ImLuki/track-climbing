import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums/ascend_characteristics.dart';
import 'package:flutter/material.dart';

class AscendStylePicker extends StatefulWidget {
  static const int elementsPerRow = 4;
  final Ascend ascend;

  const AscendStylePicker({super.key, required this.ascend});

  @override
  State<AscendStylePicker> createState() => _AscendStylePickerState();
}

class _AscendStylePickerState extends State<AscendStylePicker> {
  final List<AscendCharacteristics> ascendStyles = AscendCharacteristics.values;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Column(
        children: List.generate(
          ascendStyles.length ~/ AscendStylePicker.elementsPerRow,
          (i) => Row(
            children: List.generate(
              AscendStylePicker.elementsPerRow,
              (index) => _chipWidget(
                ascendStyle: ascendStyles[index + (i * AscendStylePicker.elementsPerRow)],
                ascend: widget.ascend,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _chipWidget({required AscendCharacteristics ascendStyle, required Ascend ascend}) {
    bool activated = AscendCharacteristics.hasFlag(ascendStyle, ascend.characteristic);

    Color activatedColor = Colors.black;
    return Expanded(
      child: Tooltip(
        message: ascendStyle.toTranslatedString(context),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 4.0),
          child: InkWell(
            borderRadius: BorderRadius.circular(14.0),
            onTap: () {
              ascend.characteristic ^= ascendStyle.value;
              setState(() {});
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 9.0, horizontal: 10.0),
              decoration: BoxDecoration(
                color: activated ? Theme.of(context).highlightColor : Colors.transparent,
                borderRadius: const BorderRadius.all(Radius.circular(14.0)),
              ),
              child: Column(
                children: [
                  Icon(ascendStyle.getIcon(), color: activated ? activatedColor : Colors.black, size: 22.0),
                  const SizedBox(height: 4.0),
                  Text(
                    ascendStyle.toTranslatedString(context),
                    style: activated
                        ? Theme.of(context).textTheme.bodySmall!.copyWith(color: activatedColor)
                        : Theme.of(context).textTheme.bodySmall,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
