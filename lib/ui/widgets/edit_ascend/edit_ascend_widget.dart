import 'dart:async';
import 'dart:math';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/enums/height_measurement.dart';
import 'package:climbing_track/backend/model/location.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:climbing_track/ui/dialog/info_dialogs.dart';
import 'package:climbing_track/ui/widgets/edit_ascend/ascend_style_picker.dart';
import 'package:climbing_track/ui/widgets/edit_ascend/scroll_picker/drop_down_grading.dart';
import 'package:climbing_track/ui/widgets/edit_ascend/scroll_picker/grade_scroll_picker.dart';
import 'package:climbing_track/ui/widgets/edit_ascend/steepness_picker.dart';
import 'package:climbing_track/ui/widgets/edit_ascend/style_picker.dart';
import 'package:climbing_track/ui/widgets/edit_ascend/type_picker.dart';
import 'package:climbing_track/ui/widgets/edit_ascend/ascend_name_suggestions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_picker_plus/flutter_picker_plus.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:toggle_switch/toggle_switch.dart';

class EditAscendWidget extends StatefulWidget {
  final Ascend ascend;
  final Session? currentSession;
  final bool addBottomPadding;

  const EditAscendWidget({
    super.key,
    required this.ascend,
    this.currentSession,
    this.addBottomPadding = true,
  });

  @override
  _EditAscendWidgetState createState() => _EditAscendWidgetState(ascend);
}

class _EditAscendWidgetState extends State<EditAscendWidget> {
  static const double SPEED_ROUTE_HEIGHT = 15.0;

  final FocusNode _commentTextFieldFocus = FocusNode();
  final FocusNode _nameTextFieldFocus = FocusNode();
  final InfoDialog _infoDialog = InfoDialog();
  final TextEditingController _ascendNameController = TextEditingController();
  final TextEditingController _commentController = TextEditingController();
  final Ascend _ascend;

  late Function() ascendListener;
  late StreamSubscription<bool> keyboardSubscription;

  _EditAscendWidgetState(this._ascend);

  @override
  void initState() {
    super.initState();

    // set current grading system to ascends grading system
    // !! check of grading system after null check, otherwise could let to null operation!
    if (_ascend.originalGradeSystem != null &&
        GradeManager()
            .getGradeMapper(type: _ascend.type)
            .getGradeSystems()
            .any((element) => element.toLowerCase() == _ascend.originalGradeSystem!.toLowerCase())) {
      GradeManager().getGradeMapper(type: _ascend.type).currentGradePickerSystem = _ascend.originalGradeSystem!;
    }

    _ascendNameController.text = _ascend.name;
    _commentController.text = _ascend.comment;

    ascendListener = () {
      // refresh page if ascend name is empty
      // All other cases are are handled within the text field
      // But this is triggered by the save button, which is out of scope for this widget
      if ((_ascend.name.isEmpty && _ascendNameController.text.isNotEmpty) ||
          _ascend.comment.isEmpty && _commentController.text.isNotEmpty) {
        _ascendNameController.text = _ascend.name;
        _commentController.text = _ascend.comment;
        if (mounted) setState(() {});
        FocusScope.of(context).unfocus();
      }
    };
    widget.ascend.addListener(ascendListener);

    // The following lines are a bit hacky
    // These will prevent the text fields from autofocussing, which is a current flutter bug
    _nameTextFieldFocus.canRequestFocus = false;
    _commentTextFieldFocus.canRequestFocus = false;

    // keyboard visibility listener
    var keyboardVisibilityController = KeyboardVisibilityController();
    keyboardSubscription = keyboardVisibilityController.onChange.listen((bool visible) {
      if (!visible) {
        _nameTextFieldFocus.canRequestFocus = false;
        _commentTextFieldFocus.canRequestFocus = false;
      }
    });
  }

  @override
  void dispose() {
    widget.ascend.removeListener(ascendListener);
    keyboardSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (tapDownDetails) {
        FocusScope.of(context).unfocus();
      },
      child: Padding(
        padding: const EdgeInsets.all(Constants.STANDARD_PADDING),
        child: Column(
          children: [
            ..._createTypeChooser(context),
            ..._createSpeedSpecificItems(),
            ..._createTopRopeToggle(context),
            ..._createDifficultyPicker(context),
            ..._createNameTextField(context),
            ..._createStylePicker(),
            ..._height(),
            ..._createRatingBar(),
            ..._createClimbingStylePicker(),
            ..._createSteepnessPicker(),
            ..._createCommentField(context, widget.ascend.comment),
            if (widget.addBottomPadding) const SizedBox(height: 220.0),
          ],
        ),
      ),
    );
  }

  List<Widget> _createTopRopeToggle(BuildContext context) {
    switch (_ascend.type) {
      case ClimbingType.SPEED_CLIMBING:
        if (_ascend.speedType == 0) return [];
        continue SPORT_CLIMBING;

      case ClimbingType.ICE_CLIMBING:
      case ClimbingType.MULTI_PITCH:
      case ClimbingType.TRAD_CLIMBING:
      SPORT_CLIMBING:
      case ClimbingType.SPORT_CLIMBING:
        return _topRopeToggle();
      case ClimbingType.BOULDER:
      case ClimbingType.FREE_SOLO:
      case ClimbingType.DEEP_WATER_SOLO:
        return [];
    }
  }

  List<Widget> _topRopeToggle() {
    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            AppLocalizations.of(context)!.ascend_edit_ascend_method,
            style: Theme.of(context).textTheme.bodyLarge,
          ),
          Padding(
            padding: const EdgeInsets.only(right: Constants.STANDARD_PADDING),
            child: InkWell(
              borderRadius: BorderRadius.circular(12.0),
              onTap: () => _infoDialog.showLeadTopRopeInfoDialog(
                context,
                shouldRenameTopropeToFollowing: _ascend.type.shouldRenameTopropeToFollowing(),
              ),
              child: const IconTheme(
                data: IconThemeData(color: AppColors.accentColor2),
                child: Icon(Icons.info_outline),
              ),
            ),
          ),
        ],
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      ToggleSwitch(
        initialLabelIndex: _ascend.toprope,
        minWidth: MediaQuery.of(context).size.width * 0.37,
        minHeight: 45.0,
        cornerRadius: 20.0,
        activeBgColor: const [Colors.green],
        activeFgColor: Colors.white,
        inactiveBgColor: AppColors.accentColor2,
        inactiveFgColor: Colors.white,
        fontSize: 16,
        labels: [
          AppLocalizations.of(context)!.style_lead.toUpperCase(),
          AppLocalizations.of(context)!.style_toprope.toUpperCase(),
        ],
        onToggle: (index) {
          _ascend.toprope = index ?? 0;
          setState(() {});
        },
        totalSwitches: 2,
      ),
      if (_ascend.type.shouldRenameTopropeToFollowing()) ...[
        const SizedBox(height: 4.0),
        _multiPitchLeadingFollowingSwitch(),
      ],
      const SizedBox(height: Constants.STANDARD_PADDING),
      const Divider(thickness: 2.0),
      const SizedBox(height: Constants.STANDARD_PADDING),
    ];
  }

  Widget _multiPitchLeadingFollowingSwitch() {
    return ToggleSwitch(
      initialLabelIndex: _ascend.toprope == 2 ? 0 : 1,
      minWidth: MediaQuery.of(context).size.width * 0.74,
      minHeight: 45.0,
      cornerRadius: 20.0,
      activeBgColor: const [Colors.green],
      activeFgColor: Colors.white,
      inactiveBgColor: AppColors.accentColor2,
      inactiveFgColor: Colors.white,
      fontSize: 16,
      labels: [AppLocalizations.of(context)!.style_alternate_leading.toUpperCase()],
      onToggle: (index) {
        _ascend.toprope = 2;
        setState(() {});
      },
      totalSwitches: 1,
    );
  }

  List<Widget> _createSpeedSpecificItems() {
    if (_ascend.type != ClimbingType.SPEED_CLIMBING) return [];
    double time = _ascend.speedTime / 1000;

    String timeString = "${time.toStringAsFixed(2)} ${AppLocalizations.of(context)!.util_seconds}";

    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Text(
              AppLocalizations.of(context)!.ascent_edit_comp_wall_or_normal,
              overflow: TextOverflow.clip,
              maxLines: 4,
              style: Theme.of(context).textTheme.bodyLarge,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: Constants.STANDARD_PADDING),
            child: InkWell(
              borderRadius: BorderRadius.circular(12.0),
              onTap: () => _infoDialog.showSpeedInfoDialog(context),
              child: const IconTheme(
                data: IconThemeData(color: AppColors.accentColor2),
                child: Icon(Icons.info_outline),
              ),
            ),
          ),
        ],
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      ToggleSwitch(
        initialLabelIndex: _ascend.speedType,
        minWidth: MediaQuery.of(context).size.width * 0.42,
        minHeight: 45.0,
        cornerRadius: 20.0,
        activeBgColor: const [Colors.green],
        activeFgColor: Colors.white,
        inactiveBgColor: AppColors.accentColor2,
        inactiveFgColor: Colors.white,
        fontSize: 16,
        labels: [
          AppLocalizations.of(context)!.ascent_edit_competition,
          AppLocalizations.of(context)!.ascent_edit_normal
        ],
        onToggle: (index) => setState(() {
          _ascend.speedType = index ?? 0;
          if (_ascend.speedType == 0) {
            _ascend.height = SPEED_ROUTE_HEIGHT;
          } else {
            Location? location;
            if (widget.currentSession != null) {
              location = DataStore().locationsDataStore.locationById(widget.currentSession!.locationId);
            }

            _ascend.height = location == null ? 0.0 : location.standardHeight;
          }
        }),
        totalSwitches: 2,
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      const Divider(thickness: 2.0),
      const SizedBox(height: Constants.STANDARD_PADDING),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.ascend_edit_your_time,
          overflow: TextOverflow.clip,
          maxLines: 1,
          style: Theme.of(context).textTheme.bodyLarge,
        ),
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      InkWell(
        borderRadius: const BorderRadius.all(
          Radius.circular(24.0),
        ),
        onTap: () => _showPickerNumber(context),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: Align(
            alignment: Alignment.center,
            child: Text(
              timeString,
              overflow: TextOverflow.clip,
              maxLines: 1,
              style: Theme.of(context).textTheme.bodyLarge,
            ),
          ),
        ),
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      const Divider(thickness: 2.0),
      const SizedBox(height: Constants.STANDARD_PADDING),
    ];
  }

  List<Widget> _createTypeChooser(BuildContext context) {
    const List<ClimbingType> labels = ClimbingType.values;

    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            AppLocalizations.of(context)!.ascend_edit_choose_type,
            style: Theme.of(context).textTheme.bodyLarge,
          ),
          Padding(
            padding: const EdgeInsets.only(right: Constants.STANDARD_PADDING),
            child: InkWell(
              borderRadius: BorderRadius.circular(12.0),
              onTap: () => _infoDialog.showClimbingTypeInfoDialog(context),
              child: const IconTheme(
                data: IconThemeData(color: AppColors.accentColor2),
                child: Icon(Icons.info_outline),
              ),
            ),
          ),
        ],
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      InkWell(
        borderRadius: const BorderRadius.all(Radius.circular(24.0)),
        onTap: () => TypePickerDialog(
          items: labels,
          value: _ascend.type,
          onChange: _onChangeClimbingType,
        ).showMyDialog(context),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20.0),
          child: Container(
            decoration: const BoxDecoration(
              color: AppColors.accentColor2,
            ),
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 13.0),
              constraints: BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 0.6),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(20.0)),
                color: Colors.amber[700]!.withOpacity(0.8),
              ),
              child: Text(
                _ascend.type.toName(context),
                style: Theme.of(context).textTheme.bodyLarge!.copyWith(color: Colors.white),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        ),
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      const Divider(thickness: 2.0),
      const SizedBox(height: Constants.STANDARD_PADDING),
    ];
  }

  List<Widget> _createNameTextField(BuildContext context) {
    if (_ascend.type == ClimbingType.SPEED_CLIMBING && _ascend.speedType == 0) return [];

    return [
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          _ascend.isBoulder()
              ? AppLocalizations.of(context)!.ascend_edit_boulder_name
              : AppLocalizations.of(context)!.ascend_edit_route_name,
          style: Theme.of(context).textTheme.bodyLarge,
        ),
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      Column(
        children: [
          Theme(
            data: ThemeData.from(
              colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor),
              useMaterial3: false,
            ),
            child: TextFormField(
              canRequestFocus: _nameTextFieldFocus.canRequestFocus,
              focusNode: _nameTextFieldFocus,
              onTap: () {
                _nameTextFieldFocus.canRequestFocus = true;
                setState(() {});
                _nameTextFieldFocus.requestFocus();
              },
              autofocus: false,
              controller: _ascendNameController,
              textAlign: TextAlign.start,
              textAlignVertical: TextAlignVertical.center,
              onChanged: (String value) => _ascend.name = value.trim(),
              style: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.normal,
              ),
              textCapitalization: TextCapitalization.sentences,
              decoration: InputDecoration(
                isDense: true,
                labelText: _ascend.isBoulder()
                    ? AppLocalizations.of(context)!.ascend_edit_enter_boulder_name
                    : AppLocalizations.of(context)!.ascend_edit_enter_route_name,
                labelStyle: const TextStyle(fontSize: 18, color: AppColors.accentColor2),
                fillColor: AppColors.accentColor,
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
                    borderSide: const BorderSide()),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
                  borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
                ),
              ),
            ),
          ),
          AscendNameSuggestion(
            location: _ascend.location ?? "",
            ascend: _ascend,
            onSelected: (value) {
              _ascendNameController.text = value.trim();
              _ascendNameController.selection = TextSelection.fromPosition(
                TextPosition(offset: _ascendNameController.text.length),
              );
              _ascend.name = value.trim();
            },
            currentSession: widget.currentSession,
          ),
        ],
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      const Divider(thickness: 2.0),
      const SizedBox(height: Constants.STANDARD_PADDING),
    ];
  }

  List<Widget> _createCommentField(BuildContext context, String initValue) {
    return [
      const SizedBox(height: Constants.STANDARD_PADDING),
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.general_comment(1),
          style: Theme.of(context).textTheme.bodyLarge,
        ),
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      Theme(
        data: ThemeData.from(
          colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor),
          useMaterial3: false,
        ),
        child: TextFormField(
          canRequestFocus: _commentTextFieldFocus.canRequestFocus,
          focusNode: _commentTextFieldFocus,
          onTap: () {
            _commentTextFieldFocus.canRequestFocus = true;
            setState(() {});
            _commentTextFieldFocus.requestFocus();
          },
          maxLines: 5,
          controller: _commentController,
          textAlign: TextAlign.start,
          textAlignVertical: TextAlignVertical.center,
          onChanged: (String value) => _ascend.comment = value.trim(),
          style: const TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
          textCapitalization: TextCapitalization.sentences,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            isDense: false,
            label: Text(
              AppLocalizations.of(context)!.ascend_edit_enter_comment,
              style: const TextStyle(fontSize: 18, color: AppColors.accentColor2, overflow: TextOverflow.ellipsis),
              maxLines: _commentController.text.isNotEmpty || _commentTextFieldFocus.hasFocus ? 1 : 2,
            ),
            fillColor: AppColors.accentColor,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
              borderSide: const BorderSide(),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: AppColors.accentColor, width: 2.0),
              borderRadius: BorderRadius.circular(Constants.STANDARD_BORDER_RADIUS),
            ),
          ),
        ),
      ),
    ];
  }

  List<Widget> _createDifficultyPicker(BuildContext context) {
    switch (_ascend.type) {
      case ClimbingType.BOULDER:
        return _difficultyPicker(
          Text(
            AppLocalizations.of(context)!.ascend_edit_boulder_grade,
            style: Theme.of(context).textTheme.bodyLarge,
          ),
          DropDownGrading(
            key: Key(_ascend.type.name),
            type: _ascend.type,
            callBack: () {
              _ascend.originalGradeSystem = GradeManager().getGradeMapper(type: _ascend.type).currentGradePickerSystem;
              setState(() {});
            },
          ),
        );
      case ClimbingType.ICE_CLIMBING:
        return _difficultyPicker(
          Text(
            AppLocalizations.of(context)!.ascend_edit_ice_grade,
            style: Theme.of(context).textTheme.bodyLarge,
          ),
          _iceGradingInfo(),
        );
      case ClimbingType.SPEED_CLIMBING:
        if (_ascend.speedType != 0) continue DEFAULT;
        return [];
      case ClimbingType.DEEP_WATER_SOLO:
      case ClimbingType.FREE_SOLO:
      case ClimbingType.MULTI_PITCH:
      case ClimbingType.TRAD_CLIMBING:
      DEFAULT:
      case ClimbingType.SPORT_CLIMBING:
        return _difficultyPicker(
          Text(
            AppLocalizations.of(context)!.ascend_edit_climbing_grade,
            style: Theme.of(context).textTheme.bodyLarge,
          ),
          DropDownGrading(
            key: Key(_ascend.type.name),
            type: _ascend.type,
            callBack: () {
              _ascend.originalGradeSystem = GradeManager().getGradeMapper(type: _ascend.type).currentGradePickerSystem;
              setState(() {});
            },
          ),
        );
    }
  }

  Widget _iceGradingInfo() {
    return InkWell(
      borderRadius: BorderRadius.circular(12.0),
      onTap: () => _infoDialog.showIceGradingInfo(context),
      child: const IconTheme(
        data: IconThemeData(color: AppColors.accentColor2),
        child: Icon(Icons.info_outline),
      ),
    );
  }

  List<Widget> _difficultyPicker(Widget title, Widget interactionWidget) {
    String grade = GradeManager().getGradeMapper(type: _ascend.type).roundedGradeFromId(
          _ascend.gradeID,
          gradingSystem: GradeManager().getGradeMapper(type: _ascend.type).currentGradePickerSystem.toLowerCase(),
        );

    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          title,
          Padding(
            padding: const EdgeInsets.only(right: Constants.STANDARD_PADDING),
            child: interactionWidget,
          )
        ],
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      Align(
        alignment: Alignment.centerLeft,
        child: SizedBox(
          height: 40,
          width: MediaQuery.of(context).size.width,
          child: ShaderMask(
            shaderCallback: (Rect rect) {
              return const LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: <Color>[Colors.black, Colors.transparent, Colors.transparent, Colors.black],
                stops: [0.001, 0.45, 0.55, 9.9],
              ).createShader(rect);
            },
            blendMode: BlendMode.dstOut,
            child: RotatedBox(
              quarterTurns: 3,
              child: GradeScrollPicker(
                key: UniqueKey(),
                initialValue: grade,
                onChanged: (String value) {
                  _ascend.gradeID = GradeManager().getGradeMapper(type: _ascend.type).idFromGrade(value) ??
                      GradeManager().getStandardGrade(_ascend.type);
                  _ascend.originalGradeSystem =
                      GradeManager().getGradeMapper(type: _ascend.type).currentGradePickerSystem;
                },
                ascend: _ascend,
              ),
            ),
          ),
        ),
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      const Divider(thickness: 2.0),
      const SizedBox(height: Constants.STANDARD_PADDING),
    ];
  }

  List<Widget> _createRatingBar() {
    return [
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          AppLocalizations.of(context)!.general_rating,
          style: Theme.of(context).textTheme.bodyLarge,
        ),
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      RatingBar(
        initialRating: _ascend.rating.toDouble(),
        minRating: 1,
        glow: false,
        direction: Axis.horizontal,
        allowHalfRating: false,
        itemCount: 5,
        itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
        onRatingUpdate: (double rating) {
          _ascend.rating = rating.toInt();
        },
        ratingWidget: RatingWidget(
          empty: Constants.EMPTY_STAR,
          full: Constants.FULL_STAR,
          half: Constants.FULL_STAR,
        ),
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      const Divider(thickness: 2.0),
    ];
  }

  List _createStylePicker() {
    if (_ascend.type == ClimbingType.SPEED_CLIMBING || _ascend.type == ClimbingType.FREE_SOLO) return [];

    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            AppLocalizations.of(context)!.ascend_edit_climbing_style,
            style: Theme.of(context).textTheme.bodyLarge,
          ),
          Padding(
            padding: const EdgeInsets.only(right: Constants.STANDARD_PADDING),
            child: InkWell(
              borderRadius: BorderRadius.circular(12.0),
              onTap: () => _infoDialog.showStyleInfoDialog(context, _ascend.type),
              child: const IconTheme(
                data: IconThemeData(color: AppColors.accentColor2),
                child: Icon(Icons.info_outline),
              ),
            ),
          ),
        ],
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      StylePicker(
        key: UniqueKey(), // Because otherwise the attempt value is not updated correctly
        onChangedStyle: (newValue) => _ascend.styleID = newValue,
        onChangedTries: (int value) => _ascend.tries = value,
        ascend: _ascend,
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      const Divider(thickness: 2.0),
      const SizedBox(height: Constants.STANDARD_PADDING),
    ];
  }

  List<Widget> _height() {
    if (_ascend.type == ClimbingType.SPEED_CLIMBING && _ascend.speedType == 0) return [];

    return [
      Align(
        alignment: Alignment.centerLeft,
        child: Text(
          _ascend.isBoulder()
              ? AppLocalizations.of(context)!.ascend_edit_boulder_length
              : AppLocalizations.of(context)!.ascend_edit_route_length,
          style: Theme.of(context).textTheme.bodyLarge,
        ),
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      Material(
        child: InkWell(
          borderRadius: BorderRadius.circular(24.0),
          onTap: () => _showHeightPicker(context),
          child: Padding(
            padding: const EdgeInsets.only(top: 10.0, bottom: 5.0),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    decoration: const BoxDecoration(
                      border: Border(
                        bottom: BorderSide(color: Colors.black26),
                      ),
                      color: Colors.transparent,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 2.0),
                      child: Text(
                        Util.getHeightString(_ascend.height),
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.bodyLarge,
                      ),
                    ),
                  ),
                  const SizedBox(width: 25),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3.0),
                    child: Text(
                      DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET
                          ? AppLocalizations.of(context)!.general_feet(Util.heightFromMeters(_ascend.height).round())
                          : AppLocalizations.of(context)!.general_meter(Util.heightFromMeters(_ascend.height).round()),
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      const SizedBox(height: Constants.STANDARD_PADDING),
      const Divider(thickness: 2.0),
      const SizedBox(height: Constants.STANDARD_PADDING),
    ];
  }

  void _showPickerNumber(BuildContext context) {
    int milliseconds = (_ascend.speedTime % 1000) ~/ 10;
    int seconds = _ascend.speedTime ~/ 1000 % 60;
    int minutes = _ascend.speedTime ~/ 1000 ~/ 60;

    Picker(
        cancelTextStyle: const TextStyle(
          color: AppColors.accentColor,
          fontSize: 18,
        ),
        confirmTextStyle: const TextStyle(
          color: AppColors.accentColor,
          fontSize: 18,
        ),
        adapter: NumberPickerAdapter(
          data: [
            const NumberPickerColumn(begin: 0, end: 999),
            NumberPickerColumn(begin: 0, end: 59, onFormatValue: (val) => val.toString().padLeft(2, "0")),
            NumberPickerColumn(
              begin: 0,
              end: 99,
              onFormatValue: (val) => val.toString().padLeft(2, "0").padRight(3, "0"),
            ),
          ],
        ),
        delimiter: [
          PickerDelimiter(
            column: 0,
            child: Container(
              color: Colors.white,
              child: const Padding(
                padding: EdgeInsets.only(left: 20.0),
                child: Align(
                  alignment: Alignment.center,
                  child: Text("min:", style: TextStyle(fontWeight: FontWeight.normal)),
                ),
              ),
            ),
          ),
          PickerDelimiter(
            column: 2,
            child: Container(
              color: Colors.white,
              child: const Padding(
                padding: EdgeInsets.only(left: 20.0),
                child: Align(
                  alignment: Alignment.center,
                  child: Text("s:", style: TextStyle(fontWeight: FontWeight.normal)),
                ),
              ),
            ),
          ),
          PickerDelimiter(
            column: 4,
            child: Container(
              color: Colors.white,
              child: const Padding(
                padding: EdgeInsets.only(left: 20.0),
                child: Align(
                  alignment: Alignment.center,
                  child: Text("ms:", style: TextStyle(fontWeight: FontWeight.normal)),
                ),
              ),
            ),
          ),
        ],
        selecteds: [minutes, seconds, milliseconds],
        hideHeader: false,
        onConfirm: (Picker picker, List value) {
          setState(() {
            int result = (value[0] * 1000 * 60) + (value[1] * 1000) + value[2] * 10;
            _ascend.speedTime = result;
          });
        }).showModal(context);
  }

  void _showHeightPicker(BuildContext context) {
    dynamic height = Util.heightFromMeters(_ascend.height);
    int meters = height.floor();
    int cm = ((height % 1) * 10).floor();

    if (DataStore().settingsDataStore.heightMeasurement == HeightMeasurement.FEET) {
      Picker(
          cancelTextStyle: const TextStyle(
            color: AppColors.accentColor,
            fontSize: 18,
          ),
          confirmTextStyle: const TextStyle(
            color: AppColors.accentColor,
            fontSize: 18,
          ),
          adapter: NumberPickerAdapter(
            data: [
              const NumberPickerColumn(begin: 0, end: 999),
            ],
          ),
          delimiter: [
            PickerDelimiter(
              column: 1,
              child: Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(left: 12.0, right: 20.0),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      AppLocalizations.of(context)!.general_feet(Util.heightFromMeters(_ascend.height)),
                      style: const TextStyle(fontWeight: FontWeight.normal),
                    ),
                  ),
                ),
              ),
            ),
          ],
          selecteds: [height],
          hideHeader: false,
          onConfirm: (Picker picker, List value) {
            setState(() {
              _ascend.height = Util.heightToMeters(value[0].toDouble());
            });
          }).showModal(context);
    } else {
      Picker(
          cancelTextStyle: const TextStyle(
            color: AppColors.accentColor,
            fontSize: 18,
          ),
          confirmTextStyle: const TextStyle(
            color: AppColors.accentColor,
            fontSize: 18,
          ),
          adapter: NumberPickerAdapter(
            data: [
              const NumberPickerColumn(begin: 0, end: 999),
              NumberPickerColumn(items: [0, 5], onFormatValue: (val) => val.toString().padRight(2, "0")),
            ],
          ),
          delimiter: [
            PickerDelimiter(
              column: 1,
              child: Container(
                color: Colors.white,
                child: const Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text("m", style: TextStyle(fontWeight: FontWeight.normal)),
                  ),
                ),
              ),
            ),
            PickerDelimiter(
              column: 3,
              child: Container(
                color: Colors.white,
                child: const Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text("cm", style: TextStyle(fontWeight: FontWeight.normal)),
                  ),
                ),
              ),
            ),
          ],
          selecteds: [meters, cm],
          hideHeader: false,
          onConfirm: (Picker picker, List value) {
            setState(() {
              double result = value[0] + (value[1] * 0.5);
              _ascend.height = Util.heightToMeters(result);
            });
          }).showModal(context);
    }
  }

  List<Widget> _createClimbingStylePicker() {
    if (_ascend.type == ClimbingType.SPEED_CLIMBING && _ascend.speedType == 0) return [];

    return [
      Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: ListTileTheme(
          contentPadding: EdgeInsets.zero,
          child: ExpansionTile(
            initiallyExpanded: DataStore().settingsDataStore.isRouteStylePickerExpanded,
            onExpansionChanged: (bool value) => DataStore().settingsDataStore.isRouteStylePickerExpanded = value,
            iconColor: Theme.of(context).unselectedWidgetColor,
            title: Text(
              AppLocalizations.of(context)!.ascend_edit_ascend_characteristic,
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            tilePadding: EdgeInsets.zero,
            childrenPadding: EdgeInsets.zero,
            children: [
              AscendStylePicker(ascend: _ascend),
            ],
          ),
        ),
      ),
      const Divider(thickness: 2.0),
    ];
  }

  List<Widget> _createSteepnessPicker() {
    if (_ascend.type == ClimbingType.SPEED_CLIMBING && _ascend.speedType == 0) return [];

    return [
      Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: ListTileTheme(
          contentPadding: EdgeInsets.zero,
          child: ExpansionTile(
            initiallyExpanded: DataStore().settingsDataStore.isSteepnessPickerExpanded,
            onExpansionChanged: (bool value) => DataStore().settingsDataStore.isSteepnessPickerExpanded = value,
            iconColor: Theme.of(context).unselectedWidgetColor,
            title: Text(
              AppLocalizations.of(context)!.steepness_title,
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            tilePadding: EdgeInsets.zero,
            childrenPadding: EdgeInsets.zero,
            children: [
              SteepnessPicker(ascend: _ascend),
            ],
          ),
        ),
      ),
      const Divider(thickness: 2.0),
    ];
  }

  void _onChangeClimbingType(ClimbingType climbingType) {
    ClimbingType climbingTypeBefore = _ascend.type;
    _ascend.type = climbingType;
    if (climbingTypeBefore == _ascend.type) return;

    // style
    if (!StyleController.getClimbingStyleIdsByClimbingType(_ascend.type).contains(_ascend.styleID)) {
      // onsight & flash -> flash (boulder)
      if ((_ascend.styleID == 2 || _ascend.styleID == 1) &&
          StyleController.getClimbingStyleIdsByClimbingType(_ascend.type).contains(100)) {
        _ascend.styleID = 100;

        // flash (boulder) -> flash (climbing)
      } else if (_ascend.styleID == 100 &&
          StyleController.getClimbingStyleIdsByClimbingType(_ascend.type).contains(2)) {
        _ascend.styleID = 2;
      } else {
        _ascend.styleID = StyleController.getDefaultClimbingStyle(_ascend.type).id;
      }
    }

    // height
    if (_ascend.type == ClimbingType.SPEED_CLIMBING) {
      _ascend.height = SPEED_ROUTE_HEIGHT;
      _ascend.characteristic = 0;
      _ascend.steepness = 0;
    }

    // Only multi pitch has 3 different roping styles
    if (_ascend.type != ClimbingType.MULTI_PITCH) {
      _ascend.toprope = _ascend.toprope >= 2 ? 0 : _ascend.toprope;
    }

    // grade
    // if new grade system is NOT equal to old grade system
    if (GradeManager().getGradeMapper(type: climbingTypeBefore) != GradeManager().getGradeMapper(type: _ascend.type)) {
      _ascend.gradeID = GradeManager().getStandardGrade(_ascend.type);
      _ascend.originalGradeSystem = GradeManager().getGradeMapper(type: _ascend.type).currentGradePickerSystem;
    }

    _ascend.speedTime = 0;
    _ascend.speedType = 0;

    _ascend.tries = max(_ascend.tries ?? 0, StyleController.getMinTriesValue(_ascend.styleID));

    setState(() {});
    FocusScope.of(context).unfocus();
  }
}
