import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:flutter/material.dart';

class DropDownGrading extends StatefulWidget {
  final ClimbingType type;
  final Function callBack;

  const DropDownGrading({super.key, required this.type, required this.callBack});

  @override
  _DropDownGradingState createState() => _DropDownGradingState();
}

class _DropDownGradingState extends State<DropDownGrading> {
  late List<String> systems;
  late String _value;

  @override
  void initState() {
    super.initState();
    systems = GradeManager().getGradeMapper(type: widget.type).getGradeSystems();
    _value = GradeManager().getGradeMapper(type: widget.type).currentGradePickerSystem;
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      borderRadius: BorderRadius.circular(20.0),
      underline: Container(),
      onChanged: (String? value) {
        if (value == null) return;
        setState(() {
          _value = value;
        });
        GradeManager().getGradeMapper(type: widget.type).currentGradePickerSystem = value;
        widget.callBack();
      },
      value: _value,
      isDense: true,
      style: Theme.of(context).textTheme.bodyMedium,
      items: List<DropdownMenuItem<String>>.generate(
        systems.length,
        (index) => DropdownMenuItem(
          value: systems[index],
          child: Text(systems[index]),
        ),
      ),
    );
  }
}
