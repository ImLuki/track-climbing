import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:flutter/material.dart';

class GradeScrollPicker extends StatefulWidget {
  const GradeScrollPicker({
    super.key,
    required this.initialValue,
    required this.onChanged,
    required this.ascend,
    this.showDivider = true,
  });

  // Events
  final ValueChanged<String> onChanged;
  final String initialValue;
  final bool showDivider;
  final Ascend ascend;

  @override
  State<GradeScrollPicker> createState() => _GradeScrollPickerState(selectedValue: initialValue);
}

class _GradeScrollPickerState extends State<GradeScrollPicker> {
  // Constants
  static const double itemWidth = 70.0;

  // Variables
  String selectedValue;
  late double widgetHeight;
  late ScrollController scrollController;
  late List<String> items;

  _GradeScrollPickerState({required this.selectedValue});

  @override
  void initState() {
    super.initState();
    items = GradeManager().getGradeMapper(type: widget.ascend.type).getPickerGrades();
    int initialItem = items.indexOf(selectedValue);
    scrollController = FixedExtentScrollController(initialItem: initialItem);
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData themeData = Theme.of(context);
    TextStyle defaultStyle = themeData.textTheme.bodyLarge!;
    TextStyle selectedStyle = themeData.textTheme.bodyLarge!.copyWith(fontWeight: FontWeight.bold);

    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        widgetHeight = constraints.maxHeight;

        return GestureDetector(
          onTapUp: _itemTapped,
          child: ListWheelScrollView.useDelegate(
            childDelegate: ListWheelChildBuilderDelegate(builder: (BuildContext context, int index) {
              if (index < 0 || index > items.length - 1) {
                return null;
              }

              var value = items[index];

              final TextStyle itemStyle = (value == selectedValue) ? selectedStyle : defaultStyle;

              return Center(
                child: RotatedBox(
                  quarterTurns: 1,
                  child: Text(
                    value,
                    style: itemStyle,
                    textAlign: TextAlign.center,
                  ),
                ),
              );
            }),
            controller: scrollController,
            itemExtent: itemWidth,
            onSelectedItemChanged: _onSelectedItemChanged,
          ),
        );
      },
    );
  }

  void _itemTapped(TapUpDetails details) {
    Offset position = details.localPosition;
    double center = widgetHeight / 2;
    double changeBy = position.dy - center;
    double newPosition = scrollController.offset + changeBy;

    // animate to and center on the selected item
    scrollController.animateTo(
      newPosition,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeInOut,
    );
  }

  void _onSelectedItemChanged(int index) {
    String newValue = items[index];
    if (newValue != selectedValue) {
      widget.onChanged(newValue);
      setState(() {
        selectedValue = newValue;
      });
    }
  }
}
