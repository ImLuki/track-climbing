import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:flutter/material.dart';

/// This helper widget manages the scrollable content inside a picker widget.
class ScrollPicker extends StatefulWidget {
  const ScrollPicker({
    super.key,
    required this.initialValue,
    required this.onChanged,
    required this.items,
    required this.itemFormatter,
  });

// Events
  final ValueChanged<ClimbingType> onChanged;

// Variables
  final ClimbingType initialValue;

  final List<ClimbingType> items;
  final String Function(BuildContext, ClimbingType) itemFormatter;

  @override
  _ScrollPickerState createState() => _ScrollPickerState(selectedValue: initialValue);
}

class _ScrollPickerState extends State<ScrollPicker> {
  // Constants
  static const double itemWidth = 60.0;

  // Variables
  double widgetHeight = 0.0;
  int numberOfVisibleItems = 0;
  int numberOfPaddingRows = 0;
  double visibleItemsHeight = 0.0;
  double offset = 0.0;

  ClimbingType selectedValue;

  late ScrollController scrollController;

  _ScrollPickerState({required this.selectedValue});

  @override
  void initState() {
    super.initState();

    int initialItem = widget.items.indexOf(selectedValue);
    scrollController = FixedExtentScrollController(initialItem: initialItem);
  }

  @override
  Widget build(BuildContext context) {
    TextStyle defaultStyle = const TextStyle(fontSize: 24, fontWeight: FontWeight.w200);
    TextStyle selectedStyle = defaultStyle.copyWith(fontWeight: FontWeight.w500);

    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        widgetHeight = constraints.maxHeight;

        return GestureDetector(
          onTapUp: _itemTapped,
          child: ListWheelScrollView.useDelegate(
            childDelegate: ListWheelChildBuilderDelegate(builder: (BuildContext context, int index) {
              if (index < 0 || index > widget.items.length - 1) {
                return null;
              }

              var value = widget.items[index];

              final TextStyle itemStyle = (value == selectedValue) ? selectedStyle : defaultStyle;

              return Center(
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: value == selectedValue ? Colors.white12 : Colors.transparent,
                  ),
                  child: Text(
                    widget.itemFormatter(context, value).replaceAll("", "\u{200B}"),
                    style: itemStyle,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                  ),
                ),
              );
            }),
            controller: scrollController,
            itemExtent: itemWidth,
            onSelectedItemChanged: _onSelectedItemChanged,
          ),
        );
      },
    );
  }

  void _itemTapped(TapUpDetails details) {
    Offset position = details.localPosition;
    double center = widgetHeight / 2;
    double changeBy = position.dy - center;
    double newPosition = scrollController.offset + changeBy;

    // animate to and center on the selected item
    scrollController.animateTo(
      newPosition,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeInOut,
    );
  }

  void _onSelectedItemChanged(int index) {
    var newValue = widget.items[index];
    if (newValue != selectedValue) {
      selectedValue = newValue;
      widget.onChanged.call(newValue);
      setState(() {});
    }
  }
}
