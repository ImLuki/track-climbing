import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums/steepness.dart';
import 'package:flutter/material.dart';

class SteepnessPicker extends StatefulWidget {
  static const int elementsPerRow = 4;
  final Ascend ascend;

  const SteepnessPicker({super.key, required this.ascend});

  @override
  State<SteepnessPicker> createState() => _SteepnessPickerState();
}

class _SteepnessPickerState extends State<SteepnessPicker> {
  final List<Steepness> steepness = Steepness.values;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Column(
        children: List.generate(
          steepness.length ~/ SteepnessPicker.elementsPerRow,
          (i) => Row(
            children: List.generate(
              SteepnessPicker.elementsPerRow,
              (index) => _chipWidget(
                steepness: steepness[index + (i * SteepnessPicker.elementsPerRow)],
                ascend: widget.ascend,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _chipWidget({required Steepness steepness, required Ascend ascend}) {
    bool activated = Steepness.hasFlag(steepness, ascend.steepness);

    Color activatedColor = Colors.black;
    return Expanded(
      child: Tooltip(
        message: steepness.toTranslatedString(context),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
          child: InkWell(
            borderRadius: BorderRadius.circular(14.0),
            onTap: () {
              ascend.steepness ^= steepness.value;
              setState(() {});
            },
            child: Container(
              height: 66.0,
              padding: const EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 4.0),
              decoration: BoxDecoration(
                color: activated ? Theme.of(context).highlightColor : Colors.transparent,
                borderRadius: const BorderRadius.all(Radius.circular(14.0)),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Icon(steepness.getIcon(), color: activated ? activatedColor : Colors.black, size: 30.0),
                  const SizedBox(height: 2),
                  const Spacer(),
                  Text(
                    steepness.toTranslatedString(context),
                    style: activated
                        ? Theme.of(context).textTheme.bodySmall!.copyWith(color: activatedColor, height: 0.0)
                        : Theme.of(context).textTheme.bodySmall!.copyWith(height: 0.0),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
