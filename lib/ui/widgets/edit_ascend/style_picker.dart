import 'dart:math';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:climbing_track/ui/dialog/info_dialogs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class StylePicker extends StatefulWidget {
  final ValueChanged<int> onChangedStyle;
  final ValueChanged<int> onChangedTries;
  final Ascend ascend;

  const StylePicker({
    super.key,
    required this.onChangedStyle,
    required this.onChangedTries,
    required this.ascend,
  });

  @override
  _StylePickerState createState() {
    if (ascend.tries == null) onChangedTries(StyleController.getMinTriesValue(ascend.styleID));
    return _StylePickerState(
      selectedStyle: ascend.styleID,
      tries: ascend.tries ?? StyleController.getMinTriesValue(ascend.styleID),
    );
  }
}

class _StylePickerState extends State<StylePicker> {
  int selectedStyle;
  int tries;
  List<Ascend> existingAscents = [];

  late Function() listener;
  late ClimbingType climbingType;

  @override
  void initState() {
    super.initState();
    climbingType = widget.ascend.type;

    listener = () async {
      existingAscents = await Util.findExistingAscents(widget.ascend);
      if (mounted) setState(() {});
    };
    widget.ascend.addListener(listener);

    WidgetsBinding.instance.addPostFrameCallback((_) => listener());
  }

  @override
  void dispose() {
    widget.ascend.removeListener(listener);
    super.dispose();
  }

  _StylePickerState({required this.selectedStyle, required this.tries});

  @override
  Widget build(BuildContext context) {
    // added this, because selectedCategory wasn't updated from outer
    if (climbingType != widget.ascend.type) {
      climbingType = widget.ascend.type;
      selectedStyle = widget.ascend.styleID;
    }
    return _createStylePicker(context);
  }

  Widget _createStylePicker(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 10.0),
      child: Column(
        children: <Widget>[
          Wrap(
            runSpacing: Constants.STANDARD_PADDING,
            spacing: Constants.STANDARD_PADDING,
            children: _createButtons(context),
          ),
          // 100 for boulder flash
          if (!ClimbingStyle.getById(selectedStyle).isFirstTry()) _createTriesPicker(context),
          if (_shouldShowExistingInfo()) ...[
            SizedBox(height: ClimbingStyle.getById(selectedStyle).isFirstTry() ? 20.0 : 8.0),
            _createExistingAscentsInfo(),
          ],
        ],
      ),
    );
  }

  List<Widget> _createButtons(BuildContext context) {
    List<ClimbingStyle> styles = StyleController.getClimbingStylesByClimbingType(climbingType);

    List<Widget> widgets = [];
    for (var style in styles) {
      widgets.add(
        InkWell(
          borderRadius: BorderRadius.circular(24.0),
          splashColor: Colors.transparent,
          onTap: () {
            selectedStyle = style.id;
            tries = _updateTries(tries, ClimbingStyle.getById(selectedStyle));
            setState(() {});
            widget.onChangedStyle(style.id);
            widget.onChangedTries(tries);
            FocusScope.of(context).unfocus();
          },
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 12.0),
            decoration: BoxDecoration(
              color: selectedStyle == style.id ? AppColors.accentColor : Colors.grey[300],
              borderRadius: const BorderRadius.all(Radius.circular(48.0)),
            ),
            child: Text(
              style.toTranslatedString(context),
              style: selectedStyle == style.id
                  ? Theme.of(context).textTheme.bodyMedium!.copyWith(color: Colors.white, fontSize: 16)
                  : Theme.of(context).textTheme.bodyMedium!.copyWith(fontSize: 16),
            ),
          ),
        ),
      );
    }
    return widgets;
  }

  Widget _createTriesPicker(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            borderRadius: BorderRadius.circular(20.0),
            onTap: () {
              tries = _updateTries(tries - 1, ClimbingStyle.getById(selectedStyle));
              setState(() {});
              widget.onChangedTries(tries);
              FocusScope.of(context).unfocus();
            },
            child: const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 6.0),
              child: Icon(Icons.remove),
            ),
          ),
          Flexible(
            child: Text(
              _getTriesString(context),
              style: Theme.of(context).textTheme.bodyLarge,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              textAlign: TextAlign.center,
            ),
          ),
          InkWell(
            borderRadius: BorderRadius.circular(20.0),
            onTap: () {
              tries = _updateTries(tries + 1, ClimbingStyle.getById(selectedStyle));
              setState(() {});
              widget.onChangedTries(tries);
              FocusScope.of(context).unfocus();
            },
            child: const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 6.0),
              child: Icon(Icons.add),
            ),
          ),
        ],
      ),
    );
  }

  String _getTriesString(BuildContext context) {
    return ClimbingStyle.getById(selectedStyle).getTriesString(tries, context);
  }

  Widget _createExistingAscentsInfo() {
    return Tooltip(
      message: AppLocalizations.of(context)!.existing_ascents_tooltip,
      child: InkWell(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onTap: () {
          if (existingAscents.isEmpty) return;
          InfoDialog().showExistingRoutesDialog(
            context: context,
            title: AppLocalizations.of(context)!.existing_ascents_list(existingAscents.length),
            existingAscents: existingAscents,
          );
        },
        onDoubleTap: () {
          if (!ClimbingStyle.getById(selectedStyle).hasAttempts()) return;
          tries = _updateTries(existingAscents.length + 1, ClimbingStyle.getById(selectedStyle));
          setState(() {});
          widget.onChangedTries(tries);
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              _getExistingAscentsInfoIcon(),
              const SizedBox(width: 4.0),
              Flexible(
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 8.0),
                  decoration: BoxDecoration(
                    color: Colors.orange.withOpacity(0.2),
                    borderRadius: const BorderRadius.all(Radius.circular(20)),
                  ),
                  child: RichText(
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                    text: TextSpan(
                      text: existingAscents.length.toString(),
                      style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
                      children: <TextSpan>[
                        TextSpan(
                          text: AppLocalizations.of(context)!
                              .previous_ascends_with_same_difficulty(existingAscents.length),
                          style: const TextStyle(fontWeight: FontWeight.normal),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _getExistingAscentsInfoIcon() {
    IconData iconData;
    Color color;

    if (ClimbingStyle.getById(selectedStyle).isFirstTry() &&
        existingAscents.isNotEmpty &&
        !ClimbingStyle.getById(selectedStyle).hasRepetitions()) {
      iconData = Icons.error_outline;
      color = Colors.red;
    } else if (tries == existingAscents.length + 1 && ClimbingStyle.getById(selectedStyle).hasAttempts()) {
      iconData = Icons.check_circle_outline;
      color = Colors.green;
    } else {
      iconData = Icons.info_outline;
      color = Colors.black54;
    }

    return Icon(iconData, size: 32, color: color);
  }

  bool _shouldShowExistingInfo() {
    return widget.ascend.name.isNotEmpty;
  }

  int _updateTries(int newTries, ClimbingStyle newClimbingStyle) {
    newTries = max(StyleController.getMinTriesValue(selectedStyle), newTries);
    switch (newClimbingStyle) {
      case ClimbingStyle.ONSIGHT:
      case ClimbingStyle.FLASH:
      case ClimbingStyle.FLASH_BOULDERING:
        return 1;
      case ClimbingStyle.REDPOINT:
      case ClimbingStyle.AF:
      case ClimbingStyle.HANGDOGGING:
      case ClimbingStyle.THREE_QUARTERS:
      case ClimbingStyle.HALF:
      case ClimbingStyle.ONE_QUARTER:
      case ClimbingStyle.AID:
      case ClimbingStyle.REPEAT:
      case ClimbingStyle.TOP:
      case ClimbingStyle.PROJECT:
      case ClimbingStyle.REPEAT_BOULDER:
        return newTries;
    }
  }
}
