import 'package:flutter/material.dart';

/// Class [FadeOutLeft]:
/// [key]: optional widget key reference
/// [child]: mandatory, widget to animate
/// [duration]: how much time the animation should take
/// [delay]: delay before the animation starts
/// [controller]: optional/mandatory, exposes the animation controller created by Animate_do
/// the controller can be use to repeat, reverse and anything you want, its just an animation controller
class FadeOut extends StatefulWidget {
  final Duration duration;
  final Duration delay;
  final double width;
  final double height;
  final Color color;

  const FadeOut({
    super.key,
    this.duration = const Duration(milliseconds: 800),
    this.delay = const Duration(milliseconds: 0),
    required this.width,
    required this.height,
    required this.color,
  });

  @override
  _FadeOutState createState() => _FadeOutState();
}

/// State class, where the magic happens
class _FadeOutState extends State<FadeOut> with SingleTickerProviderStateMixin {
  late double width;
  late Future delayedFuture;

  @override
  void initState() {
    super.initState();

    width = widget.width;

    delayedFuture = Future.delayed(
      widget.delay.inMilliseconds == 0.0 ? const Duration(milliseconds: 100) : widget.delay,
      () {
        if (context.mounted) {
          setState(() {
            width = 0;
          });
        }
      },
    );
  }

  @override
  void dispose() {
    delayedFuture.ignore();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: widget.color,
      child: AnimatedSize(
        duration: widget.duration,
        child: SizedBox(
          width: width,
          height: widget.height,
        ),
      ),
    );
  }
}
