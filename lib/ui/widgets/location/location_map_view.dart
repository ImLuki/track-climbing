import 'package:climbing_track/backend/model/location.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/widgets/location/zoombuttons_plugin_option.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:latlong2/latlong.dart';

class LocationMapView extends StatelessWidget {
  static const pointSize = 54.0;
  static const double minZoom = 3.0;
  static const double maxZoom = 17.0;
  final double initialZoom;
  final Location location;

  const LocationMapView({
    super.key,
    required this.location,
    required this.initialZoom,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          onPressed: () => Navigator.pop(context, false),
        ),
        title: Text(
          Util.replaceStringWithBlankSpaces(location.name.toUpperCase()),
        ),
      ),
      body: FlutterMap(
        mapController: MapController(),
        options: MapOptions(
          initialCenter: location.coordinates ?? const LatLng(50.5, 30.51),
          minZoom: minZoom,
          maxZoom: maxZoom,
          initialZoom: 12,
          interactionOptions: const InteractionOptions(
            flags: InteractiveFlag.pinchZoom |
                InteractiveFlag.drag |
                InteractiveFlag.flingAnimation |
                InteractiveFlag.doubleTapZoom,
          ),
        ),
        children: [
          TileLayer(
            urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
            userAgentPackageName: 'dev.fleaflet.flutter_map.example',
            errorTileCallback: (TileImage tile, _, StackTrace? error) {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    behavior: SnackBarBehavior.floating,
                    duration: const Duration(seconds: 2),
                    content: Text(
                      AppLocalizations.of(context)!.location_picker_map_warning_no_internet,
                    ),
                  ),
                );
              });
            },
          ),
          MarkerLayer(
            markers: [
              Marker(
                width: 54.0,
                height: 54.0,
                point: location.coordinates!,
                alignment: Alignment.topCenter,
                child: const Icon(
                  Icons.place,
                  size: 54,
                  color: Colors.red,
                ),
              ),
            ],
          ),
          FlutterMapZoomButtons(
            minZoom: minZoom,
            maxZoom: maxZoom,
            alignment: Alignment.bottomRight,
            tooltipZoomIn: AppLocalizations.of(context)!.location_picker_map_tooltip_zoom_in,
            tooltipZoomOut: AppLocalizations.of(context)!.location_picker_map_tooltip_zoom_out,
            zoomInColor: Colors.black,
            zoomInColorIcon: Colors.white,
            zoomOutColor: Colors.black,
            zoomOutColorIcon: Colors.white,
          ),
        ],
      ),
    );
  }
}
