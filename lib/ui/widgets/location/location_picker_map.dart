import 'dart:async';

import 'package:climbing_track/backend/handler/location_handler.dart';
import 'package:climbing_track/ui/widgets/location/zoombuttons_plugin_option.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_map_location_marker/flutter_map_location_marker.dart';

class LocationCoordinatesPickerMap extends StatefulWidget {
  final LatLng coordinates;
  final double initialZoom;

  const LocationCoordinatesPickerMap({super.key, required this.coordinates, this.initialZoom = 5});

  @override
  _LocationCoordinatesPickerMap createState() {
    return _LocationCoordinatesPickerMap();
  }
}

class _LocationCoordinatesPickerMap extends State<LocationCoordinatesPickerMap> {
  static const double minZoom = 3.0;
  static const double maxZoom = 17.0;

  late MapController mapController;
  late StreamSubscription mapEventSubscription;
  final pointSize = 54.0;
  double pointY = 0.0;
  bool _loadingPosition = false;

  late LatLng latLng;

  @override
  void initState() {
    super.initState();

    latLng = widget.coordinates;

    mapController = MapController();
    mapEventSubscription = mapController.mapEventStream.listen((mapEvent) => _onMapEvent(mapEvent, context));

    Future.delayed(Duration.zero, () {
      pointY = MediaQuery.of(context).size.height / 2 - 80;
    });
  }

  @override
  void dispose() {
    mapEventSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    bool needLoadingError = true;

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          _loadingPosition = true;
          setState(() {});
          try {
            Position position = await LocationHandler().determinePosition();
            LatLng newLatLng = LatLng(position.latitude, position.longitude);
            mapController.move(newLatLng, widget.initialZoom);
          } catch (_) {
            if (context.mounted) {
              ScaffoldMessenger.of(context)
                ..removeCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    behavior: SnackBarBehavior.floating,
                    content: Text(AppLocalizations.of(context)!.current_position_not_determined),
                  ),
                );
            }
          }
          _loadingPosition = false;
          setState(() {});
        },
        child: _loadingPosition ? const CircularProgressIndicator(value: 1.0) : const Icon(Icons.my_location),
      ),
      appBar: _appBar(),
      body: Stack(
        children: [
          FlutterMap(
            mapController: mapController,
            options: MapOptions(
              onMapReady: () => _updatePointLatLng(context),
              initialCenter: latLng,
              initialZoom: widget.initialZoom,
              minZoom: minZoom,
              maxZoom: maxZoom,
              onLongPress: (_, __) {},
              interactionOptions: const InteractionOptions(
                flags: InteractiveFlag.pinchZoom |
                    InteractiveFlag.drag |
                    InteractiveFlag.flingAnimation |
                    InteractiveFlag.doubleTapZoom,
              ),
              onPositionChanged: (MapCamera mapPosition, bool _) {
                needLoadingError = true;
              },
            ),
            children: [
              TileLayer(
                urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                userAgentPackageName: 'dev.fleaflet.flutter_map.example',
                errorTileCallback: (TileImage tile, _, StackTrace? error) {
                  if (needLoadingError) {
                    WidgetsBinding.instance.addPostFrameCallback((_) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          behavior: SnackBarBehavior.floating,
                          duration: const Duration(seconds: 2),
                          content: Text(
                            AppLocalizations.of(context)!.location_picker_map_warning_no_internet,
                          ),
                        ),
                      );
                    });
                    needLoadingError = false;
                  }
                },
              ),
              CurrentLocationLayer(),
              MarkerLayer(
                markers: [
                  Marker(
                    width: pointSize,
                    height: pointSize,
                    point: latLng,
                    alignment: Alignment.topCenter,
                    child: const Icon(
                      Icons.place,
                      size: 54,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
              FlutterMapZoomButtons(
                minZoom: minZoom,
                maxZoom: maxZoom,
                mini: false,
                padding: 14,
                alignment: Alignment.topLeft,
                tooltipZoomIn: AppLocalizations.of(context)!.location_picker_map_tooltip_zoom_in,
                tooltipZoomOut: AppLocalizations.of(context)!.location_picker_map_tooltip_zoom_out,
                zoomInColor: Colors.black,
                zoomInColorIcon: Colors.white,
                zoomOutColor: Colors.black,
                zoomOutColorIcon: Colors.white,
              ),
              const Scalebar(),
            ],
          ),
        ],
      ),
    );
  }

  PreferredSizeWidget _appBar() {
    return AppBar(
      title: Text(
        AppLocalizations.of(context)!.location_picker_map_choose_coordinates,
      ),
      leading: IconButton(
        icon: const Icon(Icons.close),
        tooltip: AppLocalizations.of(context)!.general_cancel,
        onPressed: () => {
          ScaffoldMessenger.of(context).removeCurrentSnackBar(),
          Navigator.of(context).pop(null),
        },
      ),
      actions: [
        IconButton(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          tooltip: AppLocalizations.of(context)!.general_save,
          icon: const Icon(Icons.done),
          onPressed: () => {
            ScaffoldMessenger.of(context).removeCurrentSnackBar(),
            Navigator.of(context).pop(latLng),
          },
        )
      ],
    );
  }

  void _onMapEvent(MapEvent mapEvent, BuildContext context) {
    _updatePointLatLng(context);
  }

  void _updatePointLatLng(context) {
    final LatLng latLng = mapController.camera.center;

    setState(() {
      this.latLng = latLng;
    });
  }
}
