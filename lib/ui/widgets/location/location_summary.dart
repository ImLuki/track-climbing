import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/location.dart';
import 'package:climbing_track/backend/util/custom_date_formatter.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/widgets/location/map_container_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class CragView extends StatelessWidget {
  static const double initialZoomLevel = 12.0;
  final Location location;

  const CragView({super.key, required this.location});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          onPressed: () => Navigator.pop(context, false),
        ),
        title: Text(
          Util.replaceStringWithBlankSpaces(location.name.toUpperCase()),
        ),
      ),
      body: FutureBuilder(
        future: _futureGetData(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: LinearProgressIndicator(),
            );
          }
          return SingleChildScrollView(child: _body(context, snapshot.data));
        },
      ),
    );
  }

  Widget _body(BuildContext context, List<Map<String, dynamic>> data) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (location.coordinates != null) ...[
          MapContainerView(
            initialZoomLevel: initialZoomLevel,
            location: location,
            height: 200.0,
          ),
          const Divider(height: 2.0, thickness: 2.0),
          _coordinatesWidget(context),
          const Divider(),
        ],
        if (location.coordinates == null) const SizedBox(height: 8.0),
        _lastVisited(context, data),
        const Divider(),
        ..._chartWidget(context, data),
        const Divider(),
        _countSessionsAndAscents(context, data),
        if (location.locationComment != "") ..._commentWidget(context),
        const SizedBox(height: 60.0),
      ],
    );
  }

  List<Widget> _commentWidget(BuildContext context) {
    return [
      const Divider(),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              AppLocalizations.of(context)!.crag_view_info,
              style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 4.0),
            Text(location.locationComment)
          ],
        ),
      ),
    ];
  }

  Widget _countSessionsAndAscents(BuildContext context, List<Map<String, dynamic>> data) {
    int sessionCount = data.map((e) => e[DBController.sessionId]).toSet().length;
    int ascentCount = data.map((e) => e[DBController.ascendId]).toSet().length;

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              Text(
                AppLocalizations.of(context)!.crag_view_number_sessions,
                style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
              Text(
                sessionCount.toString(),
                style: const TextStyle(fontSize: 16.0),
              ),
            ],
          ),
          Column(
            children: [
              Text(
                AppLocalizations.of(context)!.crag_view_number_ascents,
                style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
              Text(
                ascentCount.toString(),
                style: const TextStyle(fontSize: 16.0),
              ),
            ],
          )
        ],
      ),
    );
  }

  List<Widget> _chartWidget(BuildContext context, List<Map<String, dynamic>> data) {
    List<int> chartDataLast12Months = List.generate(12, (index) => 0);
    List<int> chartDataLast = List.generate(12, (index) => 0);

    DateTime now = DateTime.now();
    DateTime dateTime = DateTime(now.year - 1, now.month + 1, 0, 0, 0, 0, 0, 0);

    for (Map<String, dynamic> ascend in data) {
      DateTime tmp = DateTime.parse(ascend[DBController.sessionTimeStart]);
      if (tmp.isAfter(dateTime)) {
        chartDataLast12Months[tmp.month - 1] += 1;
      }
      chartDataLast[tmp.month - 1] += 1;
    }

    bool lastYear = chartDataLast12Months.where((element) => element > 0).length > 3;

    return [
      const SizedBox(height: 6.0),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Text(
          lastYear
              ? AppLocalizations.of(context)!.crag_view_ascents_last_year
              : AppLocalizations.of(context)!.crag_view_ascents_overall,
          style: const TextStyle(fontSize: 16.0),
        ),
      ),
      SizedBox(
        height: 180,
        child: SfCartesianChart(
          plotAreaBorderWidth: 0.0,
          isTransposed: true,
          primaryYAxis: const NumericAxis(isVisible: false),
          primaryXAxis: CategoryAxis(
            majorGridLines: const MajorGridLines(width: 0),
            axisLabelFormatter: (AxisLabelRenderDetails details) => ChartAxisLabel(
              DateFormat.MMM().format(DateTime(0, details.value.toInt() + 1)),
              details.textStyle,
            ),
          ),
          series: <CartesianSeries>[
            BarSeries<int, int>(
              spacing: 0.0,
              dataSource: lastYear ? chartDataLast12Months : chartDataLast,
              xValueMapper: (int data, int index) => index,
              yValueMapper: (int data, _) => data,
              borderRadius: const BorderRadius.only(topLeft: Radius.circular(4), topRight: Radius.circular(4)),
              dataLabelSettings: const DataLabelSettings(
                isVisible: true,
                labelPosition: ChartDataLabelPosition.outside,
                labelAlignment: ChartDataLabelAlignment.outer,
                showZeroValue: false,
              ),
            ),
          ],
        ),
      ),
    ];
  }

  Widget _coordinatesWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 30.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Icon(Icons.location_on_outlined, size: 36.0),
          const SizedBox(width: 6.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                location.coordinates!.latitude.toString(),
                style: const TextStyle(fontWeight: FontWeight.w600, color: Colors.black54, fontSize: 16.0),
              ),
              const SizedBox(height: 2.0),
              Text(
                location.coordinates!.longitude.toString(),
                style: const TextStyle(fontWeight: FontWeight.w600, color: Colors.black54, fontSize: 16.0),
              ),
            ],
          ),
          const Spacer(),
          Column(
            children: [
              CircleAvatar(
                backgroundColor: Colors.black12,
                child: IconButton(
                  onPressed: () => _showLocationOnMap(context),
                  icon: const Icon(Icons.navigation_outlined),
                ),
              ),
              const SizedBox(height: 4.0),
              Text(
                AppLocalizations.of(context)!.crag_view_navigate,
                style: const TextStyle(fontWeight: FontWeight.w600, color: Colors.black54),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _lastVisited(BuildContext context, List<Map<String, dynamic>> data) {
    List<DateTime> dateTimes = data.map((e) => DateTime.parse(e[DBController.sessionTimeStart])).toSet().toList();
    dateTimes.sort();
    DateTime lastDateTime = dateTimes.last;
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const Icon(Icons.history, size: 32.0),
          const SizedBox(width: 6.0),
          Text(
            AppLocalizations.of(context)!.crag_view_last_visited,
            style: const TextStyle(fontSize: 16.0),
          ),
          const Spacer(),
          Text(
            CustomDateFormatter.formatDateString(context, lastDateTime),
            style: const TextStyle(fontWeight: FontWeight.w600, color: Colors.black54, fontSize: 16.0),
          ),
        ],
      ),
    );
  }

  void _showLocationOnMap(BuildContext context) async {
    if (location.coordinates == null) {
      if (context.mounted) {
        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              content: Text(AppLocalizations.of(context)!.session_summary_no_coordinates),
              duration: const Duration(seconds: 2),
            ),
          );
      }
    } else {
      MapsLauncher.launchCoordinates(
        location.coordinates!.latitude,
        location.coordinates!.longitude,
        location.name,
      );
    }
  }

  Future<List<Map<String, dynamic>>> _futureGetData() async {
    return DBController().rawQuery(
      "SELECT ${DBController.ascendId}, ${DBController.sessionTimeStart}, ${DBController.sessionId} "
      "FROM ${DBController.ascendsTable} "
      "JOIN ${DBController.sessionsTable} using (${DBController.sessionId}) "
      "WHERE ${DBController.locationId} == (?)",
      [location.locationId],
    );
  }
}
