import 'package:climbing_track/backend/model/location.dart';
import 'package:climbing_track/ui/util/custom_page_route.dart';
import 'package:climbing_track/ui/widgets/location/location_map_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:latlong2/latlong.dart';

class MapContainerView extends StatelessWidget {
  final double initialZoomLevel;
  final Location location;
  final double height;

  const MapContainerView({super.key, required this.initialZoomLevel, required this.location, required this.height});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      child: FlutterMap(
        mapController: MapController(),
        options: MapOptions(
          initialCenter: location.coordinates ?? const LatLng(50.5, 30.51),
          initialZoom: initialZoomLevel,
          interactionOptions: const InteractionOptions(
            flags: InteractiveFlag.none,
          ),
          onTap: (_, __) => _pushMapView(context),
        ),
        children: [
          TileLayer(
            urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
            userAgentPackageName: 'dev.fleaflet.flutter_map.example',
            errorTileCallback: (TileImage tile, _, StackTrace? error) {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    behavior: SnackBarBehavior.floating,
                    duration: const Duration(seconds: 2),
                    content: Text(
                      AppLocalizations.of(context)!.location_picker_map_warning_no_internet,
                    ),
                  ),
                );
              });
            },
          ),
          if (location.coordinates != null)
            MarkerLayer(
              markers: [
                Marker(
                  width: 54.0,
                  height: 54.0,
                  point: location.coordinates!,
                  alignment: Alignment.topCenter,
                  child: const Icon(
                    Icons.place,
                    size: 54,
                    color: Colors.red,
                  ),
                ),
              ],
            ),
        ],
      ),
    );
  }

  void _pushMapView(BuildContext context) {
    Navigator.push(
      context,
      CustomPageRoute.build<bool>(
        builder: (BuildContext context) => LocationMapView(
          location: location,
          initialZoom: initialZoomLevel,
        ),
      ),
    );
  }
}
