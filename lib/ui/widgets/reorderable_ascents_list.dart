import 'dart:math';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/location.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/ui/pages/ascends/ascend_edit_page.dart';
import 'package:climbing_track/ui/util/custom_page_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ReorderableAscentsList extends StatefulWidget {
  final Function(Ascend ascend)? onDeleteLast;
  final bool Function(Ascend ascend)? onDelete;
  final Function(int index, Ascend ascend)? onReAdd;
  final Function(int oldIndex, int newIndex)? onReorder;
  final Function(int index, Ascend ascend)? onEditedAscends;
  final Function(Ascend ascend)? onNewAscendAdded;
  final List<Ascend> data;
  final Session session;
  final bool showEditIcon;
  final bool showNewAscendAddButton;

  const ReorderableAscentsList({
    super.key,
    required this.data,
    required this.session,
    this.showEditIcon = false,
    this.showNewAscendAddButton = false,
    this.onDelete,
    this.onReAdd,
    this.onReorder,
    this.onEditedAscends,
    this.onDeleteLast,
    this.onNewAscendAdded,
  });

  @override
  State<ReorderableAscentsList> createState() => _ReorderableAscentsListState();
}

class _ReorderableAscentsListState extends State<ReorderableAscentsList> {
  final GlobalKey _globalKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _buildList(widget.data),
        if (widget.showNewAscendAddButton)
          ..._addButton(
            widget.data.isEmpty ? 0 : widget.data.map((e) => e.orderKey ?? 0).reduce(max),
          ),
      ],
    );
  }

  List<Widget> _addButton(int maxOrderId) {
    return [
      const SizedBox(height: 8.0),
      ElevatedButton(
        key: _globalKey,
        onPressed: () => _addAscend(maxOrderId),
        style: ElevatedButton.styleFrom(foregroundColor: Colors.black),
        child: Row(
          children: [
            const Icon(Icons.add),
            const SizedBox(width: 4.0),
            Text(AppLocalizations.of(context)!.session_edit_page_add_ascend),
          ],
        ),
      ),
    ];
  }

  Widget _buildList(List<Ascend> ascents) {
    ascents.sort((Ascend a, Ascend b) => (a.orderKey ?? -1).compareTo(b.orderKey ?? -1));
    List<Widget> items = List.generate(
      ascents.length,
      (index) => _buildItem(
        ascents,
        index,
        ascents[index],
        ascents.length,
      ),
    );

    return ReorderableListView(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      buildDefaultDragHandles: false,
      children: items,
      onReorder: (int oldIndex, int newIndex) {
        ScaffoldMessenger.of(context).removeCurrentSnackBar();
        Ascend tmp = ascents[oldIndex];

        if (oldIndex == newIndex) return;
        if (newIndex < oldIndex) {
          int tmpId = ascents[newIndex].orderKey ?? ascents[newIndex].ascendId;
          int currentId = ascents[oldIndex].orderKey ?? ascents[oldIndex].ascendId;
          for (int i = oldIndex; i > newIndex; i--) {
            int orderId = currentId;
            currentId = ascents[i - 1].orderKey ?? ascents[i - 1].ascendId;
            ascents[i] = ascents[i - 1];
            ascents[i].orderKey = orderId;
          }
          ascents[newIndex] = tmp;
          ascents[newIndex].orderKey = tmpId;
        } else {
          int tmpId = ascents[newIndex - 1].orderKey ?? ascents[newIndex - 1].ascendId;
          int currentId = ascents[oldIndex].orderKey ?? ascents[oldIndex].ascendId;
          for (int i = oldIndex; i < newIndex - 1; i++) {
            int orderKey = currentId;
            currentId = ascents[i + 1].orderKey ?? ascents[i + 1].ascendId;
            ascents[i] = ascents[i + 1];
            ascents[i].orderKey = orderKey;
          }
          ascents[newIndex - 1] = tmp;
          ascents[newIndex - 1].orderKey = tmpId;
        }
        widget.onReorder?.call(oldIndex, newIndex);
        setState(() {});
      },
    );
  }

  Widget _buildItem(List ascents, int index, Ascend ascend, int ascendCount) {
    return Padding(
      key: ValueKey(index),
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          ReorderableDragStartListener(
            index: index,
            child: Container(
              color: Colors.transparent,
              width: 40,
              height: 36,
              alignment: Alignment.centerLeft,
              child: const Icon(Icons.drag_indicator),
            ),
          ),
          _buildAscendInfo(ascend),
          const SizedBox(width: 6.0),
          if (widget.showEditIcon) _buildEditIcon(ascents, ascend, index),
          _buildDeleteIcon(ascents, ascend, index),
          _buildMarkedIcon(ascend),
          _buildReminderIcon(ascend),
        ],
      ),
    );
  }

  Widget _buildAscendInfo(Ascend ascend) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            Util.getAscendName(
              context,
              ascend.name,
              type: ascend.type,
              speedType: ascend.speedType,
            ),
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0.0,
                  wordSpacing: 0.0,
                  height: 0,
                ),
          ),
          Text(
            Util.getAscendSummary(ascend, context),
            style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  letterSpacing: 0.0,
                  wordSpacing: 0.0,
                  height: 0,
                ),
          ),
        ],
      ),
    );
  }

  Widget _buildEditIcon(List ascents, Ascend ascend, int index) {
    return IconButton(
      padding: EdgeInsets.zero,
      splashRadius: 24,
      visualDensity: VisualDensity.compact,
      onPressed: () => _editAscend(ascend, index),
      tooltip: AppLocalizations.of(context)!.edit_ascend_tooltip,
      icon: const Icon(Icons.edit, color: AppColors.accentColor2),
    );
  }

  Widget _buildReminderIcon(Ascend ascend) {
    bool hasReminder = DataStore().ascendReminderDataStore.hasReminder(ascendId: ascend.ascendId);
    return IconButton(
      padding: EdgeInsets.zero,
      splashRadius: 24,
      visualDensity: VisualDensity.compact,
      onPressed: () => _toggleReminder(ascend),
      tooltip: AppLocalizations.of(context)!.ascend_reminder_tooltip,
      icon: Icon(
        hasReminder ? Icons.notifications_active : Icons.notifications_none,
        color: hasReminder ? Colors.amber.shade700 : AppColors.accentColor2,
      ),
    );
  }

  Widget _buildDeleteIcon(List ascents, Ascend ascend, int index) {
    return IconButton(
      padding: EdgeInsets.zero,
      splashRadius: 24,
      visualDensity: VisualDensity.compact,
      onPressed: () => _deleteAscend(ascents, ascend, index),
      tooltip: AppLocalizations.of(context)!.delete_ascend_tooltip,
      icon: const Icon(Icons.delete, color: AppColors.accentColor2),
    );
  }

  Widget _buildMarkedIcon(Ascend ascend) {
    return IconButton(
      splashRadius: 24,
      padding: EdgeInsets.zero,
      visualDensity: VisualDensity.compact,
      onPressed: () => _toggleMarked(ascend),
      tooltip: AppLocalizations.of(context)!.mark_ascend_tooltip,
      icon: Icon(
        ascend.marked ? Icons.favorite : Icons.favorite_border,
        color: ascend.marked ? Colors.red : AppColors.accentColor2,
      ),
    );
  }

  void _toggleMarked(Ascend ascend) {
    final DBController dbController = DBController();
    ascend.marked = !ascend.marked;
    dbController.updateAscend(
      {DBController.ascendMarked: ascend.marked ? 1 : 0},
      ascend.ascendId,
    ).then(
      (value) {
        setState(() {});
        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              content: Text(
                ascend.marked
                    ? AppLocalizations.of(context)!.dismissible_route_mark
                    : AppLocalizations.of(context)!.dismissible_route_un_mark,
              ),
            ),
          );
      },
    );
  }

  void _toggleReminder(Ascend ascend) {
    DataStore().ascendReminderDataStore.toggleReminder(ascend: ascend);

    setState(() {});
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(
            DataStore().ascendReminderDataStore.hasReminder(ascendId: ascend.ascendId)
                ? AppLocalizations.of(context)!.ascend_reminder_add_reminder
                : AppLocalizations.of(context)!.ascend_reminder_remove_reminder,
          ),
        ),
      );
  }

  void _deleteAscend(List ascents, Ascend ascend, int index) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    if (ascents.length <= 1 && widget.onDeleteLast != null) {
      widget.onDeleteLast?.call(ascend);
      return;
    }

    if (widget.onDelete?.call(ascend) ?? true) {
      widget.data.remove(ascend);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(
            AppLocalizations.of(context)!.dismissible_route_list_deleted(
              Util.getAscendName(
                context,
                ascend.name,
                type: ascend.type,
                speedType: ascend.speedType,
              ),
            ),
          ),
          action: SnackBarAction(
            textColor: Colors.lightBlue,
            label: AppLocalizations.of(context)!.dismissible_route_list_undo,
            onPressed: () {
              widget.data.insert(index, ascend);
              widget.onReAdd?.call(index, ascend);
              if (mounted) setState(() {});
            },
          ),
        ),
      );
      setState(() {});
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(AppLocalizations.of(context)!.dismissible_route_could_not_delete),
        ),
      );
    }
  }

  void _editAscend(Ascend ascend, int index) {
    Navigator.push(
      context,
      CustomPageRoute.build<bool>(
        builder: (BuildContext context) => AscendEditPage(
          ascend: ascend,
          title: "${AppLocalizations.of(context)!.general_edit} ${ascend.type.toTitle(context)}",
          onChanged: (Ascend newAscend) => widget.onEditedAscends?.call(index, newAscend),
          sessionID: widget.session.sessionId!,
          updateDatabase: false,
          hasSaveButton: false,
        ),
      ),
    ).then((value) => setState(() {}));
  }

  Future<void> _addAscend(int maxOrderId) async {
    Location location = DataStore().locationsDataStore.locationById(widget.session.locationId);
    Ascend ascend = Ascend.getDefault(
      startDate: widget.session.startDate,
      locationName: location.name,
      standardHeight: location.standardHeight,
    );
    bool? result = await Navigator.push(
      context,
      CustomPageRoute.build<bool>(
        builder: (BuildContext context) => AscendEditPage(
          ascend: ascend,
          title: AppLocalizations.of(context)!.session_edit_page_add_ascend,
          sessionID: widget.session.sessionId!,
          updateDatabase: false,
          hasSaveButton: false,
          hasAppBarSaveButton: true,
        ),
      ),
    );

    if (result != null && result) {
      ascend.orderKey = maxOrderId + 1;
      widget.data.add(ascend);
      await Future.delayed(const Duration(milliseconds: 200));
      if (mounted) setState(() {});
    }
  }
}
