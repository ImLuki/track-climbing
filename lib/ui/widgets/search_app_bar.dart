import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SearchAppBar extends StatefulWidget {
  final String title;
  final String hintText;
  final Function() onPressedSettings;

  final Function(String) onSearchStringChanged;

  const SearchAppBar({
    super.key,
    required this.title,
    required this.hintText,
    required this.onPressedSettings,
    required this.onSearchStringChanged,
  });

  @override
  State<SearchAppBar> createState() => _SearchAppBarState();
}

class _SearchAppBarState extends State<SearchAppBar> {
  bool searchBarVisibility = false;
  final TextEditingController textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: searchBarVisibility
              ? _buildFloatingSearchBar()
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Text(
                        Util.replaceStringWithBlankSpaces(widget.title),
                        textAlign: TextAlign.start,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    IconButton(
                      tooltip: AppLocalizations.of(context)!.general_search,
                      icon: const Icon(
                        Icons.search,
                        size: 30,
                      ),
                      onPressed: () {
                        setState(() => searchBarVisibility = true);
                      },
                    ),
                  ],
                ),
        ),
        Padding(
          padding: EdgeInsets.zero,
          child: IconButton(
            tooltip: AppLocalizations.of(context)!.ascent_page_ascent_settings,
            icon: const Icon(
              Icons.tune,
              size: 30,
            ),
            onPressed: widget.onPressedSettings,
          ),
        ),
      ],
    );
  }

  Widget _buildFloatingSearchBar() {
    return Container(
      height: AppBar().preferredSize.height * 0.70,
      padding: const EdgeInsets.only(right: 16.0),
      child: Theme(
        data: ThemeData.from(
          colorScheme: ColorScheme.fromSeed(seedColor: AppColors.accentColor),
          useMaterial3: false,
        ),
        child: TextFormField(
          autofocus: true,
          controller: textController,
          onChanged: widget.onSearchStringChanged,
          textAlign: TextAlign.left,
          textAlignVertical: TextAlignVertical.center,
          textCapitalization: TextCapitalization.sentences,
          maxLines: 1,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.zero,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(12.0),
              borderSide: BorderSide.none,
            ),
            filled: true,
            prefixIcon: InkWell(
              borderRadius: BorderRadius.circular(12.0),
              onTap: () => {
                setState(() {
                  searchBarVisibility = false;
                  textController.clear();
                  widget.onSearchStringChanged(textController.text);
                })
              },
              child: const Icon(Icons.arrow_back, color: Colors.black),
            ),
            suffixIcon: InkWell(
              borderRadius: BorderRadius.circular(12.0),
              onTap: () => setState(
                () {
                  textController.clear();
                  widget.onSearchStringChanged(textController.text);
                },
              ),
              child: const Icon(
                Icons.clear,
                color: Colors.black,
              ),
            ),
            hintText: widget.hintText,
            hintStyle: Theme.of(context).textTheme.bodyLarge!.copyWith(
                  color: Colors.black38,
                ),
          ),
        ),
      ),
    );
  }
}
