import 'dart:io';

import 'package:climbing_track/app/app_colors.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/handler/style_controller.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/util/custom_date_formatter.dart';
import 'package:climbing_track/backend/util/util.dart';
import 'package:climbing_track/resources/constants.dart';
import 'package:climbing_track/ui/dialog/info_dialogs.dart';
import 'package:climbing_track/ui/util/custom_page_route.dart';
import 'package:climbing_track/ui/util/screenshot.dart';
import 'package:climbing_track/ui/widgets/location/location_summary.dart';
import 'package:climbing_track/view_model/session_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:showcaseview/showcaseview.dart';

abstract class LocalConstants {
  static const double RING_STROKE_WIDTH = 32;
  static const double PADDING = 12.0;
  static const Widget divider = Divider(
    thickness: 2.0,
    indent: Constants.STANDARD_PADDING,
    endIndent: Constants.STANDARD_PADDING,
  );
}

class SessionSummary extends StatelessWidget {
  final GlobalKey _locationWidgetKey = GlobalKey();
  final GlobalKey _bestAscendWidgetKey = GlobalKey();
  final GlobalKey? editIconKey;

  final SessionViewModel _sessionViewModel;
  final bool showToolTip;

  final List<bool> boulders = [false];

  SessionSummary(this._sessionViewModel, {super.key, this.showToolTip = false, this.editIconKey});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _sessionViewModel.ascents,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const LinearProgressIndicator();
        }
        WidgetsBinding.instance.addPostFrameCallback((_) => _showToolTip(context));

        return SingleChildScrollView(
          child: Screenshot(
            controller: _sessionViewModel.screenshotController,
            child: Material(
              child: Container(
                child: _content(
                  context,
                  List<Ascend>.of(snapshot.data.map<Ascend>((e) => Ascend.fromMap(e))),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _content(BuildContext context, List<Ascend> ascents) {
    return Column(
      children: [
        ..._location(context),
        _createInfo(context, ascents),
        _createDonutChart(context, ascents),
        const SizedBox(height: 10),
        _createLegend(context),
        const SizedBox(height: 10),
        LocalConstants.divider,
        if (_sessionViewModel.session.comment.isNotEmpty) ..._sessionComment(context),
        const SizedBox(height: 10.0),
        _createAscendList(context, ascents),
        const SizedBox(height: 64),
      ],
    );
  }

  Widget _createInfo(BuildContext context, List<Ascend> ascents) {
    double height = _sessionViewModel.getClimbedMeters(ascents);
    Ascend bestAscend = _getBestAscend(ascents);

    int numElements = height > 0.0 ? 4 : 3;
    double iconContainerSize = (MediaQuery.of(context).size.width -
            (2 * Constants.STANDARD_PADDING) -
            16 - // padding
            (LocalConstants.PADDING * (numElements - 1))) /
        numElements;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            _iconInfo(
              context,
              icon: FontAwesomeIcons.hashtag,
              description: AppLocalizations.of(context)!.session_summary_total_ascents,
              value: ascents.length.toString().padLeft(2, '0'),
              width: iconContainerSize,
              iconSize: 26,
            ),
            if (height > 0.0) ...[
              _iconInfo(
                context,
                icon: Icons.height,
                description: AppLocalizations.of(context)!.session_summary_height,
                value: Util.getHeightWithShortUnit(height).replaceAll(" ", ""),
                width: iconContainerSize,
              ),
            ],
            Showcase(
              key: _bestAscendWidgetKey,
              description: AppLocalizations.of(context)!.session_edit_page_best_ascent_hint,
              targetBorderRadius: const BorderRadius.all(Radius.circular(16)),
              targetPadding: const EdgeInsets.symmetric(vertical: 6.0),
              tooltipPadding: const EdgeInsets.all(12.0),
              child: _iconInfo(
                context,
                icon: Icons.emoji_events_outlined,
                description: bestAscend.type == ClimbingType.SPEED_CLIMBING
                    ? AppLocalizations.of(context)!.session_summary_fastest_time
                    : AppLocalizations.of(context)!.session_summary_most_difficult,
                value: Util.getGrade(bestAscend.gradeID, bestAscend.type),
                width: iconContainerSize,
                onTap: () => InfoDialog().showAscendDialog(context, bestAscend),
              ),
            ),
            _iconInfo(
              context,
              icon: Icons.timelapse,
              description: AppLocalizations.of(context)!.session_summary_session_duration,
              value: _sessionViewModel.session.endDate == null ? "---" : _sessionViewModel.session.getDuration(),
              width: iconContainerSize,
            ),
          ],
        ),
        const SizedBox(height: 8.0),
        LocalConstants.divider,
        const SizedBox(height: 8.0),
      ],
    );
  }

  Widget _iconInfo(
    BuildContext context, {
    required IconData icon,
    required String description,
    required String value,
    required double width,
    double iconSize = 32,
    Function()? onTap,
  }) {
    return Material(
      child: InkWell(
        onTap: onTap,
        borderRadius: BorderRadius.circular(Constants.STANDARD_PADDING),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 6.0, vertical: 4.0),
          width: width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: 32,
                height: 32,
                child: Icon(icon, size: iconSize),
              ),
              const SizedBox(height: 6.0),
              Text(
                value,
                style: Theme.of(context).textTheme.bodyMedium?.copyWith(color: AppColors.color1, fontSize: 21),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 1.0),
              Text(
                description,
                textAlign: TextAlign.center,
                style: const TextStyle(fontSize: 12),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _sessionComment(BuildContext context) {
    return [
      const SizedBox(height: 12.0),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: Constants.STANDARD_PADDING),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Icon(Icons.comment_outlined, size: 28),
            const SizedBox(width: 20.0),
            Flexible(
              child: Text(_sessionViewModel.session.comment),
            ),
          ],
        ),
      ),
      const SizedBox(height: 12.0),
      LocalConstants.divider,
    ];
  }

  List<Widget> _location(BuildContext context) {
    return [
      const SizedBox(height: Constants.STANDARD_PADDING),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: Constants.STANDARD_PADDING),
        child: Showcase(
          key: _locationWidgetKey,
          description: AppLocalizations.of(context)!.session_edit_page_location_page_hint,
          targetBorderRadius: const BorderRadius.all(Radius.circular(16)),
          targetPadding: EdgeInsets.zero,
          tooltipPadding: const EdgeInsets.all(12.0),
          child: Material(
            child: InkWell(
              onTap: () => _pushCragView(context),
              borderRadius: BorderRadius.circular(16),
              child: Container(
                padding: const EdgeInsets.fromLTRB(10.0, 10.0, 20.0, 10.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.black.withOpacity(0.05),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    const Icon(Icons.location_on_outlined, size: 48),
                    const SizedBox(width: 12.0),
                    Flexible(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            DataStore().locationsDataStore.locationById(_sessionViewModel.session.locationId).name,
                            style: Theme.of(context).textTheme.titleLarge,
                            overflow: TextOverflow.clip,
                          ),
                          const SizedBox(height: 2.0),
                          Text(
                            _sessionViewModel.session.endDate != null
                                ? "${CustomDateFormatter.formatDateString(context, _sessionViewModel.session.startDate)}  |  "
                                    "${DateFormat.jm(Platform.localeName).format(_sessionViewModel.session.startDate)} - "
                                    "${DateFormat.jm(Platform.localeName).format(_sessionViewModel.session.endDate!)}"
                                : "${CustomDateFormatter.formatDateString(context, _sessionViewModel.session.startDate)}  |  "
                                    "${DateFormat.jm(Platform.localeName).format(_sessionViewModel.session.startDate)}",
                            style: Theme.of(context).textTheme.bodyLarge?.copyWith(fontSize: 16),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
      const SizedBox(height: 24.0),
    ];
  }

  Widget _createDonutChart(BuildContext context, List<Ascend> ascents) {
    const String flashKey = "Flash";
    const String completedKey = "completed";
    const String onsightKey = "OnSight";
    const String redpointKey = "Redpoint";

    int onsight = 0;
    int redpoint = 0;
    int completed = 0;
    int flash = 0;
    for (var ascend in ascents) {
      if (ascend.type == ClimbingType.BOULDER) {
        boulders.first = true;
      }
      if (ascend.styleID <= ClimbingStyle.ONSIGHT.id) {
        onsight++;
      }
      if (ascend.styleID <= ClimbingStyle.FLASH.id || ascend.styleID == ClimbingStyle.FLASH_BOULDERING.id) {
        flash++;
      }
      if (ascend.styleID <= ClimbingStyle.REDPOINT.id || ascend.styleID == ClimbingStyle.REPEAT.id) {
        redpoint++;
      }
      if (ascend.styleID <= ClimbingStyle.HANGDOGGING.id ||
          ascend.styleID == ClimbingStyle.REPEAT.id ||
          ascend.styleID == ClimbingStyle.TOP.id ||
          ascend.styleID == ClimbingStyle.AID.id ||
          ascend.styleID == ClimbingStyle.REPEAT_BOULDER.id ||
          ascend.styleID == ClimbingStyle.FLASH_BOULDERING.id) {
        completed++;
      }
    }

    if (boulders.first) {
      final Map<String, double> dataMap1 = {flashKey: flash.toDouble(), "1": (ascents.length - flash).toDouble()};
      final Map<String, double> dataMap2 = {
        completedKey: completed.toDouble(),
        "2": (ascents.length - completed).toDouble()
      };

      return Stack(alignment: Alignment.center, children: [
        _createPieChart(context, dataMap1, 600, [AppColors.color1, Colors.black12], 270, false, 0),
        _createPieChart(
          context,
          dataMap2,
          800,
          [AppColors.color2, Colors.black12],
          270,
          false,
          LocalConstants.RING_STROKE_WIDTH + 10.0,
        ),
        if (dataMap1[flashKey]! > 0)
          _createPieChart(
            context,
            {"1": dataMap1[flashKey]!},
            600,
            [Colors.transparent],
            135,
            true,
            -LocalConstants.RING_STROKE_WIDTH * 2.0,
          ),
        if (dataMap2[completedKey]! > 0)
          _createPieChart(context, {"1": dataMap2[completedKey]!}, 800, [Colors.transparent], 180, true, 0),
      ]);
    } else {
      final Map<String, double> dataMap1 = {onsightKey: onsight.toDouble(), "1": (ascents.length - onsight).toDouble()};
      final Map<String, double> dataMap2 = {
        redpointKey: redpoint.toDouble(),
        "2": (ascents.length - redpoint).toDouble()
      };
      final Map<String, double> dataMap3 = {
        completedKey: completed.toDouble(),
        "3": (ascents.length - completed).toDouble()
      };

      return Stack(alignment: Alignment.center, children: [
        _createPieChart(context, dataMap1, 600, [AppColors.color1, Colors.black12], 270, false, 0),
        _createPieChart(context, dataMap2, 800, [AppColors.color2, Colors.black12], 270, false,
            LocalConstants.RING_STROKE_WIDTH + 10.0),
        _createPieChart(context, dataMap3, 1000, [AppColors.color3, Colors.black12], 270, false,
            (LocalConstants.RING_STROKE_WIDTH + 10.0) * 2.0),
        if (dataMap1[onsightKey]! > 0)
          _createPieChart(context, {"1": dataMap1[onsightKey]!}, 600, [Colors.transparent], 135, true,
              -LocalConstants.RING_STROKE_WIDTH * 2.0),
        if (dataMap2[redpointKey]! > 0)
          _createPieChart(context, {"1": dataMap2[redpointKey]!}, 800, [Colors.transparent], 180, true, 0),
        if (dataMap3[completedKey]! > 0)
          _createPieChart(context, {"1": dataMap3[completedKey]!}, 1000, [Colors.transparent], 270, true,
              LocalConstants.RING_STROKE_WIDTH * 2),
      ]);
    }
  }

  Widget _createPieChart(BuildContext context, Map<String, double> dataMap, int duration, List<Color> colorList,
      double initialAngleInDegree, bool showChartValues, double offset) {
    return PieChart(
      dataMap: dataMap,
      animationDuration: Duration(milliseconds: duration),
      chartRadius: MediaQuery.of(context).size.width / 3.2 - offset,
      colorList: colorList,
      initialAngleInDegree: initialAngleInDegree,
      chartType: ChartType.ring,
      ringStrokeWidth: 16,
      legendOptions: const LegendOptions(
        showLegends: false,
      ),
      chartValuesOptions: ChartValuesOptions(
        decimalPlaces: 0,
        showChartValueBackground: true,
        showChartValues: showChartValues,
        showChartValuesInPercentage: false,
        showChartValuesOutside: false,
      ),
    );
  }

  Widget _createLegend(BuildContext context) {
    final List<Color> colors = [AppColors.color1, AppColors.color2, AppColors.color3];
    final List<String> styles = boulders.first
        ? [
            AppLocalizations.of(context)!.style_flash.toUpperCase(),
            AppLocalizations.of(context)!.session_summary_completed.toUpperCase(),
          ]
        : [
            AppLocalizations.of(context)!.style_onsight.toUpperCase(),
            AppLocalizations.of(context)!.style_redpoint.toUpperCase(),
            AppLocalizations.of(context)!.session_summary_completed.toUpperCase(),
          ];
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: Constants.STANDARD_PADDING),
      child: Wrap(
        direction: Axis.horizontal,
        spacing: 16,
        children: styles
            .map(
              (item) => Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 2.0),
                    height: 12.0,
                    width: 12.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: colors.elementAt(styles.indexOf(item)),
                    ),
                  ),
                  const SizedBox(width: 6.0),
                  Flexible(
                    fit: FlexFit.loose,
                    child: Opacity(
                      opacity: 0.75,
                      child: Text(item, style: Theme.of(context).textTheme.bodyLarge, softWrap: true),
                    ),
                  ),
                ],
              ),
            )
            .toList(),
      ),
    );
  }

  Widget _createAscendList(BuildContext context, List<Ascend> ascents) {
    ascents.sort((a, b) => (a.orderKey ?? -1).compareTo(b.orderKey ?? -1));
    List<Widget> items = [];
    for (Ascend ascend in ascents) {
      Ascend ascendPlus = ascend;
      ascendPlus.location = DataStore().locationsDataStore.locationById(_sessionViewModel.session.locationId).name;
      ascendPlus.dateTime = _sessionViewModel.session.startDate;
      items.add(
        Material(
          child: InkWell(
            onTap: () => InfoDialog().showAscendDialog(context, ascendPlus),
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: Constants.STANDARD_PADDING,
                vertical: 4.0,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Text(
                          Util.getAscendName(
                            context,
                            ascend.name,
                            type: ascend.type,
                            speedType: ascend.speedType,
                          ),
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                fontSize: 18.0,
                                height: 0.0,
                                wordSpacing: 0.0,
                              ),
                        ),
                      ),
                      Util.getRating(ascend.rating),
                    ],
                  ),
                  Text(
                    Util.getAscendSummary(ascend, context),
                    style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                          height: 0.0,
                          fontSize: 13,
                          fontWeight: FontWeight.normal,
                          wordSpacing: 0.0,
                        ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }
    return Column(children: items);
  }

  Ascend _getBestAscend(List<Ascend> ascents) {
    bool onlySpeed = !ascents.any(
      (element) => element.type != ClimbingType.SPEED_CLIMBING,
    );
    if (onlySpeed) return _getBestSpeedRoute(ascents);

    Ascend best = ascents.first;

    for (Ascend ascend in ascents) {
      if (ascend.gradeID > best.gradeID) {
        best = ascend;
      } else if (ascend.gradeID == best.gradeID && ascend.styleID < best.styleID) {
        best = ascend;
      } else if (ascend.gradeID == best.gradeID &&
          ascend.styleID == best.styleID &&
          ascend.speedTime <= best.speedTime) {
        best = ascend;
      }
    }
    return best;
  }

  Ascend _getBestSpeedRoute(List<Ascend> ascents) {
    Ascend best = ascents.first;
    for (Ascend ascend in ascents) {
      if (ascend.speedTime <= best.speedTime) {
        best = ascend;
      }
    }
    return best;
  }

  void _pushCragView(BuildContext context) {
    Navigator.push(
      context,
      CustomPageRoute.build<bool>(
        builder: (BuildContext context) => CragView(
          location: DataStore().locationsDataStore.locationById(
                _sessionViewModel.session.locationId,
              ),
        ),
      ),
    );
  }

  Future<void> _showToolTip(BuildContext context) async {
    if (!showToolTip) return;
    if (!DataStore().toolTipsDataStore.tooltipSessionSummaryPage) return;

    await Future.delayed(const Duration(milliseconds: 500));

    if (context.mounted) {
      DataStore().toolTipsDataStore.tooltipSessionSummaryPage = false;
      ShowCaseWidget.of(context).startShowCase([
        editIconKey!,
        _locationWidgetKey,
        _bestAscendWidgetKey,
      ]);
    }
  }
}
