import 'dart:async';
import 'dart:io';

import 'package:climbing_track/view_model/google_drive_backup_result.dart';
import 'package:googleapis/drive/v3.dart' as drive;
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

class GoogleDriveBackupHandler {
  static const FILE_NAME = "climbing_tracker_backup.json";
  static const APP_FOLDER = "appDataFolder";

  final googleSignIn = GoogleSignIn.standard(scopes: [
    drive.DriveApi.driveAppdataScope,
  ]);

  Future<GoogleDriveBackupResult> uploadFileToGoogleDrive(String content, {bool signeIn = true}) async {
    // google drive authentication stuff
    if (signeIn) {
      GoogleDriveBackupResult resultSignIn = await signIn();
      if (!resultSignIn.result) return resultSignIn;
    } else {
      if (!await googleSignIn.isSignedIn()) {
        return GoogleDriveBackupResult(result: false, errorMessage: "Error: User is not signed in");
      }
    }

    try {
      final drive.DriveApi? driveApi = await _getDriveApi();
      if (driveApi == null) {
        return GoogleDriveBackupResult(result: false, errorMessage: "Error: Google drive API not available");
      }

      // create file
      File localFile = await createBackupFile(jsonString: content);
      drive.Media media = drive.Media(localFile.openRead(), localFile.lengthSync());
      drive.File driveFile = drive.File()..name = FILE_NAME;
      driveFile.modifiedTime = DateTime.now().toUtc();

      List<drive.File> files = await _getFiles();
      if (files.any((element) => element.name == FILE_NAME)) {
        String? fileId = files.firstWhere((element) => element.name == FILE_NAME).id;

        if (fileId == null) throw Exception();

        await driveApi.files.update(
          driveFile,
          fileId,
          addParents: APP_FOLDER,
          uploadMedia: media,
        );
      } else {
        driveFile.parents = [APP_FOLDER];
        await driveApi.files.create(driveFile, uploadMedia: media);
      }
    } catch (e) {
      return GoogleDriveBackupResult(result: false, errorMessage: "$e");
    }

    return GoogleDriveBackupResult(result: true, errorMessage: "");
  }

  Future<GoogleDriveBackupResult> signIn() async {
    if (await googleSignIn.isSignedIn()) return GoogleDriveBackupResult(result: true, errorMessage: "");

    try {
      final googleUser = await googleSignIn.signIn();

      if (googleUser != null) {
        final googleAuth = await googleUser.authentication;
        final credential = GoogleAuthProvider.credential(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );
        final UserCredential loginUser = await FirebaseAuth.instance.signInWithCredential(credential);

        assert(loginUser.user?.uid == FirebaseAuth.instance.currentUser?.uid);
      } else {
        return GoogleDriveBackupResult(result: false, errorMessage: "Error: Could not find user");
      }
    } catch (e) {
      return GoogleDriveBackupResult(result: false, errorMessage: "$e");
    }
    return GoogleDriveBackupResult(result: true, errorMessage: "");
  }

  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
    await googleSignIn.signOut();
  }

  Future<GoogleDriveDownloadResult?> downloadGoogleDriveFile() async {
    GoogleDriveBackupResult signedInResult = await signIn();
    if (!signedInResult.result) {
      return GoogleDriveDownloadResult(content: null, errorMessage: signedInResult.errorMessage);
    }
    try {
      final driveApi = await _getDriveApi();
      if (driveApi == null) {
        return GoogleDriveDownloadResult(content: null, errorMessage: "Error: Google drive API not available");
      }

      // check file exists
      List<drive.File> files = await _getFiles();

      if (!files.any((element) => element.name == FILE_NAME)) return null;

      // get file id
      String? fileId = files.firstWhere((element) => element.name == FILE_NAME).id;

      if (fileId == null) throw Exception();

      drive.Media response =
          await driveApi.files.get(fileId, downloadOptions: drive.DownloadOptions.fullMedia) as drive.Media;

      return GoogleDriveDownloadResult(content: await _getStringFromMediaStream(response), errorMessage: "");
    } catch (e) {
      return GoogleDriveDownloadResult(content: null, errorMessage: "$e");
    }
  }

  Future<String> _getStringFromMediaStream(drive.Media response) async {
    Completer<String> complete = Completer();
    List<int> dataStore = [];
    response.stream.listen((data) {
      dataStore.insertAll(dataStore.length, data);
    }, onDone: () async {
      Directory tempDir = await getTemporaryDirectory(); //Get temp folder using Path Provider
      String tempPath = tempDir.path; //Get path to that location
      File file = File('$tempPath/climbing_tracker.json'); //Create a dummy file

      // write file
      await file.writeAsBytes(dataStore);

      // read file
      String result = file.readAsStringSync();

      return complete.complete(result);
    }, onError: (error) {
      return complete.completeError(error);
    });
    return complete.future;
  }

  Future<drive.DriveApi?> _getDriveApi() async {
    final googleUser = await googleSignIn.signInSilently();
    final headers = await googleUser?.authHeaders;
    if (headers == null) {
      return null;
    }

    final client = GoogleAuthClient(headers);
    final driveApi = drive.DriveApi(client);
    return driveApi;
  }

  Future<List<drive.File>> _getFiles() async {
    if (!await googleSignIn.isSignedIn()) return [];

    final driveApi = await _getDriveApi();
    if (driveApi == null) {
      return [];
    }

    final fileList = await driveApi.files.list(
      spaces: APP_FOLDER,
      $fields: 'files(id, name, modifiedTime)',
    );
    final files = fileList.files;
    if (files == null) {
      return [];
    }

    return files;
  }

  Future<File> createBackupFile({required String jsonString}) async {
    final directory = (await getTemporaryDirectory()).path;

    // create backup file
    String filePath = '$directory/$FILE_NAME';
    File file = File(filePath);
    file.writeAsStringSync(jsonString);
    return file;
  }
}

class GoogleAuthClient extends http.BaseClient {
  final Map<String, String> _headers;
  final _client = http.Client();

  GoogleAuthClient(this._headers);

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
    request.headers.addAll(_headers);
    return _client.send(request);
  }
}
