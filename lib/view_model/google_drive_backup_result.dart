import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class GoogleDriveBackupResult {
  final bool result;
  final String errorMessage;

  GoogleDriveBackupResult({required this.result, required this.errorMessage});

  /// Only for debugging and error finding
  void showErrorDialog(BuildContext context) {
    //Do nothing if not in debug mode
    if (!kDebugMode) return;

    // set up the button
    Widget okButton = TextButton(
      onPressed: Navigator.of(context).pop,
      child: const Text("Dismiss"),
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text("Google Drive Backup Error:"),
      content: Text(errorMessage),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

class GoogleDriveDownloadResult extends GoogleDriveBackupResult {
  final String? content;

  GoogleDriveDownloadResult({required this.content, required super.errorMessage}) : super(result: content != null);
}
