import 'package:climbing_track/backend/backup/backup_handler.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/grade_mapper/abstract_grade_mapper.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/handler/location_handler.dart';
import 'package:climbing_track/backend/model/active_session.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/location.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:climbing_track/ui/dialog/confirm_dialog.dart';
import 'package:climbing_track/backend/handler/notifications_handler.dart';
import 'package:climbing_track/view_model/best_performance_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeViewModel extends ChangeNotifier {
  static const String LAST_GO_TIME_KEY = "last_go_time_key";

  String _locationName = "";
  bool _outdoor = false;
  double standardHeight = 0;
  String locationComment = "";
  DateTime? dateTime;
  DateTime? _lastGoTime;

  late Ascend currentAscend;
  late final TextEditingController commentEditingController = TextEditingController();

  HomeViewModel() {
    Session? activeSession = ActiveSession().activeSession;
    Location? location;
    if (activeSession != null) {
      location = DataStore().locationsDataStore.locationById(activeSession.locationId);
    }
    SharedPreferences.getInstance().then((prefs) {
      String? dateTime = prefs.getString(LAST_GO_TIME_KEY);
      if (dateTime == null) {
        lastGoTime = null;
      } else {
        lastGoTime = DateTime.tryParse(dateTime);

        if (_lastGoTime != null && (activeSession?.startDate.isAfter(_lastGoTime!) ?? true)) {
          lastGoTime = null;
        }
      }
    });

    currentAscend = Ascend.getDefault(
      startDate: activeSession?.startDate,
      locationName: location?.name,
      standardHeight: location?.standardHeight ?? 0.0,
      climbingType: location?.standardClimbingType,
      gradeId: location?.standardDifficulty,
    );
  }

  void onInitSession() {
    _locationName = "";
    _outdoor = false;
    standardHeight = 0;
    locationComment = "";
    commentEditingController.text = locationComment;
    SessionState().setState(SessionStateEnum.start);
  }

  void onEndSession() {
    ActiveSession().clear();
    SessionState().nextState();
  }

  Future<void> onCancelEndSession(BuildContext context, Session session) async {
    try {
      await NotificationHandler().scheduleReminderNotification(
        context,
        startTime: session.startDate,
      );
    } on Exception catch (_) {}
    if (context.mounted) Navigator.of(context).pop(false);
  }

  void onSubmitEndSession(
    BuildContext context, {
    required int routeCount,
    required Session session,
    required DateTime endTime,
  }) async {
    NotificationHandler().cancelNotification();
    lastGoTime = null;

    // If no routes => delete active session
    if (routeCount <= 0) {
      if (session.sessionId != null) {
        DBController().deleteAscends(session.sessionId!);
        DBController().deleteSession(id: session.sessionId!, locationId: session.locationId);
      }

      SessionState().setState(SessionStateEnum.none);
      Navigator.of(context).pop(false);
    } else {
      await _closeSession(context, session: session, endTime: endTime);
      SessionState().nextState();
      if (context.mounted) Navigator.of(context).pop(true);
    }
  }

  Future<void> _closeSession(
    BuildContext context, {
    required Session session,
    required DateTime endTime,
  }) async {
    session.endDate = endTime;

    String msg1 = AppLocalizations.of(context)!.notification_drive_backup_failed_title;
    String msg2 = AppLocalizations.of(context)!.notification_drive_backup_failed_body;

    await DBController().updateSession(
      row: {
        DBController.sessionTimeEnd: session.endDate!.toIso8601String(),
        DBController.sessionStatus: Session.SESSION_CLOSED_STATE,
        DBController.sessionComment: session.comment,
      },
      sessionId: session.sessionId!,
    );

    updateLocationStandards(sessionId: session.sessionId!, locationId: session.locationId);

    BackupHandler backupHandler = BackupHandler();
    backupHandler.createAutomaticBackup();

    backupHandler.createGoogleDriveBackup(
      failNotificationTitle: msg1,
      failNotificationBody: msg2,
      sendNotificationOnError: true,
    );
  }

  void onStartSession(BuildContext context) async {
    if (_locationName == "") {
      // needs location name to start session!
      if (context.mounted) {
        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              content: Text(AppLocalizations.of(context)!.home_page_enter_location_hint),
            ),
          );
      }
      return;
    }

    Location? location = DataStore().locationsDataStore.findExistingLocation(_locationName, _outdoor);

    if (location == null) {
      location = Location(null, _locationName, null, standardHeight, locationComment, _outdoor);
      await DataStore().locationsDataStore.addLocation(location);
    } else {
      if (location.standardHeight != standardHeight) {
        bool res = await ConfirmDialog().showHeightUpdateDialog(context) ?? false;
        location.updateStandardHeight(standardHeight, res);
      }
      location.locationComment = locationComment;
    }

    Session session = Session(
      sessionId: null,
      startDate: dateTime ?? DateTime.now(),
      locationId: location.locationId!,
    );

    BestPerformanceViewModel.informed = false;

    int rowId = await DBController().insert(DBController.sessionsTable, session.toMap(Session.SESSION_ACTIVE_STATE));
    session.sessionId = rowId;

    _createCoordinates(location);

    ActiveSession().activeSession = session;
    ActiveSession().ascents = [];

    currentAscend = Ascend.getDefault(
      startDate: session.startDate,
      locationName: location.name,
      standardHeight: location.standardHeight,
      climbingType: location.standardClimbingType,
      gradeId: location.standardDifficulty,
    );

    // update ascend reminders
    await DataStore().ascendReminderDataStore.init();

    try {
      if (context.mounted) await NotificationHandler().scheduleReminderNotification(context);
    } catch (_) {}

    SessionState().nextState();

    return;
  }

  Future<void> _createCoordinates(Location location) async {
    if (LocationHandler().saveLocation) {
      if (location.coordinates == null) {
        try {
          Position position = await LocationHandler().determinePosition();
          location.coordinates = LatLng(position.latitude, position.longitude);
        } on Exception catch (_) {
          // if permission is not granted
        }
      }
    }
  }

  bool deleteAscend(Ascend ascend) {
    notifyListeners();
    if (ascend.ascendId != -1) {
      DBController().deleteAscend(ascend.ascendId);
      return true;
    } else {
      return false;
    }
  }

  void onReAdd(int index, Ascend ascend) async {
    int sessionId = ActiveSession().activeSession!.sessionId!;
    int value = await DBController().insert(DBController.ascendsTable, ascend.toMap(sessionId));
    ascend.ascendId = value;
    ascend.orderKey = ascend.orderKey ?? ascend.ascendId;
    await DBController().updateAscend(ascend.toMap(sessionId), ascend.ascendId);
    notifyListeners();
  }

  void updateAscendsReorder() {
    // update ascend
    for (Ascend ascend in ActiveSession().ascents) {
      DBController().updateAscend(
        ascend.toMap(ActiveSession().activeSession!.sessionId!),
        ascend.ascendId,
      );
    }
  }

  String get location => _locationName;

  bool get outdoor => _outdoor;

  set outdoor(bool value) {
    _outdoor = value;
    notifyListeners();
  }

  set location(String value) {
    if (_locationName == value) return;
    _locationName = value;
    Location? result = DataStore().locationsDataStore.findExistingLocation(value, _outdoor);
    if (result != null) {
      setLocation(result);
    } else {
      notifyListeners();
    }
  }

  void setLocation(Location location) {
    _locationName = location.name;
    _outdoor = location.outdoor;
    standardHeight = location.standardHeight;
    locationComment = location.locationComment.isEmpty ? locationComment : location.locationComment;
    commentEditingController.text = locationComment;
    notifyListeners();
  }

  void addCurrentRoute() async {
    Ascend copy = currentAscend.copy();
    ActiveSession().ascents.add(copy);
    int sessionId = ActiveSession().activeSession!.sessionId!;
    int value = await DBController().insert(DBController.ascendsTable, copy.toMap(sessionId));
    copy.ascendId = value;
    copy.orderKey = copy.ascendId;
    await DBController().updateAscend(copy.toMap(sessionId), copy.ascendId);

    lastGoTime = DateTime.now();

    // Order important, as only name change triggers notifier in ascend class
    currentAscend.comment = "";
    currentAscend.characteristic = 0;
    currentAscend.steepness = 0;
    currentAscend.name = "";
    notifyListeners();

    // update location standards if first ascent and standards not set
    Location location = DataStore().locationsDataStore.locationById(ActiveSession().activeSession!.locationId);
    if (location.standardDifficulty == null && location.standardClimbingType == null) {
      location.standardClimbingType = copy.type;
      location.standardDifficulty = copy.gradeID;
    }
  }

  void updateLocationComment() {
    Location? location = DataStore().locationsDataStore.findExistingLocation(_locationName, _outdoor);
    if (location != null) {
      location.locationComment = locationComment;
    }
  }

  Future<void> updateLocationStandards({required int sessionId, required int locationId}) async {
    List<Map<String, dynamic>> ascents = await DBController().queryAscendsBySessionId(sessionId);
    Map<String, int> ascentTypeCounts = {};
    for (var ascent in ascents) {
      String ascentType = ascent[DBController.ascendType];
      if (ascentTypeCounts.containsKey(ascentType)) {
        ascentTypeCounts[ascentType] = ascentTypeCounts[ascentType]! + 1;
      } else {
        ascentTypeCounts[ascentType] = 1;
      }
    }
    // Find the ascent type with the maximum count

    String mostFrequentAscentType = ClimbingType.SPORT_CLIMBING.name;
    int maxCount = 0;
    ascentTypeCounts.forEach((type, count) {
      if (count > maxCount) {
        mostFrequentAscentType = type;
        maxCount = count;
      }
    });

    ClimbingType climbingType = ClimbingType.values.byName(mostFrequentAscentType);
    AbstractGradeMapper gradeManager = GradeManager().getGradeMapper(type: climbingType);

    Iterable tmp = ascents
        .where((element) => element[DBController.ascendType] == mostFrequentAscentType)
        .map((e) => gradeManager.normalize(e[DBController.ascendGradeId], expandedSystem: false));

    double climbingGradeNormalized = tmp.reduce((a, b) => a + b) / tmp.length;

    int climbingGrade = climbingGradeNormalized <= 0
        ? GradeManager().getStandardGrade(climbingType)
        : gradeManager.gradeIdFromNormalized(climbingGradeNormalized, expandedSystem: false);

    Location location = DataStore().locationsDataStore.locationById(locationId);

    location.standardClimbingType = climbingType;
    location.standardDifficulty = climbingGrade;
  }

  DateTime? get lastGoTime => _lastGoTime;

  set lastGoTime(DateTime? value) {
    _lastGoTime = value;

    SharedPreferences.getInstance().then((prefs) {
      if (_lastGoTime == null) {
        prefs.remove(LAST_GO_TIME_KEY);
      } else {
        prefs.setString(LAST_GO_TIME_KEY, _lastGoTime!.toIso8601String());
      }
    });
    notifyListeners();
  }
}
