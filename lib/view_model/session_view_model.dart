import 'dart:io';
import 'dart:math';

import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/grade_mapper/grade_manager.dart';
import 'package:climbing_track/backend/model/ascent.dart';
import 'package:climbing_track/backend/model/enums/climbing_types.dart';
import 'package:climbing_track/backend/model/location.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:climbing_track/backend/util/extensions.dart';
import 'package:climbing_track/ui/util/custom_page_route.dart';
import 'package:climbing_track/ui/util/screenshot.dart';
import 'package:climbing_track/ui/widgets/location/location_picker_map.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:latlong2/latlong.dart';
import 'package:share_plus/share_plus.dart';

class SessionViewModel {
  static const String AVERAGE_KEY = "average_key";
  static const String MAX_KEY = "max_key";
  static const String HEIGHT_KEY = "height_key";
  static const String COUNT_KEY = "count_key";
  static const String CLIMBING_SYSTEM_KEY = "climbing_type_key";

  ScreenshotController screenshotController;

  Map<dynamic, dynamic> sessionSummaryData = {
    SessionViewModel.HEIGHT_KEY: 0.0,
    SessionViewModel.AVERAGE_KEY: 0,
    SessionViewModel.COUNT_KEY: 0,
  };

  // original data
  Session session;
  late Location location;
  late Future<List<Map<String, dynamic>>> ascents;

  // edited data
  late Session editedSession;
  late Location editedLocation;
  List<Ascend> editedAscents = [];
  List<Ascend> toDelete = [];

  SessionViewModel({required this.session, required this.screenshotController}) {
    editedSession = session.copy();
    location = DataStore().locationsDataStore.locationById(session.locationId);
    editedLocation = location.copy();
    editedLocation.locationId = null;
    ascents = fetchAscents();
  }

  Future<void> loadEditableAscentsList() async {
    if (editedAscents.isNotEmpty) return;
    List<Map<String, dynamic>> tmp = await ascents;
    editedAscents = tmp.map((e) => Ascend.fromMap(e)).toList();
  }

  Future<List<Map<String, dynamic>>> fetchAscents() async {
    return DBController().rawQuery(
        "SELECT * FROM ${DBController.ascendsTable} "
        "JOIN ${DBController.sessionsTable} using (${DBController.sessionId}) "
        "JOIN ${DBController.locationsTable} using (${DBController.locationId}) "
        "WHERE ${DBController.sessionId} == (?) "
        "ORDER BY ${DBController.ascendOrder}",
        [session.sessionId]);
  }

  Future<List<Map<String, dynamic>>> getAscentsAndCalculateSessionSummary() async {
    sessionSummaryData = _calculateSessionSummaryData(await ascents);
    return ascents;
  }

  Map<dynamic, dynamic> _calculateSessionSummaryData(List<Map<String, dynamic>> ascents) {
    Map<dynamic, dynamic> result = {
      AVERAGE_KEY: 0,
      MAX_KEY: null,
      HEIGHT_KEY: 0.0,
      COUNT_KEY: ascents.length,
      CLIMBING_SYSTEM_KEY: ClimbingType.SPORT_CLIMBING,
    };
    int climbing = 0, boulder = 0, ice = 0;

    for (Map ascent in ascents) {
      String type = ascent[DBController.ascendType];
      if (type == ClimbingType.BOULDER.name) {
        boulder++;
      } else if (type == ClimbingType.ICE_CLIMBING.name) {
        ice++;
      } else {
        climbing++;
      }
    }

    if (climbing >= boulder && climbing >= ice) {
      result[CLIMBING_SYSTEM_KEY] = ClimbingType.SPORT_CLIMBING;
    } else if (boulder >= climbing && boulder >= ice) {
      result[CLIMBING_SYSTEM_KEY] = ClimbingType.BOULDER;
    } else {
      result[CLIMBING_SYSTEM_KEY] = ClimbingType.ICE_CLIMBING;
    }

    for (Map ascent in ascents) {
      try {
        result[HEIGHT_KEY] += ascent[DBController.ascendHeight];

        final bool check = result[CLIMBING_SYSTEM_KEY] == ClimbingType.SPORT_CLIMBING
            ? ascent[DBController.ascendType] != ClimbingType.BOULDER.name &&
                ascent[DBController.ascendType] != ClimbingType.ICE_CLIMBING.name
            : ascent[DBController.ascendType] == (result[CLIMBING_SYSTEM_KEY] as ClimbingType).name;

        if (check) {
          result[AVERAGE_KEY] += GradeManager()
              .getGradeMapper(type: ClimbingType.values.byName(ascent[DBController.ascendType]))
              .normalize(
                ascent[DBController.ascendGradeId],
              );

          if (result[MAX_KEY] == null ||
              ascent[DBController.ascendGradeId] > result[MAX_KEY][DBController.ascendGradeId]) {
            result[MAX_KEY] = ascent;
          }
        }
      } catch (error, stack) {
        debugPrint(stack.toString());
      }
    }

    if (ascents.isNotEmpty) {
      result[AVERAGE_KEY] /= max(climbing, max(boulder, ice));
      result[AVERAGE_KEY] = GradeManager()
          .getGradeMapper(
            type: ClimbingType.values.byName(result[MAX_KEY][DBController.ascendType]),
          )
          .indexFromNormalized(result[AVERAGE_KEY]);
    }
    return result;
  }

  String get maxGrade {
    return sessionSummaryData[MAX_KEY] != null
        ? GradeManager()
            .getGradeMapper(
              type: ClimbingType.values.byName(sessionSummaryData[MAX_KEY][DBController.ascendType]),
            )
            .gradeFromId(sessionSummaryData[MAX_KEY][DBController.ascendGradeId])
        : "---";
  }

  String get avgGrade {
    return sessionSummaryData[AVERAGE_KEY] != 0
        ? GradeManager()
            .getGradeMapper(type: sessionSummaryData[CLIMBING_SYSTEM_KEY])
            .labelFromIndex(sessionSummaryData[AVERAGE_KEY])
        : "---";
  }

  int get count => sessionSummaryData[COUNT_KEY];

  double getClimbedMeters(List<Ascend> ascendList) {
    return ascendList.fold(0.0, (previousValue, element) => previousValue += element.height);
  }

  void createScreenShot() {
    String dateString = DateFormat("yyyy-MM-dd").format(session.startDate);

    screenshotController.capture(fileName: "$dateString-session-summary").then((File image) {
      Share.shareXFiles([XFile(image.path)]);
    }).catchError((onError) {
      debugPrint(onError);
    });
  }

  void deleteSession(BuildContext context) async {
    // If Session Summary in session page is open, then delete of this session would lead to null pointer.
    // So session state must be updated
    if (SessionState().currentState == SessionStateEnum.end) SessionState().setState(SessionStateEnum.none);

    DBController dbController = DBController();
    if (session.sessionId != null) {
      await dbController.deleteAscends(session.sessionId!);
      await dbController.deleteSession(id: session.sessionId!, locationId: session.locationId);
    }

    if (context.mounted) {
      Navigator.of(context).popUntil((route) => route.isFirst);
    }
  }

  void onSave(BuildContext context) async {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();

    // delete ascends
    for (Ascend ascend in toDelete) {
      if (ascend.ascendId == -1) continue;
      await DBController().deleteAscend(ascend.ascendId);
    }

    // update ascend
    for (Ascend ascend in editedAscents) {
      if (ascend.ascendId == -1) {
        await DBController().insert(DBController.ascendsTable, ascend.toMap(editedSession.sessionId!));
      } else {
        await DBController().updateAscend(ascend.toMap(editedSession.sessionId!), ascend.ascendId);
      }
    }

    toDelete.clear();
    editedAscents.clear();

    // update or create location
    int? tmpId = location.locationId;
    location = await DataStore().locationsDataStore.createUpdateOrGetLocation(editedLocation);
    editedSession.locationId = location.locationId!;

    await DBController().updateSession(
      row: editedSession.toJson(editedSession.sessionId!),
      sessionId: editedSession.sessionId!,
    );

    session = editedSession;
    ascents = fetchAscents();
    await ascents;

    // remove location if not used anymore
    await DataStore().locationsDataStore.removeLocationIfNoMoreSessionsExists(locationId: tmpId, removedSessionId: -1);

    if (context.mounted) {
      Navigator.of(context).pop(true);
    }
  }

  void initEditFunctions() {
    toDelete = [];
    editedSession = session.copy();
    Location location = DataStore().locationsDataStore.locationById(session.locationId);
    editedLocation = location.copy();
    editedLocation.locationId = null;
    editedAscents.clear();
  }

  Future<void> chooseLocationCoordinatesFromMap(BuildContext context, Function setState) async {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    LatLng coordinates = const LatLng(51.5, -0.09);
    double zoom = 5;

    if (editedLocation.coordinates != null) {
      coordinates = editedLocation.coordinates!.copy();
      zoom = 13;
    }

    LatLng? result = await Navigator.push(
      context,
      CustomPageRoute.build<LatLng>(
        builder: (BuildContext context) => LocationCoordinatesPickerMap(
          coordinates: coordinates,
          initialZoom: zoom,
        ),
      ),
    );

    if (result != null) {
      editedLocation.coordinates = result;
      setState(() {});
    }
  }

  bool deleteAscend(Ascend ascend) {
    toDelete.add(ascend);
    return true;
  }

  void onReAdd(int index, Ascend ascend) {
    toDelete.remove(ascend);
  }
}
