import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/model/enums/search_order_enum.dart';
import 'package:climbing_track/backend/model/session.dart';
import 'package:climbing_track/view_model/session_view_model.dart';
import 'package:flutter/material.dart';

class SessionsOverviewViewModel with ChangeNotifier {
  static const String IMAGE_PATH = "assets/images/6195619.png";

  final Map<int, SessionViewModel> sessionViewModels = {};
  late Future _futureData;
  String _searchString = "";
  bool _indoor = true;
  bool _outdoor = true;
  bool _settingsExpanded = false;
  bool _onlyShowSessionComments = false;

  SessionsOverviewViewModel() {
    _futureData = _fetchSessionData();
  }

  void updateData() {
    _futureData = _fetchSessionData();
    notifyListeners();
  }

  List<Map<String, dynamic>>? filterAndSort(List? data) {
    if (data != null) {
      List<Map<String, dynamic>> filteredData = data
          .where((item) => item[DBController.locationName].toString().toLowerCase().contains(_searchString))
          .where((item) => ((item[DBController.locationOutdoor] == 1 && outdoor) ||
              (item[DBController.locationOutdoor] == 0 && indoor)))
          .where((item) => _onlyShowSessionComments
              ? item[DBController.sessionComment] != null && item[DBController.sessionComment] != ""
              : true)
          .toList() as List<Map<String, dynamic>>;

      filteredData.sort(DataStore().settingsDataStore.sessionSearchOrder.compareFunction());

      return filteredData;
    } else {
      return null;
    }
  }

  Future<List<Map<String, dynamic>>> _fetchSessionData() async {
    return DBController().rawQuery(
      "SELECT * FROM ${DBController.sessionsTable} "
      "JOIN ${DBController.locationsTable} using(${DBController.locationId})"
      "WHERE ${DBController.sessionStatus} != (?) "
      "ORDER BY ${DBController.sessionTimeStart} DESC",
      [Session.SESSION_ACTIVE_STATE],
    );
  }

  Future get futureData => _futureData;

  bool get outdoor => _outdoor;

  bool get indoor => _indoor;

  bool get settingsExpanded => _settingsExpanded;

  bool get onlyShowComments => _onlyShowSessionComments;

  set searchString(String value) {
    _searchString = value;
    notifyListeners();
  }

  void toggleOutdoor() {
    _outdoor = !_outdoor;
    notifyListeners();
  }

  void toggleIndoor() {
    _indoor = !_indoor;
    notifyListeners();
  }

  void toggleSettingsExpanded({bool? value}) {
    _settingsExpanded = value ?? !_settingsExpanded;
    notifyListeners();
  }

  void toggleOnlyShowSessionComments() {
    _onlyShowSessionComments = !_onlyShowSessionComments;
    notifyListeners();
  }
}
