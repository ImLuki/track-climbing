import 'dart:io';

import 'package:climbing_track/backend/backup/backup_creator.dart';
import 'package:climbing_track/backend/backup/backup_handler.dart';
import 'package:climbing_track/backend/data_store/data_store.dart';
import 'package:climbing_track/backend/data_store/setting_data_store.dart';
import 'package:climbing_track/backend/data_store/tooltips_data_store.dart';
import 'package:climbing_track/backend/database/database_controller.dart';
import 'package:climbing_track/backend/handler/location_handler.dart';
import 'package:climbing_track/backend/model/active_session.dart';
import 'package:climbing_track/backend/state.dart';
import 'package:climbing_track/view_model/google_drive_backup_handler.dart';
import 'package:climbing_track/view_model/google_drive_backup_result.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:open_store/open_store.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:share_plus/share_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsViewModel with ChangeNotifier {
  String versionNumber = '';
  String buildNumber = '';

  SettingsDataStore settings = DataStore().settingsDataStore;
  ToolTipsDataStore toolTipsDataStore = DataStore().toolTipsDataStore;
  late GoogleDriveBackupHandler _googleDriveBackupHandler;
  SharedPreferences? prefs;

  bool isUploadingGoogleDrive = false;
  bool isDownloadingGoogleDrive = false;
  bool isCreatingBackup = false;
  bool isLoadingBackup = false;
  bool isLoadingAutomaticBackup = false;

  Future<void> init() async {
    if (Platform.isAndroid) {
      _googleDriveBackupHandler = GoogleDriveBackupHandler();
    }
    await _loadVersionNumber();
    await _loadSharedPrefs();
    notifyListeners();
  }

  DateTime? getLastDriveBackup() {
    if (prefs == null) return null;
    String? result = prefs?.getString(BackupHandler.LAST_DRIVE_BACKUP_KEY);
    if (result == null) return null;
    return DateTime.parse(result);
  }

  Future<bool> checkStoragePermission() async {
    PermissionStatus status = await Permission.storage.request();
    return status.isGranted;
  }

  Future<void> _loadSharedPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  Future<void> _loadVersionNumber() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    versionNumber = packageInfo.version;
    buildNumber = packageInfo.buildNumber;
  }

  Future<bool> checkLocationPermission() async {
    bool status = await LocationHandler().hasPermission();
    return status;
  }

  void resetTooltips() {
    toolTipsDataStore.reset();
  }

  Future<bool> loadBackup(String? filePath) async {
    return BackupHandler().loadFilBackupFromFile(filePath);
  }

  void deleteDataBase() {
    DBController().deleteAll();
    SessionState().setState(SessionStateEnum.none);
    ActiveSession().clear();
    DataStore().clear();
  }

  static Future<void> sendFeedBack({String? feedback = "Hey Lukas,\n"}) async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      final Email email = Email(
        body: "device: ${androidInfo.model}\n"
            "app_version: ${packageInfo.version} (${packageInfo.buildNumber})\n"
            "sdk_version: ${androidInfo.version.sdkInt}\n\n\n"
            "$feedback",
        recipients: ['freudenmann.business+climbing-tracker@gmail.com'],
        isHTML: false,
      );
      await FlutterEmailSender.send(email);
    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      final Email email = Email(
        body: "device: ${iosInfo.model}\n"
            "app_version: ${packageInfo.version} (${packageInfo.buildNumber})\n"
            "system_version: ${iosInfo.systemVersion}\n\n\n"
            "Hey Lukas,\n",
        recipients: ['freudenmann.business+climbing-tracker@gmail.com'],
        isHTML: false,
      );
      await FlutterEmailSender.send(email);
    }
  }

  void rateApp() {
    OpenStore.instance.open(
      appStoreId: '1629878730', // AppStore id of your app for iOS
      androidAppBundleId: 'freudenmann.climbing_track', // Android app bundle package name
    );
  }

  Future<void> createBackupFile() async {
    String result = await BackupCreator().createBackupFile();
    isCreatingBackup = false;
    _shareFile(result);
  }

  void _shareFile(String filePath) async {
    Share.shareXFiles([XFile(filePath)]);
  }

  Future<void> uploadGoogleDriveBackup(BuildContext context) async {
    isUploadingGoogleDrive = true;
    String message = "";
    if (await BackupHandler().createGoogleDriveBackup(signIn: true)) {
      if (context.mounted) message = AppLocalizations.of(context)!.google_drive_backup_uploaded;
    } else {
      if (context.mounted) message = AppLocalizations.of(context)!.google_drive_backup_not_uploaded;
    }

    if (context.mounted) {
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            behavior: SnackBarBehavior.floating,
            content: Text(message),
          ),
        );
    }
    isUploadingGoogleDrive = false;
  }

  Future<void> restoreGoogleDriveBackup(BuildContext context) async {
    isDownloadingGoogleDrive = true;
    String message = "";

    bool? result = await BackupHandler().restoreGoogleDriveBackup();

    if (result == null) {
      if (context.mounted) message = AppLocalizations.of(context)!.google_drive_backup_not_downloaded;
    } else if (result) {
      if (context.mounted) message = AppLocalizations.of(context)!.google_drive_backup_restored;
    } else {
      if (context.mounted) message = AppLocalizations.of(context)!.google_drive_backup_not_restored;
    }
    if (context.mounted) {
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            behavior: SnackBarBehavior.floating,
            content: Text(message),
          ),
        );
    }
    isDownloadingGoogleDrive = false;
  }

  Future<void> toggleGoogleCloudBackups(BuildContext context, bool value) async {
    if (value) {
      GoogleDriveBackupResult signInResult = await _googleDriveBackupHandler.signIn();
      if (!signInResult.result) {
        settings.googleDriveActivated = false;

        if (context.mounted) {
          // Only for debugging and error finding
          if (kDebugMode) signInResult.showErrorDialog(context);

          ScaffoldMessenger.of(context)
            ..removeCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                behavior: SnackBarBehavior.floating,
                content: Text(AppLocalizations.of(context)!.google_drive_backup_not_activated),
              ),
            );
        }
      } else {
        settings.googleDriveActivated = true;
      }
    } else {
      settings.googleDriveActivated = false;
      _googleDriveBackupHandler.signOut();
    }
  }

  String getVersionString() => '$versionNumber ($buildNumber)';

  Future<bool> checkNotificationPermission() async {
    PermissionStatus status = await Permission.notification.request();
    return status.isGranted;
  }
}
